﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void JetBrains.Annotations.CanBeNullAttribute::.ctor()
extern void CanBeNullAttribute__ctor_m6F19C7002DD42E3AF42DB6F8996CACD816126237 ();
// 0x00000002 System.Void JetBrains.Annotations.NotNullAttribute::.ctor()
extern void NotNullAttribute__ctor_mC042005FB58C4FDBF56356830703305AAC7AFDDE ();
// 0x00000003 System.Void JetBrains.Annotations.ItemNotNullAttribute::.ctor()
extern void ItemNotNullAttribute__ctor_m367958CC9A39DE676E462B7F673E8B9DD100BD20 ();
// 0x00000004 System.Void JetBrains.Annotations.ItemCanBeNullAttribute::.ctor()
extern void ItemCanBeNullAttribute__ctor_m59095B6741ACE9A3555CCCD3231CD47C5B5B2F12 ();
// 0x00000005 System.Void JetBrains.Annotations.StringFormatMethodAttribute::.ctor(System.String)
extern void StringFormatMethodAttribute__ctor_mA0F0DD9D94622A65E2B454CA70021BC3EBA105FA ();
// 0x00000006 System.String JetBrains.Annotations.StringFormatMethodAttribute::get_FormatParameterName()
extern void StringFormatMethodAttribute_get_FormatParameterName_m78E78C4A4FBAC9F3EFC3A67BB717400284268A4B ();
// 0x00000007 System.Void JetBrains.Annotations.StringFormatMethodAttribute::set_FormatParameterName(System.String)
extern void StringFormatMethodAttribute_set_FormatParameterName_m7F599817D270C2AE791FADD1F3F467AB800EC5AC ();
// 0x00000008 System.Void JetBrains.Annotations.ValueProviderAttribute::.ctor(System.String)
extern void ValueProviderAttribute__ctor_mEC341339F02E1ECA9E12E6EC363DA583153CD90E ();
// 0x00000009 System.String JetBrains.Annotations.ValueProviderAttribute::get_Name()
extern void ValueProviderAttribute_get_Name_m85A47FD4BDE7531EEAD0021846301D1ED66F8260 ();
// 0x0000000A System.Void JetBrains.Annotations.ValueProviderAttribute::set_Name(System.String)
extern void ValueProviderAttribute_set_Name_m189466822274D76436002174983AC4BDF229B82C ();
// 0x0000000B System.Void JetBrains.Annotations.InvokerParameterNameAttribute::.ctor()
extern void InvokerParameterNameAttribute__ctor_m6D07C117B1D500B738EC7DE7418B6C873D2485E0 ();
// 0x0000000C System.Void JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::.ctor()
extern void NotifyPropertyChangedInvocatorAttribute__ctor_mC032202F6CCFA2F3431FE63D0EFBA6A646FECB89 ();
// 0x0000000D System.Void JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::.ctor(System.String)
extern void NotifyPropertyChangedInvocatorAttribute__ctor_m27B201DC58347E6CB19AD57B9C197BA55570452D ();
// 0x0000000E System.String JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::get_ParameterName()
extern void NotifyPropertyChangedInvocatorAttribute_get_ParameterName_mEE85B381BC8D2037911F54E5974FB48948F52F86 ();
// 0x0000000F System.Void JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::set_ParameterName(System.String)
extern void NotifyPropertyChangedInvocatorAttribute_set_ParameterName_m4042408E5BD997B59E313B781D8FD778DE132D5D ();
// 0x00000010 System.Void JetBrains.Annotations.ContractAnnotationAttribute::.ctor(System.String)
extern void ContractAnnotationAttribute__ctor_m601EFEEDF152647707C025B7438A97D53FE65632 ();
// 0x00000011 System.Void JetBrains.Annotations.ContractAnnotationAttribute::.ctor(System.String,System.Boolean)
extern void ContractAnnotationAttribute__ctor_m39389343D4F1D30EADBE6CDFC5A91BBB2CF0406E ();
// 0x00000012 System.String JetBrains.Annotations.ContractAnnotationAttribute::get_Contract()
extern void ContractAnnotationAttribute_get_Contract_m25CDDC707037F6507880BFB6E87FAC91A18C61FB ();
// 0x00000013 System.Void JetBrains.Annotations.ContractAnnotationAttribute::set_Contract(System.String)
extern void ContractAnnotationAttribute_set_Contract_m46117527529866206D93D24AC03B8A5B97054D34 ();
// 0x00000014 System.Boolean JetBrains.Annotations.ContractAnnotationAttribute::get_ForceFullStates()
extern void ContractAnnotationAttribute_get_ForceFullStates_m1F6FC2F24CE7006D5E351E183FA945BE1C76C140 ();
// 0x00000015 System.Void JetBrains.Annotations.ContractAnnotationAttribute::set_ForceFullStates(System.Boolean)
extern void ContractAnnotationAttribute_set_ForceFullStates_m3273626E1D840A51CE3373012B591A0A4652EBC0 ();
// 0x00000016 System.Void JetBrains.Annotations.LocalizationRequiredAttribute::.ctor()
extern void LocalizationRequiredAttribute__ctor_m31C88794DC9E45D017667001AB510ABD3EA91264 ();
// 0x00000017 System.Void JetBrains.Annotations.LocalizationRequiredAttribute::.ctor(System.Boolean)
extern void LocalizationRequiredAttribute__ctor_mC9ED48DF8302CB9F4D0217AAA4A81AA28A19E352 ();
// 0x00000018 System.Boolean JetBrains.Annotations.LocalizationRequiredAttribute::get_Required()
extern void LocalizationRequiredAttribute_get_Required_mF4EDBE1AC5F6B245F44062703216F023D3E02092 ();
// 0x00000019 System.Void JetBrains.Annotations.LocalizationRequiredAttribute::set_Required(System.Boolean)
extern void LocalizationRequiredAttribute_set_Required_m28CDD5F78263BE6179EE3E29E80EBCF93FA51021 ();
// 0x0000001A System.Void JetBrains.Annotations.CannotApplyEqualityOperatorAttribute::.ctor()
extern void CannotApplyEqualityOperatorAttribute__ctor_m62FF7A1B6FD14BC114841E92C06CDF10060D7857 ();
// 0x0000001B System.Void JetBrains.Annotations.BaseTypeRequiredAttribute::.ctor(System.Type)
extern void BaseTypeRequiredAttribute__ctor_m78EAA13CF24B87AA473439C4273861E6FB034F0F ();
// 0x0000001C System.Type JetBrains.Annotations.BaseTypeRequiredAttribute::get_BaseType()
extern void BaseTypeRequiredAttribute_get_BaseType_m6B331E97F682B2371C7E5C1BAAD65B6030AFEF7E ();
// 0x0000001D System.Void JetBrains.Annotations.BaseTypeRequiredAttribute::set_BaseType(System.Type)
extern void BaseTypeRequiredAttribute_set_BaseType_m0E7A68B87BCBF97208AB3B7D975773835CCB6B8C ();
// 0x0000001E System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor()
extern void UsedImplicitlyAttribute__ctor_m9967794C58FA80A15A2EB59A219C9211DB2161E8 ();
// 0x0000001F System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags)
extern void UsedImplicitlyAttribute__ctor_mFD9C6C2E2B1CF69D3CAAA0C308226F009E3D1852 ();
// 0x00000020 System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor(JetBrains.Annotations.ImplicitUseTargetFlags)
extern void UsedImplicitlyAttribute__ctor_m5780F7D5CA60ADEE1A8C8B6B66319D8C66EF0667 ();
// 0x00000021 System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags,JetBrains.Annotations.ImplicitUseTargetFlags)
extern void UsedImplicitlyAttribute__ctor_mF053D1548C9F0310AAE782C1AE30BC81A417BE5E ();
// 0x00000022 JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.UsedImplicitlyAttribute::get_UseKindFlags()
extern void UsedImplicitlyAttribute_get_UseKindFlags_m78456325D2370062434A60F23EEE53CA2061D1E0 ();
// 0x00000023 System.Void JetBrains.Annotations.UsedImplicitlyAttribute::set_UseKindFlags(JetBrains.Annotations.ImplicitUseKindFlags)
extern void UsedImplicitlyAttribute_set_UseKindFlags_m5D8FE4E5A37C11453F7C376FFC0ED0E84B9D0ADB ();
// 0x00000024 JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.UsedImplicitlyAttribute::get_TargetFlags()
extern void UsedImplicitlyAttribute_get_TargetFlags_mD2C04C854F767492D225660763574027B3C9D98C ();
// 0x00000025 System.Void JetBrains.Annotations.UsedImplicitlyAttribute::set_TargetFlags(JetBrains.Annotations.ImplicitUseTargetFlags)
extern void UsedImplicitlyAttribute_set_TargetFlags_mE8D6A7BABDB5B3846B11D2E3BCD0C2AEE644132F ();
// 0x00000026 System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor()
extern void MeansImplicitUseAttribute__ctor_mEFFCE62C8ECBAB317DF92843D6645F8FEDE7D539 ();
// 0x00000027 System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags)
extern void MeansImplicitUseAttribute__ctor_m231D188014C6BF4ADA08F119810722D7D182E289 ();
// 0x00000028 System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseTargetFlags)
extern void MeansImplicitUseAttribute__ctor_mA48E7E71BD8C8A022384A792977F329C2D7E5755 ();
// 0x00000029 System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags,JetBrains.Annotations.ImplicitUseTargetFlags)
extern void MeansImplicitUseAttribute__ctor_m8B1CB0647B10039C21C2A9A456E9E4C502331FFF ();
// 0x0000002A JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.MeansImplicitUseAttribute::get_UseKindFlags()
extern void MeansImplicitUseAttribute_get_UseKindFlags_m9E68CD91BECB87F9140185F69D2C66C8E12FEAFA ();
// 0x0000002B System.Void JetBrains.Annotations.MeansImplicitUseAttribute::set_UseKindFlags(JetBrains.Annotations.ImplicitUseKindFlags)
extern void MeansImplicitUseAttribute_set_UseKindFlags_mC020328ACCA94C5816E6F418E7CC6EB9E1E2C9F0 ();
// 0x0000002C JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.MeansImplicitUseAttribute::get_TargetFlags()
extern void MeansImplicitUseAttribute_get_TargetFlags_mB42E1B2CF4F2D83F4D8738F9DD46AD02B0C2BA5B ();
// 0x0000002D System.Void JetBrains.Annotations.MeansImplicitUseAttribute::set_TargetFlags(JetBrains.Annotations.ImplicitUseTargetFlags)
extern void MeansImplicitUseAttribute_set_TargetFlags_mCCCE7B4BCF2EA74538E4C90496208ACAD32B7A8F ();
// 0x0000002E System.Void JetBrains.Annotations.PublicAPIAttribute::.ctor()
extern void PublicAPIAttribute__ctor_m819E9E7C36012471D43F2465EA4D7CFB59548673 ();
// 0x0000002F System.Void JetBrains.Annotations.PublicAPIAttribute::.ctor(System.String)
extern void PublicAPIAttribute__ctor_m9DE454053E55A92BC7D309F4CBA1E42822B2F532 ();
// 0x00000030 System.String JetBrains.Annotations.PublicAPIAttribute::get_Comment()
extern void PublicAPIAttribute_get_Comment_m2E3F85A747F4F5EB1277D90AC9405837AD679F38 ();
// 0x00000031 System.Void JetBrains.Annotations.PublicAPIAttribute::set_Comment(System.String)
extern void PublicAPIAttribute_set_Comment_m803065FC1B1C3AE5533C775B09668E5F210ABAC2 ();
// 0x00000032 System.Void JetBrains.Annotations.InstantHandleAttribute::.ctor()
extern void InstantHandleAttribute__ctor_m2FB33A477515F7831182D5B0848BB50171F84FB5 ();
// 0x00000033 System.Void JetBrains.Annotations.PureAttribute::.ctor()
extern void PureAttribute__ctor_mCDEF6E5AB727A4452739AEECD7E5FA3EA59C77C8 ();
// 0x00000034 System.Void JetBrains.Annotations.MustUseReturnValueAttribute::.ctor()
extern void MustUseReturnValueAttribute__ctor_mA0B62B6EAF331274159186A4C5305B95A08572C7 ();
// 0x00000035 System.Void JetBrains.Annotations.MustUseReturnValueAttribute::.ctor(System.String)
extern void MustUseReturnValueAttribute__ctor_m8A84924BE475325A76AEEA74C27CD170627C5CAF ();
// 0x00000036 System.String JetBrains.Annotations.MustUseReturnValueAttribute::get_Justification()
extern void MustUseReturnValueAttribute_get_Justification_m9049DF7AA84BEE837E554F3DE6AB79E6D7071D63 ();
// 0x00000037 System.Void JetBrains.Annotations.MustUseReturnValueAttribute::set_Justification(System.String)
extern void MustUseReturnValueAttribute_set_Justification_m1DAE6CCA661B24A56C45C0ACF84FD195C7D44A04 ();
// 0x00000038 System.Void JetBrains.Annotations.ProvidesContextAttribute::.ctor()
extern void ProvidesContextAttribute__ctor_mBDC4E52D357F22267F945104CA9219E3DF030743 ();
// 0x00000039 System.Void JetBrains.Annotations.PathReferenceAttribute::.ctor()
extern void PathReferenceAttribute__ctor_mB7F021886F17C36D1C711E35D6BDCD912A066A09 ();
// 0x0000003A System.Void JetBrains.Annotations.PathReferenceAttribute::.ctor(System.String)
extern void PathReferenceAttribute__ctor_m66BFC638B85ACEBAAD54F177D979157825922263 ();
// 0x0000003B System.String JetBrains.Annotations.PathReferenceAttribute::get_BasePath()
extern void PathReferenceAttribute_get_BasePath_mA9878FEB3B935EF9EB1CAAC91F206459B10363EA ();
// 0x0000003C System.Void JetBrains.Annotations.PathReferenceAttribute::set_BasePath(System.String)
extern void PathReferenceAttribute_set_BasePath_m6039B47746370E59513C5C7161053AE20F741255 ();
// 0x0000003D System.Void JetBrains.Annotations.SourceTemplateAttribute::.ctor()
extern void SourceTemplateAttribute__ctor_mF671F6BE3261CDC2EBE1AC338149B62C6425045D ();
// 0x0000003E System.String JetBrains.Annotations.MacroAttribute::get_Expression()
extern void MacroAttribute_get_Expression_m2CCFF5D806E7E97ADAC0EEBC9BAF05F89C0D2AAA ();
// 0x0000003F System.Void JetBrains.Annotations.MacroAttribute::set_Expression(System.String)
extern void MacroAttribute_set_Expression_m0582A717F271662A01010819A8E6DFC21873CD32 ();
// 0x00000040 System.Int32 JetBrains.Annotations.MacroAttribute::get_Editable()
extern void MacroAttribute_get_Editable_mC6D969B99E215839850D37F56E090BC3FF630DE0 ();
// 0x00000041 System.Void JetBrains.Annotations.MacroAttribute::set_Editable(System.Int32)
extern void MacroAttribute_set_Editable_mCDF7F281D18B0DBD6902F6297B2AD01B3D9534AF ();
// 0x00000042 System.String JetBrains.Annotations.MacroAttribute::get_Target()
extern void MacroAttribute_get_Target_mBDCBF8052A69036B262E81773A25AF159B568B3A ();
// 0x00000043 System.Void JetBrains.Annotations.MacroAttribute::set_Target(System.String)
extern void MacroAttribute_set_Target_m4D64FFE273A7BD4BF911FD0ABE35DF2F46B134EB ();
// 0x00000044 System.Void JetBrains.Annotations.MacroAttribute::.ctor()
extern void MacroAttribute__ctor_m9113EDB7BD048A9585647F7B4A888DC43A9303F0 ();
// 0x00000045 System.Void JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::.ctor(System.String)
extern void AspMvcAreaMasterLocationFormatAttribute__ctor_mDF9E66FCDBFA3CF0C2F526280B3BEB73E1C760CE ();
// 0x00000046 System.String JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::get_Format()
extern void AspMvcAreaMasterLocationFormatAttribute_get_Format_mBCD5EFD48945348F59A7A204502B3D0C51C315FA ();
// 0x00000047 System.Void JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::set_Format(System.String)
extern void AspMvcAreaMasterLocationFormatAttribute_set_Format_m23924305517051ABCC0C5A64FBC969E57DD64CC1 ();
// 0x00000048 System.Void JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::.ctor(System.String)
extern void AspMvcAreaPartialViewLocationFormatAttribute__ctor_m22B96DDFD5669C96F54AC79899DB78FA8B07181E ();
// 0x00000049 System.String JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::get_Format()
extern void AspMvcAreaPartialViewLocationFormatAttribute_get_Format_m0ED3DECD9BC9DEEF7551EC70424316B78F63E7C1 ();
// 0x0000004A System.Void JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::set_Format(System.String)
extern void AspMvcAreaPartialViewLocationFormatAttribute_set_Format_mBA39E10F8E9F267F28B653341FECA4E8D75E29C0 ();
// 0x0000004B System.Void JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::.ctor(System.String)
extern void AspMvcAreaViewLocationFormatAttribute__ctor_mFBA83D70929DAA000B726DB007FC0FA6BEBDAD4F ();
// 0x0000004C System.String JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::get_Format()
extern void AspMvcAreaViewLocationFormatAttribute_get_Format_m7F2E5A569366D48B3831CED9CA51DB28EA87B3F4 ();
// 0x0000004D System.Void JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::set_Format(System.String)
extern void AspMvcAreaViewLocationFormatAttribute_set_Format_mFA517B963A59B6F24995B1051BBD644B19A6E1B6 ();
// 0x0000004E System.Void JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::.ctor(System.String)
extern void AspMvcMasterLocationFormatAttribute__ctor_m957883C543F9B6A78CF0AC66B4ED0729BA8FF4D0 ();
// 0x0000004F System.String JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::get_Format()
extern void AspMvcMasterLocationFormatAttribute_get_Format_m2616278737402EE51EC14B7DD4E790226808FA38 ();
// 0x00000050 System.Void JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::set_Format(System.String)
extern void AspMvcMasterLocationFormatAttribute_set_Format_m852EA80CBBEED7BB8853D695FE37C1C9E5A707C3 ();
// 0x00000051 System.Void JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::.ctor(System.String)
extern void AspMvcPartialViewLocationFormatAttribute__ctor_mE4E72137B00EEFBFFEF64FAECB98DB9C1E70217A ();
// 0x00000052 System.String JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::get_Format()
extern void AspMvcPartialViewLocationFormatAttribute_get_Format_m2D0B7F0A062EEDFB7EFF147252917D3D590ABEB8 ();
// 0x00000053 System.Void JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::set_Format(System.String)
extern void AspMvcPartialViewLocationFormatAttribute_set_Format_m1D39A9129B1D56D968870065FDD2E65244FFB3F9 ();
// 0x00000054 System.Void JetBrains.Annotations.AspMvcViewLocationFormatAttribute::.ctor(System.String)
extern void AspMvcViewLocationFormatAttribute__ctor_m69C7FCA1E0359B5BD5F8867DD3B785D0B1EE1A17 ();
// 0x00000055 System.String JetBrains.Annotations.AspMvcViewLocationFormatAttribute::get_Format()
extern void AspMvcViewLocationFormatAttribute_get_Format_mE9CEBE3AE63F8049ED45B1D507E5680BECB333D9 ();
// 0x00000056 System.Void JetBrains.Annotations.AspMvcViewLocationFormatAttribute::set_Format(System.String)
extern void AspMvcViewLocationFormatAttribute_set_Format_m9AB87404E45FA9C2AAFD4057124EE2181ACCE963 ();
// 0x00000057 System.Void JetBrains.Annotations.AspMvcActionAttribute::.ctor()
extern void AspMvcActionAttribute__ctor_m5224998BAD51F889DB341A7242CA569C5461B732 ();
// 0x00000058 System.Void JetBrains.Annotations.AspMvcActionAttribute::.ctor(System.String)
extern void AspMvcActionAttribute__ctor_m4EDD633B43CC7F9929C553E4C2DDC480F9E352B1 ();
// 0x00000059 System.String JetBrains.Annotations.AspMvcActionAttribute::get_AnonymousProperty()
extern void AspMvcActionAttribute_get_AnonymousProperty_mECC7F953EB784ECECCD91779A41F81BC31B77774 ();
// 0x0000005A System.Void JetBrains.Annotations.AspMvcActionAttribute::set_AnonymousProperty(System.String)
extern void AspMvcActionAttribute_set_AnonymousProperty_m83EA3BE21BBB1F4735FBE043543BD4984590BBDA ();
// 0x0000005B System.Void JetBrains.Annotations.AspMvcAreaAttribute::.ctor()
extern void AspMvcAreaAttribute__ctor_m08704D0ED240F06DB2295D2E96CA1C76394D6D31 ();
// 0x0000005C System.Void JetBrains.Annotations.AspMvcAreaAttribute::.ctor(System.String)
extern void AspMvcAreaAttribute__ctor_mBB4BB8321027B8620A47FEFF5A5ABE7DBF6669BB ();
// 0x0000005D System.String JetBrains.Annotations.AspMvcAreaAttribute::get_AnonymousProperty()
extern void AspMvcAreaAttribute_get_AnonymousProperty_m1A0AD54CBC1596C72E9B9DC1AE1C1EBBFD843DC6 ();
// 0x0000005E System.Void JetBrains.Annotations.AspMvcAreaAttribute::set_AnonymousProperty(System.String)
extern void AspMvcAreaAttribute_set_AnonymousProperty_m6217B255496EF23DBEFF147D69F4CC75028DD372 ();
// 0x0000005F System.Void JetBrains.Annotations.AspMvcControllerAttribute::.ctor()
extern void AspMvcControllerAttribute__ctor_mCFBCD113CFB32BB3B557F418E22D3144321D7278 ();
// 0x00000060 System.Void JetBrains.Annotations.AspMvcControllerAttribute::.ctor(System.String)
extern void AspMvcControllerAttribute__ctor_mBAB20CE3BE9F3E5356A3D46BF585E1AB5CC0034E ();
// 0x00000061 System.String JetBrains.Annotations.AspMvcControllerAttribute::get_AnonymousProperty()
extern void AspMvcControllerAttribute_get_AnonymousProperty_m1D0891365029E522461959DA55BF330645E623FD ();
// 0x00000062 System.Void JetBrains.Annotations.AspMvcControllerAttribute::set_AnonymousProperty(System.String)
extern void AspMvcControllerAttribute_set_AnonymousProperty_mDB5062FA1F14FB6FBA2034FFD85CDA4EDFAF5083 ();
// 0x00000063 System.Void JetBrains.Annotations.AspMvcMasterAttribute::.ctor()
extern void AspMvcMasterAttribute__ctor_mDB7FBDE7214AB5996EC26B1CCA452C8C8D93BF16 ();
// 0x00000064 System.Void JetBrains.Annotations.AspMvcModelTypeAttribute::.ctor()
extern void AspMvcModelTypeAttribute__ctor_mF2A57B73788689B0DA7EB3709EE02416ED518CA1 ();
// 0x00000065 System.Void JetBrains.Annotations.AspMvcPartialViewAttribute::.ctor()
extern void AspMvcPartialViewAttribute__ctor_m1B14296015A3BB5BF56E0B722F7A85BE15D70765 ();
// 0x00000066 System.Void JetBrains.Annotations.AspMvcSuppressViewErrorAttribute::.ctor()
extern void AspMvcSuppressViewErrorAttribute__ctor_mB98050F8997645798F653FD5FCE352857095348F ();
// 0x00000067 System.Void JetBrains.Annotations.AspMvcDisplayTemplateAttribute::.ctor()
extern void AspMvcDisplayTemplateAttribute__ctor_m4F28D2E29737294FF54CD12C087DE3C52B813254 ();
// 0x00000068 System.Void JetBrains.Annotations.AspMvcEditorTemplateAttribute::.ctor()
extern void AspMvcEditorTemplateAttribute__ctor_mD0BC86FB6BD7E0092EFFBD2E2B98A62C49580201 ();
// 0x00000069 System.Void JetBrains.Annotations.AspMvcTemplateAttribute::.ctor()
extern void AspMvcTemplateAttribute__ctor_m78BE9483D62067D284B3A8FD59B7A11872013C74 ();
// 0x0000006A System.Void JetBrains.Annotations.AspMvcViewAttribute::.ctor()
extern void AspMvcViewAttribute__ctor_m03E325150A64E35683DC6FCD44476D5412E24CA0 ();
// 0x0000006B System.Void JetBrains.Annotations.AspMvcViewComponentAttribute::.ctor()
extern void AspMvcViewComponentAttribute__ctor_m47DE1A1823403724779B3F93ACE1D4629DE579C7 ();
// 0x0000006C System.Void JetBrains.Annotations.AspMvcViewComponentViewAttribute::.ctor()
extern void AspMvcViewComponentViewAttribute__ctor_m9E60CD0D9B3108F801BD7543A33DAB071F97D57F ();
// 0x0000006D System.Void JetBrains.Annotations.AspMvcActionSelectorAttribute::.ctor()
extern void AspMvcActionSelectorAttribute__ctor_m99959ADAF51BF6F8F3C91DAA68C49D184C3E44BE ();
// 0x0000006E System.Void JetBrains.Annotations.HtmlElementAttributesAttribute::.ctor()
extern void HtmlElementAttributesAttribute__ctor_m488A5CFA47EFEA4166ACBB51212AB8FE01BFC2AC ();
// 0x0000006F System.Void JetBrains.Annotations.HtmlElementAttributesAttribute::.ctor(System.String)
extern void HtmlElementAttributesAttribute__ctor_mEF36AA00424E0F2FE683B95A2AC6BAEB0CE98CD3 ();
// 0x00000070 System.String JetBrains.Annotations.HtmlElementAttributesAttribute::get_Name()
extern void HtmlElementAttributesAttribute_get_Name_mD22BB01CEDBF1E1ACE7373CEEF4DC693E0CB1D37 ();
// 0x00000071 System.Void JetBrains.Annotations.HtmlElementAttributesAttribute::set_Name(System.String)
extern void HtmlElementAttributesAttribute_set_Name_mC66C3677ECE785266657B3BCCE620ED99AF9D4AE ();
// 0x00000072 System.Void JetBrains.Annotations.HtmlAttributeValueAttribute::.ctor(System.String)
extern void HtmlAttributeValueAttribute__ctor_mCAA079679C0191C23EF6DFBC710A0D875207E329 ();
// 0x00000073 System.String JetBrains.Annotations.HtmlAttributeValueAttribute::get_Name()
extern void HtmlAttributeValueAttribute_get_Name_m84B4C87BEF3F505B24072A0AE18CFE3C619719E3 ();
// 0x00000074 System.Void JetBrains.Annotations.HtmlAttributeValueAttribute::set_Name(System.String)
extern void HtmlAttributeValueAttribute_set_Name_m40857D8975B094CCA401959469C902D8E4EF2F13 ();
// 0x00000075 System.Void JetBrains.Annotations.RazorSectionAttribute::.ctor()
extern void RazorSectionAttribute__ctor_m9A7DFE508BD5E260951EFDD17E5CAB93C54A759F ();
// 0x00000076 System.Void JetBrains.Annotations.CollectionAccessAttribute::.ctor(JetBrains.Annotations.CollectionAccessType)
extern void CollectionAccessAttribute__ctor_mD77E92806695EDAC272BBB1BA6E4F2CFE66FF413 ();
// 0x00000077 JetBrains.Annotations.CollectionAccessType JetBrains.Annotations.CollectionAccessAttribute::get_CollectionAccessType()
extern void CollectionAccessAttribute_get_CollectionAccessType_m1BFB8DFEBA87E31D69B182791AFADCEE41827673 ();
// 0x00000078 System.Void JetBrains.Annotations.CollectionAccessAttribute::set_CollectionAccessType(JetBrains.Annotations.CollectionAccessType)
extern void CollectionAccessAttribute_set_CollectionAccessType_mBE8F74690533AF7885DF9E691A758BB6CDA15529 ();
// 0x00000079 System.Void JetBrains.Annotations.AssertionMethodAttribute::.ctor()
extern void AssertionMethodAttribute__ctor_mAE5114C050EAB7D5663F20F9F46D3E302B8EE53E ();
// 0x0000007A System.Void JetBrains.Annotations.AssertionConditionAttribute::.ctor(JetBrains.Annotations.AssertionConditionType)
extern void AssertionConditionAttribute__ctor_mF0B5B121B7C0396DB7F13F1BC78BAFDCAA24CA8B ();
// 0x0000007B JetBrains.Annotations.AssertionConditionType JetBrains.Annotations.AssertionConditionAttribute::get_ConditionType()
extern void AssertionConditionAttribute_get_ConditionType_m0EFFDFD6C0C912B759E4380A035A30B3C8966167 ();
// 0x0000007C System.Void JetBrains.Annotations.AssertionConditionAttribute::set_ConditionType(JetBrains.Annotations.AssertionConditionType)
extern void AssertionConditionAttribute_set_ConditionType_m2E1C8FDBA6328631D71E7EDF3A655F521B606727 ();
// 0x0000007D System.Void JetBrains.Annotations.TerminatesProgramAttribute::.ctor()
extern void TerminatesProgramAttribute__ctor_m3879D60EAD0C1E01E14AF7A6B28F8F489CBF01C7 ();
// 0x0000007E System.Void JetBrains.Annotations.LinqTunnelAttribute::.ctor()
extern void LinqTunnelAttribute__ctor_m7FBC5773938344987792E196D4E9CCF6F9973841 ();
// 0x0000007F System.Void JetBrains.Annotations.NoEnumerationAttribute::.ctor()
extern void NoEnumerationAttribute__ctor_mADC051CDC91A0FFB0DF756783DB7684445444A92 ();
// 0x00000080 System.Void JetBrains.Annotations.RegexPatternAttribute::.ctor()
extern void RegexPatternAttribute__ctor_mF1530725493264C8C2E70C4048CF05B1F90FD38B ();
// 0x00000081 System.Void JetBrains.Annotations.NoReorderAttribute::.ctor()
extern void NoReorderAttribute__ctor_mE22C5847FFD0A1FDD96AB2F7620F802D1A45120E ();
// 0x00000082 System.Void JetBrains.Annotations.XamlItemsControlAttribute::.ctor()
extern void XamlItemsControlAttribute__ctor_mA3FBD1004D93BEB2A4CE7CDD7B1B6B591A2B86B7 ();
// 0x00000083 System.Void JetBrains.Annotations.XamlItemBindingOfItemsControlAttribute::.ctor()
extern void XamlItemBindingOfItemsControlAttribute__ctor_m3C12FC84D10A36593227C7D61A4AB5517A99AC22 ();
// 0x00000084 System.Void JetBrains.Annotations.AspChildControlTypeAttribute::.ctor(System.String,System.Type)
extern void AspChildControlTypeAttribute__ctor_mEEED24DA23322F148E3E1B0988462D99D712202E ();
// 0x00000085 System.String JetBrains.Annotations.AspChildControlTypeAttribute::get_TagName()
extern void AspChildControlTypeAttribute_get_TagName_m47ED2DE7A3A29BED2CA5CB925C49EBD7548276D3 ();
// 0x00000086 System.Void JetBrains.Annotations.AspChildControlTypeAttribute::set_TagName(System.String)
extern void AspChildControlTypeAttribute_set_TagName_m6A9A3D854C58738EBD8A0D379FB6FD5AA2D69BAD ();
// 0x00000087 System.Type JetBrains.Annotations.AspChildControlTypeAttribute::get_ControlType()
extern void AspChildControlTypeAttribute_get_ControlType_m4C91CD8ECEC1EDF68E1FC66970BA667F9D0930BC ();
// 0x00000088 System.Void JetBrains.Annotations.AspChildControlTypeAttribute::set_ControlType(System.Type)
extern void AspChildControlTypeAttribute_set_ControlType_mF8FD2F6F1BD65DB6966EC199D6DCBF9930D71EA0 ();
// 0x00000089 System.Void JetBrains.Annotations.AspDataFieldAttribute::.ctor()
extern void AspDataFieldAttribute__ctor_m6CC0DE0330087CD5EF8CA17A4161C3363A9755D9 ();
// 0x0000008A System.Void JetBrains.Annotations.AspDataFieldsAttribute::.ctor()
extern void AspDataFieldsAttribute__ctor_m2693844D5D26ED410122666D69D7FA02C89E24CA ();
// 0x0000008B System.Void JetBrains.Annotations.AspMethodPropertyAttribute::.ctor()
extern void AspMethodPropertyAttribute__ctor_m7CD3D54FD2697B7C221007A1BA3A6D67970A9D90 ();
// 0x0000008C System.Void JetBrains.Annotations.AspRequiredAttributeAttribute::.ctor(System.String)
extern void AspRequiredAttributeAttribute__ctor_m006E2662B92F085BC9FADD256D8BBF64C32A8F47 ();
// 0x0000008D System.String JetBrains.Annotations.AspRequiredAttributeAttribute::get_Attribute()
extern void AspRequiredAttributeAttribute_get_Attribute_m0D61E17144E7303EFFE4C383656C8E716B819542 ();
// 0x0000008E System.Void JetBrains.Annotations.AspRequiredAttributeAttribute::set_Attribute(System.String)
extern void AspRequiredAttributeAttribute_set_Attribute_m4C27C1CF35F50F5A398DE77CBD9E3BA4E49FFE83 ();
// 0x0000008F System.Boolean JetBrains.Annotations.AspTypePropertyAttribute::get_CreateConstructorReferences()
extern void AspTypePropertyAttribute_get_CreateConstructorReferences_m418D57DFE0F81617864CF81B8E042085FD4CE464 ();
// 0x00000090 System.Void JetBrains.Annotations.AspTypePropertyAttribute::set_CreateConstructorReferences(System.Boolean)
extern void AspTypePropertyAttribute_set_CreateConstructorReferences_m953788280CA7DCB18C58C6CDB8DB221C857F4037 ();
// 0x00000091 System.Void JetBrains.Annotations.AspTypePropertyAttribute::.ctor(System.Boolean)
extern void AspTypePropertyAttribute__ctor_mA5F306BCD34B2EDCF448F2B0F9445A345936C955 ();
// 0x00000092 System.Void JetBrains.Annotations.RazorImportNamespaceAttribute::.ctor(System.String)
extern void RazorImportNamespaceAttribute__ctor_m88390011700E4FA1A761B6CA2E98C7D3BB22F34D ();
// 0x00000093 System.String JetBrains.Annotations.RazorImportNamespaceAttribute::get_Name()
extern void RazorImportNamespaceAttribute_get_Name_m786E27B225452D992829499D9D512D1C3396701C ();
// 0x00000094 System.Void JetBrains.Annotations.RazorImportNamespaceAttribute::set_Name(System.String)
extern void RazorImportNamespaceAttribute_set_Name_m140C4B453B55F6C62F62B7B38A56B2FEE8D0C090 ();
// 0x00000095 System.Void JetBrains.Annotations.RazorInjectionAttribute::.ctor(System.String,System.String)
extern void RazorInjectionAttribute__ctor_m90A7D9C396BFB384D3B30B2D65E5F81D0DB53ECD ();
// 0x00000096 System.String JetBrains.Annotations.RazorInjectionAttribute::get_Type()
extern void RazorInjectionAttribute_get_Type_mFCF5011ED4738143B9262DD5DAA3730AB3D80479 ();
// 0x00000097 System.Void JetBrains.Annotations.RazorInjectionAttribute::set_Type(System.String)
extern void RazorInjectionAttribute_set_Type_mC7841CBEBEF456EE2AD385F6C4E4F86154A4BC84 ();
// 0x00000098 System.String JetBrains.Annotations.RazorInjectionAttribute::get_FieldName()
extern void RazorInjectionAttribute_get_FieldName_m278F9CED40DBE4C653C100D7B67A81B6D9138DE2 ();
// 0x00000099 System.Void JetBrains.Annotations.RazorInjectionAttribute::set_FieldName(System.String)
extern void RazorInjectionAttribute_set_FieldName_m57A348CB52BE094401092C2631B2500E8E840BFD ();
// 0x0000009A System.Void JetBrains.Annotations.RazorDirectiveAttribute::.ctor(System.String)
extern void RazorDirectiveAttribute__ctor_mA28CF8516DE82819A685A5219FD4BFC6B68EA5C4 ();
// 0x0000009B System.String JetBrains.Annotations.RazorDirectiveAttribute::get_Directive()
extern void RazorDirectiveAttribute_get_Directive_mA77DF0374D01746A4D860EE4007565AD038C8143 ();
// 0x0000009C System.Void JetBrains.Annotations.RazorDirectiveAttribute::set_Directive(System.String)
extern void RazorDirectiveAttribute_set_Directive_m13E8B8A3D7AA46CFC68ADD972BECE00EE489A1D2 ();
// 0x0000009D System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::.ctor(System.String)
extern void RazorPageBaseTypeAttribute__ctor_mBAC60C3D4748379F9C5AD2991C423BED385F8CF2 ();
// 0x0000009E System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::.ctor(System.String,System.String)
extern void RazorPageBaseTypeAttribute__ctor_m544D67B7090DF8044982853BED74D31DE07D608C ();
// 0x0000009F System.String JetBrains.Annotations.RazorPageBaseTypeAttribute::get_BaseType()
extern void RazorPageBaseTypeAttribute_get_BaseType_m333BD2487E7BB969B2D5FCAB026E0487950FEFD6 ();
// 0x000000A0 System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::set_BaseType(System.String)
extern void RazorPageBaseTypeAttribute_set_BaseType_m68D508FAD7FB7558AD5B63D899D61AA9339113D7 ();
// 0x000000A1 System.String JetBrains.Annotations.RazorPageBaseTypeAttribute::get_PageName()
extern void RazorPageBaseTypeAttribute_get_PageName_m1E0BC993F961B70AABED35A9C3D9FCF96A29598A ();
// 0x000000A2 System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::set_PageName(System.String)
extern void RazorPageBaseTypeAttribute_set_PageName_m919DD65C86D7D50B0D64004F4A11B5D08066DC25 ();
// 0x000000A3 System.Void JetBrains.Annotations.RazorHelperCommonAttribute::.ctor()
extern void RazorHelperCommonAttribute__ctor_mA324FDB54824F3BD2D42C585F4382A8F02F8680A ();
// 0x000000A4 System.Void JetBrains.Annotations.RazorLayoutAttribute::.ctor()
extern void RazorLayoutAttribute__ctor_m51575EA28B21943AB294FAFD2D6B5A96809CE63B ();
// 0x000000A5 System.Void JetBrains.Annotations.RazorWriteLiteralMethodAttribute::.ctor()
extern void RazorWriteLiteralMethodAttribute__ctor_mDB6F23E5D037253D1563E88EAE3DDB008575EEA1 ();
// 0x000000A6 System.Void JetBrains.Annotations.RazorWriteMethodAttribute::.ctor()
extern void RazorWriteMethodAttribute__ctor_m02A0D0B08AA62C9A40187C135A6F1746C0464BAC ();
// 0x000000A7 System.Void JetBrains.Annotations.RazorWriteMethodParameterAttribute::.ctor()
extern void RazorWriteMethodParameterAttribute__ctor_m86B45C312985B19500293D1DADEAA112CC573D69 ();
// 0x000000A8 System.Void Zenject.IGuiRenderable::GuiRender()
// 0x000000A9 System.Void Zenject.IInitializable::Initialize()
// 0x000000AA System.Void Zenject.InjectableInfo::.ctor(System.Boolean,System.Object,System.String,System.Type,System.Object,Zenject.InjectSources)
extern void InjectableInfo__ctor_m658D7FA40B2FD6046E44B67EF6B59BBC2F63F058 ();
// 0x000000AB System.Void Zenject.InjectAttribute::.ctor()
extern void InjectAttribute__ctor_m98AD3B24006CAF44B6D24E3FA1A6841F9BD45342 ();
// 0x000000AC System.Void Zenject.InjectLocalAttribute::.ctor()
extern void InjectLocalAttribute__ctor_mC795F3A21C4A90B9CA501FD84ECAF6939157FF04 ();
// 0x000000AD System.Void Zenject.InjectOptionalAttribute::.ctor()
extern void InjectOptionalAttribute__ctor_mF21688E8D80BDD59EB82431F8FBD12D7891673A1 ();
// 0x000000AE System.Void Zenject.ZenInjectMethod::.ctor(System.Object,System.IntPtr)
extern void ZenInjectMethod__ctor_mAE02246B1ECE29A84AAB66E1730F2F2206BC2F3E ();
// 0x000000AF System.Void Zenject.ZenInjectMethod::Invoke(System.Object,System.Object[])
extern void ZenInjectMethod_Invoke_m9F84D1E5F9B6E000D5B103FB8ECA26AB8814E408 ();
// 0x000000B0 System.IAsyncResult Zenject.ZenInjectMethod::BeginInvoke(System.Object,System.Object[],System.AsyncCallback,System.Object)
extern void ZenInjectMethod_BeginInvoke_m53CFF280203B2FDCC1BBADD133C0A0FD304204CA ();
// 0x000000B1 System.Void Zenject.ZenInjectMethod::EndInvoke(System.IAsyncResult)
extern void ZenInjectMethod_EndInvoke_mA44C12A7FE1416726ABC14A2C51D1B6156C626DA ();
// 0x000000B2 System.Void Zenject.ZenFactoryMethod::.ctor(System.Object,System.IntPtr)
extern void ZenFactoryMethod__ctor_m461B117D19C422609F609B2454BF059DE6696E46 ();
// 0x000000B3 System.Object Zenject.ZenFactoryMethod::Invoke(System.Object[])
extern void ZenFactoryMethod_Invoke_m141F13459ED8ED452242759B11B781225A90FEAB ();
// 0x000000B4 System.IAsyncResult Zenject.ZenFactoryMethod::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern void ZenFactoryMethod_BeginInvoke_m0219A0A66C3D4DC63EACAF92304B2A01B4F489E8 ();
// 0x000000B5 System.Object Zenject.ZenFactoryMethod::EndInvoke(System.IAsyncResult)
extern void ZenFactoryMethod_EndInvoke_m2676D16EE20429C9D848CA8672ECEA359824926F ();
// 0x000000B6 System.Void Zenject.ZenMemberSetterMethod::.ctor(System.Object,System.IntPtr)
extern void ZenMemberSetterMethod__ctor_m2F7603EFCA3FFA557C57F782AB6515377C581338 ();
// 0x000000B7 System.Void Zenject.ZenMemberSetterMethod::Invoke(System.Object,System.Object)
extern void ZenMemberSetterMethod_Invoke_m478200CD23399D996E8B8CCF38638F03A369DB8C ();
// 0x000000B8 System.IAsyncResult Zenject.ZenMemberSetterMethod::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern void ZenMemberSetterMethod_BeginInvoke_mD54AEF0DEB5C012710B5540975C6C7FDA9B90B1E ();
// 0x000000B9 System.Void Zenject.ZenMemberSetterMethod::EndInvoke(System.IAsyncResult)
extern void ZenMemberSetterMethod_EndInvoke_mB1D14DDDE7B307CA49E7E416290ECC0FD3488280 ();
// 0x000000BA System.Void Zenject.InjectTypeInfo::.ctor(System.Type,Zenject.InjectTypeInfo_InjectConstructorInfo,Zenject.InjectTypeInfo_InjectMethodInfo[],Zenject.InjectTypeInfo_InjectMemberInfo[])
extern void InjectTypeInfo__ctor_m4F1AABE23B2C121781BED8AA5118C426626689D2 ();
// 0x000000BB Zenject.InjectTypeInfo Zenject.InjectTypeInfo::get_BaseTypeInfo()
extern void InjectTypeInfo_get_BaseTypeInfo_m3651B4151F25B3B32E0752590E3B223E5B809E44 ();
// 0x000000BC System.Void Zenject.InjectTypeInfo::set_BaseTypeInfo(Zenject.InjectTypeInfo)
extern void InjectTypeInfo_set_BaseTypeInfo_m421413DB209610F6DB8DAB4192B7FD2E8281EC85 ();
// 0x000000BD System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.InjectTypeInfo::get_AllInjectables()
extern void InjectTypeInfo_get_AllInjectables_m4459958ADFD4FCCD62A54B3C571EB2A2BBFE2417 ();
// 0x000000BE System.Void Zenject.InjectTypeInfo_InjectMemberInfo::.ctor(Zenject.ZenMemberSetterMethod,Zenject.InjectableInfo)
extern void InjectMemberInfo__ctor_m6332CC8B6797368DFF7B36287E75A3B7F4E5123D ();
// 0x000000BF System.Void Zenject.InjectTypeInfo_InjectConstructorInfo::.ctor(Zenject.ZenFactoryMethod,Zenject.InjectableInfo[])
extern void InjectConstructorInfo__ctor_m3A5ABBF6F67CD8B233A41C024CB45588E2159135 ();
// 0x000000C0 System.Void Zenject.InjectTypeInfo_InjectMethodInfo::.ctor(Zenject.ZenInjectMethod,Zenject.InjectableInfo[],System.String)
extern void InjectMethodInfo__ctor_m35025625F36C9D1178F6A85CCA817793161418A9 ();
// 0x000000C1 System.Void Zenject.InjectTypeInfo_<>c::.cctor()
extern void U3CU3Ec__cctor_mE2354A854E5D0CFE8FF62DCF6A806ECF6D80C289 ();
// 0x000000C2 System.Void Zenject.InjectTypeInfo_<>c::.ctor()
extern void U3CU3Ec__ctor_m8BA7B247476AA2C17B97E0C9A67F8472E8AE5112 ();
// 0x000000C3 Zenject.InjectableInfo Zenject.InjectTypeInfo_<>c::<get_AllInjectables>b__10_0(Zenject.InjectTypeInfo_InjectMemberInfo)
extern void U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_0_m9FBC75220E285F59B6EE734E6014BBA81A75B34E ();
// 0x000000C4 System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.InjectTypeInfo_<>c::<get_AllInjectables>b__10_1(Zenject.InjectTypeInfo_InjectMethodInfo)
extern void U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_1_m73B39E0ED66D688733647E0A291A54B19FA2DAB6 ();
// 0x000000C5 System.Void Zenject.IPoolable::OnDespawned()
// 0x000000C6 System.Void Zenject.IPoolable::OnSpawned()
// 0x000000C7 System.Void Zenject.IPoolable`1::OnDespawned()
// 0x000000C8 System.Void Zenject.IPoolable`1::OnSpawned(TParam1)
// 0x000000C9 System.Void Zenject.IPoolable`2::OnDespawned()
// 0x000000CA System.Void Zenject.IPoolable`2::OnSpawned(TParam1,TParam2)
// 0x000000CB System.Void Zenject.IPoolable`3::OnDespawned()
// 0x000000CC System.Void Zenject.IPoolable`3::OnSpawned(TParam1,TParam2,TParam3)
// 0x000000CD System.Void Zenject.IPoolable`4::OnDespawned()
// 0x000000CE System.Void Zenject.IPoolable`4::OnSpawned(TParam1,TParam2,TParam3,TParam4)
// 0x000000CF System.Void Zenject.IPoolable`5::OnDespawned()
// 0x000000D0 System.Void Zenject.IPoolable`5::OnSpawned(TParam1,TParam2,TParam3,TParam4,TParam5)
// 0x000000D1 System.Void Zenject.IPoolable`6::OnDespawned()
// 0x000000D2 System.Void Zenject.IPoolable`6::OnSpawned(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6)
// 0x000000D3 System.Void Zenject.IPoolable`7::OnDespawned()
// 0x000000D4 System.Void Zenject.IPoolable`7::OnSpawned(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7)
// 0x000000D5 System.Void Zenject.IPoolable`8::OnDespawned()
// 0x000000D6 System.Void Zenject.IPoolable`8::OnSpawned(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8)
// 0x000000D7 System.Void Zenject.IPoolable`9::OnDespawned()
// 0x000000D8 System.Void Zenject.IPoolable`9::OnSpawned(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9)
// 0x000000D9 System.Void Zenject.IPoolable`10::OnDespawned()
// 0x000000DA System.Void Zenject.IPoolable`10::OnSpawned(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10)
// 0x000000DB System.Void Zenject.IPoolable`11::OnDespawned()
// 0x000000DC System.Void Zenject.IPoolable`11::OnSpawned(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10,TParam11)
// 0x000000DD System.Void Zenject.ITickable::Tick()
// 0x000000DE System.Void Zenject.IFixedTickable::FixedTick()
// 0x000000DF System.Void Zenject.ILateTickable::LateTick()
// 0x000000E0 System.Void Zenject.ILateDisposable::LateDispose()
// 0x000000E1 System.Boolean Zenject.InjectAttributeBase::get_Optional()
extern void InjectAttributeBase_get_Optional_m117D4303470CA796A9CBECCBCCAA1B075E534A0E ();
// 0x000000E2 System.Void Zenject.InjectAttributeBase::set_Optional(System.Boolean)
extern void InjectAttributeBase_set_Optional_m93C08A9815412E918B346A7351BF871DB5ABD54D ();
// 0x000000E3 System.Object Zenject.InjectAttributeBase::get_Id()
extern void InjectAttributeBase_get_Id_m62DA30B28C66A2B042B321BB38E01F619A47E8E9 ();
// 0x000000E4 System.Void Zenject.InjectAttributeBase::set_Id(System.Object)
extern void InjectAttributeBase_set_Id_m83E44EE92C2434514B7E1BBC7F40D7AFE0849EDB ();
// 0x000000E5 Zenject.InjectSources Zenject.InjectAttributeBase::get_Source()
extern void InjectAttributeBase_get_Source_m9C4136AB1B6AC3A8CD74B8FA7241B18CFAE6ADD6 ();
// 0x000000E6 System.Void Zenject.InjectAttributeBase::set_Source(Zenject.InjectSources)
extern void InjectAttributeBase_set_Source_mE83EFBACAA0B787C77EA0B2C73D2A9CFA654C819 ();
// 0x000000E7 System.Void Zenject.InjectAttributeBase::.ctor()
extern void InjectAttributeBase__ctor_m2C0314D84470B5111B9276530B6759706691E2BA ();
// 0x000000E8 System.Void Zenject.NoReflectionBakingAttribute::.ctor()
extern void NoReflectionBakingAttribute__ctor_mFF965F6241370B8C49707C74B8B0292F9954C847 ();
// 0x000000E9 System.Void Zenject.ZenjectAllowDuringValidationAttribute::.ctor()
extern void ZenjectAllowDuringValidationAttribute__ctor_m6FB09FC9DDD462E6D93EC638B368CFD12F6565E8 ();
// 0x000000EA System.Void Zenject.Internal.PreserveAttribute::.ctor()
extern void PreserveAttribute__ctor_mCDD191D80D202DAD6E07BFA266003EFF5996E4D9 ();
static Il2CppMethodPointer s_methodPointers[234] = 
{
	CanBeNullAttribute__ctor_m6F19C7002DD42E3AF42DB6F8996CACD816126237,
	NotNullAttribute__ctor_mC042005FB58C4FDBF56356830703305AAC7AFDDE,
	ItemNotNullAttribute__ctor_m367958CC9A39DE676E462B7F673E8B9DD100BD20,
	ItemCanBeNullAttribute__ctor_m59095B6741ACE9A3555CCCD3231CD47C5B5B2F12,
	StringFormatMethodAttribute__ctor_mA0F0DD9D94622A65E2B454CA70021BC3EBA105FA,
	StringFormatMethodAttribute_get_FormatParameterName_m78E78C4A4FBAC9F3EFC3A67BB717400284268A4B,
	StringFormatMethodAttribute_set_FormatParameterName_m7F599817D270C2AE791FADD1F3F467AB800EC5AC,
	ValueProviderAttribute__ctor_mEC341339F02E1ECA9E12E6EC363DA583153CD90E,
	ValueProviderAttribute_get_Name_m85A47FD4BDE7531EEAD0021846301D1ED66F8260,
	ValueProviderAttribute_set_Name_m189466822274D76436002174983AC4BDF229B82C,
	InvokerParameterNameAttribute__ctor_m6D07C117B1D500B738EC7DE7418B6C873D2485E0,
	NotifyPropertyChangedInvocatorAttribute__ctor_mC032202F6CCFA2F3431FE63D0EFBA6A646FECB89,
	NotifyPropertyChangedInvocatorAttribute__ctor_m27B201DC58347E6CB19AD57B9C197BA55570452D,
	NotifyPropertyChangedInvocatorAttribute_get_ParameterName_mEE85B381BC8D2037911F54E5974FB48948F52F86,
	NotifyPropertyChangedInvocatorAttribute_set_ParameterName_m4042408E5BD997B59E313B781D8FD778DE132D5D,
	ContractAnnotationAttribute__ctor_m601EFEEDF152647707C025B7438A97D53FE65632,
	ContractAnnotationAttribute__ctor_m39389343D4F1D30EADBE6CDFC5A91BBB2CF0406E,
	ContractAnnotationAttribute_get_Contract_m25CDDC707037F6507880BFB6E87FAC91A18C61FB,
	ContractAnnotationAttribute_set_Contract_m46117527529866206D93D24AC03B8A5B97054D34,
	ContractAnnotationAttribute_get_ForceFullStates_m1F6FC2F24CE7006D5E351E183FA945BE1C76C140,
	ContractAnnotationAttribute_set_ForceFullStates_m3273626E1D840A51CE3373012B591A0A4652EBC0,
	LocalizationRequiredAttribute__ctor_m31C88794DC9E45D017667001AB510ABD3EA91264,
	LocalizationRequiredAttribute__ctor_mC9ED48DF8302CB9F4D0217AAA4A81AA28A19E352,
	LocalizationRequiredAttribute_get_Required_mF4EDBE1AC5F6B245F44062703216F023D3E02092,
	LocalizationRequiredAttribute_set_Required_m28CDD5F78263BE6179EE3E29E80EBCF93FA51021,
	CannotApplyEqualityOperatorAttribute__ctor_m62FF7A1B6FD14BC114841E92C06CDF10060D7857,
	BaseTypeRequiredAttribute__ctor_m78EAA13CF24B87AA473439C4273861E6FB034F0F,
	BaseTypeRequiredAttribute_get_BaseType_m6B331E97F682B2371C7E5C1BAAD65B6030AFEF7E,
	BaseTypeRequiredAttribute_set_BaseType_m0E7A68B87BCBF97208AB3B7D975773835CCB6B8C,
	UsedImplicitlyAttribute__ctor_m9967794C58FA80A15A2EB59A219C9211DB2161E8,
	UsedImplicitlyAttribute__ctor_mFD9C6C2E2B1CF69D3CAAA0C308226F009E3D1852,
	UsedImplicitlyAttribute__ctor_m5780F7D5CA60ADEE1A8C8B6B66319D8C66EF0667,
	UsedImplicitlyAttribute__ctor_mF053D1548C9F0310AAE782C1AE30BC81A417BE5E,
	UsedImplicitlyAttribute_get_UseKindFlags_m78456325D2370062434A60F23EEE53CA2061D1E0,
	UsedImplicitlyAttribute_set_UseKindFlags_m5D8FE4E5A37C11453F7C376FFC0ED0E84B9D0ADB,
	UsedImplicitlyAttribute_get_TargetFlags_mD2C04C854F767492D225660763574027B3C9D98C,
	UsedImplicitlyAttribute_set_TargetFlags_mE8D6A7BABDB5B3846B11D2E3BCD0C2AEE644132F,
	MeansImplicitUseAttribute__ctor_mEFFCE62C8ECBAB317DF92843D6645F8FEDE7D539,
	MeansImplicitUseAttribute__ctor_m231D188014C6BF4ADA08F119810722D7D182E289,
	MeansImplicitUseAttribute__ctor_mA48E7E71BD8C8A022384A792977F329C2D7E5755,
	MeansImplicitUseAttribute__ctor_m8B1CB0647B10039C21C2A9A456E9E4C502331FFF,
	MeansImplicitUseAttribute_get_UseKindFlags_m9E68CD91BECB87F9140185F69D2C66C8E12FEAFA,
	MeansImplicitUseAttribute_set_UseKindFlags_mC020328ACCA94C5816E6F418E7CC6EB9E1E2C9F0,
	MeansImplicitUseAttribute_get_TargetFlags_mB42E1B2CF4F2D83F4D8738F9DD46AD02B0C2BA5B,
	MeansImplicitUseAttribute_set_TargetFlags_mCCCE7B4BCF2EA74538E4C90496208ACAD32B7A8F,
	PublicAPIAttribute__ctor_m819E9E7C36012471D43F2465EA4D7CFB59548673,
	PublicAPIAttribute__ctor_m9DE454053E55A92BC7D309F4CBA1E42822B2F532,
	PublicAPIAttribute_get_Comment_m2E3F85A747F4F5EB1277D90AC9405837AD679F38,
	PublicAPIAttribute_set_Comment_m803065FC1B1C3AE5533C775B09668E5F210ABAC2,
	InstantHandleAttribute__ctor_m2FB33A477515F7831182D5B0848BB50171F84FB5,
	PureAttribute__ctor_mCDEF6E5AB727A4452739AEECD7E5FA3EA59C77C8,
	MustUseReturnValueAttribute__ctor_mA0B62B6EAF331274159186A4C5305B95A08572C7,
	MustUseReturnValueAttribute__ctor_m8A84924BE475325A76AEEA74C27CD170627C5CAF,
	MustUseReturnValueAttribute_get_Justification_m9049DF7AA84BEE837E554F3DE6AB79E6D7071D63,
	MustUseReturnValueAttribute_set_Justification_m1DAE6CCA661B24A56C45C0ACF84FD195C7D44A04,
	ProvidesContextAttribute__ctor_mBDC4E52D357F22267F945104CA9219E3DF030743,
	PathReferenceAttribute__ctor_mB7F021886F17C36D1C711E35D6BDCD912A066A09,
	PathReferenceAttribute__ctor_m66BFC638B85ACEBAAD54F177D979157825922263,
	PathReferenceAttribute_get_BasePath_mA9878FEB3B935EF9EB1CAAC91F206459B10363EA,
	PathReferenceAttribute_set_BasePath_m6039B47746370E59513C5C7161053AE20F741255,
	SourceTemplateAttribute__ctor_mF671F6BE3261CDC2EBE1AC338149B62C6425045D,
	MacroAttribute_get_Expression_m2CCFF5D806E7E97ADAC0EEBC9BAF05F89C0D2AAA,
	MacroAttribute_set_Expression_m0582A717F271662A01010819A8E6DFC21873CD32,
	MacroAttribute_get_Editable_mC6D969B99E215839850D37F56E090BC3FF630DE0,
	MacroAttribute_set_Editable_mCDF7F281D18B0DBD6902F6297B2AD01B3D9534AF,
	MacroAttribute_get_Target_mBDCBF8052A69036B262E81773A25AF159B568B3A,
	MacroAttribute_set_Target_m4D64FFE273A7BD4BF911FD0ABE35DF2F46B134EB,
	MacroAttribute__ctor_m9113EDB7BD048A9585647F7B4A888DC43A9303F0,
	AspMvcAreaMasterLocationFormatAttribute__ctor_mDF9E66FCDBFA3CF0C2F526280B3BEB73E1C760CE,
	AspMvcAreaMasterLocationFormatAttribute_get_Format_mBCD5EFD48945348F59A7A204502B3D0C51C315FA,
	AspMvcAreaMasterLocationFormatAttribute_set_Format_m23924305517051ABCC0C5A64FBC969E57DD64CC1,
	AspMvcAreaPartialViewLocationFormatAttribute__ctor_m22B96DDFD5669C96F54AC79899DB78FA8B07181E,
	AspMvcAreaPartialViewLocationFormatAttribute_get_Format_m0ED3DECD9BC9DEEF7551EC70424316B78F63E7C1,
	AspMvcAreaPartialViewLocationFormatAttribute_set_Format_mBA39E10F8E9F267F28B653341FECA4E8D75E29C0,
	AspMvcAreaViewLocationFormatAttribute__ctor_mFBA83D70929DAA000B726DB007FC0FA6BEBDAD4F,
	AspMvcAreaViewLocationFormatAttribute_get_Format_m7F2E5A569366D48B3831CED9CA51DB28EA87B3F4,
	AspMvcAreaViewLocationFormatAttribute_set_Format_mFA517B963A59B6F24995B1051BBD644B19A6E1B6,
	AspMvcMasterLocationFormatAttribute__ctor_m957883C543F9B6A78CF0AC66B4ED0729BA8FF4D0,
	AspMvcMasterLocationFormatAttribute_get_Format_m2616278737402EE51EC14B7DD4E790226808FA38,
	AspMvcMasterLocationFormatAttribute_set_Format_m852EA80CBBEED7BB8853D695FE37C1C9E5A707C3,
	AspMvcPartialViewLocationFormatAttribute__ctor_mE4E72137B00EEFBFFEF64FAECB98DB9C1E70217A,
	AspMvcPartialViewLocationFormatAttribute_get_Format_m2D0B7F0A062EEDFB7EFF147252917D3D590ABEB8,
	AspMvcPartialViewLocationFormatAttribute_set_Format_m1D39A9129B1D56D968870065FDD2E65244FFB3F9,
	AspMvcViewLocationFormatAttribute__ctor_m69C7FCA1E0359B5BD5F8867DD3B785D0B1EE1A17,
	AspMvcViewLocationFormatAttribute_get_Format_mE9CEBE3AE63F8049ED45B1D507E5680BECB333D9,
	AspMvcViewLocationFormatAttribute_set_Format_m9AB87404E45FA9C2AAFD4057124EE2181ACCE963,
	AspMvcActionAttribute__ctor_m5224998BAD51F889DB341A7242CA569C5461B732,
	AspMvcActionAttribute__ctor_m4EDD633B43CC7F9929C553E4C2DDC480F9E352B1,
	AspMvcActionAttribute_get_AnonymousProperty_mECC7F953EB784ECECCD91779A41F81BC31B77774,
	AspMvcActionAttribute_set_AnonymousProperty_m83EA3BE21BBB1F4735FBE043543BD4984590BBDA,
	AspMvcAreaAttribute__ctor_m08704D0ED240F06DB2295D2E96CA1C76394D6D31,
	AspMvcAreaAttribute__ctor_mBB4BB8321027B8620A47FEFF5A5ABE7DBF6669BB,
	AspMvcAreaAttribute_get_AnonymousProperty_m1A0AD54CBC1596C72E9B9DC1AE1C1EBBFD843DC6,
	AspMvcAreaAttribute_set_AnonymousProperty_m6217B255496EF23DBEFF147D69F4CC75028DD372,
	AspMvcControllerAttribute__ctor_mCFBCD113CFB32BB3B557F418E22D3144321D7278,
	AspMvcControllerAttribute__ctor_mBAB20CE3BE9F3E5356A3D46BF585E1AB5CC0034E,
	AspMvcControllerAttribute_get_AnonymousProperty_m1D0891365029E522461959DA55BF330645E623FD,
	AspMvcControllerAttribute_set_AnonymousProperty_mDB5062FA1F14FB6FBA2034FFD85CDA4EDFAF5083,
	AspMvcMasterAttribute__ctor_mDB7FBDE7214AB5996EC26B1CCA452C8C8D93BF16,
	AspMvcModelTypeAttribute__ctor_mF2A57B73788689B0DA7EB3709EE02416ED518CA1,
	AspMvcPartialViewAttribute__ctor_m1B14296015A3BB5BF56E0B722F7A85BE15D70765,
	AspMvcSuppressViewErrorAttribute__ctor_mB98050F8997645798F653FD5FCE352857095348F,
	AspMvcDisplayTemplateAttribute__ctor_m4F28D2E29737294FF54CD12C087DE3C52B813254,
	AspMvcEditorTemplateAttribute__ctor_mD0BC86FB6BD7E0092EFFBD2E2B98A62C49580201,
	AspMvcTemplateAttribute__ctor_m78BE9483D62067D284B3A8FD59B7A11872013C74,
	AspMvcViewAttribute__ctor_m03E325150A64E35683DC6FCD44476D5412E24CA0,
	AspMvcViewComponentAttribute__ctor_m47DE1A1823403724779B3F93ACE1D4629DE579C7,
	AspMvcViewComponentViewAttribute__ctor_m9E60CD0D9B3108F801BD7543A33DAB071F97D57F,
	AspMvcActionSelectorAttribute__ctor_m99959ADAF51BF6F8F3C91DAA68C49D184C3E44BE,
	HtmlElementAttributesAttribute__ctor_m488A5CFA47EFEA4166ACBB51212AB8FE01BFC2AC,
	HtmlElementAttributesAttribute__ctor_mEF36AA00424E0F2FE683B95A2AC6BAEB0CE98CD3,
	HtmlElementAttributesAttribute_get_Name_mD22BB01CEDBF1E1ACE7373CEEF4DC693E0CB1D37,
	HtmlElementAttributesAttribute_set_Name_mC66C3677ECE785266657B3BCCE620ED99AF9D4AE,
	HtmlAttributeValueAttribute__ctor_mCAA079679C0191C23EF6DFBC710A0D875207E329,
	HtmlAttributeValueAttribute_get_Name_m84B4C87BEF3F505B24072A0AE18CFE3C619719E3,
	HtmlAttributeValueAttribute_set_Name_m40857D8975B094CCA401959469C902D8E4EF2F13,
	RazorSectionAttribute__ctor_m9A7DFE508BD5E260951EFDD17E5CAB93C54A759F,
	CollectionAccessAttribute__ctor_mD77E92806695EDAC272BBB1BA6E4F2CFE66FF413,
	CollectionAccessAttribute_get_CollectionAccessType_m1BFB8DFEBA87E31D69B182791AFADCEE41827673,
	CollectionAccessAttribute_set_CollectionAccessType_mBE8F74690533AF7885DF9E691A758BB6CDA15529,
	AssertionMethodAttribute__ctor_mAE5114C050EAB7D5663F20F9F46D3E302B8EE53E,
	AssertionConditionAttribute__ctor_mF0B5B121B7C0396DB7F13F1BC78BAFDCAA24CA8B,
	AssertionConditionAttribute_get_ConditionType_m0EFFDFD6C0C912B759E4380A035A30B3C8966167,
	AssertionConditionAttribute_set_ConditionType_m2E1C8FDBA6328631D71E7EDF3A655F521B606727,
	TerminatesProgramAttribute__ctor_m3879D60EAD0C1E01E14AF7A6B28F8F489CBF01C7,
	LinqTunnelAttribute__ctor_m7FBC5773938344987792E196D4E9CCF6F9973841,
	NoEnumerationAttribute__ctor_mADC051CDC91A0FFB0DF756783DB7684445444A92,
	RegexPatternAttribute__ctor_mF1530725493264C8C2E70C4048CF05B1F90FD38B,
	NoReorderAttribute__ctor_mE22C5847FFD0A1FDD96AB2F7620F802D1A45120E,
	XamlItemsControlAttribute__ctor_mA3FBD1004D93BEB2A4CE7CDD7B1B6B591A2B86B7,
	XamlItemBindingOfItemsControlAttribute__ctor_m3C12FC84D10A36593227C7D61A4AB5517A99AC22,
	AspChildControlTypeAttribute__ctor_mEEED24DA23322F148E3E1B0988462D99D712202E,
	AspChildControlTypeAttribute_get_TagName_m47ED2DE7A3A29BED2CA5CB925C49EBD7548276D3,
	AspChildControlTypeAttribute_set_TagName_m6A9A3D854C58738EBD8A0D379FB6FD5AA2D69BAD,
	AspChildControlTypeAttribute_get_ControlType_m4C91CD8ECEC1EDF68E1FC66970BA667F9D0930BC,
	AspChildControlTypeAttribute_set_ControlType_mF8FD2F6F1BD65DB6966EC199D6DCBF9930D71EA0,
	AspDataFieldAttribute__ctor_m6CC0DE0330087CD5EF8CA17A4161C3363A9755D9,
	AspDataFieldsAttribute__ctor_m2693844D5D26ED410122666D69D7FA02C89E24CA,
	AspMethodPropertyAttribute__ctor_m7CD3D54FD2697B7C221007A1BA3A6D67970A9D90,
	AspRequiredAttributeAttribute__ctor_m006E2662B92F085BC9FADD256D8BBF64C32A8F47,
	AspRequiredAttributeAttribute_get_Attribute_m0D61E17144E7303EFFE4C383656C8E716B819542,
	AspRequiredAttributeAttribute_set_Attribute_m4C27C1CF35F50F5A398DE77CBD9E3BA4E49FFE83,
	AspTypePropertyAttribute_get_CreateConstructorReferences_m418D57DFE0F81617864CF81B8E042085FD4CE464,
	AspTypePropertyAttribute_set_CreateConstructorReferences_m953788280CA7DCB18C58C6CDB8DB221C857F4037,
	AspTypePropertyAttribute__ctor_mA5F306BCD34B2EDCF448F2B0F9445A345936C955,
	RazorImportNamespaceAttribute__ctor_m88390011700E4FA1A761B6CA2E98C7D3BB22F34D,
	RazorImportNamespaceAttribute_get_Name_m786E27B225452D992829499D9D512D1C3396701C,
	RazorImportNamespaceAttribute_set_Name_m140C4B453B55F6C62F62B7B38A56B2FEE8D0C090,
	RazorInjectionAttribute__ctor_m90A7D9C396BFB384D3B30B2D65E5F81D0DB53ECD,
	RazorInjectionAttribute_get_Type_mFCF5011ED4738143B9262DD5DAA3730AB3D80479,
	RazorInjectionAttribute_set_Type_mC7841CBEBEF456EE2AD385F6C4E4F86154A4BC84,
	RazorInjectionAttribute_get_FieldName_m278F9CED40DBE4C653C100D7B67A81B6D9138DE2,
	RazorInjectionAttribute_set_FieldName_m57A348CB52BE094401092C2631B2500E8E840BFD,
	RazorDirectiveAttribute__ctor_mA28CF8516DE82819A685A5219FD4BFC6B68EA5C4,
	RazorDirectiveAttribute_get_Directive_mA77DF0374D01746A4D860EE4007565AD038C8143,
	RazorDirectiveAttribute_set_Directive_m13E8B8A3D7AA46CFC68ADD972BECE00EE489A1D2,
	RazorPageBaseTypeAttribute__ctor_mBAC60C3D4748379F9C5AD2991C423BED385F8CF2,
	RazorPageBaseTypeAttribute__ctor_m544D67B7090DF8044982853BED74D31DE07D608C,
	RazorPageBaseTypeAttribute_get_BaseType_m333BD2487E7BB969B2D5FCAB026E0487950FEFD6,
	RazorPageBaseTypeAttribute_set_BaseType_m68D508FAD7FB7558AD5B63D899D61AA9339113D7,
	RazorPageBaseTypeAttribute_get_PageName_m1E0BC993F961B70AABED35A9C3D9FCF96A29598A,
	RazorPageBaseTypeAttribute_set_PageName_m919DD65C86D7D50B0D64004F4A11B5D08066DC25,
	RazorHelperCommonAttribute__ctor_mA324FDB54824F3BD2D42C585F4382A8F02F8680A,
	RazorLayoutAttribute__ctor_m51575EA28B21943AB294FAFD2D6B5A96809CE63B,
	RazorWriteLiteralMethodAttribute__ctor_mDB6F23E5D037253D1563E88EAE3DDB008575EEA1,
	RazorWriteMethodAttribute__ctor_m02A0D0B08AA62C9A40187C135A6F1746C0464BAC,
	RazorWriteMethodParameterAttribute__ctor_m86B45C312985B19500293D1DADEAA112CC573D69,
	NULL,
	NULL,
	InjectableInfo__ctor_m658D7FA40B2FD6046E44B67EF6B59BBC2F63F058,
	InjectAttribute__ctor_m98AD3B24006CAF44B6D24E3FA1A6841F9BD45342,
	InjectLocalAttribute__ctor_mC795F3A21C4A90B9CA501FD84ECAF6939157FF04,
	InjectOptionalAttribute__ctor_mF21688E8D80BDD59EB82431F8FBD12D7891673A1,
	ZenInjectMethod__ctor_mAE02246B1ECE29A84AAB66E1730F2F2206BC2F3E,
	ZenInjectMethod_Invoke_m9F84D1E5F9B6E000D5B103FB8ECA26AB8814E408,
	ZenInjectMethod_BeginInvoke_m53CFF280203B2FDCC1BBADD133C0A0FD304204CA,
	ZenInjectMethod_EndInvoke_mA44C12A7FE1416726ABC14A2C51D1B6156C626DA,
	ZenFactoryMethod__ctor_m461B117D19C422609F609B2454BF059DE6696E46,
	ZenFactoryMethod_Invoke_m141F13459ED8ED452242759B11B781225A90FEAB,
	ZenFactoryMethod_BeginInvoke_m0219A0A66C3D4DC63EACAF92304B2A01B4F489E8,
	ZenFactoryMethod_EndInvoke_m2676D16EE20429C9D848CA8672ECEA359824926F,
	ZenMemberSetterMethod__ctor_m2F7603EFCA3FFA557C57F782AB6515377C581338,
	ZenMemberSetterMethod_Invoke_m478200CD23399D996E8B8CCF38638F03A369DB8C,
	ZenMemberSetterMethod_BeginInvoke_mD54AEF0DEB5C012710B5540975C6C7FDA9B90B1E,
	ZenMemberSetterMethod_EndInvoke_mB1D14DDDE7B307CA49E7E416290ECC0FD3488280,
	InjectTypeInfo__ctor_m4F1AABE23B2C121781BED8AA5118C426626689D2,
	InjectTypeInfo_get_BaseTypeInfo_m3651B4151F25B3B32E0752590E3B223E5B809E44,
	InjectTypeInfo_set_BaseTypeInfo_m421413DB209610F6DB8DAB4192B7FD2E8281EC85,
	InjectTypeInfo_get_AllInjectables_m4459958ADFD4FCCD62A54B3C571EB2A2BBFE2417,
	InjectMemberInfo__ctor_m6332CC8B6797368DFF7B36287E75A3B7F4E5123D,
	InjectConstructorInfo__ctor_m3A5ABBF6F67CD8B233A41C024CB45588E2159135,
	InjectMethodInfo__ctor_m35025625F36C9D1178F6A85CCA817793161418A9,
	U3CU3Ec__cctor_mE2354A854E5D0CFE8FF62DCF6A806ECF6D80C289,
	U3CU3Ec__ctor_m8BA7B247476AA2C17B97E0C9A67F8472E8AE5112,
	U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_0_m9FBC75220E285F59B6EE734E6014BBA81A75B34E,
	U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_1_m73B39E0ED66D688733647E0A291A54B19FA2DAB6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InjectAttributeBase_get_Optional_m117D4303470CA796A9CBECCBCCAA1B075E534A0E,
	InjectAttributeBase_set_Optional_m93C08A9815412E918B346A7351BF871DB5ABD54D,
	InjectAttributeBase_get_Id_m62DA30B28C66A2B042B321BB38E01F619A47E8E9,
	InjectAttributeBase_set_Id_m83E44EE92C2434514B7E1BBC7F40D7AFE0849EDB,
	InjectAttributeBase_get_Source_m9C4136AB1B6AC3A8CD74B8FA7241B18CFAE6ADD6,
	InjectAttributeBase_set_Source_mE83EFBACAA0B787C77EA0B2C73D2A9CFA654C819,
	InjectAttributeBase__ctor_m2C0314D84470B5111B9276530B6759706691E2BA,
	NoReflectionBakingAttribute__ctor_mFF965F6241370B8C49707C74B8B0292F9954C847,
	ZenjectAllowDuringValidationAttribute__ctor_m6FB09FC9DDD462E6D93EC638B368CFD12F6565E8,
	PreserveAttribute__ctor_mCDD191D80D202DAD6E07BFA266003EFF5996E4D9,
};
static const int32_t s_InvokerIndices[234] = 
{
	23,
	23,
	23,
	23,
	26,
	14,
	26,
	26,
	14,
	26,
	23,
	23,
	26,
	14,
	26,
	26,
	401,
	14,
	26,
	114,
	31,
	23,
	31,
	114,
	31,
	23,
	26,
	14,
	26,
	23,
	32,
	32,
	169,
	10,
	32,
	10,
	32,
	23,
	32,
	32,
	169,
	10,
	32,
	10,
	32,
	23,
	26,
	14,
	26,
	23,
	23,
	23,
	26,
	14,
	26,
	23,
	23,
	26,
	14,
	26,
	23,
	14,
	26,
	10,
	32,
	14,
	26,
	23,
	26,
	14,
	26,
	26,
	14,
	26,
	26,
	14,
	26,
	26,
	14,
	26,
	26,
	14,
	26,
	26,
	14,
	26,
	23,
	26,
	14,
	26,
	23,
	26,
	14,
	26,
	23,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	26,
	26,
	14,
	26,
	23,
	32,
	10,
	32,
	23,
	32,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	26,
	14,
	26,
	114,
	31,
	31,
	26,
	14,
	26,
	27,
	14,
	26,
	14,
	26,
	26,
	14,
	26,
	26,
	27,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1615,
	23,
	23,
	23,
	102,
	27,
	213,
	26,
	102,
	28,
	177,
	28,
	102,
	27,
	213,
	26,
	388,
	14,
	26,
	14,
	27,
	27,
	168,
	3,
	23,
	28,
	28,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	114,
	31,
	14,
	26,
	10,
	32,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_ZenjectU2DusageCodeGenModule;
const Il2CppCodeGenModule g_ZenjectU2DusageCodeGenModule = 
{
	"Zenject-usage.dll",
	234,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
