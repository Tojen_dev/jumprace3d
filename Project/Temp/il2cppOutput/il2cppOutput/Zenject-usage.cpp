﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct GenericVirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericVirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct GenericInterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericInterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// JetBrains.Annotations.AspChildControlTypeAttribute
struct AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655;
// JetBrains.Annotations.AspDataFieldAttribute
struct AspDataFieldAttribute_t940FB396A4669B558533B0720636F56B51E15D18;
// JetBrains.Annotations.AspDataFieldsAttribute
struct AspDataFieldsAttribute_tAB5D3A23E81D2780D2F5C8DEF615E62BBAD9E4D7;
// JetBrains.Annotations.AspMethodPropertyAttribute
struct AspMethodPropertyAttribute_tF1E364481C749DE3255CF0DDF7F32D427ADDD7EE;
// JetBrains.Annotations.AspMvcActionAttribute
struct AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65;
// JetBrains.Annotations.AspMvcActionSelectorAttribute
struct AspMvcActionSelectorAttribute_t11C6F6F0183F81E71464F18A21FC849D3E6953D4;
// JetBrains.Annotations.AspMvcAreaAttribute
struct AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143;
// JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute
struct AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460;
// JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute
struct AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE;
// JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute
struct AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101;
// JetBrains.Annotations.AspMvcControllerAttribute
struct AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F;
// JetBrains.Annotations.AspMvcDisplayTemplateAttribute
struct AspMvcDisplayTemplateAttribute_tF7CAA1E2E66276CBD40C471CB41ACDFCE42DDD57;
// JetBrains.Annotations.AspMvcEditorTemplateAttribute
struct AspMvcEditorTemplateAttribute_tEC434D592971B5C11E261AED277B32A6F243D68C;
// JetBrains.Annotations.AspMvcMasterAttribute
struct AspMvcMasterAttribute_t8A29644556A2389ABE4BA7F46CB12DD6AB23AFBD;
// JetBrains.Annotations.AspMvcMasterLocationFormatAttribute
struct AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03;
// JetBrains.Annotations.AspMvcModelTypeAttribute
struct AspMvcModelTypeAttribute_t91DBB2C1DDDE5F76823832C7F23354C4A296CD6F;
// JetBrains.Annotations.AspMvcPartialViewAttribute
struct AspMvcPartialViewAttribute_t7EEC1A645243D02EA396745377B8ED662C47F290;
// JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute
struct AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E;
// JetBrains.Annotations.AspMvcSuppressViewErrorAttribute
struct AspMvcSuppressViewErrorAttribute_tA3108D0508FE7B43BFDC041EFF9C8E0FBD53123B;
// JetBrains.Annotations.AspMvcTemplateAttribute
struct AspMvcTemplateAttribute_tA4B152A320093AFB342AE4C4B5B24A6F019CD0F8;
// JetBrains.Annotations.AspMvcViewAttribute
struct AspMvcViewAttribute_t0745A873B4C68EE712EE0E79BB474770507601E6;
// JetBrains.Annotations.AspMvcViewComponentAttribute
struct AspMvcViewComponentAttribute_t597F801F316EC6648949580A739D04AAA2628DDC;
// JetBrains.Annotations.AspMvcViewComponentViewAttribute
struct AspMvcViewComponentViewAttribute_t52125A3852EC185D7C36A77E532BAB751D32FF60;
// JetBrains.Annotations.AspMvcViewLocationFormatAttribute
struct AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7;
// JetBrains.Annotations.AspRequiredAttributeAttribute
struct AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7;
// JetBrains.Annotations.AspTypePropertyAttribute
struct AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA;
// JetBrains.Annotations.AssertionConditionAttribute
struct AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1;
// JetBrains.Annotations.AssertionMethodAttribute
struct AssertionMethodAttribute_t9B265B34C1B6FD821D5BC3EB3A7F342F92F3C236;
// JetBrains.Annotations.BaseTypeRequiredAttribute
struct BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6;
// JetBrains.Annotations.CanBeNullAttribute
struct CanBeNullAttribute_t6E141B9F14D794ACDC3DC13FB87ED8180E9B908D;
// JetBrains.Annotations.CannotApplyEqualityOperatorAttribute
struct CannotApplyEqualityOperatorAttribute_t76BF7A501FA5EB0A134C4F39A61A5DE7CA5B45CC;
// JetBrains.Annotations.CollectionAccessAttribute
struct CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E;
// JetBrains.Annotations.ContractAnnotationAttribute
struct ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B;
// JetBrains.Annotations.HtmlAttributeValueAttribute
struct HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78;
// JetBrains.Annotations.HtmlElementAttributesAttribute
struct HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8;
// JetBrains.Annotations.InstantHandleAttribute
struct InstantHandleAttribute_t75BD43B7B196BBB162084A2DD3C9CB1E717CA66F;
// JetBrains.Annotations.InvokerParameterNameAttribute
struct InvokerParameterNameAttribute_t75C6B9095D54A0DE657CE40F6677669BBB68221B;
// JetBrains.Annotations.ItemCanBeNullAttribute
struct ItemCanBeNullAttribute_t45743AFAC9CC28B3EA4E687AEC4BB8111EE971D5;
// JetBrains.Annotations.ItemNotNullAttribute
struct ItemNotNullAttribute_t4EE880F50F3721F545953DCCB982C0E96F59C4F2;
// JetBrains.Annotations.LinqTunnelAttribute
struct LinqTunnelAttribute_tB1D84829BB4C1FC08EDFB97CC903908F435597C2;
// JetBrains.Annotations.LocalizationRequiredAttribute
struct LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416;
// JetBrains.Annotations.MacroAttribute
struct MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C;
// JetBrains.Annotations.MeansImplicitUseAttribute
struct MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F;
// JetBrains.Annotations.MustUseReturnValueAttribute
struct MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2;
// JetBrains.Annotations.NoEnumerationAttribute
struct NoEnumerationAttribute_t6261D712EC3D4C55A028AF9BEBA896C6B42864C7;
// JetBrains.Annotations.NoReorderAttribute
struct NoReorderAttribute_tE5D8B57DF55ABEC6A1A04AAFB1BC4C7BC86D317F;
// JetBrains.Annotations.NotNullAttribute
struct NotNullAttribute_t13EA67CC218B36A5FB93BBDF6C7FC422AC84E0EB;
// JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute
struct NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C;
// JetBrains.Annotations.PathReferenceAttribute
struct PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7;
// JetBrains.Annotations.ProvidesContextAttribute
struct ProvidesContextAttribute_t018F8C277FAF081B25021559156B37F5CD05D908;
// JetBrains.Annotations.PublicAPIAttribute
struct PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8;
// JetBrains.Annotations.PureAttribute
struct PureAttribute_t88CF1F9EDCAE55E72BA1C026F4AB10943B451057;
// JetBrains.Annotations.RazorDirectiveAttribute
struct RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E;
// JetBrains.Annotations.RazorHelperCommonAttribute
struct RazorHelperCommonAttribute_t1E02250F84DB9E222211DCB0B990F23243E43C70;
// JetBrains.Annotations.RazorImportNamespaceAttribute
struct RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19;
// JetBrains.Annotations.RazorInjectionAttribute
struct RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF;
// JetBrains.Annotations.RazorLayoutAttribute
struct RazorLayoutAttribute_t279E1BF2A55E4D945C9F48FDB3C5F0DDE8BC4192;
// JetBrains.Annotations.RazorPageBaseTypeAttribute
struct RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F;
// JetBrains.Annotations.RazorSectionAttribute
struct RazorSectionAttribute_t5AA6D2F94C7018B03FF362B7E90EE774D1E6F51F;
// JetBrains.Annotations.RazorWriteLiteralMethodAttribute
struct RazorWriteLiteralMethodAttribute_t64AA2D6F82DA8131822E06EA6F27BD92907728C2;
// JetBrains.Annotations.RazorWriteMethodAttribute
struct RazorWriteMethodAttribute_t2BF3FF98004544723D2533D75C2CE5797D6483A0;
// JetBrains.Annotations.RazorWriteMethodParameterAttribute
struct RazorWriteMethodParameterAttribute_t386D71275AEE065917E2324F16C057D3A729BE02;
// JetBrains.Annotations.RegexPatternAttribute
struct RegexPatternAttribute_t0C4A75B79AD6457C708345386202B8F05C9B2C1B;
// JetBrains.Annotations.SourceTemplateAttribute
struct SourceTemplateAttribute_tFA362C973C19E229A137F5B77CC31DDBD1C53607;
// JetBrains.Annotations.StringFormatMethodAttribute
struct StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14;
// JetBrains.Annotations.TerminatesProgramAttribute
struct TerminatesProgramAttribute_tA0AECA56847F077E969CA259515E90EE724AD488;
// JetBrains.Annotations.UsedImplicitlyAttribute
struct UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7;
// JetBrains.Annotations.ValueProviderAttribute
struct ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192;
// JetBrains.Annotations.XamlItemBindingOfItemsControlAttribute
struct XamlItemBindingOfItemsControlAttribute_t4D95AE96BC401E816B6D94988CFEA63015FE330B;
// JetBrains.Annotations.XamlItemsControlAttribute
struct XamlItemsControlAttribute_tA6BF5767DF1774A8781DB8CB6DAED08C643DE44E;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2F75FCBEC68AFE08982DA43985F9D04056E2BE73;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectTypeInfo/InjectMemberInfo>
struct IEnumerable_1_tE235BFD5B9A26AC785569C3AF117A6E5F2F9B1A8;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectTypeInfo/InjectMethodInfo>
struct IEnumerable_1_tD75F9F7E1343BBA4474230AFA5A48E70A087C16F;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>
struct IEnumerable_1_t7CD6154000B49D3E0983014002900BEB32D25D92;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_t122180C7631A517B0F4BF97E5754C5F998E8923D;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<Zenject.InjectTypeInfo/InjectMemberInfo,Zenject.InjectableInfo>
struct Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92;
// System.Func`2<Zenject.InjectTypeInfo/InjectMethodInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>>
struct Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Zenject.InjectAttribute
struct InjectAttribute_t8D23ED3F95574DFF6119775BAAC731099F592B2E;
// Zenject.InjectAttributeBase
struct InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC;
// Zenject.InjectLocalAttribute
struct InjectLocalAttribute_tE96A924A3CDE8393549396FC2BBA80BBBDCB0C2D;
// Zenject.InjectOptionalAttribute
struct InjectOptionalAttribute_t958E1718D848609A8BD2E95719B0BD23A88A51E4;
// Zenject.InjectTypeInfo
struct InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1;
// Zenject.InjectTypeInfo/<>c
struct U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233;
// Zenject.InjectTypeInfo/InjectConstructorInfo
struct InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5;
// Zenject.InjectTypeInfo/InjectMemberInfo
struct InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82;
// Zenject.InjectTypeInfo/InjectMemberInfo[]
struct InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E;
// Zenject.InjectTypeInfo/InjectMethodInfo
struct InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A;
// Zenject.InjectTypeInfo/InjectMethodInfo[]
struct InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912;
// Zenject.InjectableInfo
struct InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC;
// Zenject.InjectableInfo[]
struct InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571;
// Zenject.Internal.PreserveAttribute
struct PreserveAttribute_t2D26C6DD68541293CA3738A57BC67377AAB1EDCA;
// Zenject.NoReflectionBakingAttribute
struct NoReflectionBakingAttribute_tEF78DB88D1E3F4057AB9F7AC1C62E232B9A9B2A4;
// Zenject.ZenFactoryMethod
struct ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525;
// Zenject.ZenInjectMethod
struct ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161;
// Zenject.ZenMemberSetterMethod
struct ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759;
// Zenject.ZenjectAllowDuringValidationAttribute
struct ZenjectAllowDuringValidationAttribute_t85FD03316B9D1E412DBCA415A1616B005D8AF540;

IL2CPP_EXTERN_C RuntimeClass* Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Concat_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mA9EDADA7EF8F1F3DFDA1FB3EDD13280EEF0D4BEC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_SelectMany_TisInjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_m425531658AB47EF78A6F559A7AD4F91E8FCF5F23_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisInjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mC65110D147A9A63DFF3F5EEEE3BCA524CFB608C1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m5810862148CA2671FE549A0D28FDE3D9F8ABDCF0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m7D6423527B62548E6DA02AF095EEBFE2956753E0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_0_m9FBC75220E285F59B6EE734E6014BBA81A75B34E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_1_m73B39E0ED66D688733647E0A291A54B19FA2DAB6_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t InjectTypeInfo_get_AllInjectables_m4459958ADFD4FCCD62A54B3C571EB2A2BBFE2417_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_mE2354A854E5D0CFE8FF62DCF6A806ECF6D80C289_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E;
struct InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912;
struct InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t524D03C74BC111BBF00F703A6D0B8E06470E760C 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// Zenject.InjectTypeInfo
struct  InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1  : public RuntimeObject
{
public:
	// System.Type Zenject.InjectTypeInfo::Type
	Type_t * ___Type_0;
	// Zenject.InjectTypeInfo_InjectMethodInfo[] Zenject.InjectTypeInfo::InjectMethods
	InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* ___InjectMethods_1;
	// Zenject.InjectTypeInfo_InjectMemberInfo[] Zenject.InjectTypeInfo::InjectMembers
	InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* ___InjectMembers_2;
	// Zenject.InjectTypeInfo_InjectConstructorInfo Zenject.InjectTypeInfo::InjectConstructor
	InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 * ___InjectConstructor_3;
	// Zenject.InjectTypeInfo Zenject.InjectTypeInfo::<BaseTypeInfo>k__BackingField
	InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * ___U3CBaseTypeInfoU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Type_0), (void*)value);
	}

	inline static int32_t get_offset_of_InjectMethods_1() { return static_cast<int32_t>(offsetof(InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1, ___InjectMethods_1)); }
	inline InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* get_InjectMethods_1() const { return ___InjectMethods_1; }
	inline InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912** get_address_of_InjectMethods_1() { return &___InjectMethods_1; }
	inline void set_InjectMethods_1(InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* value)
	{
		___InjectMethods_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InjectMethods_1), (void*)value);
	}

	inline static int32_t get_offset_of_InjectMembers_2() { return static_cast<int32_t>(offsetof(InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1, ___InjectMembers_2)); }
	inline InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* get_InjectMembers_2() const { return ___InjectMembers_2; }
	inline InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E** get_address_of_InjectMembers_2() { return &___InjectMembers_2; }
	inline void set_InjectMembers_2(InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* value)
	{
		___InjectMembers_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InjectMembers_2), (void*)value);
	}

	inline static int32_t get_offset_of_InjectConstructor_3() { return static_cast<int32_t>(offsetof(InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1, ___InjectConstructor_3)); }
	inline InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 * get_InjectConstructor_3() const { return ___InjectConstructor_3; }
	inline InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 ** get_address_of_InjectConstructor_3() { return &___InjectConstructor_3; }
	inline void set_InjectConstructor_3(InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 * value)
	{
		___InjectConstructor_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InjectConstructor_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBaseTypeInfoU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1, ___U3CBaseTypeInfoU3Ek__BackingField_4)); }
	inline InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * get_U3CBaseTypeInfoU3Ek__BackingField_4() const { return ___U3CBaseTypeInfoU3Ek__BackingField_4; }
	inline InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 ** get_address_of_U3CBaseTypeInfoU3Ek__BackingField_4() { return &___U3CBaseTypeInfoU3Ek__BackingField_4; }
	inline void set_U3CBaseTypeInfoU3Ek__BackingField_4(InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * value)
	{
		___U3CBaseTypeInfoU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBaseTypeInfoU3Ek__BackingField_4), (void*)value);
	}
};


// Zenject.InjectTypeInfo_<>c
struct  U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields
{
public:
	// Zenject.InjectTypeInfo_<>c Zenject.InjectTypeInfo_<>c::<>9
	U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * ___U3CU3E9_0;
	// System.Func`2<Zenject.InjectTypeInfo_InjectMemberInfo,Zenject.InjectableInfo> Zenject.InjectTypeInfo_<>c::<>9__10_0
	Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * ___U3CU3E9__10_0_1;
	// System.Func`2<Zenject.InjectTypeInfo_InjectMethodInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>> Zenject.InjectTypeInfo_<>c::<>9__10_1
	Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * ___U3CU3E9__10_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__10_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields, ___U3CU3E9__10_1_2)); }
	inline Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * get_U3CU3E9__10_1_2() const { return ___U3CU3E9__10_1_2; }
	inline Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 ** get_address_of_U3CU3E9__10_1_2() { return &___U3CU3E9__10_1_2; }
	inline void set_U3CU3E9__10_1_2(Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * value)
	{
		___U3CU3E9__10_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__10_1_2), (void*)value);
	}
};


// Zenject.InjectTypeInfo_InjectConstructorInfo
struct  InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5  : public RuntimeObject
{
public:
	// Zenject.ZenFactoryMethod Zenject.InjectTypeInfo_InjectConstructorInfo::Factory
	ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * ___Factory_0;
	// Zenject.InjectableInfo[] Zenject.InjectTypeInfo_InjectConstructorInfo::Parameters
	InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* ___Parameters_1;

public:
	inline static int32_t get_offset_of_Factory_0() { return static_cast<int32_t>(offsetof(InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5, ___Factory_0)); }
	inline ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * get_Factory_0() const { return ___Factory_0; }
	inline ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 ** get_address_of_Factory_0() { return &___Factory_0; }
	inline void set_Factory_0(ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * value)
	{
		___Factory_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Factory_0), (void*)value);
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5, ___Parameters_1)); }
	inline InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* get_Parameters_1() const { return ___Parameters_1; }
	inline InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Parameters_1), (void*)value);
	}
};


// Zenject.InjectTypeInfo_InjectMemberInfo
struct  InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82  : public RuntimeObject
{
public:
	// Zenject.ZenMemberSetterMethod Zenject.InjectTypeInfo_InjectMemberInfo::Setter
	ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * ___Setter_0;
	// Zenject.InjectableInfo Zenject.InjectTypeInfo_InjectMemberInfo::Info
	InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * ___Info_1;

public:
	inline static int32_t get_offset_of_Setter_0() { return static_cast<int32_t>(offsetof(InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82, ___Setter_0)); }
	inline ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * get_Setter_0() const { return ___Setter_0; }
	inline ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 ** get_address_of_Setter_0() { return &___Setter_0; }
	inline void set_Setter_0(ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * value)
	{
		___Setter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Setter_0), (void*)value);
	}

	inline static int32_t get_offset_of_Info_1() { return static_cast<int32_t>(offsetof(InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82, ___Info_1)); }
	inline InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * get_Info_1() const { return ___Info_1; }
	inline InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC ** get_address_of_Info_1() { return &___Info_1; }
	inline void set_Info_1(InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * value)
	{
		___Info_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Info_1), (void*)value);
	}
};


// Zenject.InjectTypeInfo_InjectMethodInfo
struct  InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A  : public RuntimeObject
{
public:
	// System.String Zenject.InjectTypeInfo_InjectMethodInfo::Name
	String_t* ___Name_0;
	// Zenject.ZenInjectMethod Zenject.InjectTypeInfo_InjectMethodInfo::Action
	ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * ___Action_1;
	// Zenject.InjectableInfo[] Zenject.InjectTypeInfo_InjectMethodInfo::Parameters
	InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* ___Parameters_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_Action_1() { return static_cast<int32_t>(offsetof(InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A, ___Action_1)); }
	inline ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * get_Action_1() const { return ___Action_1; }
	inline ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 ** get_address_of_Action_1() { return &___Action_1; }
	inline void set_Action_1(ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * value)
	{
		___Action_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_1), (void*)value);
	}

	inline static int32_t get_offset_of_Parameters_2() { return static_cast<int32_t>(offsetof(InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A, ___Parameters_2)); }
	inline InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* get_Parameters_2() const { return ___Parameters_2; }
	inline InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571** get_address_of_Parameters_2() { return &___Parameters_2; }
	inline void set_Parameters_2(InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* value)
	{
		___Parameters_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Parameters_2), (void*)value);
	}
};


// JetBrains.Annotations.AspChildControlTypeAttribute
struct  AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspChildControlTypeAttribute::<TagName>k__BackingField
	String_t* ___U3CTagNameU3Ek__BackingField_0;
	// System.Type JetBrains.Annotations.AspChildControlTypeAttribute::<ControlType>k__BackingField
	Type_t * ___U3CControlTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTagNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655, ___U3CTagNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CTagNameU3Ek__BackingField_0() const { return ___U3CTagNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTagNameU3Ek__BackingField_0() { return &___U3CTagNameU3Ek__BackingField_0; }
	inline void set_U3CTagNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CTagNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTagNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CControlTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655, ___U3CControlTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CControlTypeU3Ek__BackingField_1() const { return ___U3CControlTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CControlTypeU3Ek__BackingField_1() { return &___U3CControlTypeU3Ek__BackingField_1; }
	inline void set_U3CControlTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CControlTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CControlTypeU3Ek__BackingField_1), (void*)value);
	}
};


// JetBrains.Annotations.AspDataFieldAttribute
struct  AspDataFieldAttribute_t940FB396A4669B558533B0720636F56B51E15D18  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspDataFieldsAttribute
struct  AspDataFieldsAttribute_tAB5D3A23E81D2780D2F5C8DEF615E62BBAD9E4D7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMethodPropertyAttribute
struct  AspMethodPropertyAttribute_tF1E364481C749DE3255CF0DDF7F32D427ADDD7EE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcActionAttribute
struct  AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcActionAttribute::<AnonymousProperty>k__BackingField
	String_t* ___U3CAnonymousPropertyU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAnonymousPropertyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65, ___U3CAnonymousPropertyU3Ek__BackingField_0)); }
	inline String_t* get_U3CAnonymousPropertyU3Ek__BackingField_0() const { return ___U3CAnonymousPropertyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAnonymousPropertyU3Ek__BackingField_0() { return &___U3CAnonymousPropertyU3Ek__BackingField_0; }
	inline void set_U3CAnonymousPropertyU3Ek__BackingField_0(String_t* value)
	{
		___U3CAnonymousPropertyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAnonymousPropertyU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcActionSelectorAttribute
struct  AspMvcActionSelectorAttribute_t11C6F6F0183F81E71464F18A21FC849D3E6953D4  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcAreaAttribute
struct  AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcAreaAttribute::<AnonymousProperty>k__BackingField
	String_t* ___U3CAnonymousPropertyU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAnonymousPropertyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143, ___U3CAnonymousPropertyU3Ek__BackingField_0)); }
	inline String_t* get_U3CAnonymousPropertyU3Ek__BackingField_0() const { return ___U3CAnonymousPropertyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAnonymousPropertyU3Ek__BackingField_0() { return &___U3CAnonymousPropertyU3Ek__BackingField_0; }
	inline void set_U3CAnonymousPropertyU3Ek__BackingField_0(String_t* value)
	{
		___U3CAnonymousPropertyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAnonymousPropertyU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute
struct  AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460, ___U3CFormatU3Ek__BackingField_0)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_0() const { return ___U3CFormatU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_0() { return &___U3CFormatU3Ek__BackingField_0; }
	inline void set_U3CFormatU3Ek__BackingField_0(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute
struct  AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE, ___U3CFormatU3Ek__BackingField_0)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_0() const { return ___U3CFormatU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_0() { return &___U3CFormatU3Ek__BackingField_0; }
	inline void set_U3CFormatU3Ek__BackingField_0(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute
struct  AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101, ___U3CFormatU3Ek__BackingField_0)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_0() const { return ___U3CFormatU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_0() { return &___U3CFormatU3Ek__BackingField_0; }
	inline void set_U3CFormatU3Ek__BackingField_0(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcControllerAttribute
struct  AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcControllerAttribute::<AnonymousProperty>k__BackingField
	String_t* ___U3CAnonymousPropertyU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAnonymousPropertyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F, ___U3CAnonymousPropertyU3Ek__BackingField_0)); }
	inline String_t* get_U3CAnonymousPropertyU3Ek__BackingField_0() const { return ___U3CAnonymousPropertyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAnonymousPropertyU3Ek__BackingField_0() { return &___U3CAnonymousPropertyU3Ek__BackingField_0; }
	inline void set_U3CAnonymousPropertyU3Ek__BackingField_0(String_t* value)
	{
		___U3CAnonymousPropertyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAnonymousPropertyU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcDisplayTemplateAttribute
struct  AspMvcDisplayTemplateAttribute_tF7CAA1E2E66276CBD40C471CB41ACDFCE42DDD57  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcEditorTemplateAttribute
struct  AspMvcEditorTemplateAttribute_tEC434D592971B5C11E261AED277B32A6F243D68C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcMasterAttribute
struct  AspMvcMasterAttribute_t8A29644556A2389ABE4BA7F46CB12DD6AB23AFBD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcMasterLocationFormatAttribute
struct  AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03, ___U3CFormatU3Ek__BackingField_0)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_0() const { return ___U3CFormatU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_0() { return &___U3CFormatU3Ek__BackingField_0; }
	inline void set_U3CFormatU3Ek__BackingField_0(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcModelTypeAttribute
struct  AspMvcModelTypeAttribute_t91DBB2C1DDDE5F76823832C7F23354C4A296CD6F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcPartialViewAttribute
struct  AspMvcPartialViewAttribute_t7EEC1A645243D02EA396745377B8ED662C47F290  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute
struct  AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E, ___U3CFormatU3Ek__BackingField_0)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_0() const { return ___U3CFormatU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_0() { return &___U3CFormatU3Ek__BackingField_0; }
	inline void set_U3CFormatU3Ek__BackingField_0(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspMvcSuppressViewErrorAttribute
struct  AspMvcSuppressViewErrorAttribute_tA3108D0508FE7B43BFDC041EFF9C8E0FBD53123B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcTemplateAttribute
struct  AspMvcTemplateAttribute_tA4B152A320093AFB342AE4C4B5B24A6F019CD0F8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcViewAttribute
struct  AspMvcViewAttribute_t0745A873B4C68EE712EE0E79BB474770507601E6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcViewComponentAttribute
struct  AspMvcViewComponentAttribute_t597F801F316EC6648949580A739D04AAA2628DDC  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcViewComponentViewAttribute
struct  AspMvcViewComponentViewAttribute_t52125A3852EC185D7C36A77E532BAB751D32FF60  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AspMvcViewLocationFormatAttribute
struct  AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspMvcViewLocationFormatAttribute::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7, ___U3CFormatU3Ek__BackingField_0)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_0() const { return ___U3CFormatU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_0() { return &___U3CFormatU3Ek__BackingField_0; }
	inline void set_U3CFormatU3Ek__BackingField_0(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspRequiredAttributeAttribute
struct  AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.AspRequiredAttributeAttribute::<Attribute>k__BackingField
	String_t* ___U3CAttributeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAttributeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7, ___U3CAttributeU3Ek__BackingField_0)); }
	inline String_t* get_U3CAttributeU3Ek__BackingField_0() const { return ___U3CAttributeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAttributeU3Ek__BackingField_0() { return &___U3CAttributeU3Ek__BackingField_0; }
	inline void set_U3CAttributeU3Ek__BackingField_0(String_t* value)
	{
		___U3CAttributeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAttributeU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.AspTypePropertyAttribute
struct  AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean JetBrains.Annotations.AspTypePropertyAttribute::<CreateConstructorReferences>k__BackingField
	bool ___U3CCreateConstructorReferencesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCreateConstructorReferencesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA, ___U3CCreateConstructorReferencesU3Ek__BackingField_0)); }
	inline bool get_U3CCreateConstructorReferencesU3Ek__BackingField_0() const { return ___U3CCreateConstructorReferencesU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCreateConstructorReferencesU3Ek__BackingField_0() { return &___U3CCreateConstructorReferencesU3Ek__BackingField_0; }
	inline void set_U3CCreateConstructorReferencesU3Ek__BackingField_0(bool value)
	{
		___U3CCreateConstructorReferencesU3Ek__BackingField_0 = value;
	}
};


// JetBrains.Annotations.AssertionMethodAttribute
struct  AssertionMethodAttribute_t9B265B34C1B6FD821D5BC3EB3A7F342F92F3C236  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.BaseTypeRequiredAttribute
struct  BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type JetBrains.Annotations.BaseTypeRequiredAttribute::<BaseType>k__BackingField
	Type_t * ___U3CBaseTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBaseTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6, ___U3CBaseTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CBaseTypeU3Ek__BackingField_0() const { return ___U3CBaseTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CBaseTypeU3Ek__BackingField_0() { return &___U3CBaseTypeU3Ek__BackingField_0; }
	inline void set_U3CBaseTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CBaseTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBaseTypeU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.CanBeNullAttribute
struct  CanBeNullAttribute_t6E141B9F14D794ACDC3DC13FB87ED8180E9B908D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.CannotApplyEqualityOperatorAttribute
struct  CannotApplyEqualityOperatorAttribute_t76BF7A501FA5EB0A134C4F39A61A5DE7CA5B45CC  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.ContractAnnotationAttribute
struct  ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.ContractAnnotationAttribute::<Contract>k__BackingField
	String_t* ___U3CContractU3Ek__BackingField_0;
	// System.Boolean JetBrains.Annotations.ContractAnnotationAttribute::<ForceFullStates>k__BackingField
	bool ___U3CForceFullStatesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CContractU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B, ___U3CContractU3Ek__BackingField_0)); }
	inline String_t* get_U3CContractU3Ek__BackingField_0() const { return ___U3CContractU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CContractU3Ek__BackingField_0() { return &___U3CContractU3Ek__BackingField_0; }
	inline void set_U3CContractU3Ek__BackingField_0(String_t* value)
	{
		___U3CContractU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CContractU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CForceFullStatesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B, ___U3CForceFullStatesU3Ek__BackingField_1)); }
	inline bool get_U3CForceFullStatesU3Ek__BackingField_1() const { return ___U3CForceFullStatesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CForceFullStatesU3Ek__BackingField_1() { return &___U3CForceFullStatesU3Ek__BackingField_1; }
	inline void set_U3CForceFullStatesU3Ek__BackingField_1(bool value)
	{
		___U3CForceFullStatesU3Ek__BackingField_1 = value;
	}
};


// JetBrains.Annotations.HtmlAttributeValueAttribute
struct  HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.HtmlAttributeValueAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.HtmlElementAttributesAttribute
struct  HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.HtmlElementAttributesAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.InstantHandleAttribute
struct  InstantHandleAttribute_t75BD43B7B196BBB162084A2DD3C9CB1E717CA66F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.InvokerParameterNameAttribute
struct  InvokerParameterNameAttribute_t75C6B9095D54A0DE657CE40F6677669BBB68221B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.ItemCanBeNullAttribute
struct  ItemCanBeNullAttribute_t45743AFAC9CC28B3EA4E687AEC4BB8111EE971D5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.ItemNotNullAttribute
struct  ItemNotNullAttribute_t4EE880F50F3721F545953DCCB982C0E96F59C4F2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.LinqTunnelAttribute
struct  LinqTunnelAttribute_tB1D84829BB4C1FC08EDFB97CC903908F435597C2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.LocalizationRequiredAttribute
struct  LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean JetBrains.Annotations.LocalizationRequiredAttribute::<Required>k__BackingField
	bool ___U3CRequiredU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRequiredU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416, ___U3CRequiredU3Ek__BackingField_0)); }
	inline bool get_U3CRequiredU3Ek__BackingField_0() const { return ___U3CRequiredU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CRequiredU3Ek__BackingField_0() { return &___U3CRequiredU3Ek__BackingField_0; }
	inline void set_U3CRequiredU3Ek__BackingField_0(bool value)
	{
		___U3CRequiredU3Ek__BackingField_0 = value;
	}
};


// JetBrains.Annotations.MacroAttribute
struct  MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.MacroAttribute::<Expression>k__BackingField
	String_t* ___U3CExpressionU3Ek__BackingField_0;
	// System.Int32 JetBrains.Annotations.MacroAttribute::<Editable>k__BackingField
	int32_t ___U3CEditableU3Ek__BackingField_1;
	// System.String JetBrains.Annotations.MacroAttribute::<Target>k__BackingField
	String_t* ___U3CTargetU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CExpressionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C, ___U3CExpressionU3Ek__BackingField_0)); }
	inline String_t* get_U3CExpressionU3Ek__BackingField_0() const { return ___U3CExpressionU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CExpressionU3Ek__BackingField_0() { return &___U3CExpressionU3Ek__BackingField_0; }
	inline void set_U3CExpressionU3Ek__BackingField_0(String_t* value)
	{
		___U3CExpressionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExpressionU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEditableU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C, ___U3CEditableU3Ek__BackingField_1)); }
	inline int32_t get_U3CEditableU3Ek__BackingField_1() const { return ___U3CEditableU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CEditableU3Ek__BackingField_1() { return &___U3CEditableU3Ek__BackingField_1; }
	inline void set_U3CEditableU3Ek__BackingField_1(int32_t value)
	{
		___U3CEditableU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C, ___U3CTargetU3Ek__BackingField_2)); }
	inline String_t* get_U3CTargetU3Ek__BackingField_2() const { return ___U3CTargetU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTargetU3Ek__BackingField_2() { return &___U3CTargetU3Ek__BackingField_2; }
	inline void set_U3CTargetU3Ek__BackingField_2(String_t* value)
	{
		___U3CTargetU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTargetU3Ek__BackingField_2), (void*)value);
	}
};


// JetBrains.Annotations.MustUseReturnValueAttribute
struct  MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.MustUseReturnValueAttribute::<Justification>k__BackingField
	String_t* ___U3CJustificationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CJustificationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2, ___U3CJustificationU3Ek__BackingField_0)); }
	inline String_t* get_U3CJustificationU3Ek__BackingField_0() const { return ___U3CJustificationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CJustificationU3Ek__BackingField_0() { return &___U3CJustificationU3Ek__BackingField_0; }
	inline void set_U3CJustificationU3Ek__BackingField_0(String_t* value)
	{
		___U3CJustificationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CJustificationU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.NoEnumerationAttribute
struct  NoEnumerationAttribute_t6261D712EC3D4C55A028AF9BEBA896C6B42864C7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.NoReorderAttribute
struct  NoReorderAttribute_tE5D8B57DF55ABEC6A1A04AAFB1BC4C7BC86D317F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.NotNullAttribute
struct  NotNullAttribute_t13EA67CC218B36A5FB93BBDF6C7FC422AC84E0EB  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute
struct  NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::<ParameterName>k__BackingField
	String_t* ___U3CParameterNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CParameterNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C, ___U3CParameterNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CParameterNameU3Ek__BackingField_0() const { return ___U3CParameterNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CParameterNameU3Ek__BackingField_0() { return &___U3CParameterNameU3Ek__BackingField_0; }
	inline void set_U3CParameterNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CParameterNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CParameterNameU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.PathReferenceAttribute
struct  PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.PathReferenceAttribute::<BasePath>k__BackingField
	String_t* ___U3CBasePathU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBasePathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7, ___U3CBasePathU3Ek__BackingField_0)); }
	inline String_t* get_U3CBasePathU3Ek__BackingField_0() const { return ___U3CBasePathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CBasePathU3Ek__BackingField_0() { return &___U3CBasePathU3Ek__BackingField_0; }
	inline void set_U3CBasePathU3Ek__BackingField_0(String_t* value)
	{
		___U3CBasePathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBasePathU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.ProvidesContextAttribute
struct  ProvidesContextAttribute_t018F8C277FAF081B25021559156B37F5CD05D908  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.PublicAPIAttribute
struct  PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.PublicAPIAttribute::<Comment>k__BackingField
	String_t* ___U3CCommentU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCommentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8, ___U3CCommentU3Ek__BackingField_0)); }
	inline String_t* get_U3CCommentU3Ek__BackingField_0() const { return ___U3CCommentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCommentU3Ek__BackingField_0() { return &___U3CCommentU3Ek__BackingField_0; }
	inline void set_U3CCommentU3Ek__BackingField_0(String_t* value)
	{
		___U3CCommentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCommentU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.PureAttribute
struct  PureAttribute_t88CF1F9EDCAE55E72BA1C026F4AB10943B451057  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.RazorDirectiveAttribute
struct  RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.RazorDirectiveAttribute::<Directive>k__BackingField
	String_t* ___U3CDirectiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CDirectiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E, ___U3CDirectiveU3Ek__BackingField_0)); }
	inline String_t* get_U3CDirectiveU3Ek__BackingField_0() const { return ___U3CDirectiveU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDirectiveU3Ek__BackingField_0() { return &___U3CDirectiveU3Ek__BackingField_0; }
	inline void set_U3CDirectiveU3Ek__BackingField_0(String_t* value)
	{
		___U3CDirectiveU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDirectiveU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.RazorHelperCommonAttribute
struct  RazorHelperCommonAttribute_t1E02250F84DB9E222211DCB0B990F23243E43C70  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.RazorImportNamespaceAttribute
struct  RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.RazorImportNamespaceAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.RazorInjectionAttribute
struct  RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.RazorInjectionAttribute::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_0;
	// System.String JetBrains.Annotations.RazorInjectionAttribute::<FieldName>k__BackingField
	String_t* ___U3CFieldNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF, ___U3CTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTypeU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFieldNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF, ___U3CFieldNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CFieldNameU3Ek__BackingField_1() const { return ___U3CFieldNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFieldNameU3Ek__BackingField_1() { return &___U3CFieldNameU3Ek__BackingField_1; }
	inline void set_U3CFieldNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CFieldNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFieldNameU3Ek__BackingField_1), (void*)value);
	}
};


// JetBrains.Annotations.RazorLayoutAttribute
struct  RazorLayoutAttribute_t279E1BF2A55E4D945C9F48FDB3C5F0DDE8BC4192  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.RazorPageBaseTypeAttribute
struct  RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.RazorPageBaseTypeAttribute::<BaseType>k__BackingField
	String_t* ___U3CBaseTypeU3Ek__BackingField_0;
	// System.String JetBrains.Annotations.RazorPageBaseTypeAttribute::<PageName>k__BackingField
	String_t* ___U3CPageNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBaseTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F, ___U3CBaseTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CBaseTypeU3Ek__BackingField_0() const { return ___U3CBaseTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CBaseTypeU3Ek__BackingField_0() { return &___U3CBaseTypeU3Ek__BackingField_0; }
	inline void set_U3CBaseTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CBaseTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBaseTypeU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPageNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F, ___U3CPageNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CPageNameU3Ek__BackingField_1() const { return ___U3CPageNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CPageNameU3Ek__BackingField_1() { return &___U3CPageNameU3Ek__BackingField_1; }
	inline void set_U3CPageNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CPageNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPageNameU3Ek__BackingField_1), (void*)value);
	}
};


// JetBrains.Annotations.RazorSectionAttribute
struct  RazorSectionAttribute_t5AA6D2F94C7018B03FF362B7E90EE774D1E6F51F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.RazorWriteLiteralMethodAttribute
struct  RazorWriteLiteralMethodAttribute_t64AA2D6F82DA8131822E06EA6F27BD92907728C2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.RazorWriteMethodAttribute
struct  RazorWriteMethodAttribute_t2BF3FF98004544723D2533D75C2CE5797D6483A0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.RazorWriteMethodParameterAttribute
struct  RazorWriteMethodParameterAttribute_t386D71275AEE065917E2324F16C057D3A729BE02  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.RegexPatternAttribute
struct  RegexPatternAttribute_t0C4A75B79AD6457C708345386202B8F05C9B2C1B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.SourceTemplateAttribute
struct  SourceTemplateAttribute_tFA362C973C19E229A137F5B77CC31DDBD1C53607  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.StringFormatMethodAttribute
struct  StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.StringFormatMethodAttribute::<FormatParameterName>k__BackingField
	String_t* ___U3CFormatParameterNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFormatParameterNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14, ___U3CFormatParameterNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CFormatParameterNameU3Ek__BackingField_0() const { return ___U3CFormatParameterNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFormatParameterNameU3Ek__BackingField_0() { return &___U3CFormatParameterNameU3Ek__BackingField_0; }
	inline void set_U3CFormatParameterNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CFormatParameterNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatParameterNameU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.TerminatesProgramAttribute
struct  TerminatesProgramAttribute_tA0AECA56847F077E969CA259515E90EE724AD488  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.ValueProviderAttribute
struct  ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String JetBrains.Annotations.ValueProviderAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}
};


// JetBrains.Annotations.XamlItemBindingOfItemsControlAttribute
struct  XamlItemBindingOfItemsControlAttribute_t4D95AE96BC401E816B6D94988CFEA63015FE330B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.XamlItemsControlAttribute
struct  XamlItemsControlAttribute_tA6BF5767DF1774A8781DB8CB6DAED08C643DE44E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// Zenject.Internal.PreserveAttribute
struct  PreserveAttribute_t2D26C6DD68541293CA3738A57BC67377AAB1EDCA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Zenject.NoReflectionBakingAttribute
struct  NoReflectionBakingAttribute_tEF78DB88D1E3F4057AB9F7AC1C62E232B9A9B2A4  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Zenject.ZenjectAllowDuringValidationAttribute
struct  ZenjectAllowDuringValidationAttribute_t85FD03316B9D1E412DBCA415A1616B005D8AF540  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// JetBrains.Annotations.AssertionConditionType
struct  AssertionConditionType_t01A46136A20783FB1097A63F564E73CF5178A8EC 
{
public:
	// System.Int32 JetBrains.Annotations.AssertionConditionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssertionConditionType_t01A46136A20783FB1097A63F564E73CF5178A8EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.CollectionAccessType
struct  CollectionAccessType_t826368C4938DAC7836EC49A7696623F564A305BF 
{
public:
	// System.Int32 JetBrains.Annotations.CollectionAccessType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollectionAccessType_t826368C4938DAC7836EC49A7696623F564A305BF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.ImplicitUseKindFlags
struct  ImplicitUseKindFlags_tBA994A79C2F484468AD89B47E0C21C8DC3F8AD7E 
{
public:
	// System.Int32 JetBrains.Annotations.ImplicitUseKindFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImplicitUseKindFlags_tBA994A79C2F484468AD89B47E0C21C8DC3F8AD7E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.ImplicitUseTargetFlags
struct  ImplicitUseTargetFlags_t55F5AC6930A35FB700B54C90C6886A99AD8F180F 
{
public:
	// System.Int32 JetBrains.Annotations.ImplicitUseTargetFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImplicitUseTargetFlags_t55F5AC6930A35FB700B54C90C6886A99AD8F180F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Zenject.InjectSources
struct  InjectSources_tA4A3519C44AAFA01BA177DCA963CAD6D25CB72E2 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_tA4A3519C44AAFA01BA177DCA963CAD6D25CB72E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.AssertionConditionAttribute
struct  AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// JetBrains.Annotations.AssertionConditionType JetBrains.Annotations.AssertionConditionAttribute::<ConditionType>k__BackingField
	int32_t ___U3CConditionTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CConditionTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1, ___U3CConditionTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CConditionTypeU3Ek__BackingField_0() const { return ___U3CConditionTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CConditionTypeU3Ek__BackingField_0() { return &___U3CConditionTypeU3Ek__BackingField_0; }
	inline void set_U3CConditionTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CConditionTypeU3Ek__BackingField_0 = value;
	}
};


// JetBrains.Annotations.CollectionAccessAttribute
struct  CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// JetBrains.Annotations.CollectionAccessType JetBrains.Annotations.CollectionAccessAttribute::<CollectionAccessType>k__BackingField
	int32_t ___U3CCollectionAccessTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCollectionAccessTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E, ___U3CCollectionAccessTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CCollectionAccessTypeU3Ek__BackingField_0() const { return ___U3CCollectionAccessTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCollectionAccessTypeU3Ek__BackingField_0() { return &___U3CCollectionAccessTypeU3Ek__BackingField_0; }
	inline void set_U3CCollectionAccessTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CCollectionAccessTypeU3Ek__BackingField_0 = value;
	}
};


// JetBrains.Annotations.MeansImplicitUseAttribute
struct  MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.MeansImplicitUseAttribute::<UseKindFlags>k__BackingField
	int32_t ___U3CUseKindFlagsU3Ek__BackingField_0;
	// JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.MeansImplicitUseAttribute::<TargetFlags>k__BackingField
	int32_t ___U3CTargetFlagsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUseKindFlagsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F, ___U3CUseKindFlagsU3Ek__BackingField_0)); }
	inline int32_t get_U3CUseKindFlagsU3Ek__BackingField_0() const { return ___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CUseKindFlagsU3Ek__BackingField_0() { return &___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline void set_U3CUseKindFlagsU3Ek__BackingField_0(int32_t value)
	{
		___U3CUseKindFlagsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTargetFlagsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F, ___U3CTargetFlagsU3Ek__BackingField_1)); }
	inline int32_t get_U3CTargetFlagsU3Ek__BackingField_1() const { return ___U3CTargetFlagsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTargetFlagsU3Ek__BackingField_1() { return &___U3CTargetFlagsU3Ek__BackingField_1; }
	inline void set_U3CTargetFlagsU3Ek__BackingField_1(int32_t value)
	{
		___U3CTargetFlagsU3Ek__BackingField_1 = value;
	}
};


// JetBrains.Annotations.UsedImplicitlyAttribute
struct  UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.UsedImplicitlyAttribute::<UseKindFlags>k__BackingField
	int32_t ___U3CUseKindFlagsU3Ek__BackingField_0;
	// JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.UsedImplicitlyAttribute::<TargetFlags>k__BackingField
	int32_t ___U3CTargetFlagsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUseKindFlagsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7, ___U3CUseKindFlagsU3Ek__BackingField_0)); }
	inline int32_t get_U3CUseKindFlagsU3Ek__BackingField_0() const { return ___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CUseKindFlagsU3Ek__BackingField_0() { return &___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline void set_U3CUseKindFlagsU3Ek__BackingField_0(int32_t value)
	{
		___U3CUseKindFlagsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTargetFlagsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7, ___U3CTargetFlagsU3Ek__BackingField_1)); }
	inline int32_t get_U3CTargetFlagsU3Ek__BackingField_1() const { return ___U3CTargetFlagsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTargetFlagsU3Ek__BackingField_1() { return &___U3CTargetFlagsU3Ek__BackingField_1; }
	inline void set_U3CTargetFlagsU3Ek__BackingField_1(int32_t value)
	{
		___U3CTargetFlagsU3Ek__BackingField_1 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// Zenject.InjectAttributeBase
struct  InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC  : public PreserveAttribute_t2D26C6DD68541293CA3738A57BC67377AAB1EDCA
{
public:
	// System.Boolean Zenject.InjectAttributeBase::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_0;
	// System.Object Zenject.InjectAttributeBase::<Id>k__BackingField
	RuntimeObject * ___U3CIdU3Ek__BackingField_1;
	// Zenject.InjectSources Zenject.InjectAttributeBase::<Source>k__BackingField
	int32_t ___U3CSourceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC, ___U3COptionalU3Ek__BackingField_0)); }
	inline bool get_U3COptionalU3Ek__BackingField_0() const { return ___U3COptionalU3Ek__BackingField_0; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_0() { return &___U3COptionalU3Ek__BackingField_0; }
	inline void set_U3COptionalU3Ek__BackingField_0(bool value)
	{
		___U3COptionalU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC, ___U3CIdU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CIdU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC, ___U3CSourceU3Ek__BackingField_2)); }
	inline int32_t get_U3CSourceU3Ek__BackingField_2() const { return ___U3CSourceU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CSourceU3Ek__BackingField_2() { return &___U3CSourceU3Ek__BackingField_2; }
	inline void set_U3CSourceU3Ek__BackingField_2(int32_t value)
	{
		___U3CSourceU3Ek__BackingField_2 = value;
	}
};


// Zenject.InjectableInfo
struct  InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC  : public RuntimeObject
{
public:
	// System.Boolean Zenject.InjectableInfo::Optional
	bool ___Optional_0;
	// System.Object Zenject.InjectableInfo::Identifier
	RuntimeObject * ___Identifier_1;
	// Zenject.InjectSources Zenject.InjectableInfo::SourceType
	int32_t ___SourceType_2;
	// System.String Zenject.InjectableInfo::MemberName
	String_t* ___MemberName_3;
	// System.Type Zenject.InjectableInfo::MemberType
	Type_t * ___MemberType_4;
	// System.Object Zenject.InjectableInfo::DefaultValue
	RuntimeObject * ___DefaultValue_5;

public:
	inline static int32_t get_offset_of_Optional_0() { return static_cast<int32_t>(offsetof(InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC, ___Optional_0)); }
	inline bool get_Optional_0() const { return ___Optional_0; }
	inline bool* get_address_of_Optional_0() { return &___Optional_0; }
	inline void set_Optional_0(bool value)
	{
		___Optional_0 = value;
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC, ___Identifier_1)); }
	inline RuntimeObject * get_Identifier_1() const { return ___Identifier_1; }
	inline RuntimeObject ** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(RuntimeObject * value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Identifier_1), (void*)value);
	}

	inline static int32_t get_offset_of_SourceType_2() { return static_cast<int32_t>(offsetof(InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC, ___SourceType_2)); }
	inline int32_t get_SourceType_2() const { return ___SourceType_2; }
	inline int32_t* get_address_of_SourceType_2() { return &___SourceType_2; }
	inline void set_SourceType_2(int32_t value)
	{
		___SourceType_2 = value;
	}

	inline static int32_t get_offset_of_MemberName_3() { return static_cast<int32_t>(offsetof(InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC, ___MemberName_3)); }
	inline String_t* get_MemberName_3() const { return ___MemberName_3; }
	inline String_t** get_address_of_MemberName_3() { return &___MemberName_3; }
	inline void set_MemberName_3(String_t* value)
	{
		___MemberName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_3), (void*)value);
	}

	inline static int32_t get_offset_of_MemberType_4() { return static_cast<int32_t>(offsetof(InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC, ___MemberType_4)); }
	inline Type_t * get_MemberType_4() const { return ___MemberType_4; }
	inline Type_t ** get_address_of_MemberType_4() { return &___MemberType_4; }
	inline void set_MemberType_4(Type_t * value)
	{
		___MemberType_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberType_4), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultValue_5() { return static_cast<int32_t>(offsetof(InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC, ___DefaultValue_5)); }
	inline RuntimeObject * get_DefaultValue_5() const { return ___DefaultValue_5; }
	inline RuntimeObject ** get_address_of_DefaultValue_5() { return &___DefaultValue_5; }
	inline void set_DefaultValue_5(RuntimeObject * value)
	{
		___DefaultValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultValue_5), (void*)value);
	}
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<Zenject.InjectTypeInfo_InjectMemberInfo,Zenject.InjectableInfo>
struct  Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<Zenject.InjectTypeInfo_InjectMethodInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>>
struct  Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0  : public MulticastDelegate_t
{
public:

public:
};


// Zenject.InjectAttribute
struct  InjectAttribute_t8D23ED3F95574DFF6119775BAAC731099F592B2E  : public InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC
{
public:

public:
};


// Zenject.InjectLocalAttribute
struct  InjectLocalAttribute_tE96A924A3CDE8393549396FC2BBA80BBBDCB0C2D  : public InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC
{
public:

public:
};


// Zenject.InjectOptionalAttribute
struct  InjectOptionalAttribute_t958E1718D848609A8BD2E95719B0BD23A88A51E4  : public InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC
{
public:

public:
};


// Zenject.ZenFactoryMethod
struct  ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525  : public MulticastDelegate_t
{
public:

public:
};


// Zenject.ZenInjectMethod
struct  ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161  : public MulticastDelegate_t
{
public:

public:
};


// Zenject.ZenMemberSetterMethod
struct  ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Zenject.InjectTypeInfo_InjectMethodInfo[]
struct InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * m_Items[1];

public:
	inline InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Zenject.InjectTypeInfo_InjectMemberInfo[]
struct InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * m_Items[1];

public:
	inline InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Zenject.InjectableInfo[]
struct InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * m_Items[1];

public:
	inline InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m93DBD723B5A365BD92FAF21BECDDCAFF67D0CA72_gshared (RuntimeObject* ___source0, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___selector1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Concat_TisRuntimeObject_m3AA74ABA7EF399A306E36C2171474C81486A4F71_gshared (RuntimeObject* ___first0, RuntimeObject* ___second1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::SelectMany<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Collections.Generic.IEnumerable`1<!!1>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_SelectMany_TisRuntimeObject_TisRuntimeObject_m45857CDA63601D5F28356D15915F67628A17ACDF_gshared (RuntimeObject* ___source0, Func_2_t122180C7631A517B0F4BF97E5754C5F998E8923D * ___selector1, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspChildControlTypeAttribute::set_TagName(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspChildControlTypeAttribute_set_TagName_m6A9A3D854C58738EBD8A0D379FB6FD5AA2D69BAD_inline (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspChildControlTypeAttribute::set_ControlType(System.Type)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspChildControlTypeAttribute_set_ControlType_mF8FD2F6F1BD65DB6966EC199D6DCBF9930D71EA0_inline (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, Type_t * ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcActionAttribute::set_AnonymousProperty(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcActionAttribute_set_AnonymousProperty_m83EA3BE21BBB1F4735FBE043543BD4984590BBDA_inline (AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcAreaAttribute::set_AnonymousProperty(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaAttribute_set_AnonymousProperty_m6217B255496EF23DBEFF147D69F4CC75028DD372_inline (AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaMasterLocationFormatAttribute_set_Format_m23924305517051ABCC0C5A64FBC969E57DD64CC1_inline (AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaPartialViewLocationFormatAttribute_set_Format_mBA39E10F8E9F267F28B653341FECA4E8D75E29C0_inline (AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaViewLocationFormatAttribute_set_Format_mFA517B963A59B6F24995B1051BBD644B19A6E1B6_inline (AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcControllerAttribute::set_AnonymousProperty(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcControllerAttribute_set_AnonymousProperty_mDB5062FA1F14FB6FBA2034FFD85CDA4EDFAF5083_inline (AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcMasterLocationFormatAttribute_set_Format_m852EA80CBBEED7BB8853D695FE37C1C9E5A707C3_inline (AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcPartialViewLocationFormatAttribute_set_Format_m1D39A9129B1D56D968870065FDD2E65244FFB3F9_inline (AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspMvcViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcViewLocationFormatAttribute_set_Format_m9AB87404E45FA9C2AAFD4057124EE2181ACCE963_inline (AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspRequiredAttributeAttribute::set_Attribute(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspRequiredAttributeAttribute_set_Attribute_m4C27C1CF35F50F5A398DE77CBD9E3BA4E49FFE83_inline (AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AspTypePropertyAttribute::set_CreateConstructorReferences(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspTypePropertyAttribute_set_CreateConstructorReferences_m953788280CA7DCB18C58C6CDB8DB221C857F4037_inline (AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA * __this, bool ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.AssertionConditionAttribute::set_ConditionType(JetBrains.Annotations.AssertionConditionType)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AssertionConditionAttribute_set_ConditionType_m2E1C8FDBA6328631D71E7EDF3A655F521B606727_inline (AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.BaseTypeRequiredAttribute::set_BaseType(System.Type)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void BaseTypeRequiredAttribute_set_BaseType_m0E7A68B87BCBF97208AB3B7D975773835CCB6B8C_inline (BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6 * __this, Type_t * ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.CollectionAccessAttribute::set_CollectionAccessType(JetBrains.Annotations.CollectionAccessType)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void CollectionAccessAttribute_set_CollectionAccessType_mBE8F74690533AF7885DF9E691A758BB6CDA15529_inline (CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.ContractAnnotationAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContractAnnotationAttribute__ctor_m39389343D4F1D30EADBE6CDFC5A91BBB2CF0406E (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, String_t* ___contract0, bool ___forceFullStates1, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.ContractAnnotationAttribute::set_Contract(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ContractAnnotationAttribute_set_Contract_m46117527529866206D93D24AC03B8A5B97054D34_inline (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.ContractAnnotationAttribute::set_ForceFullStates(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ContractAnnotationAttribute_set_ForceFullStates_m3273626E1D840A51CE3373012B591A0A4652EBC0_inline (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, bool ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.HtmlAttributeValueAttribute::set_Name(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HtmlAttributeValueAttribute_set_Name_m40857D8975B094CCA401959469C902D8E4EF2F13_inline (HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.HtmlElementAttributesAttribute::set_Name(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HtmlElementAttributesAttribute_set_Name_mC66C3677ECE785266657B3BCCE620ED99AF9D4AE_inline (HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.LocalizationRequiredAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalizationRequiredAttribute__ctor_mC9ED48DF8302CB9F4D0217AAA4A81AA28A19E352 (LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416 * __this, bool ___required0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.LocalizationRequiredAttribute::set_Required(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void LocalizationRequiredAttribute_set_Required_m28CDD5F78263BE6179EE3E29E80EBCF93FA51021_inline (LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags,JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute__ctor_m8B1CB0647B10039C21C2A9A456E9E4C502331FFF (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___useKindFlags0, int32_t ___targetFlags1, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::set_UseKindFlags(JetBrains.Annotations.ImplicitUseKindFlags)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute_set_UseKindFlags_mC020328ACCA94C5816E6F418E7CC6EB9E1E2C9F0_inline (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::set_TargetFlags(JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute_set_TargetFlags_mCCCE7B4BCF2EA74538E4C90496208ACAD32B7A8F_inline (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.MustUseReturnValueAttribute::set_Justification(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MustUseReturnValueAttribute_set_Justification_m1DAE6CCA661B24A56C45C0ACF84FD195C7D44A04_inline (MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::set_ParameterName(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void NotifyPropertyChangedInvocatorAttribute_set_ParameterName_m4042408E5BD997B59E313B781D8FD778DE132D5D_inline (NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.PathReferenceAttribute::set_BasePath(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PathReferenceAttribute_set_BasePath_m6039B47746370E59513C5C7161053AE20F741255_inline (PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.PublicAPIAttribute::set_Comment(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PublicAPIAttribute_set_Comment_m803065FC1B1C3AE5533C775B09668E5F210ABAC2_inline (PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.RazorDirectiveAttribute::set_Directive(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorDirectiveAttribute_set_Directive_m13E8B8A3D7AA46CFC68ADD972BECE00EE489A1D2_inline (RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.RazorImportNamespaceAttribute::set_Name(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorImportNamespaceAttribute_set_Name_m140C4B453B55F6C62F62B7B38A56B2FEE8D0C090_inline (RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.RazorInjectionAttribute::set_Type(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorInjectionAttribute_set_Type_mC7841CBEBEF456EE2AD385F6C4E4F86154A4BC84_inline (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.RazorInjectionAttribute::set_FieldName(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorInjectionAttribute_set_FieldName_m57A348CB52BE094401092C2631B2500E8E840BFD_inline (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::set_BaseType(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute_set_BaseType_m68D508FAD7FB7558AD5B63D899D61AA9339113D7_inline (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::set_PageName(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute_set_PageName_m919DD65C86D7D50B0D64004F4A11B5D08066DC25_inline (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.StringFormatMethodAttribute::set_FormatParameterName(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StringFormatMethodAttribute_set_FormatParameterName_m7F599817D270C2AE791FADD1F3F467AB800EC5AC_inline (StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags,JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute__ctor_mF053D1548C9F0310AAE782C1AE30BC81A417BE5E (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___useKindFlags0, int32_t ___targetFlags1, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::set_UseKindFlags(JetBrains.Annotations.ImplicitUseKindFlags)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute_set_UseKindFlags_m5D8FE4E5A37C11453F7C376FFC0ED0E84B9D0ADB_inline (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::set_TargetFlags(JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute_set_TargetFlags_mE8D6A7BABDB5B3846B11D2E3BCD0C2AEE644132F_inline (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.ValueProviderAttribute::set_Name(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ValueProviderAttribute_set_Name_m189466822274D76436002174983AC4BDF229B82C_inline (ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Zenject.InjectAttributeBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectAttributeBase__ctor_m2C0314D84470B5111B9276530B6759706691E2BA (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, const RuntimeMethod* method);
// System.Void Zenject.Internal.PreserveAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreserveAttribute__ctor_mCDD191D80D202DAD6E07BFA266003EFF5996E4D9 (PreserveAttribute_t2D26C6DD68541293CA3738A57BC67377AAB1EDCA * __this, const RuntimeMethod* method);
// System.Void Zenject.InjectAttributeBase::set_Source(Zenject.InjectSources)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Source_mE83EFBACAA0B787C77EA0B2C73D2A9CFA654C819_inline (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Zenject.InjectAttributeBase::set_Optional(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Optional_m93C08A9815412E918B346A7351BF871DB5ABD54D_inline (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Func`2<Zenject.InjectTypeInfo/InjectMemberInfo,Zenject.InjectableInfo>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m7D6423527B62548E6DA02AF095EEBFE2956753E0 (Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<Zenject.InjectTypeInfo/InjectMemberInfo,Zenject.InjectableInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisInjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mC65110D147A9A63DFF3F5EEEE3BCA524CFB608C1 (RuntimeObject* ___source0, Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 *, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m93DBD723B5A365BD92FAF21BECDDCAFF67D0CA72_gshared)(___source0, ___selector1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<Zenject.InjectableInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
inline RuntimeObject* Enumerable_Concat_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mA9EDADA7EF8F1F3DFDA1FB3EDD13280EEF0D4BEC (RuntimeObject* ___first0, RuntimeObject* ___second1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*))Enumerable_Concat_TisRuntimeObject_m3AA74ABA7EF399A306E36C2171474C81486A4F71_gshared)(___first0, ___second1, method);
}
// System.Void System.Func`2<Zenject.InjectTypeInfo/InjectMethodInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5810862148CA2671FE549A0D28FDE3D9F8ABDCF0 (Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::SelectMany<Zenject.InjectTypeInfo/InjectMethodInfo,Zenject.InjectableInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Collections.Generic.IEnumerable`1<!!1>>)
inline RuntimeObject* Enumerable_SelectMany_TisInjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_m425531658AB47EF78A6F559A7AD4F91E8FCF5F23 (RuntimeObject* ___source0, Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 *, const RuntimeMethod*))Enumerable_SelectMany_TisRuntimeObject_TisRuntimeObject_m45857CDA63601D5F28356D15915F67628A17ACDF_gshared)(___source0, ___selector1, method);
}
// System.Void Zenject.InjectTypeInfo/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8BA7B247476AA2C17B97E0C9A67F8472E8AE5112 (U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspChildControlTypeAttribute::.ctor(System.String,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspChildControlTypeAttribute__ctor_mEEED24DA23322F148E3E1B0988462D99D712202E (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, String_t* ___tagName0, Type_t * ___controlType1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tagName0;
		AspChildControlTypeAttribute_set_TagName_m6A9A3D854C58738EBD8A0D379FB6FD5AA2D69BAD_inline(__this, L_0, /*hidden argument*/NULL);
		Type_t * L_1 = ___controlType1;
		AspChildControlTypeAttribute_set_ControlType_mF8FD2F6F1BD65DB6966EC199D6DCBF9930D71EA0_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspChildControlTypeAttribute::get_TagName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspChildControlTypeAttribute_get_TagName_m47ED2DE7A3A29BED2CA5CB925C49EBD7548276D3 (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTagNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspChildControlTypeAttribute::set_TagName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspChildControlTypeAttribute_set_TagName_m6A9A3D854C58738EBD8A0D379FB6FD5AA2D69BAD (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTagNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Type JetBrains.Annotations.AspChildControlTypeAttribute::get_ControlType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * AspChildControlTypeAttribute_get_ControlType_m4C91CD8ECEC1EDF68E1FC66970BA667F9D0930BC (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = __this->get_U3CControlTypeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspChildControlTypeAttribute::set_ControlType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspChildControlTypeAttribute_set_ControlType_mF8FD2F6F1BD65DB6966EC199D6DCBF9930D71EA0 (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, Type_t * ___value0, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_U3CControlTypeU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspDataFieldAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspDataFieldAttribute__ctor_m6CC0DE0330087CD5EF8CA17A4161C3363A9755D9 (AspDataFieldAttribute_t940FB396A4669B558533B0720636F56B51E15D18 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspDataFieldsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspDataFieldsAttribute__ctor_m2693844D5D26ED410122666D69D7FA02C89E24CA (AspDataFieldsAttribute_tAB5D3A23E81D2780D2F5C8DEF615E62BBAD9E4D7 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMethodPropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMethodPropertyAttribute__ctor_m7CD3D54FD2697B7C221007A1BA3A6D67970A9D90 (AspMethodPropertyAttribute_tF1E364481C749DE3255CF0DDF7F32D427ADDD7EE * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcActionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcActionAttribute__ctor_m5224998BAD51F889DB341A7242CA569C5461B732 (AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.AspMvcActionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcActionAttribute__ctor_m4EDD633B43CC7F9929C553E4C2DDC480F9E352B1 (AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65 * __this, String_t* ___anonymousProperty0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___anonymousProperty0;
		AspMvcActionAttribute_set_AnonymousProperty_m83EA3BE21BBB1F4735FBE043543BD4984590BBDA_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcActionAttribute::get_AnonymousProperty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcActionAttribute_get_AnonymousProperty_mECC7F953EB784ECECCD91779A41F81BC31B77774 (AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CAnonymousPropertyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcActionAttribute::set_AnonymousProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcActionAttribute_set_AnonymousProperty_m83EA3BE21BBB1F4735FBE043543BD4984590BBDA (AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnonymousPropertyU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcActionSelectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcActionSelectorAttribute__ctor_m99959ADAF51BF6F8F3C91DAA68C49D184C3E44BE (AspMvcActionSelectorAttribute_t11C6F6F0183F81E71464F18A21FC849D3E6953D4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcAreaAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaAttribute__ctor_m08704D0ED240F06DB2295D2E96CA1C76394D6D31 (AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.AspMvcAreaAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaAttribute__ctor_mBB4BB8321027B8620A47FEFF5A5ABE7DBF6669BB (AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143 * __this, String_t* ___anonymousProperty0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___anonymousProperty0;
		AspMvcAreaAttribute_set_AnonymousProperty_m6217B255496EF23DBEFF147D69F4CC75028DD372_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcAreaAttribute::get_AnonymousProperty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcAreaAttribute_get_AnonymousProperty_m1A0AD54CBC1596C72E9B9DC1AE1C1EBBFD843DC6 (AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CAnonymousPropertyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcAreaAttribute::set_AnonymousProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaAttribute_set_AnonymousProperty_m6217B255496EF23DBEFF147D69F4CC75028DD372 (AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnonymousPropertyU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaMasterLocationFormatAttribute__ctor_mDF9E66FCDBFA3CF0C2F526280B3BEB73E1C760CE (AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460 * __this, String_t* ___format0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___format0;
		AspMvcAreaMasterLocationFormatAttribute_set_Format_m23924305517051ABCC0C5A64FBC969E57DD64CC1_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::get_Format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcAreaMasterLocationFormatAttribute_get_Format_mBCD5EFD48945348F59A7A204502B3D0C51C315FA (AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcAreaMasterLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaMasterLocationFormatAttribute_set_Format_m23924305517051ABCC0C5A64FBC969E57DD64CC1 (AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaPartialViewLocationFormatAttribute__ctor_m22B96DDFD5669C96F54AC79899DB78FA8B07181E (AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE * __this, String_t* ___format0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___format0;
		AspMvcAreaPartialViewLocationFormatAttribute_set_Format_mBA39E10F8E9F267F28B653341FECA4E8D75E29C0_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::get_Format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcAreaPartialViewLocationFormatAttribute_get_Format_m0ED3DECD9BC9DEEF7551EC70424316B78F63E7C1 (AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcAreaPartialViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaPartialViewLocationFormatAttribute_set_Format_mBA39E10F8E9F267F28B653341FECA4E8D75E29C0 (AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaViewLocationFormatAttribute__ctor_mFBA83D70929DAA000B726DB007FC0FA6BEBDAD4F (AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101 * __this, String_t* ___format0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___format0;
		AspMvcAreaViewLocationFormatAttribute_set_Format_mFA517B963A59B6F24995B1051BBD644B19A6E1B6_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::get_Format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcAreaViewLocationFormatAttribute_get_Format_m7F2E5A569366D48B3831CED9CA51DB28EA87B3F4 (AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcAreaViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcAreaViewLocationFormatAttribute_set_Format_mFA517B963A59B6F24995B1051BBD644B19A6E1B6 (AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcControllerAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcControllerAttribute__ctor_mCFBCD113CFB32BB3B557F418E22D3144321D7278 (AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.AspMvcControllerAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcControllerAttribute__ctor_mBAB20CE3BE9F3E5356A3D46BF585E1AB5CC0034E (AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F * __this, String_t* ___anonymousProperty0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___anonymousProperty0;
		AspMvcControllerAttribute_set_AnonymousProperty_mDB5062FA1F14FB6FBA2034FFD85CDA4EDFAF5083_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcControllerAttribute::get_AnonymousProperty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcControllerAttribute_get_AnonymousProperty_m1D0891365029E522461959DA55BF330645E623FD (AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CAnonymousPropertyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcControllerAttribute::set_AnonymousProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcControllerAttribute_set_AnonymousProperty_mDB5062FA1F14FB6FBA2034FFD85CDA4EDFAF5083 (AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnonymousPropertyU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcDisplayTemplateAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcDisplayTemplateAttribute__ctor_m4F28D2E29737294FF54CD12C087DE3C52B813254 (AspMvcDisplayTemplateAttribute_tF7CAA1E2E66276CBD40C471CB41ACDFCE42DDD57 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcEditorTemplateAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcEditorTemplateAttribute__ctor_mD0BC86FB6BD7E0092EFFBD2E2B98A62C49580201 (AspMvcEditorTemplateAttribute_tEC434D592971B5C11E261AED277B32A6F243D68C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcMasterAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcMasterAttribute__ctor_mDB7FBDE7214AB5996EC26B1CCA452C8C8D93BF16 (AspMvcMasterAttribute_t8A29644556A2389ABE4BA7F46CB12DD6AB23AFBD * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcMasterLocationFormatAttribute__ctor_m957883C543F9B6A78CF0AC66B4ED0729BA8FF4D0 (AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03 * __this, String_t* ___format0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___format0;
		AspMvcMasterLocationFormatAttribute_set_Format_m852EA80CBBEED7BB8853D695FE37C1C9E5A707C3_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::get_Format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcMasterLocationFormatAttribute_get_Format_m2616278737402EE51EC14B7DD4E790226808FA38 (AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcMasterLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcMasterLocationFormatAttribute_set_Format_m852EA80CBBEED7BB8853D695FE37C1C9E5A707C3 (AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcModelTypeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcModelTypeAttribute__ctor_mF2A57B73788689B0DA7EB3709EE02416ED518CA1 (AspMvcModelTypeAttribute_t91DBB2C1DDDE5F76823832C7F23354C4A296CD6F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcPartialViewAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcPartialViewAttribute__ctor_m1B14296015A3BB5BF56E0B722F7A85BE15D70765 (AspMvcPartialViewAttribute_t7EEC1A645243D02EA396745377B8ED662C47F290 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcPartialViewLocationFormatAttribute__ctor_mE4E72137B00EEFBFFEF64FAECB98DB9C1E70217A (AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E * __this, String_t* ___format0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___format0;
		AspMvcPartialViewLocationFormatAttribute_set_Format_m1D39A9129B1D56D968870065FDD2E65244FFB3F9_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::get_Format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcPartialViewLocationFormatAttribute_get_Format_m2D0B7F0A062EEDFB7EFF147252917D3D590ABEB8 (AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcPartialViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcPartialViewLocationFormatAttribute_set_Format_m1D39A9129B1D56D968870065FDD2E65244FFB3F9 (AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcSuppressViewErrorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcSuppressViewErrorAttribute__ctor_mB98050F8997645798F653FD5FCE352857095348F (AspMvcSuppressViewErrorAttribute_tA3108D0508FE7B43BFDC041EFF9C8E0FBD53123B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcTemplateAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcTemplateAttribute__ctor_m78BE9483D62067D284B3A8FD59B7A11872013C74 (AspMvcTemplateAttribute_tA4B152A320093AFB342AE4C4B5B24A6F019CD0F8 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcViewAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcViewAttribute__ctor_m03E325150A64E35683DC6FCD44476D5412E24CA0 (AspMvcViewAttribute_t0745A873B4C68EE712EE0E79BB474770507601E6 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcViewComponentAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcViewComponentAttribute__ctor_m47DE1A1823403724779B3F93ACE1D4629DE579C7 (AspMvcViewComponentAttribute_t597F801F316EC6648949580A739D04AAA2628DDC * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcViewComponentViewAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcViewComponentViewAttribute__ctor_m9E60CD0D9B3108F801BD7543A33DAB071F97D57F (AspMvcViewComponentViewAttribute_t52125A3852EC185D7C36A77E532BAB751D32FF60 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspMvcViewLocationFormatAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcViewLocationFormatAttribute__ctor_m69C7FCA1E0359B5BD5F8867DD3B785D0B1EE1A17 (AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7 * __this, String_t* ___format0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___format0;
		AspMvcViewLocationFormatAttribute_set_Format_m9AB87404E45FA9C2AAFD4057124EE2181ACCE963_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspMvcViewLocationFormatAttribute::get_Format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspMvcViewLocationFormatAttribute_get_Format_mE9CEBE3AE63F8049ED45B1D507E5680BECB333D9 (AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspMvcViewLocationFormatAttribute::set_Format(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspMvcViewLocationFormatAttribute_set_Format_m9AB87404E45FA9C2AAFD4057124EE2181ACCE963 (AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AspRequiredAttributeAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspRequiredAttributeAttribute__ctor_m006E2662B92F085BC9FADD256D8BBF64C32A8F47 (AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7 * __this, String_t* ___attribute0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___attribute0;
		AspRequiredAttributeAttribute_set_Attribute_m4C27C1CF35F50F5A398DE77CBD9E3BA4E49FFE83_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.AspRequiredAttributeAttribute::get_Attribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AspRequiredAttributeAttribute_get_Attribute_m0D61E17144E7303EFFE4C383656C8E716B819542 (AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CAttributeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspRequiredAttributeAttribute::set_Attribute(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspRequiredAttributeAttribute_set_Attribute_m4C27C1CF35F50F5A398DE77CBD9E3BA4E49FFE83 (AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAttributeU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean JetBrains.Annotations.AspTypePropertyAttribute::get_CreateConstructorReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AspTypePropertyAttribute_get_CreateConstructorReferences_m418D57DFE0F81617864CF81B8E042085FD4CE464 (AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CCreateConstructorReferencesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AspTypePropertyAttribute::set_CreateConstructorReferences(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspTypePropertyAttribute_set_CreateConstructorReferences_m953788280CA7DCB18C58C6CDB8DB221C857F4037 (AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CCreateConstructorReferencesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void JetBrains.Annotations.AspTypePropertyAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspTypePropertyAttribute__ctor_mA5F306BCD34B2EDCF448F2B0F9445A345936C955 (AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA * __this, bool ___createConstructorReferences0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		bool L_0 = ___createConstructorReferences0;
		AspTypePropertyAttribute_set_CreateConstructorReferences_m953788280CA7DCB18C58C6CDB8DB221C857F4037_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AssertionConditionAttribute::.ctor(JetBrains.Annotations.AssertionConditionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssertionConditionAttribute__ctor_mF0B5B121B7C0396DB7F13F1BC78BAFDCAA24CA8B (AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1 * __this, int32_t ___conditionType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___conditionType0;
		AssertionConditionAttribute_set_ConditionType_m2E1C8FDBA6328631D71E7EDF3A655F521B606727_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// JetBrains.Annotations.AssertionConditionType JetBrains.Annotations.AssertionConditionAttribute::get_ConditionType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AssertionConditionAttribute_get_ConditionType_m0EFFDFD6C0C912B759E4380A035A30B3C8966167 (AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CConditionTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.AssertionConditionAttribute::set_ConditionType(JetBrains.Annotations.AssertionConditionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssertionConditionAttribute_set_ConditionType_m2E1C8FDBA6328631D71E7EDF3A655F521B606727 (AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CConditionTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.AssertionMethodAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssertionMethodAttribute__ctor_mAE5114C050EAB7D5663F20F9F46D3E302B8EE53E (AssertionMethodAttribute_t9B265B34C1B6FD821D5BC3EB3A7F342F92F3C236 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.BaseTypeRequiredAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseTypeRequiredAttribute__ctor_m78EAA13CF24B87AA473439C4273861E6FB034F0F (BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6 * __this, Type_t * ___baseType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___baseType0;
		BaseTypeRequiredAttribute_set_BaseType_m0E7A68B87BCBF97208AB3B7D975773835CCB6B8C_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Type JetBrains.Annotations.BaseTypeRequiredAttribute::get_BaseType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * BaseTypeRequiredAttribute_get_BaseType_m6B331E97F682B2371C7E5C1BAAD65B6030AFEF7E (BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6 * __this, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = __this->get_U3CBaseTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.BaseTypeRequiredAttribute::set_BaseType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseTypeRequiredAttribute_set_BaseType_m0E7A68B87BCBF97208AB3B7D975773835CCB6B8C (BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6 * __this, Type_t * ___value0, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_U3CBaseTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.CanBeNullAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CanBeNullAttribute__ctor_m6F19C7002DD42E3AF42DB6F8996CACD816126237 (CanBeNullAttribute_t6E141B9F14D794ACDC3DC13FB87ED8180E9B908D * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.CannotApplyEqualityOperatorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CannotApplyEqualityOperatorAttribute__ctor_m62FF7A1B6FD14BC114841E92C06CDF10060D7857 (CannotApplyEqualityOperatorAttribute_t76BF7A501FA5EB0A134C4F39A61A5DE7CA5B45CC * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.CollectionAccessAttribute::.ctor(JetBrains.Annotations.CollectionAccessType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollectionAccessAttribute__ctor_mD77E92806695EDAC272BBB1BA6E4F2CFE66FF413 (CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E * __this, int32_t ___collectionAccessType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___collectionAccessType0;
		CollectionAccessAttribute_set_CollectionAccessType_mBE8F74690533AF7885DF9E691A758BB6CDA15529_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// JetBrains.Annotations.CollectionAccessType JetBrains.Annotations.CollectionAccessAttribute::get_CollectionAccessType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CollectionAccessAttribute_get_CollectionAccessType_m1BFB8DFEBA87E31D69B182791AFADCEE41827673 (CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CCollectionAccessTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.CollectionAccessAttribute::set_CollectionAccessType(JetBrains.Annotations.CollectionAccessType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollectionAccessAttribute_set_CollectionAccessType_mBE8F74690533AF7885DF9E691A758BB6CDA15529 (CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CCollectionAccessTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.ContractAnnotationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContractAnnotationAttribute__ctor_m601EFEEDF152647707C025B7438A97D53FE65632 (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, String_t* ___contract0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___contract0;
		ContractAnnotationAttribute__ctor_m39389343D4F1D30EADBE6CDFC5A91BBB2CF0406E(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.ContractAnnotationAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContractAnnotationAttribute__ctor_m39389343D4F1D30EADBE6CDFC5A91BBB2CF0406E (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, String_t* ___contract0, bool ___forceFullStates1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___contract0;
		ContractAnnotationAttribute_set_Contract_m46117527529866206D93D24AC03B8A5B97054D34_inline(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___forceFullStates1;
		ContractAnnotationAttribute_set_ForceFullStates_m3273626E1D840A51CE3373012B591A0A4652EBC0_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.ContractAnnotationAttribute::get_Contract()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ContractAnnotationAttribute_get_Contract_m25CDDC707037F6507880BFB6E87FAC91A18C61FB (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CContractU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.ContractAnnotationAttribute::set_Contract(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContractAnnotationAttribute_set_Contract_m46117527529866206D93D24AC03B8A5B97054D34 (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CContractU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean JetBrains.Annotations.ContractAnnotationAttribute::get_ForceFullStates()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ContractAnnotationAttribute_get_ForceFullStates_m1F6FC2F24CE7006D5E351E183FA945BE1C76C140 (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CForceFullStatesU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.ContractAnnotationAttribute::set_ForceFullStates(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContractAnnotationAttribute_set_ForceFullStates_m3273626E1D840A51CE3373012B591A0A4652EBC0 (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CForceFullStatesU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.HtmlAttributeValueAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HtmlAttributeValueAttribute__ctor_mCAA079679C0191C23EF6DFBC710A0D875207E329 (HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		HtmlAttributeValueAttribute_set_Name_m40857D8975B094CCA401959469C902D8E4EF2F13_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.HtmlAttributeValueAttribute::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HtmlAttributeValueAttribute_get_Name_m84B4C87BEF3F505B24072A0AE18CFE3C619719E3 (HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.HtmlAttributeValueAttribute::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HtmlAttributeValueAttribute_set_Name_m40857D8975B094CCA401959469C902D8E4EF2F13 (HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.HtmlElementAttributesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HtmlElementAttributesAttribute__ctor_m488A5CFA47EFEA4166ACBB51212AB8FE01BFC2AC (HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.HtmlElementAttributesAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HtmlElementAttributesAttribute__ctor_mEF36AA00424E0F2FE683B95A2AC6BAEB0CE98CD3 (HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		HtmlElementAttributesAttribute_set_Name_mC66C3677ECE785266657B3BCCE620ED99AF9D4AE_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.HtmlElementAttributesAttribute::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HtmlElementAttributesAttribute_get_Name_mD22BB01CEDBF1E1ACE7373CEEF4DC693E0CB1D37 (HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.HtmlElementAttributesAttribute::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HtmlElementAttributesAttribute_set_Name_mC66C3677ECE785266657B3BCCE620ED99AF9D4AE (HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.InstantHandleAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InstantHandleAttribute__ctor_m2FB33A477515F7831182D5B0848BB50171F84FB5 (InstantHandleAttribute_t75BD43B7B196BBB162084A2DD3C9CB1E717CA66F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.InvokerParameterNameAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvokerParameterNameAttribute__ctor_m6D07C117B1D500B738EC7DE7418B6C873D2485E0 (InvokerParameterNameAttribute_t75C6B9095D54A0DE657CE40F6677669BBB68221B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.ItemCanBeNullAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ItemCanBeNullAttribute__ctor_m59095B6741ACE9A3555CCCD3231CD47C5B5B2F12 (ItemCanBeNullAttribute_t45743AFAC9CC28B3EA4E687AEC4BB8111EE971D5 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.ItemNotNullAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ItemNotNullAttribute__ctor_m367958CC9A39DE676E462B7F673E8B9DD100BD20 (ItemNotNullAttribute_t4EE880F50F3721F545953DCCB982C0E96F59C4F2 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.LinqTunnelAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinqTunnelAttribute__ctor_m7FBC5773938344987792E196D4E9CCF6F9973841 (LinqTunnelAttribute_tB1D84829BB4C1FC08EDFB97CC903908F435597C2 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.LocalizationRequiredAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalizationRequiredAttribute__ctor_m31C88794DC9E45D017667001AB510ABD3EA91264 (LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416 * __this, const RuntimeMethod* method)
{
	{
		LocalizationRequiredAttribute__ctor_mC9ED48DF8302CB9F4D0217AAA4A81AA28A19E352(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.LocalizationRequiredAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalizationRequiredAttribute__ctor_mC9ED48DF8302CB9F4D0217AAA4A81AA28A19E352 (LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416 * __this, bool ___required0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		bool L_0 = ___required0;
		LocalizationRequiredAttribute_set_Required_m28CDD5F78263BE6179EE3E29E80EBCF93FA51021_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JetBrains.Annotations.LocalizationRequiredAttribute::get_Required()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LocalizationRequiredAttribute_get_Required_mF4EDBE1AC5F6B245F44062703216F023D3E02092 (LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CRequiredU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.LocalizationRequiredAttribute::set_Required(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalizationRequiredAttribute_set_Required_m28CDD5F78263BE6179EE3E29E80EBCF93FA51021 (LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CRequiredU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String JetBrains.Annotations.MacroAttribute::get_Expression()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MacroAttribute_get_Expression_m2CCFF5D806E7E97ADAC0EEBC9BAF05F89C0D2AAA (MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CExpressionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.MacroAttribute::set_Expression(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MacroAttribute_set_Expression_m0582A717F271662A01010819A8E6DFC21873CD32 (MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CExpressionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 JetBrains.Annotations.MacroAttribute::get_Editable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MacroAttribute_get_Editable_mC6D969B99E215839850D37F56E090BC3FF630DE0 (MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CEditableU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.MacroAttribute::set_Editable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MacroAttribute_set_Editable_mCDF7F281D18B0DBD6902F6297B2AD01B3D9534AF (MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CEditableU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String JetBrains.Annotations.MacroAttribute::get_Target()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MacroAttribute_get_Target_mBDCBF8052A69036B262E81773A25AF159B568B3A (MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTargetU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.MacroAttribute::set_Target(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MacroAttribute_set_Target_m4D64FFE273A7BD4BF911FD0ABE35DF2F46B134EB (MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTargetU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void JetBrains.Annotations.MacroAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MacroAttribute__ctor_m9113EDB7BD048A9585647F7B4A888DC43A9303F0 (MacroAttribute_tB41C2B477CB4284E5C80F3C2CC579DC87177390C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute__ctor_mEFFCE62C8ECBAB317DF92843D6645F8FEDE7D539 (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, const RuntimeMethod* method)
{
	{
		MeansImplicitUseAttribute__ctor_m8B1CB0647B10039C21C2A9A456E9E4C502331FFF(__this, 7, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute__ctor_m231D188014C6BF4ADA08F119810722D7D182E289 (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___useKindFlags0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___useKindFlags0;
		MeansImplicitUseAttribute__ctor_m8B1CB0647B10039C21C2A9A456E9E4C502331FFF(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute__ctor_mA48E7E71BD8C8A022384A792977F329C2D7E5755 (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___targetFlags0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___targetFlags0;
		MeansImplicitUseAttribute__ctor_m8B1CB0647B10039C21C2A9A456E9E4C502331FFF(__this, 7, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags,JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute__ctor_m8B1CB0647B10039C21C2A9A456E9E4C502331FFF (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___useKindFlags0, int32_t ___targetFlags1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___useKindFlags0;
		MeansImplicitUseAttribute_set_UseKindFlags_mC020328ACCA94C5816E6F418E7CC6EB9E1E2C9F0_inline(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___targetFlags1;
		MeansImplicitUseAttribute_set_TargetFlags_mCCCE7B4BCF2EA74538E4C90496208ACAD32B7A8F_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.MeansImplicitUseAttribute::get_UseKindFlags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeansImplicitUseAttribute_get_UseKindFlags_m9E68CD91BECB87F9140185F69D2C66C8E12FEAFA (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CUseKindFlagsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::set_UseKindFlags(JetBrains.Annotations.ImplicitUseKindFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute_set_UseKindFlags_mC020328ACCA94C5816E6F418E7CC6EB9E1E2C9F0 (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CUseKindFlagsU3Ek__BackingField_0(L_0);
		return;
	}
}
// JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.MeansImplicitUseAttribute::get_TargetFlags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeansImplicitUseAttribute_get_TargetFlags_mB42E1B2CF4F2D83F4D8738F9DD46AD02B0C2BA5B (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTargetFlagsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::set_TargetFlags(JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute_set_TargetFlags_mCCCE7B4BCF2EA74538E4C90496208ACAD32B7A8F (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTargetFlagsU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.MustUseReturnValueAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MustUseReturnValueAttribute__ctor_mA0B62B6EAF331274159186A4C5305B95A08572C7 (MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.MustUseReturnValueAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MustUseReturnValueAttribute__ctor_m8A84924BE475325A76AEEA74C27CD170627C5CAF (MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2 * __this, String_t* ___justification0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___justification0;
		MustUseReturnValueAttribute_set_Justification_m1DAE6CCA661B24A56C45C0ACF84FD195C7D44A04_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.MustUseReturnValueAttribute::get_Justification()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MustUseReturnValueAttribute_get_Justification_m9049DF7AA84BEE837E554F3DE6AB79E6D7071D63 (MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CJustificationU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.MustUseReturnValueAttribute::set_Justification(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MustUseReturnValueAttribute_set_Justification_m1DAE6CCA661B24A56C45C0ACF84FD195C7D44A04 (MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CJustificationU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.NoEnumerationAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NoEnumerationAttribute__ctor_mADC051CDC91A0FFB0DF756783DB7684445444A92 (NoEnumerationAttribute_t6261D712EC3D4C55A028AF9BEBA896C6B42864C7 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.NoReorderAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NoReorderAttribute__ctor_mE22C5847FFD0A1FDD96AB2F7620F802D1A45120E (NoReorderAttribute_tE5D8B57DF55ABEC6A1A04AAFB1BC4C7BC86D317F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.NotNullAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotNullAttribute__ctor_mC042005FB58C4FDBF56356830703305AAC7AFDDE (NotNullAttribute_t13EA67CC218B36A5FB93BBDF6C7FC422AC84E0EB * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotifyPropertyChangedInvocatorAttribute__ctor_mC032202F6CCFA2F3431FE63D0EFBA6A646FECB89 (NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotifyPropertyChangedInvocatorAttribute__ctor_m27B201DC58347E6CB19AD57B9C197BA55570452D (NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C * __this, String_t* ___parameterName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___parameterName0;
		NotifyPropertyChangedInvocatorAttribute_set_ParameterName_m4042408E5BD997B59E313B781D8FD778DE132D5D_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::get_ParameterName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NotifyPropertyChangedInvocatorAttribute_get_ParameterName_mEE85B381BC8D2037911F54E5974FB48948F52F86 (NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CParameterNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.NotifyPropertyChangedInvocatorAttribute::set_ParameterName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotifyPropertyChangedInvocatorAttribute_set_ParameterName_m4042408E5BD997B59E313B781D8FD778DE132D5D (NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CParameterNameU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.PathReferenceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PathReferenceAttribute__ctor_mB7F021886F17C36D1C711E35D6BDCD912A066A09 (PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.PathReferenceAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PathReferenceAttribute__ctor_m66BFC638B85ACEBAAD54F177D979157825922263 (PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7 * __this, String_t* ___basePath0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___basePath0;
		PathReferenceAttribute_set_BasePath_m6039B47746370E59513C5C7161053AE20F741255_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.PathReferenceAttribute::get_BasePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PathReferenceAttribute_get_BasePath_mA9878FEB3B935EF9EB1CAAC91F206459B10363EA (PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CBasePathU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.PathReferenceAttribute::set_BasePath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PathReferenceAttribute_set_BasePath_m6039B47746370E59513C5C7161053AE20F741255 (PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBasePathU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.ProvidesContextAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProvidesContextAttribute__ctor_mBDC4E52D357F22267F945104CA9219E3DF030743 (ProvidesContextAttribute_t018F8C277FAF081B25021559156B37F5CD05D908 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.PublicAPIAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PublicAPIAttribute__ctor_m819E9E7C36012471D43F2465EA4D7CFB59548673 (PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.PublicAPIAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PublicAPIAttribute__ctor_m9DE454053E55A92BC7D309F4CBA1E42822B2F532 (PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8 * __this, String_t* ___comment0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___comment0;
		PublicAPIAttribute_set_Comment_m803065FC1B1C3AE5533C775B09668E5F210ABAC2_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.PublicAPIAttribute::get_Comment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PublicAPIAttribute_get_Comment_m2E3F85A747F4F5EB1277D90AC9405837AD679F38 (PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCommentU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.PublicAPIAttribute::set_Comment(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PublicAPIAttribute_set_Comment_m803065FC1B1C3AE5533C775B09668E5F210ABAC2 (PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCommentU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.PureAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PureAttribute__ctor_mCDEF6E5AB727A4452739AEECD7E5FA3EA59C77C8 (PureAttribute_t88CF1F9EDCAE55E72BA1C026F4AB10943B451057 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorDirectiveAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorDirectiveAttribute__ctor_mA28CF8516DE82819A685A5219FD4BFC6B68EA5C4 (RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E * __this, String_t* ___directive0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___directive0;
		RazorDirectiveAttribute_set_Directive_m13E8B8A3D7AA46CFC68ADD972BECE00EE489A1D2_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.RazorDirectiveAttribute::get_Directive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RazorDirectiveAttribute_get_Directive_mA77DF0374D01746A4D860EE4007565AD038C8143 (RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDirectiveU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.RazorDirectiveAttribute::set_Directive(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorDirectiveAttribute_set_Directive_m13E8B8A3D7AA46CFC68ADD972BECE00EE489A1D2 (RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDirectiveU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorHelperCommonAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorHelperCommonAttribute__ctor_mA324FDB54824F3BD2D42C585F4382A8F02F8680A (RazorHelperCommonAttribute_t1E02250F84DB9E222211DCB0B990F23243E43C70 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorImportNamespaceAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorImportNamespaceAttribute__ctor_m88390011700E4FA1A761B6CA2E98C7D3BB22F34D (RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		RazorImportNamespaceAttribute_set_Name_m140C4B453B55F6C62F62B7B38A56B2FEE8D0C090_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.RazorImportNamespaceAttribute::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RazorImportNamespaceAttribute_get_Name_m786E27B225452D992829499D9D512D1C3396701C (RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.RazorImportNamespaceAttribute::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorImportNamespaceAttribute_set_Name_m140C4B453B55F6C62F62B7B38A56B2FEE8D0C090 (RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorInjectionAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorInjectionAttribute__ctor_m90A7D9C396BFB384D3B30B2D65E5F81D0DB53ECD (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, String_t* ___type0, String_t* ___fieldName1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___type0;
		RazorInjectionAttribute_set_Type_mC7841CBEBEF456EE2AD385F6C4E4F86154A4BC84_inline(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___fieldName1;
		RazorInjectionAttribute_set_FieldName_m57A348CB52BE094401092C2631B2500E8E840BFD_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.RazorInjectionAttribute::get_Type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RazorInjectionAttribute_get_Type_mFCF5011ED4738143B9262DD5DAA3730AB3D80479 (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.RazorInjectionAttribute::set_Type(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorInjectionAttribute_set_Type_mC7841CBEBEF456EE2AD385F6C4E4F86154A4BC84 (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String JetBrains.Annotations.RazorInjectionAttribute::get_FieldName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RazorInjectionAttribute_get_FieldName_m278F9CED40DBE4C653C100D7B67A81B6D9138DE2 (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFieldNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.RazorInjectionAttribute::set_FieldName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorInjectionAttribute_set_FieldName_m57A348CB52BE094401092C2631B2500E8E840BFD (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFieldNameU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorLayoutAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorLayoutAttribute__ctor_m51575EA28B21943AB294FAFD2D6B5A96809CE63B (RazorLayoutAttribute_t279E1BF2A55E4D945C9F48FDB3C5F0DDE8BC4192 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute__ctor_mBAC60C3D4748379F9C5AD2991C423BED385F8CF2 (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___baseType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___baseType0;
		RazorPageBaseTypeAttribute_set_BaseType_m68D508FAD7FB7558AD5B63D899D61AA9339113D7_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute__ctor_m544D67B7090DF8044982853BED74D31DE07D608C (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___baseType0, String_t* ___pageName1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___baseType0;
		RazorPageBaseTypeAttribute_set_BaseType_m68D508FAD7FB7558AD5B63D899D61AA9339113D7_inline(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___pageName1;
		RazorPageBaseTypeAttribute_set_PageName_m919DD65C86D7D50B0D64004F4A11B5D08066DC25_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.RazorPageBaseTypeAttribute::get_BaseType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RazorPageBaseTypeAttribute_get_BaseType_m333BD2487E7BB969B2D5FCAB026E0487950FEFD6 (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CBaseTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::set_BaseType(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute_set_BaseType_m68D508FAD7FB7558AD5B63D899D61AA9339113D7 (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBaseTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String JetBrains.Annotations.RazorPageBaseTypeAttribute::get_PageName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RazorPageBaseTypeAttribute_get_PageName_m1E0BC993F961B70AABED35A9C3D9FCF96A29598A (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CPageNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.RazorPageBaseTypeAttribute::set_PageName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute_set_PageName_m919DD65C86D7D50B0D64004F4A11B5D08066DC25 (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPageNameU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorSectionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorSectionAttribute__ctor_m9A7DFE508BD5E260951EFDD17E5CAB93C54A759F (RazorSectionAttribute_t5AA6D2F94C7018B03FF362B7E90EE774D1E6F51F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorWriteLiteralMethodAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorWriteLiteralMethodAttribute__ctor_mDB6F23E5D037253D1563E88EAE3DDB008575EEA1 (RazorWriteLiteralMethodAttribute_t64AA2D6F82DA8131822E06EA6F27BD92907728C2 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorWriteMethodAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorWriteMethodAttribute__ctor_m02A0D0B08AA62C9A40187C135A6F1746C0464BAC (RazorWriteMethodAttribute_t2BF3FF98004544723D2533D75C2CE5797D6483A0 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RazorWriteMethodParameterAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RazorWriteMethodParameterAttribute__ctor_m86B45C312985B19500293D1DADEAA112CC573D69 (RazorWriteMethodParameterAttribute_t386D71275AEE065917E2324F16C057D3A729BE02 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.RegexPatternAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RegexPatternAttribute__ctor_mF1530725493264C8C2E70C4048CF05B1F90FD38B (RegexPatternAttribute_t0C4A75B79AD6457C708345386202B8F05C9B2C1B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.SourceTemplateAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SourceTemplateAttribute__ctor_mF671F6BE3261CDC2EBE1AC338149B62C6425045D (SourceTemplateAttribute_tFA362C973C19E229A137F5B77CC31DDBD1C53607 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.StringFormatMethodAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringFormatMethodAttribute__ctor_mA0F0DD9D94622A65E2B454CA70021BC3EBA105FA (StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14 * __this, String_t* ___formatParameterName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___formatParameterName0;
		StringFormatMethodAttribute_set_FormatParameterName_m7F599817D270C2AE791FADD1F3F467AB800EC5AC_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.StringFormatMethodAttribute::get_FormatParameterName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StringFormatMethodAttribute_get_FormatParameterName_m78E78C4A4FBAC9F3EFC3A67BB717400284268A4B (StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatParameterNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.StringFormatMethodAttribute::set_FormatParameterName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringFormatMethodAttribute_set_FormatParameterName_m7F599817D270C2AE791FADD1F3F467AB800EC5AC (StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatParameterNameU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.TerminatesProgramAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TerminatesProgramAttribute__ctor_m3879D60EAD0C1E01E14AF7A6B28F8F489CBF01C7 (TerminatesProgramAttribute_tA0AECA56847F077E969CA259515E90EE724AD488 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute__ctor_m9967794C58FA80A15A2EB59A219C9211DB2161E8 (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, const RuntimeMethod* method)
{
	{
		UsedImplicitlyAttribute__ctor_mF053D1548C9F0310AAE782C1AE30BC81A417BE5E(__this, 7, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute__ctor_mFD9C6C2E2B1CF69D3CAAA0C308226F009E3D1852 (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___useKindFlags0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___useKindFlags0;
		UsedImplicitlyAttribute__ctor_mF053D1548C9F0310AAE782C1AE30BC81A417BE5E(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor(JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute__ctor_m5780F7D5CA60ADEE1A8C8B6B66319D8C66EF0667 (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___targetFlags0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___targetFlags0;
		UsedImplicitlyAttribute__ctor_mF053D1548C9F0310AAE782C1AE30BC81A417BE5E(__this, 7, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags,JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute__ctor_mF053D1548C9F0310AAE782C1AE30BC81A417BE5E (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___useKindFlags0, int32_t ___targetFlags1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___useKindFlags0;
		UsedImplicitlyAttribute_set_UseKindFlags_m5D8FE4E5A37C11453F7C376FFC0ED0E84B9D0ADB_inline(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___targetFlags1;
		UsedImplicitlyAttribute_set_TargetFlags_mE8D6A7BABDB5B3846B11D2E3BCD0C2AEE644132F_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.UsedImplicitlyAttribute::get_UseKindFlags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UsedImplicitlyAttribute_get_UseKindFlags_m78456325D2370062434A60F23EEE53CA2061D1E0 (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CUseKindFlagsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::set_UseKindFlags(JetBrains.Annotations.ImplicitUseKindFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute_set_UseKindFlags_m5D8FE4E5A37C11453F7C376FFC0ED0E84B9D0ADB (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CUseKindFlagsU3Ek__BackingField_0(L_0);
		return;
	}
}
// JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.UsedImplicitlyAttribute::get_TargetFlags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UsedImplicitlyAttribute_get_TargetFlags_mD2C04C854F767492D225660763574027B3C9D98C (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTargetFlagsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.UsedImplicitlyAttribute::set_TargetFlags(JetBrains.Annotations.ImplicitUseTargetFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute_set_TargetFlags_mE8D6A7BABDB5B3846B11D2E3BCD0C2AEE644132F (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTargetFlagsU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.ValueProviderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueProviderAttribute__ctor_mEC341339F02E1ECA9E12E6EC363DA583153CD90E (ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		ValueProviderAttribute_set_Name_m189466822274D76436002174983AC4BDF229B82C_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JetBrains.Annotations.ValueProviderAttribute::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueProviderAttribute_get_Name_m85A47FD4BDE7531EEAD0021846301D1ED66F8260 (ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void JetBrains.Annotations.ValueProviderAttribute::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueProviderAttribute_set_Name_m189466822274D76436002174983AC4BDF229B82C (ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.XamlItemBindingOfItemsControlAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XamlItemBindingOfItemsControlAttribute__ctor_m3C12FC84D10A36593227C7D61A4AB5517A99AC22 (XamlItemBindingOfItemsControlAttribute_t4D95AE96BC401E816B6D94988CFEA63015FE330B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JetBrains.Annotations.XamlItemsControlAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XamlItemsControlAttribute__ctor_mA3FBD1004D93BEB2A4CE7CDD7B1B6B591A2B86B7 (XamlItemsControlAttribute_tA6BF5767DF1774A8781DB8CB6DAED08C643DE44E * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectAttribute__ctor_m98AD3B24006CAF44B6D24E3FA1A6841F9BD45342 (InjectAttribute_t8D23ED3F95574DFF6119775BAAC731099F592B2E * __this, const RuntimeMethod* method)
{
	{
		InjectAttributeBase__ctor_m2C0314D84470B5111B9276530B6759706691E2BA(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Zenject.InjectAttributeBase::get_Optional()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InjectAttributeBase_get_Optional_m117D4303470CA796A9CBECCBCCAA1B075E534A0E (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3COptionalU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Zenject.InjectAttributeBase::set_Optional(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Optional_m93C08A9815412E918B346A7351BF871DB5ABD54D (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3COptionalU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Object Zenject.InjectAttributeBase::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * InjectAttributeBase_get_Id_m62DA30B28C66A2B042B321BB38E01F619A47E8E9 (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Zenject.InjectAttributeBase::set_Id(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Id_m83E44EE92C2434514B7E1BBC7F40D7AFE0849EDB (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// Zenject.InjectSources Zenject.InjectAttributeBase::get_Source()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InjectAttributeBase_get_Source_m9C4136AB1B6AC3A8CD74B8FA7241B18CFAE6ADD6 (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CSourceU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Zenject.InjectAttributeBase::set_Source(Zenject.InjectSources)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Source_mE83EFBACAA0B787C77EA0B2C73D2A9CFA654C819 (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CSourceU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Zenject.InjectAttributeBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectAttributeBase__ctor_m2C0314D84470B5111B9276530B6759706691E2BA (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, const RuntimeMethod* method)
{
	{
		PreserveAttribute__ctor_mCDD191D80D202DAD6E07BFA266003EFF5996E4D9(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectLocalAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectLocalAttribute__ctor_mC795F3A21C4A90B9CA501FD84ECAF6939157FF04 (InjectLocalAttribute_tE96A924A3CDE8393549396FC2BBA80BBBDCB0C2D * __this, const RuntimeMethod* method)
{
	{
		InjectAttributeBase__ctor_m2C0314D84470B5111B9276530B6759706691E2BA(__this, /*hidden argument*/NULL);
		InjectAttributeBase_set_Source_mE83EFBACAA0B787C77EA0B2C73D2A9CFA654C819_inline(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectOptionalAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectOptionalAttribute__ctor_mF21688E8D80BDD59EB82431F8FBD12D7891673A1 (InjectOptionalAttribute_t958E1718D848609A8BD2E95719B0BD23A88A51E4 * __this, const RuntimeMethod* method)
{
	{
		InjectAttributeBase__ctor_m2C0314D84470B5111B9276530B6759706691E2BA(__this, /*hidden argument*/NULL);
		InjectAttributeBase_set_Optional_m93C08A9815412E918B346A7351BF871DB5ABD54D_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectTypeInfo::.ctor(System.Type,Zenject.InjectTypeInfo_InjectConstructorInfo,Zenject.InjectTypeInfo_InjectMethodInfo[],Zenject.InjectTypeInfo_InjectMemberInfo[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectTypeInfo__ctor_m4F1AABE23B2C121781BED8AA5118C426626689D2 (InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * __this, Type_t * ___type0, InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 * ___injectConstructor1, InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* ___injectMethods2, InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* ___injectMembers3, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___type0;
		__this->set_Type_0(L_0);
		InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* L_1 = ___injectMethods2;
		__this->set_InjectMethods_1(L_1);
		InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* L_2 = ___injectMembers3;
		__this->set_InjectMembers_2(L_2);
		InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 * L_3 = ___injectConstructor1;
		__this->set_InjectConstructor_3(L_3);
		return;
	}
}
// Zenject.InjectTypeInfo Zenject.InjectTypeInfo::get_BaseTypeInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * InjectTypeInfo_get_BaseTypeInfo_m3651B4151F25B3B32E0752590E3B223E5B809E44 (InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * __this, const RuntimeMethod* method)
{
	{
		InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * L_0 = __this->get_U3CBaseTypeInfoU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Zenject.InjectTypeInfo::set_BaseTypeInfo(Zenject.InjectTypeInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectTypeInfo_set_BaseTypeInfo_m421413DB209610F6DB8DAB4192B7FD2E8281EC85 (InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * __this, InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * ___value0, const RuntimeMethod* method)
{
	{
		InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * L_0 = ___value0;
		__this->set_U3CBaseTypeInfoU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.InjectTypeInfo::get_AllInjectables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InjectTypeInfo_get_AllInjectables_m4459958ADFD4FCCD62A54B3C571EB2A2BBFE2417 (InjectTypeInfo_t6F2BB06A9620A6449E18C3786120A4560A914FE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InjectTypeInfo_get_AllInjectables_m4459958ADFD4FCCD62A54B3C571EB2A2BBFE2417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * G_B2_0 = NULL;
	InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* G_B2_1 = NULL;
	InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* G_B2_2 = NULL;
	Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * G_B1_0 = NULL;
	InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* G_B1_1 = NULL;
	InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* G_B1_2 = NULL;
	Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * G_B4_0 = NULL;
	InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* G_B4_1 = NULL;
	RuntimeObject* G_B4_2 = NULL;
	Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * G_B3_0 = NULL;
	InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* G_B3_1 = NULL;
	RuntimeObject* G_B3_2 = NULL;
	{
		InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 * L_0 = __this->get_InjectConstructor_3();
		NullCheck(L_0);
		InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* L_1 = L_0->get_Parameters_1();
		InjectMemberInfoU5BU5D_t36A01B349FC94A413193C52B62F36155C53D235E* L_2 = __this->get_InjectMembers_2();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var);
		Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * L_3 = ((U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var))->get_U3CU3E9__10_0_1();
		Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * L_4 = L_3;
		G_B1_0 = L_4;
		G_B1_1 = L_2;
		G_B1_2 = L_1;
		if (L_4)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_2;
			G_B2_2 = L_1;
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var);
		U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * L_5 = ((U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * L_6 = (Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 *)il2cpp_codegen_object_new(Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92_il2cpp_TypeInfo_var);
		Func_2__ctor_m7D6423527B62548E6DA02AF095EEBFE2956753E0(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_0_m9FBC75220E285F59B6EE734E6014BBA81A75B34E_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7D6423527B62548E6DA02AF095EEBFE2956753E0_RuntimeMethod_var);
		Func_2_tBDB9632C8928A1B19E0055ED9A3AC0A900425A92 * L_7 = L_6;
		((U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var))->set_U3CU3E9__10_0_1(L_7);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0030:
	{
		RuntimeObject* L_8 = Enumerable_Select_TisInjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mC65110D147A9A63DFF3F5EEEE3BCA524CFB608C1((RuntimeObject*)(RuntimeObject*)G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Select_TisInjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mC65110D147A9A63DFF3F5EEEE3BCA524CFB608C1_RuntimeMethod_var);
		RuntimeObject* L_9 = Enumerable_Concat_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mA9EDADA7EF8F1F3DFDA1FB3EDD13280EEF0D4BEC((RuntimeObject*)(RuntimeObject*)G_B2_2, L_8, /*hidden argument*/Enumerable_Concat_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mA9EDADA7EF8F1F3DFDA1FB3EDD13280EEF0D4BEC_RuntimeMethod_var);
		InjectMethodInfoU5BU5D_t81FF45FFEE36F50B04025E73C86ABA4346994912* L_10 = __this->get_InjectMethods_1();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var);
		Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * L_11 = ((U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var))->get_U3CU3E9__10_1_2();
		Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * L_12 = L_11;
		G_B3_0 = L_12;
		G_B3_1 = L_10;
		G_B3_2 = L_9;
		if (L_12)
		{
			G_B4_0 = L_12;
			G_B4_1 = L_10;
			G_B4_2 = L_9;
			goto IL_005f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var);
		U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * L_13 = ((U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * L_14 = (Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 *)il2cpp_codegen_object_new(Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0_il2cpp_TypeInfo_var);
		Func_2__ctor_m5810862148CA2671FE549A0D28FDE3D9F8ABDCF0(L_14, L_13, (intptr_t)((intptr_t)U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_1_m73B39E0ED66D688733647E0A291A54B19FA2DAB6_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m5810862148CA2671FE549A0D28FDE3D9F8ABDCF0_RuntimeMethod_var);
		Func_2_t51A4456D6338DDE5F45EE6F27B017E63F05E6FB0 * L_15 = L_14;
		((U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var))->set_U3CU3E9__10_1_2(L_15);
		G_B4_0 = L_15;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_005f:
	{
		RuntimeObject* L_16 = Enumerable_SelectMany_TisInjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_m425531658AB47EF78A6F559A7AD4F91E8FCF5F23((RuntimeObject*)(RuntimeObject*)G_B4_1, G_B4_0, /*hidden argument*/Enumerable_SelectMany_TisInjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_m425531658AB47EF78A6F559A7AD4F91E8FCF5F23_RuntimeMethod_var);
		RuntimeObject* L_17 = Enumerable_Concat_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mA9EDADA7EF8F1F3DFDA1FB3EDD13280EEF0D4BEC(G_B4_2, L_16, /*hidden argument*/Enumerable_Concat_TisInjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC_mA9EDADA7EF8F1F3DFDA1FB3EDD13280EEF0D4BEC_RuntimeMethod_var);
		return L_17;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectTypeInfo_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mE2354A854E5D0CFE8FF62DCF6A806ECF6D80C289 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mE2354A854E5D0CFE8FF62DCF6A806ECF6D80C289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * L_0 = (U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 *)il2cpp_codegen_object_new(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m8BA7B247476AA2C17B97E0C9A67F8472E8AE5112(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Zenject.InjectTypeInfo_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8BA7B247476AA2C17B97E0C9A67F8472E8AE5112 (U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.InjectableInfo Zenject.InjectTypeInfo_<>c::<get_AllInjectables>b__10_0(Zenject.InjectTypeInfo_InjectMemberInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_0_m9FBC75220E285F59B6EE734E6014BBA81A75B34E (U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * __this, InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * ___x0, const RuntimeMethod* method)
{
	{
		InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * L_0 = ___x0;
		NullCheck(L_0);
		InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * L_1 = L_0->get_Info_1();
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.InjectTypeInfo_<>c::<get_AllInjectables>b__10_1(Zenject.InjectTypeInfo_InjectMethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec_U3Cget_AllInjectablesU3Eb__10_1_m73B39E0ED66D688733647E0A291A54B19FA2DAB6 (U3CU3Ec_tB06E59D53C83942EE14ADEFFBC14F035EE56D233 * __this, InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * ___x0, const RuntimeMethod* method)
{
	{
		InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * L_0 = ___x0;
		NullCheck(L_0);
		InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* L_1 = L_0->get_Parameters_2();
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectTypeInfo_InjectConstructorInfo::.ctor(Zenject.ZenFactoryMethod,Zenject.InjectableInfo[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectConstructorInfo__ctor_m3A5ABBF6F67CD8B233A41C024CB45588E2159135 (InjectConstructorInfo_t302721BF0A184AECF98A1D458FAC9483542A3FA5 * __this, ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * ___factory0, InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* ___parameters1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* L_0 = ___parameters1;
		__this->set_Parameters_1(L_0);
		ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * L_1 = ___factory0;
		__this->set_Factory_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectTypeInfo_InjectMemberInfo::.ctor(Zenject.ZenMemberSetterMethod,Zenject.InjectableInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectMemberInfo__ctor_m6332CC8B6797368DFF7B36287E75A3B7F4E5123D (InjectMemberInfo_t4D2E1554B697C2737C86A41A97185F852D73EF82 * __this, ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * ___setter0, InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * ___info1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * L_0 = ___setter0;
		__this->set_Setter_0(L_0);
		InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * L_1 = ___info1;
		__this->set_Info_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectTypeInfo_InjectMethodInfo::.ctor(Zenject.ZenInjectMethod,Zenject.InjectableInfo[],System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectMethodInfo__ctor_m35025625F36C9D1178F6A85CCA817793161418A9 (InjectMethodInfo_tFE22F47B51FE8FA06F68071DEBBBD6C45AB70E8A * __this, ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * ___action0, InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* ___parameters1, String_t* ___name2, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		InjectableInfoU5BU5D_tE12DD98052952D209A90EC11DD74078966DCA571* L_0 = ___parameters1;
		__this->set_Parameters_2(L_0);
		ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * L_1 = ___action0;
		__this->set_Action_1(L_1);
		String_t* L_2 = ___name2;
		__this->set_Name_0(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.InjectableInfo::.ctor(System.Boolean,System.Object,System.String,System.Type,System.Object,Zenject.InjectSources)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectableInfo__ctor_m658D7FA40B2FD6046E44B67EF6B59BBC2F63F058 (InjectableInfo_tC7597AFE59062AAF18E986713E91B61176E7EAAC * __this, bool ___optional0, RuntimeObject * ___identifier1, String_t* ___memberName2, Type_t * ___memberType3, RuntimeObject * ___defaultValue4, int32_t ___sourceType5, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		bool L_0 = ___optional0;
		__this->set_Optional_0(L_0);
		Type_t * L_1 = ___memberType3;
		__this->set_MemberType_4(L_1);
		String_t* L_2 = ___memberName2;
		__this->set_MemberName_3(L_2);
		RuntimeObject * L_3 = ___identifier1;
		__this->set_Identifier_1(L_3);
		RuntimeObject * L_4 = ___defaultValue4;
		__this->set_DefaultValue_5(L_4);
		int32_t L_5 = ___sourceType5;
		__this->set_SourceType_2(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.Internal.PreserveAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreserveAttribute__ctor_mCDD191D80D202DAD6E07BFA266003EFF5996E4D9 (PreserveAttribute_t2D26C6DD68541293CA3738A57BC67377AAB1EDCA * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.NoReflectionBakingAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NoReflectionBakingAttribute__ctor_mFF965F6241370B8C49707C74B8B0292F9954C847 (NoReflectionBakingAttribute_tEF78DB88D1E3F4057AB9F7AC1C62E232B9A9B2A4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.ZenFactoryMethod::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenFactoryMethod__ctor_m461B117D19C422609F609B2454BF059DE6696E46 (ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object Zenject.ZenFactoryMethod::Invoke(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZenFactoryMethod_Invoke_m141F13459ED8ED452242759B11B781225A90FEAB (ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * __this, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args0, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___args0, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___args0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, ___args0);
					else
						result = GenericVirtFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, ___args0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___args0);
					else
						result = VirtFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___args0);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___args0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___args0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(targetMethod, targetThis, ___args0);
					else
						result = GenericVirtFuncInvoker1< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(targetMethod, targetThis, ___args0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___args0);
					else
						result = VirtFuncInvoker1< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___args0);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (void*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___args0, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult Zenject.ZenFactoryMethod::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZenFactoryMethod_BeginInvoke_m0219A0A66C3D4DC63EACAF92304B2A01B4F489E8 (ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * __this, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___args0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Object Zenject.ZenFactoryMethod::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZenFactoryMethod_EndInvoke_m2676D16EE20429C9D848CA8672ECEA359824926F (ZenFactoryMethod_t0304F5FA946CC03D63271B7DDBED563AFD913525 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.ZenInjectMethod::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenInjectMethod__ctor_mAE02246B1ECE29A84AAB66E1730F2F2206BC2F3E (ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Zenject.ZenInjectMethod::Invoke(System.Object,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenInjectMethod_Invoke_m9F84D1E5F9B6E000D5B103FB8ECA26AB8814E408 (ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * __this, RuntimeObject * ___obj0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___obj0, ___args1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___obj0, ___args1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(targetMethod, ___obj0, ___args1);
					else
						GenericVirtActionInvoker1< ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(targetMethod, ___obj0, ___args1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___obj0, ___args1);
					else
						VirtActionInvoker1< ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___obj0, ___args1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___obj0, ___args1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___obj0, ___args1, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(targetMethod, targetThis, ___obj0, ___args1);
					else
						GenericVirtActionInvoker2< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(targetMethod, targetThis, ___obj0, ___args1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___obj0, ___args1);
					else
						VirtActionInvoker2< RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___obj0, ___args1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___obj0, ___args1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Zenject.ZenInjectMethod::BeginInvoke(System.Object,System.Object[],System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZenInjectMethod_BeginInvoke_m53CFF280203B2FDCC1BBADD133C0A0FD304204CA (ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * __this, RuntimeObject * ___obj0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___args1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void Zenject.ZenInjectMethod::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenInjectMethod_EndInvoke_mA44C12A7FE1416726ABC14A2C51D1B6156C626DA (ZenInjectMethod_t9CB05BD673E09503BF81C42C6CEC6EABC212E161 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.ZenMemberSetterMethod::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenMemberSetterMethod__ctor_m2F7603EFCA3FFA557C57F782AB6515377C581338 (ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Zenject.ZenMemberSetterMethod::Invoke(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenMemberSetterMethod_Invoke_m478200CD23399D996E8B8CCF38638F03A369DB8C (ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * __this, RuntimeObject * ___obj0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___obj0, ___value1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___obj0, ___value1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___obj0, ___value1);
					else
						GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___obj0, ___value1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___obj0, ___value1);
					else
						VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___obj0, ___value1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___obj0, ___value1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___obj0, ___value1, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___obj0, ___value1);
					else
						GenericVirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___obj0, ___value1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___obj0, ___value1);
					else
						VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___obj0, ___value1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___obj0, ___value1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Zenject.ZenMemberSetterMethod::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZenMemberSetterMethod_BeginInvoke_mD54AEF0DEB5C012710B5540975C6C7FDA9B90B1E (ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * __this, RuntimeObject * ___obj0, RuntimeObject * ___value1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___value1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void Zenject.ZenMemberSetterMethod::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenMemberSetterMethod_EndInvoke_mB1D14DDDE7B307CA49E7E416290ECC0FD3488280 (ZenMemberSetterMethod_t2389E586D9EEB6124692653E0C876D90BB34B759 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.ZenjectAllowDuringValidationAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZenjectAllowDuringValidationAttribute__ctor_m6FB09FC9DDD462E6D93EC638B368CFD12F6565E8 (ZenjectAllowDuringValidationAttribute_t85FD03316B9D1E412DBCA415A1616B005D8AF540 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspChildControlTypeAttribute_set_TagName_m6A9A3D854C58738EBD8A0D379FB6FD5AA2D69BAD_inline (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTagNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspChildControlTypeAttribute_set_ControlType_mF8FD2F6F1BD65DB6966EC199D6DCBF9930D71EA0_inline (AspChildControlTypeAttribute_tCFF37B3DC767A366FABCFE1AD7C954585AB5F655 * __this, Type_t * ___value0, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_U3CControlTypeU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcActionAttribute_set_AnonymousProperty_m83EA3BE21BBB1F4735FBE043543BD4984590BBDA_inline (AspMvcActionAttribute_t239D084D487B5A6824FDBD01AA8532CAEE60BE65 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnonymousPropertyU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaAttribute_set_AnonymousProperty_m6217B255496EF23DBEFF147D69F4CC75028DD372_inline (AspMvcAreaAttribute_t49DE5AD9046ACF8DA9E78E3C7065D474BD611143 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnonymousPropertyU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaMasterLocationFormatAttribute_set_Format_m23924305517051ABCC0C5A64FBC969E57DD64CC1_inline (AspMvcAreaMasterLocationFormatAttribute_t74468A30FD5AA65F6145E636D59210828C81E460 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaPartialViewLocationFormatAttribute_set_Format_mBA39E10F8E9F267F28B653341FECA4E8D75E29C0_inline (AspMvcAreaPartialViewLocationFormatAttribute_t16A91787AFCBF33563E4A76F0553FBE762F572DE * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcAreaViewLocationFormatAttribute_set_Format_mFA517B963A59B6F24995B1051BBD644B19A6E1B6_inline (AspMvcAreaViewLocationFormatAttribute_t8B3747322E0AC294038D36ADC56E273B8C443101 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcControllerAttribute_set_AnonymousProperty_mDB5062FA1F14FB6FBA2034FFD85CDA4EDFAF5083_inline (AspMvcControllerAttribute_tFA9086FB514F3FD15BD500AF1644AE33B9E34E2F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnonymousPropertyU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcMasterLocationFormatAttribute_set_Format_m852EA80CBBEED7BB8853D695FE37C1C9E5A707C3_inline (AspMvcMasterLocationFormatAttribute_t7D7FAD6131A9E8687468BAB9B5822DB428520C03 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcPartialViewLocationFormatAttribute_set_Format_m1D39A9129B1D56D968870065FDD2E65244FFB3F9_inline (AspMvcPartialViewLocationFormatAttribute_t2E8CB61F8752F443060705A966DF255BBE70014E * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspMvcViewLocationFormatAttribute_set_Format_m9AB87404E45FA9C2AAFD4057124EE2181ACCE963_inline (AspMvcViewLocationFormatAttribute_t604AB257FFC9A3F2E7963F7954FF49B8BD228CE7 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspRequiredAttributeAttribute_set_Attribute_m4C27C1CF35F50F5A398DE77CBD9E3BA4E49FFE83_inline (AspRequiredAttributeAttribute_tF0547F4E1B567405F9F138F676033C3875B4ACE7 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAttributeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AspTypePropertyAttribute_set_CreateConstructorReferences_m953788280CA7DCB18C58C6CDB8DB221C857F4037_inline (AspTypePropertyAttribute_t0031A67C6A14FB2F227F8544EAF4E98A42E0E0FA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CCreateConstructorReferencesU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AssertionConditionAttribute_set_ConditionType_m2E1C8FDBA6328631D71E7EDF3A655F521B606727_inline (AssertionConditionAttribute_tE140887CE85AD55C0C7E0F732BC7ADF86A284DA1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CConditionTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void BaseTypeRequiredAttribute_set_BaseType_m0E7A68B87BCBF97208AB3B7D975773835CCB6B8C_inline (BaseTypeRequiredAttribute_tA0C7BA0365E8612C1BFBF9A08A4E1C6FF5491FF6 * __this, Type_t * ___value0, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_U3CBaseTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void CollectionAccessAttribute_set_CollectionAccessType_mBE8F74690533AF7885DF9E691A758BB6CDA15529_inline (CollectionAccessAttribute_t59B7A16680C4CB13B2E362936B611DFF7078CD5E * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CCollectionAccessTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ContractAnnotationAttribute_set_Contract_m46117527529866206D93D24AC03B8A5B97054D34_inline (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CContractU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ContractAnnotationAttribute_set_ForceFullStates_m3273626E1D840A51CE3373012B591A0A4652EBC0_inline (ContractAnnotationAttribute_t585640147906C575C5AB50F443EE45DE84B79F9B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CForceFullStatesU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HtmlAttributeValueAttribute_set_Name_m40857D8975B094CCA401959469C902D8E4EF2F13_inline (HtmlAttributeValueAttribute_t95F30E004CA5D7DBDF9C3E9C7CCAC3B809C7FF78 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HtmlElementAttributesAttribute_set_Name_mC66C3677ECE785266657B3BCCE620ED99AF9D4AE_inline (HtmlElementAttributesAttribute_t3D27F6AB0159CC1C9B6AF8D1DDB9AB06BC500BD8 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void LocalizationRequiredAttribute_set_Required_m28CDD5F78263BE6179EE3E29E80EBCF93FA51021_inline (LocalizationRequiredAttribute_tD434BDA928DE1D56667D5FE4AD77D2A47C5D4416 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CRequiredU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute_set_UseKindFlags_mC020328ACCA94C5816E6F418E7CC6EB9E1E2C9F0_inline (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CUseKindFlagsU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute_set_TargetFlags_mCCCE7B4BCF2EA74538E4C90496208ACAD32B7A8F_inline (MeansImplicitUseAttribute_tE0139192B86E11214717742CE0ACFFFBEC2F773F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTargetFlagsU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MustUseReturnValueAttribute_set_Justification_m1DAE6CCA661B24A56C45C0ACF84FD195C7D44A04_inline (MustUseReturnValueAttribute_tE3640221D1B1CB96ACB65549B11E03574C0EB5F2 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CJustificationU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void NotifyPropertyChangedInvocatorAttribute_set_ParameterName_m4042408E5BD997B59E313B781D8FD778DE132D5D_inline (NotifyPropertyChangedInvocatorAttribute_t0F7162742EF22E0197D510059A40956F5ED05B4C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CParameterNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PathReferenceAttribute_set_BasePath_m6039B47746370E59513C5C7161053AE20F741255_inline (PathReferenceAttribute_tBE72CF39D938ED1691426695A75276D2A5A239A7 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBasePathU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PublicAPIAttribute_set_Comment_m803065FC1B1C3AE5533C775B09668E5F210ABAC2_inline (PublicAPIAttribute_tC54806E3FBD2472A4D2181AEC0CB70B5F39404C8 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCommentU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorDirectiveAttribute_set_Directive_m13E8B8A3D7AA46CFC68ADD972BECE00EE489A1D2_inline (RazorDirectiveAttribute_t00141EE15C879809115B10BF61BE619D0B4A392E * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDirectiveU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorImportNamespaceAttribute_set_Name_m140C4B453B55F6C62F62B7B38A56B2FEE8D0C090_inline (RazorImportNamespaceAttribute_t944F5D591F7EAE8C8B8657E45E9B30119537EA19 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorInjectionAttribute_set_Type_mC7841CBEBEF456EE2AD385F6C4E4F86154A4BC84_inline (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorInjectionAttribute_set_FieldName_m57A348CB52BE094401092C2631B2500E8E840BFD_inline (RazorInjectionAttribute_t0D7C9591A6D0D5C8A46505242F06C83FFAAB03EF * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFieldNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute_set_BaseType_m68D508FAD7FB7558AD5B63D899D61AA9339113D7_inline (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBaseTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void RazorPageBaseTypeAttribute_set_PageName_m919DD65C86D7D50B0D64004F4A11B5D08066DC25_inline (RazorPageBaseTypeAttribute_tC28B072F89A5205C9673265DF63EBA79702F200F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPageNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StringFormatMethodAttribute_set_FormatParameterName_m7F599817D270C2AE791FADD1F3F467AB800EC5AC_inline (StringFormatMethodAttribute_t310C988E45C78A6079CD3B3E93ACF513CEBBDF14 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatParameterNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute_set_UseKindFlags_m5D8FE4E5A37C11453F7C376FFC0ED0E84B9D0ADB_inline (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CUseKindFlagsU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void UsedImplicitlyAttribute_set_TargetFlags_mE8D6A7BABDB5B3846B11D2E3BCD0C2AEE644132F_inline (UsedImplicitlyAttribute_t80988676F1B0AC23FBF49A03FB235264C34083B7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTargetFlagsU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ValueProviderAttribute_set_Name_m189466822274D76436002174983AC4BDF229B82C_inline (ValueProviderAttribute_t775161064491443E5AB17A5CA6F1AB5C707C9192 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Source_mE83EFBACAA0B787C77EA0B2C73D2A9CFA654C819_inline (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CSourceU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Optional_m93C08A9815412E918B346A7351BF871DB5ABD54D_inline (InjectAttributeBase_t4CADCC973DDF4C378C5D626E751C2739560AB5EC * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3COptionalU3Ek__BackingField_0(L_0);
		return;
	}
}
