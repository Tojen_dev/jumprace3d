﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void CameraOverlay::Start()
extern void CameraOverlay_Start_mC9B778CBC5AAB94083AC40A1BABC95FA6AB17127 ();
// 0x00000002 System.Void CameraOverlay::Update()
extern void CameraOverlay_Update_m973486C41CC119B454D3EDA386FB5551C4C6264F ();
// 0x00000003 System.Void CameraOverlay::.ctor()
extern void CameraOverlay__ctor_mACDB65E8D2A652A0BAD046531C1A376D9E593EB4 ();
// 0x00000004 System.Void CubeMover::Start()
extern void CubeMover_Start_m570A20598D617A3BD6E5D1A330B19A44A8B4D950 ();
// 0x00000005 System.Void CubeMover::Update()
extern void CubeMover_Update_m0AAF543E15B3D4622F114123F21937986B450D3C ();
// 0x00000006 System.Void CubeMover::.ctor()
extern void CubeMover__ctor_m6E0ECE3CBFACAB6CCAA098470E9F52AECE69D980 ();
// 0x00000007 System.Void CustomLightingManager::Start()
extern void CustomLightingManager_Start_m5ABA9134C79F1FDC3C52D374BFFEE9D3B76457A4 ();
// 0x00000008 System.Void CustomLightingManager::findMaterials()
extern void CustomLightingManager_findMaterials_mE2EA9ABC1F6AC40206049F43664833DA510848C5 ();
// 0x00000009 System.Void CustomLightingManager::updateMaterials()
extern void CustomLightingManager_updateMaterials_m4D5E86D2A03751D0B042E9F46B45D8A1A45884E5 ();
// 0x0000000A System.Void CustomLightingManager::Update()
extern void CustomLightingManager_Update_mF9137C459BEF1D9DAF01C6BC010756C3D19695FD ();
// 0x0000000B System.Void CustomLightingManager::.ctor()
extern void CustomLightingManager__ctor_m7FA3AB09E209D25F594CDCB7F209265004BB5D6D ();
// 0x0000000C System.Void DirectionalLightManager::Start()
extern void DirectionalLightManager_Start_mC4A8F1038A28506BA8C2D1572879F33C97BC1096 ();
// 0x0000000D System.Void DirectionalLightManager::findMaterials()
extern void DirectionalLightManager_findMaterials_m35E9BA77334513D3EEFD8E58FFCA878E8BADDD72 ();
// 0x0000000E System.Void DirectionalLightManager::updateMaterials()
extern void DirectionalLightManager_updateMaterials_m083F16E64F71C199D82E6030B1EB218B46C479E6 ();
// 0x0000000F System.Void DirectionalLightManager::Update()
extern void DirectionalLightManager_Update_m6418256EBC4969D21B7DEF052DC83CB0AAA82C09 ();
// 0x00000010 System.Void DirectionalLightManager::.ctor()
extern void DirectionalLightManager__ctor_m95AC2244142A3BDB8FCDD04107066CB46FEE5944 ();
// 0x00000011 System.Void DisableRendererOnMouseDown::OnMouseDown()
extern void DisableRendererOnMouseDown_OnMouseDown_mF628E85DE4E59C6C6CD3B1DAD636866233165318 ();
// 0x00000012 System.Void DisableRendererOnMouseDown::.ctor()
extern void DisableRendererOnMouseDown__ctor_mD1FF4B17391A1B8FA7D24319C050916DE2B779E8 ();
// 0x00000013 System.Void DistanceLight::Start()
extern void DistanceLight_Start_m5F91023F4416AD4123358CEBC33555022F29B688 ();
// 0x00000014 System.Void DistanceLight::distanceLightChanged()
extern void DistanceLight_distanceLightChanged_m460F9CAD9CF0DC430160B2FE2901AD2CF43B561D ();
// 0x00000015 System.Void DistanceLight::checkMaterial(UnityEngine.Material)
extern void DistanceLight_checkMaterial_m188A61EC3ADF3EC7CD6E3B73BC6863026D367A1D ();
// 0x00000016 System.Void DistanceLight::findMaterials()
extern void DistanceLight_findMaterials_m48EAE9347642A5FAE228583B3B4C3EFB19114470 ();
// 0x00000017 System.Void DistanceLight::updateMaterials()
extern void DistanceLight_updateMaterials_m3B9EE839CFC8BF80D322D63EE84C0108B6347CB6 ();
// 0x00000018 System.Void DistanceLight::Update()
extern void DistanceLight_Update_mF0D5B24E08A5DB9780B256F61F4F2FEE66ABAB77 ();
// 0x00000019 System.Void DistanceLight::.ctor()
extern void DistanceLight__ctor_m02861263920E8908B49AB81FF1899EBC77468EA2 ();
// 0x0000001A System.Void DistanceLightMulti::Start()
extern void DistanceLightMulti_Start_m716C5AA49D99434A906735B13B8D36F40D4FE31A ();
// 0x0000001B System.Void DistanceLightMulti::distanceLightChanged()
extern void DistanceLightMulti_distanceLightChanged_m68FDDB44C0E9482A45E5E4FEA169B1267A0C2E6F ();
// 0x0000001C System.Void DistanceLightMulti::checkMaterial(UnityEngine.Material)
extern void DistanceLightMulti_checkMaterial_mAAB724E632DFC86F70764EECD27B56237B2F2900 ();
// 0x0000001D System.Void DistanceLightMulti::findMaterials()
extern void DistanceLightMulti_findMaterials_m426D4C33D58241F3036BC4CFBD5BF0C10A704F88 ();
// 0x0000001E System.Void DistanceLightMulti::updatePositions(UnityEngine.Material)
extern void DistanceLightMulti_updatePositions_m3EF921EE4C6806192748F745B9231B58FFE05444 ();
// 0x0000001F System.Void DistanceLightMulti::updateMaterials()
extern void DistanceLightMulti_updateMaterials_mB7B8CC867A8E02C9ED850D697F58CC52A32B06C8 ();
// 0x00000020 System.Void DistanceLightMulti::Update()
extern void DistanceLightMulti_Update_m55FCF69457310E685C6391BCD5677F981EDDE649 ();
// 0x00000021 System.Void DistanceLightMulti::.ctor()
extern void DistanceLightMulti__ctor_mD5BB0DE9BDE294A1E34974401DEF3C5D4E1FE14D ();
// 0x00000022 System.Void EditModeGridSnap::Update()
extern void EditModeGridSnap_Update_mB8AC12C90FF3381863F4F7B879F1664FEC762852 ();
// 0x00000023 System.Void EditModeGridSnap::.ctor()
extern void EditModeGridSnap__ctor_m4615ABB8702E5B9CE946C80DE4A87437CD2849DD ();
// 0x00000024 System.Void EnableCameraDepthTexture::Start()
extern void EnableCameraDepthTexture_Start_mB06120463A9270FBABAA5A04A5877C9F7D77FBAF ();
// 0x00000025 System.Void EnableCameraDepthTexture::.ctor()
extern void EnableCameraDepthTexture__ctor_m78491BF2B304F7FFDFA3C0FB2607B0EAB14B19BD ();
// 0x00000026 System.Collections.IEnumerator Fps::Start()
extern void Fps_Start_mC7AB37773117CB2EDFC9C80D5A9AE19E454FE5AB ();
// 0x00000027 System.Void Fps::OnGUI()
extern void Fps_OnGUI_m95053FF3C95E986C136935AA7B68C8F06B839A87 ();
// 0x00000028 System.Void Fps::.ctor()
extern void Fps__ctor_m42437A4084EE57B3A5C1AC534E09C0C3586BDE15 ();
// 0x00000029 System.Void FrameRate::Start()
extern void FrameRate_Start_mFE1A1664BAD62484071C323578428CCF0BC10BAB ();
// 0x0000002A System.Void FrameRate::Update()
extern void FrameRate_Update_m4324AAAF07D6B09E953C27E1B412BCD70058F76E ();
// 0x0000002B System.Void FrameRate::.ctor()
extern void FrameRate__ctor_mC75295DC110A4DEEF16C0050CB2F5FB937A42A67 ();
// 0x0000002C System.Void DragDirection::addItem(System.Single)
extern void DragDirection_addItem_mB3B712CE7A7808A7A82D22A18DFB5DC7190F4D26 ();
// 0x0000002D System.Single DragDirection::getDirection()
extern void DragDirection_getDirection_m85E50536F04175E8FF3F4A5988D1CA9A4D5E1D3C ();
// 0x0000002E System.Void DragDirection::clear()
extern void DragDirection_clear_m35FAAD482C8CE13EC88FB2EED2C5A2AB56E2555E ();
// 0x0000002F System.Void DragDirection::.ctor()
extern void DragDirection__ctor_m83CC82FE715AE59D2BB225420E8A36AA97AE0FDF ();
// 0x00000030 System.Void DragDirection::.cctor()
extern void DragDirection__cctor_m4ACDE286136C008D27F6BDBF5EF42435C28C4C8E ();
// 0x00000031 System.Void DragForce::addItem(System.Single)
extern void DragForce_addItem_m6F1A8C2381285976DBA9FBD44AB6A7166A467FC0 ();
// 0x00000032 System.Single DragForce::getForce()
extern void DragForce_getForce_m793157C614B698F7EAA679786D8708FFCF532240 ();
// 0x00000033 System.Void DragForce::clear()
extern void DragForce_clear_m22694A1420B3C6F6EECE81566A37F567CF7252DB ();
// 0x00000034 System.Void DragForce::.ctor()
extern void DragForce__ctor_mA54B2865F0FD575C6B30235FD9A61A949C564885 ();
// 0x00000035 System.Void DragForce::.cctor()
extern void DragForce__cctor_mA616B3EB92ADA1BF7344F58A195F395CE635A98D ();
// 0x00000036 System.Void ObjectRotator::Start()
extern void ObjectRotator_Start_mD8C03B9ACE581E5444A717E90F5E537B60C3668F ();
// 0x00000037 System.Void ObjectRotator::OnMouseDown()
extern void ObjectRotator_OnMouseDown_mCBBD95E7E3FA3FFA9ABA2D8A2B04114B78CFF3E9 ();
// 0x00000038 System.Void ObjectRotator::OnMouseUp()
extern void ObjectRotator_OnMouseUp_m894FEA80B05E05A2B35F2572E8C2FEC1E9C97035 ();
// 0x00000039 System.Void ObjectRotator::OnMouseDrag()
extern void ObjectRotator_OnMouseDrag_m99057105141B9C61CBC9EACC218B991B52DF27D3 ();
// 0x0000003A System.Void ObjectRotator::rotate(System.Single)
extern void ObjectRotator_rotate_mAE96F20FF82DEAA8253B745FA22A3961811C3665 ();
// 0x0000003B System.Void ObjectRotator::setRotation(System.Single)
extern void ObjectRotator_setRotation_m1F7E0B59D156A43ECF846D5E5D5E2578364556B7 ();
// 0x0000003C System.Single ObjectRotator::getRotationForce(System.Single,System.Single,System.Single)
extern void ObjectRotator_getRotationForce_mD08364BF6146802E71B9FA29B6C7E9B2B857FBD7 ();
// 0x0000003D System.Single ObjectRotator::getCurrentMousePosition()
extern void ObjectRotator_getCurrentMousePosition_m6E5EFF7951556BA3BE61EFA272D85AC9C5F8A936 ();
// 0x0000003E System.Boolean ObjectRotator::snapToRotation()
extern void ObjectRotator_snapToRotation_mA618FE00ECEDF4AD00963FBFCEB5B2135ECA7214 ();
// 0x0000003F System.Boolean ObjectRotator::snapToRotationWithDir(System.Single)
extern void ObjectRotator_snapToRotationWithDir_mD0FBF6AAF5F62BF314D17F06D7E0FFF8AD4A5CEB ();
// 0x00000040 System.Single ObjectRotator::getSnapRotationFromDirection(System.Single,System.Single)
extern void ObjectRotator_getSnapRotationFromDirection_m81C4942BB107F7010F9F01061FE728858B1D2259 ();
// 0x00000041 System.Single ObjectRotator::getClosestSnapDirection()
extern void ObjectRotator_getClosestSnapDirection_mD80AE39E7B9F76982E13126913043DB8D420FE1D ();
// 0x00000042 System.Single ObjectRotator::getClosestSnapRotation()
extern void ObjectRotator_getClosestSnapRotation_m83E259C0AAB43060E2492BBAC650FFD704611C7D ();
// 0x00000043 System.Single ObjectRotator::elasticEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void ObjectRotator_elasticEaseInOut_mAF0874FBED9B404CC30886CDB95C820A7C3F85B8 ();
// 0x00000044 System.Collections.IEnumerator ObjectRotator::slowRotationDown(System.Single,System.Single)
extern void ObjectRotator_slowRotationDown_mDEB957419D12E918A3E3CC246AA2B1DD07DFE291 ();
// 0x00000045 System.Void ObjectRotator::.ctor()
extern void ObjectRotator__ctor_m54BA472CDB703812D24B017F92EE629C57C2D55B ();
// 0x00000046 System.Void ObjectRotator::.cctor()
extern void ObjectRotator__cctor_m2E967B166231BAFBD3C854944859B0B9846009B8 ();
// 0x00000047 System.Void SimpleRotator::Start()
extern void SimpleRotator_Start_m744E4CE23BB83905D331B6A96F733802F8DF5C27 ();
// 0x00000048 System.Void SimpleRotator::Update()
extern void SimpleRotator_Update_m8A71CB924200645D8BF84F3232253C647D3A1700 ();
// 0x00000049 System.Void SimpleRotator::.ctor()
extern void SimpleRotator__ctor_m248AFE6DFB960EE28988DF90765F8EE22B29344A ();
// 0x0000004A System.Void Splasher::Start()
extern void Splasher_Start_m4BBF1A524A8ACC699B3B64067788DD890066FAFA ();
// 0x0000004B System.Void Splasher::nextValues()
extern void Splasher_nextValues_m9B09224E42B015F5CAF04B5547C059FE2C847AA8 ();
// 0x0000004C System.Void Splasher::Update()
extern void Splasher_Update_mFBE425A06DD5CF2B6461DFA4CBA60E5EFDFAFD32 ();
// 0x0000004D System.Void Splasher::createSplash()
extern void Splasher_createSplash_mAE5CF3D886863A8B5563971C6EA683355121FD1A ();
// 0x0000004E System.Void Splasher::.ctor()
extern void Splasher__ctor_m72CD0BA5E8FCEAAB3216CD4D0AD488917D81DBBA ();
// 0x0000004F System.Void UVHeightGenerator::GenerateUVs()
extern void UVHeightGenerator_GenerateUVs_m19019A8493204D5AE90D724525F7256EFC2EB4B5 ();
// 0x00000050 System.Void UVHeightGenerator::calculateUVs(UnityEngine.Transform,UnityEngine.GameObject)
extern void UVHeightGenerator_calculateUVs_m74443DA34DEB58708CFACA07DFF9CC58A17FE8E0 ();
// 0x00000051 UnityEngine.Vector3 UVHeightGenerator::Vabs(UnityEngine.Vector3)
extern void UVHeightGenerator_Vabs_mC274D6FB1C4C96A81859CC9B8457A3D3C13BBC04 ();
// 0x00000052 System.Void UVHeightGenerator::.ctor()
extern void UVHeightGenerator__ctor_mB09C8842DB7703D32AF45D6521EAADD3F149DC63 ();
// 0x00000053 System.Void UVLayoutGenerator::GenerateUVs()
extern void UVLayoutGenerator_GenerateUVs_m09E1B1D11D73E7950EE17704B7B4E3D372A55EC4 ();
// 0x00000054 System.Void UVLayoutGenerator::calculateUVs(UnityEngine.Transform,UnityEngine.GameObject)
extern void UVLayoutGenerator_calculateUVs_m454B507E34C60168FC213396604ABCBC9917F33E ();
// 0x00000055 UnityEngine.Vector3 UVLayoutGenerator::Vabs(UnityEngine.Vector3)
extern void UVLayoutGenerator_Vabs_m586DF019306DB4F4FC0A5F077292893564B0343A ();
// 0x00000056 System.Void UVLayoutGenerator::.ctor()
extern void UVLayoutGenerator__ctor_m68711D87C70C2F91B13F4E03D49CF99AE85623FF ();
// 0x00000057 System.Void CameraLogic::Start()
extern void CameraLogic_Start_m3182E65DE6B1B969ACD025225E7986AA5D138EF5 ();
// 0x00000058 System.Void CameraLogic::SwitchTarget(System.Int32)
extern void CameraLogic_SwitchTarget_m7527A4DD144BBEE099D8601BC963F1E41F0E25BF ();
// 0x00000059 System.Void CameraLogic::NextTarget()
extern void CameraLogic_NextTarget_m8BA15C99203E83F2392EB68E9CD286B6AF170EE6 ();
// 0x0000005A System.Void CameraLogic::PreviousTarget()
extern void CameraLogic_PreviousTarget_mF370A3DE9C4B270DBCCAE7038E21F993A31D6564 ();
// 0x0000005B System.Void CameraLogic::Update()
extern void CameraLogic_Update_mF99E78C85C3F976A437FEAFE8AF7CA4B143A2E57 ();
// 0x0000005C System.Void CameraLogic::LateUpdate()
extern void CameraLogic_LateUpdate_mDC5EA32B5AC8A556267CCF2F262D81CA75E1A99B ();
// 0x0000005D System.Void CameraLogic::.ctor()
extern void CameraLogic__ctor_m16E26336513324615189BC600743C3353EB44A51 ();
// 0x0000005E System.Void CharacterEffectsController::ShowPerfect(System.Boolean)
extern void CharacterEffectsController_ShowPerfect_m6F5C2661E8BCF4AB6DA1C21EBDDBAB3E3AFD0D33 ();
// 0x0000005F System.Void CharacterEffectsController::.ctor()
extern void CharacterEffectsController__ctor_m435261A99E38B43C98D31DB899D4CD95FB9103D3 ();
// 0x00000060 System.Int32 ScoreManager::get_Score()
extern void ScoreManager_get_Score_m67AD6782CB6F36C23CB29ABB02B4B0C30E2C40D9 ();
// 0x00000061 System.Void ScoreManager::set_Score(System.Int32)
extern void ScoreManager_set_Score_m4DD1EAC6BCFC820F2B16F210C00EAAB6C6E4BECA ();
// 0x00000062 System.Int32 ScoreManager::get_BestScore()
extern void ScoreManager_get_BestScore_m264AC512BFFE95DD179986C12A7BFD8AA7C0F4AC ();
// 0x00000063 System.Void ScoreManager::set_BestScore(System.Int32)
extern void ScoreManager_set_BestScore_m3C7F4995025C7C087CF09EB3524FD9B5C1347BE3 ();
// 0x00000064 System.Int32 ScoreManager::get_Multiplier()
extern void ScoreManager_get_Multiplier_m09434D375EEB5C6BB124365A2C6098F9775D1808 ();
// 0x00000065 System.Void ScoreManager::set_Multiplier(System.Int32)
extern void ScoreManager_set_Multiplier_m202CF780B2380DABBE6BBAE383B187217E8958CE ();
// 0x00000066 System.Void ScoreManager::Initialize()
extern void ScoreManager_Initialize_m5D38C63945FFEB8BF5311C18B82C8AC57C2B2D8F ();
// 0x00000067 System.Void ScoreManager::Add(System.Int32)
extern void ScoreManager_Add_m84AF805BDB17F1DFB19EAC83146B3FBBFEF1EDC4 ();
// 0x00000068 System.Int32 ScoreManager::Add(System.Int32,System.Boolean)
extern void ScoreManager_Add_m66E7520C9D936F3933546242DE8FC54128969FB8 ();
// 0x00000069 System.Void ScoreManager::TrySaveBestScore()
extern void ScoreManager_TrySaveBestScore_mC822CAD6B86C3F8DA4BDEB0A9F0A26D31A9BC654 ();
// 0x0000006A System.Void ScoreManager::Reset()
extern void ScoreManager_Reset_m23F7E7B0959CAF8614B9DFC1AC0CAF2015C42483 ();
// 0x0000006B System.Void ScoreManager::.ctor()
extern void ScoreManager__ctor_mFA7EC3F474306DB06EAA63B19DEFA71F4E00B9E4 ();
// 0x0000006C System.Void AndroidVibration::Vibrate()
extern void AndroidVibration_Vibrate_m1F2FD1AFB0C4ECB51BFA238AAD7DFD31138FF261 ();
// 0x0000006D System.Void AndroidVibration::Vibrate(System.Int64)
extern void AndroidVibration_Vibrate_m8108BECB9EC7A829E6E82B954007CF1B44ECD9A6 ();
// 0x0000006E System.Void AndroidVibration::Vibrate(System.Int64[],System.Int32)
extern void AndroidVibration_Vibrate_m12D2CB4A920E85DC9AA554931FE0E733B5B527ED ();
// 0x0000006F System.Boolean AndroidVibration::HasVibrator()
extern void AndroidVibration_HasVibrator_mA885636E8951BFD8C32888CA353C5FE03EA3A1FB ();
// 0x00000070 System.Void AndroidVibration::Cancel()
extern void AndroidVibration_Cancel_m9AEFDF629BE64051C3D4D877737F8B8BA1C6717B ();
// 0x00000071 System.Boolean AndroidVibration::isAndroid()
extern void AndroidVibration_isAndroid_mBEEB3605503CED75911A510D4AD4806D447CDE9F ();
// 0x00000072 System.Void Plugins.GameInstaller::InstallBindings()
extern void GameInstaller_InstallBindings_mE6C40F4F868E9ED79A497A9F74493E3C5D618BBA ();
// 0x00000073 System.Void Plugins.GameInstaller::.ctor()
extern void GameInstaller__ctor_m7A805F750419749CEB11F2CEE1F40CB2DDF5DCC1 ();
// 0x00000074 System.Void Plugins.GameSettingsInstaller::InstallBindings()
extern void GameSettingsInstaller_InstallBindings_m687E5D756E89B55934FB0BCC4E0636972750AB4B ();
// 0x00000075 System.Void Plugins.GameSettingsInstaller::.ctor()
extern void GameSettingsInstaller__ctor_m05CC86031A7F7FD97661E07409BFEAFFCB1CAF72 ();
// 0x00000076 System.Void Zenject.CharacterSettingsInstaller::InstallBindings()
extern void CharacterSettingsInstaller_InstallBindings_m8661901FEE8E8838DE4B3E7CBB022880F546C7EC ();
// 0x00000077 System.Void Zenject.CharacterSettingsInstaller::.ctor()
extern void CharacterSettingsInstaller__ctor_m69447442475EFB8374304EBE632A24254AB43407 ();
// 0x00000078 System.Void Zenject.GameplayInstaller::InstallBindings()
extern void GameplayInstaller_InstallBindings_mEB4CCD4634EF923F48BFEFC02289FB256CB647CD ();
// 0x00000079 System.Void Zenject.GameplayInstaller::.ctor()
extern void GameplayInstaller__ctor_m1B639320411928209C40EF1AFB61B287FE94AC06 ();
// 0x0000007A System.Void Zenject.GameplaySettingsInstaller::InstallBindings()
extern void GameplaySettingsInstaller_InstallBindings_mCC74ACF8CD4DD9303E50D9E36739B2ACBE85D066 ();
// 0x0000007B System.Void Zenject.GameplaySettingsInstaller::.ctor()
extern void GameplaySettingsInstaller__ctor_mDF3D1A1A9ACF9FF32885BEECBA1C4C19B3EF796A ();
// 0x0000007C System.Void Zenject.UiInstaller::InstallBindings()
extern void UiInstaller_InstallBindings_mE1749C9FDE46FCCBFB401D5AB35BCA9D58717246 ();
// 0x0000007D System.Void Zenject.UiInstaller::.ctor()
extern void UiInstaller__ctor_mC759712B46EC7611C402410863430B256BF9EEA0 ();
// 0x0000007E System.Void UiControllers.APanel::Initialize()
extern void APanel_Initialize_mC27E64C4C080AC2A4FEB792669BA6D87DF71B526 ();
// 0x0000007F System.Void UiControllers.APanel::Show()
extern void APanel_Show_m1E17F80E7790CDF8285D23F7F77281A0E65F0592 ();
// 0x00000080 System.Void UiControllers.APanel::Hide()
extern void APanel_Hide_mEA987CA1D6C59CF472B62CDBF2F528FDB2C0B934 ();
// 0x00000081 System.Void UiControllers.APanel::.ctor()
extern void APanel__ctor_m13C84A4F1417E607427838CC5331A85629210E66 ();
// 0x00000082 System.Void UiControllers.CompletePanel::UpdateContent(System.Int32,System.Int32,System.Int32)
extern void CompletePanel_UpdateContent_m17F42E7C9B66B6B4DABB8ED0F7273F208B5BB452 ();
// 0x00000083 System.Void UiControllers.CompletePanel::UpdateText(TMPro.TextMeshProUGUI,System.String)
extern void CompletePanel_UpdateText_mAEA15174FC19B5D5137985307A57E47C95CA566B ();
// 0x00000084 System.Void UiControllers.CompletePanel::.ctor()
extern void CompletePanel__ctor_m3EA8F33F1588DD8A5B0D7DBE663E687EBDFE2CA1 ();
// 0x00000085 System.Void UiControllers.GameOverPanel::UpdateContent(System.Int32,System.Int32,System.Int32,System.Single)
extern void GameOverPanel_UpdateContent_m408AE77CE043528AE817EDADB96471B113C34A33 ();
// 0x00000086 System.Void UiControllers.GameOverPanel::UpdateText(TMPro.TextMeshProUGUI,System.String)
extern void GameOverPanel_UpdateText_mD75C6DA7F5147D089185B6392AE455EC9A004C16 ();
// 0x00000087 System.Void UiControllers.GameOverPanel::.ctor()
extern void GameOverPanel__ctor_mEA66650214E894E76AF04C4680D8EF273553FAEE ();
// 0x00000088 System.Void UiControllers.GamePanel::Initialize()
extern void GamePanel_Initialize_m2CB57DE055ACA3C6A5B6035A23A184B13D71090D ();
// 0x00000089 System.Void UiControllers.GamePanel::ShowMotivationText()
extern void GamePanel_ShowMotivationText_mCFEF23DFF1E58387B44CA7D59FE327F21325346A ();
// 0x0000008A System.Void UiControllers.GamePanel::Reset()
extern void GamePanel_Reset_mFBB16C3075168912F33CC2F82707FEE3D05F87D2 ();
// 0x0000008B System.Void UiControllers.GamePanel::UpdateProgression(System.Single)
extern void GamePanel_UpdateProgression_m75C7411159C917C58004EE865D6831769F45690B ();
// 0x0000008C System.Void UiControllers.GamePanel::UpdateProgressLevel(System.Int32)
extern void GamePanel_UpdateProgressLevel_m72DE951C7553C9FA29E968869448275582AFC3D3 ();
// 0x0000008D System.Void UiControllers.GamePanel::.ctor()
extern void GamePanel__ctor_mB7F94623EE13AC71D62AD9A54D6660E4F53C1A0E ();
// 0x0000008E System.Void UiControllers.MenuPanel::.ctor()
extern void MenuPanel__ctor_mDCC9E913ED6054BC65292999FB5910A04A3D9788 ();
// 0x0000008F System.Void UiComponents.ProgressBar::Update()
extern void ProgressBar_Update_mCB439C15888996CFE7BEF8F3D21CC156CF4D1AC4 ();
// 0x00000090 System.Void UiComponents.ProgressBar::FillSmooth()
extern void ProgressBar_FillSmooth_m4227F6605D50755C5623526381D19D2232ED015B ();
// 0x00000091 System.Void UiComponents.ProgressBar::UpdateFill(System.Single)
extern void ProgressBar_UpdateFill_m41549F9624D27646B0E6D4C5733540E33F6847DF ();
// 0x00000092 System.Void UiComponents.ProgressBar::UpdateLevels(System.Int32)
extern void ProgressBar_UpdateLevels_m4AC2CC4E7F709AB1C7AFD0F7A6C59F466DA22BF6 ();
// 0x00000093 System.Void UiComponents.ProgressBar::Show()
extern void ProgressBar_Show_mF35ACF511FA286256FA873A1FC637C66E5E6F467 ();
// 0x00000094 System.Void UiComponents.ProgressBar::Hide()
extern void ProgressBar_Hide_m008E27E5B0DA5CA66C23AA7A8139E505835ADEF3 ();
// 0x00000095 System.Void UiComponents.ProgressBar::Reset()
extern void ProgressBar_Reset_m90C606E7B9DC72EB462EAE98E1C583F1F1A8B4EF ();
// 0x00000096 System.Void UiComponents.ProgressBar::.ctor()
extern void ProgressBar__ctor_m2D16B36718B42D1BB55FDF14BA40DA26363759A0 ();
// 0x00000097 System.Void States.AState::Enter(States.AState)
// 0x00000098 System.Void States.AState::Exit(States.AState)
// 0x00000099 System.Void States.AState::Tick()
// 0x0000009A System.Void States.AState::FixedTick()
// 0x0000009B States.State States.AState::get_Name()
// 0x0000009C System.Void States.AState::Initialize(States.GameStatesManager)
extern void AState_Initialize_m570936E7588BBE0E27189B826EC543D4BEE0893B ();
// 0x0000009D System.Void States.AState::.ctor()
extern void AState__ctor_m3346E6690A7E25A7F60FD2A59102BDBA479398C2 ();
// 0x0000009E System.Void States.CompleteState::Enter(States.AState)
extern void CompleteState_Enter_m70B6CD481FCA2C1A312EEEA89802A4CCA057BE71 ();
// 0x0000009F System.Void States.CompleteState::ShowPanel()
extern void CompleteState_ShowPanel_m8B944EAC8D86F0A24042D14F857D9C85F4062A1F ();
// 0x000000A0 System.Void States.CompleteState::Exit(States.AState)
extern void CompleteState_Exit_mACE98F1A0FEB22A10E09BFE4FB33942D4208AFE5 ();
// 0x000000A1 System.Void States.CompleteState::Tick()
extern void CompleteState_Tick_m49C78A5D6F61436D52DBC7496B709D0EE83FF536 ();
// 0x000000A2 System.Void States.CompleteState::FixedTick()
extern void CompleteState_FixedTick_m2742FC6DC7EA0BAA34CB60EBBAF10FD510279EE5 ();
// 0x000000A3 States.State States.CompleteState::get_Name()
extern void CompleteState_get_Name_mC7EF31549E2950D1AB85837656D3FCABC5EFA8C6 ();
// 0x000000A4 System.Void States.CompleteState::.ctor()
extern void CompleteState__ctor_m1A6977601A37985D4BC66A03865FF48DBE179033 ();
// 0x000000A5 System.Void States.GameOverState::Enter(States.AState)
extern void GameOverState_Enter_m08D99C85675060A41600B86A5FD8A1EA45726435 ();
// 0x000000A6 System.Void States.GameOverState::ShowPanel()
extern void GameOverState_ShowPanel_m5C1AC4D06E6535AADFF2D278280FC2C7456E2E2B ();
// 0x000000A7 System.Void States.GameOverState::Exit(States.AState)
extern void GameOverState_Exit_mCF8524311EBAE70DB0E96017DF5AD1A327A09CB1 ();
// 0x000000A8 System.Void States.GameOverState::Tick()
extern void GameOverState_Tick_mB2F2AAB3F381471B6ABC0B27C03D960E3026710A ();
// 0x000000A9 System.Void States.GameOverState::FixedTick()
extern void GameOverState_FixedTick_m745ADE789B1200A75B1D7CD1B49B07ED8B768D33 ();
// 0x000000AA States.State States.GameOverState::get_Name()
extern void GameOverState_get_Name_m13FEAAD38B9D23F29C48E535E7B92E911411E8BB ();
// 0x000000AB System.Void States.GameOverState::.ctor()
extern void GameOverState__ctor_m3FA2CCC87D34B231FE1267C6AF26FE6073F80310 ();
// 0x000000AC System.Void States.GameState::Initialize(States.GameStatesManager)
extern void GameState_Initialize_mD0714CE299F41175DFE16CA19FC9AD01ED5EFDF6 ();
// 0x000000AD System.Void States.GameState::Enter(States.AState)
extern void GameState_Enter_mA7A9956094B9F6A70BD817E3242CB0BA81540F39 ();
// 0x000000AE System.Void States.GameState::Exit(States.AState)
extern void GameState_Exit_m72D3EEF58BDCA4DF8B4906EAEEC2C0BE53F69DA1 ();
// 0x000000AF System.Void States.GameState::Tick()
extern void GameState_Tick_m759B141FCB4171D99AD581AD6CD8264E7D07880F ();
// 0x000000B0 System.Void States.GameState::FixedTick()
extern void GameState_FixedTick_mADE597D74FA0E5E78EF48EA68AE3F920E6A3DD03 ();
// 0x000000B1 System.Void States.GameState::TileLandedHandler(Gameplay.Tile,System.Boolean)
extern void GameState_TileLandedHandler_mDB1C6D3127C03724735FFFF67DA4781BB7E0725D ();
// 0x000000B2 States.State States.GameState::get_Name()
extern void GameState_get_Name_m7000C8EB4A4579150E4606CC0E0F2D4FA2CD070E ();
// 0x000000B3 System.Void States.GameState::.ctor()
extern void GameState__ctor_m21808F8C9BC761BCFAFDD27B60FFC1AA6DB58A65 ();
// 0x000000B4 System.Void States.GameState::<Initialize>b__6_0()
extern void GameState_U3CInitializeU3Eb__6_0_m53072A32A4D6FAA63DC17F44C8483DBBACB442F0 ();
// 0x000000B5 System.Void States.GameState::<Initialize>b__6_1()
extern void GameState_U3CInitializeU3Eb__6_1_m2D3C53F283B3458F2712E933E0BCDD5FF25DB915 ();
// 0x000000B6 States.GameStatesManager States.GameStatesManager::get_Instance()
extern void GameStatesManager_get_Instance_m25708E0AF02A2BE401FF9EB23686752C3456D2B5 ();
// 0x000000B7 System.Void States.GameStatesManager::set_Instance(States.GameStatesManager)
extern void GameStatesManager_set_Instance_m2348E6E491ACF2FC7569280496AA46EB4BB2F694 ();
// 0x000000B8 States.AState States.GameStatesManager::get_TopState()
extern void GameStatesManager_get_TopState_m94AE7AD29894A96791FA06EE710CA267410261B4 ();
// 0x000000B9 System.Void States.GameStatesManager::Initialize()
extern void GameStatesManager_Initialize_mD2AF1BA9B70F158E462AC373D473A7712AB45537 ();
// 0x000000BA System.Void States.GameStatesManager::Start()
extern void GameStatesManager_Start_mA26409D35499F51BE93266121D90AA7BD0777ADB ();
// 0x000000BB System.Void States.GameStatesManager::Update()
extern void GameStatesManager_Update_mFF21E7011A6BF997B711F9C60FE54B0291FAF7E8 ();
// 0x000000BC System.Void States.GameStatesManager::FixedUpdate()
extern void GameStatesManager_FixedUpdate_m46F816288497A2CA381DB21AA8D0B929B533BF48 ();
// 0x000000BD System.Void States.GameStatesManager::StartGame()
extern void GameStatesManager_StartGame_m5CB6004BC340660F94591B637E4F8DF3A829A12A ();
// 0x000000BE System.Void States.GameStatesManager::ToMenu()
extern void GameStatesManager_ToMenu_m662DB236586D5D7EBA71FEC98B61D6A9E73E349A ();
// 0x000000BF System.Void States.GameStatesManager::SwitchState(States.State)
extern void GameStatesManager_SwitchState_m2EB61F21296FD884EE659A1305729F69CDDB7E0D ();
// 0x000000C0 States.AState States.GameStatesManager::FindState(States.State)
extern void GameStatesManager_FindState_mEBC5C966AE0F55CB426013336397C10A439F6D25 ();
// 0x000000C1 System.Void States.GameStatesManager::PopState()
extern void GameStatesManager_PopState_m35355865F02EC0B431E6324C7BD293A19D232749 ();
// 0x000000C2 System.Void States.GameStatesManager::PushState(States.State)
extern void GameStatesManager_PushState_m52FAE643766A075BABEA0C5062490BFE33C613BD ();
// 0x000000C3 System.Void States.GameStatesManager::.ctor()
extern void GameStatesManager__ctor_m7DF3694EA6442E23885D846988F8A9B0500F9FC2 ();
// 0x000000C4 System.Void States.MenuState::Initialize(States.GameStatesManager)
extern void MenuState_Initialize_mC37BFE060CE95EE3B464BF831F98B7D84B3C22B5 ();
// 0x000000C5 System.Void States.MenuState::Enter(States.AState)
extern void MenuState_Enter_m07640E719CF0DED99917CC8BA186A64C6DC05DF7 ();
// 0x000000C6 System.Void States.MenuState::Exit(States.AState)
extern void MenuState_Exit_m27417206C5C425607D0CB27E3AAACEDBD91A01E4 ();
// 0x000000C7 System.Void States.MenuState::Tick()
extern void MenuState_Tick_mB72FF3890494C65D341E83BE68F00E9CBACD6911 ();
// 0x000000C8 System.Void States.MenuState::FixedTick()
extern void MenuState_FixedTick_m579F348793D631A33606989B860B0A8FF71339B7 ();
// 0x000000C9 States.State States.MenuState::get_Name()
extern void MenuState_get_Name_m4776E385588A20AD09E97D541C2837CB32F99F22 ();
// 0x000000CA System.Void States.MenuState::.ctor()
extern void MenuState__ctor_mC4073E3C9236E32B338CF7F6EDDF39E35A9F4032 ();
// 0x000000CB System.Void States.SublevelCompleteState::Initialize(States.GameStatesManager)
extern void SublevelCompleteState_Initialize_m90DF53E0C145598BA065BE9F804ACE3AD1776A84 ();
// 0x000000CC System.Void States.SublevelCompleteState::Enter(States.AState)
extern void SublevelCompleteState_Enter_m8696BE10F9ED26385C0EBD77E58C453D46642542 ();
// 0x000000CD System.Void States.SublevelCompleteState::Exit(States.AState)
extern void SublevelCompleteState_Exit_m5FB6D8A5AD4E286AFA4A2E722DCB6488736730FC ();
// 0x000000CE System.Void States.SublevelCompleteState::Tick()
extern void SublevelCompleteState_Tick_m2AD065D622C5AFB3C20BE6A2A0263A2F93E72CDE ();
// 0x000000CF System.Void States.SublevelCompleteState::FixedTick()
extern void SublevelCompleteState_FixedTick_m3A3D7EFE44952949A4CF14CF0733E841652A9CC1 ();
// 0x000000D0 States.State States.SublevelCompleteState::get_Name()
extern void SublevelCompleteState_get_Name_mF84C041A7423846C608D3E54EB45F314782CC031 ();
// 0x000000D1 System.Void States.SublevelCompleteState::.ctor()
extern void SublevelCompleteState__ctor_m1608C5026256CD09220093012B19447204201321 ();
// 0x000000D2 System.Single Others.Bounds::Lerp(Others.FloatBounds,System.Single)
extern void Bounds_Lerp_m34286B19C815FCF4C83A2D72F6CBF7EF782607C2 ();
// 0x000000D3 System.Single Others.Bounds::InverseLerp(Others.FloatBounds,System.Single)
extern void Bounds_InverseLerp_mF1083736A8BC1491EC3B9C620C3A0213915553AC ();
// 0x000000D4 System.Single Others.Bounds::Lerp(Others.IntBounds,System.Single)
extern void Bounds_Lerp_m13DFF59389B2F90AB609177B1A287AA784CABF48 ();
// 0x000000D5 System.Single Others.Bounds::InverseLerp(Others.IntBounds,System.Single)
extern void Bounds_InverseLerp_mD9C636F05360A047F318C9330038BF59BBF6E4FA ();
// 0x000000D6 UnityEngine.Vector2 Others.Bounds::Lerp(Others.Vector2Bounds,System.Single)
extern void Bounds_Lerp_mAA4FAD37539B1D079CBF75133BF3A4710F5AD063 ();
// 0x000000D7 System.Single Others.Bounds::InverseLerp(Others.Vector2Bounds,UnityEngine.Vector2)
extern void Bounds_InverseLerp_m55F7B3E11F377AB1EC448FEAA5DB18B13C0B9E2F ();
// 0x000000D8 UnityEngine.Vector3 Others.Bounds::Lerp(Others.Vector3Bounds,System.Single)
extern void Bounds_Lerp_m66AA0601E31BB65270178C06C56A8AE67392D4DB ();
// 0x000000D9 System.Single Others.Bounds::InverseLerp(Others.Vector3Bounds,UnityEngine.Vector3)
extern void Bounds_InverseLerp_mC0564AEF339A3882B34FE1AD32024F1C8D58E9CF ();
// 0x000000DA System.Void Others.FloatBounds::.ctor()
extern void FloatBounds__ctor_m64232EFD95559216A26784D6098C337300DC598E ();
// 0x000000DB System.Void Others.IntBounds::.ctor()
extern void IntBounds__ctor_m24E4006218E0BD1900E824CD370BC4F43C8879A4 ();
// 0x000000DC System.Void Others.Vector2Bounds::.ctor()
extern void Vector2Bounds__ctor_m33D91F897BA6C1DD2D45E7CEE6A10CE72AC33FD0 ();
// 0x000000DD System.Void Others.Vector3Bounds::.ctor()
extern void Vector3Bounds__ctor_m6579707B250DBB743D4FDA459801A3C21C130B91 ();
// 0x000000DE System.Void Others.DataSaver::Save(T)
// 0x000000DF T Others.DataSaver::Get(T)
// 0x000000E0 System.Void Others.DataSaver::SaveProperty(System.String,T)
// 0x000000E1 System.Int32 Others.DataSaver::GetIntProperty(System.String,System.Int32)
extern void DataSaver_GetIntProperty_mD2E9AC95C603DBA4B3BF0C0AA9E5D40B89B4CF13 ();
// 0x000000E2 System.Void Others.DataSaver::Clear(System.Type)
extern void DataSaver_Clear_mA777DD1256F6217433148F78E7E00CF156CA9824 ();
// 0x000000E3 System.String Others.DataSaver::Encrypt(System.String)
extern void DataSaver_Encrypt_m4638014303EB3E481E24AE53F5D84689D12E9F5A ();
// 0x000000E4 System.String Others.DataSaver::Decrypt(System.String)
extern void DataSaver_Decrypt_m24ACE7043E0CC4C1AADB08C9E79E1794C6EEA42E ();
// 0x000000E5 System.Int32 Others.IndexNormalizer::GetLoop(System.Int32,System.Int32)
extern void IndexNormalizer_GetLoop_m94E9FC9C50F6D3FB082356470DA05DA20342CE30 ();
// 0x000000E6 System.Int32 Others.IndexNormalizer::GetRandom(System.Int32,System.Int32)
extern void IndexNormalizer_GetRandom_m0E6831B541673AA31313E2D39231435AD02ECB79 ();
// 0x000000E7 System.Int32 Managers.LevelsManager::get_Level()
extern void LevelsManager_get_Level_m08CB2E5521CE685B66161E7D0C675F2D0567F6E1 ();
// 0x000000E8 System.Void Managers.LevelsManager::set_Level(System.Int32)
extern void LevelsManager_set_Level_m769B5AAD411EE858B0565AE088394641751058C5 ();
// 0x000000E9 System.Int32 Managers.LevelsManager::get_SubLevel()
extern void LevelsManager_get_SubLevel_m93FF655A7FE26A08EC26077C1F2B2660287906B4 ();
// 0x000000EA System.Void Managers.LevelsManager::set_SubLevel(System.Int32)
extern void LevelsManager_set_SubLevel_m560C57125B6EF618E0580492AB1E6A3B15C1BF69 ();
// 0x000000EB System.Int32 Managers.LevelsManager::get_Step()
extern void LevelsManager_get_Step_mD06868D3025B8180F3B04691F60B1FC038545B2F ();
// 0x000000EC System.Void Managers.LevelsManager::set_Step(System.Int32)
extern void LevelsManager_set_Step_mAD7098F023D00E75843BB325C94E872AA559F8BB ();
// 0x000000ED System.Single Managers.LevelsManager::get_Progression()
extern void LevelsManager_get_Progression_m708A0FA6AF9B029D5BB718459144782C19A27920 ();
// 0x000000EE System.Void Managers.LevelsManager::Initialize()
extern void LevelsManager_Initialize_m3648F2ACEB2513C88F0A7B7D51A075A05969FCE4 ();
// 0x000000EF System.Boolean Managers.LevelsManager::NextStep()
extern void LevelsManager_NextStep_mB0B5CCACA48BEE7A27F0BBA925F5C4628A8FF8D8 ();
// 0x000000F0 System.Boolean Managers.LevelsManager::NextSubLevel()
extern void LevelsManager_NextSubLevel_mA3878E21A2D0C43124AE1DBEE0428EFD04E99476 ();
// 0x000000F1 System.Void Managers.LevelsManager::NextLevel()
extern void LevelsManager_NextLevel_m57755737CAB78CC2B7CA527367EF7E04617681AD ();
// 0x000000F2 System.Void Managers.LevelsManager::Reset()
extern void LevelsManager_Reset_mF16722E0C1E3F3AFBAC1475B6675083477692CAE ();
// 0x000000F3 System.Void Managers.LevelsManager::.ctor()
extern void LevelsManager__ctor_mD2EDB80C1971C44558046A6F83E4CF40AAB39C02 ();
// 0x000000F4 System.Single Managers.TilesManager::GetProgression(Gameplay.Tile)
extern void TilesManager_GetProgression_m01C3A7A88C4F0A7C6AD70439CF1C1F4FC7DB58BE ();
// 0x000000F5 System.Void Managers.TilesManager::Reset()
extern void TilesManager_Reset_m70C510580D819F6CB120CA42BB0A93E417B56B56 ();
// 0x000000F6 System.Void Managers.TilesManager::.ctor()
extern void TilesManager__ctor_m4B6020293F135B3C73F48E35EA9811DC338738AC ();
// 0x000000F7 System.Void Gameplay.FinishTile::PlayCompleteEffects()
extern void FinishTile_PlayCompleteEffects_m8C3F4419AECBC07AE7D8937EFDBF3FF0D4D522A0 ();
// 0x000000F8 System.Void Gameplay.FinishTile::.ctor()
extern void FinishTile__ctor_m113F0F8E86C69E2BEB0A0ADA0C224254C56158C0 ();
// 0x000000F9 System.Void Gameplay.CharacterMoveController::Initialize()
extern void CharacterMoveController_Initialize_m57249762D8D876E29EA24CD9E51C368AF12A57CF ();
// 0x000000FA System.Void Gameplay.CharacterMoveController::Move(UnityEngine.Rigidbody,UnityEngine.Vector2)
extern void CharacterMoveController_Move_m6F6E2696A81FE90614FAC2AC24383EAD278E6851 ();
// 0x000000FB System.Void Gameplay.CharacterMoveController::Jump(UnityEngine.Rigidbody)
extern void CharacterMoveController_Jump_m258CC21069974A46BCE2CE02DF6CC7CA4DB46869 ();
// 0x000000FC System.Void Gameplay.CharacterMoveController::Reset(UnityEngine.Transform)
extern void CharacterMoveController_Reset_mD6A1E4650E370D9AE4F5B220E43D35A7608D2C28 ();
// 0x000000FD System.Void Gameplay.CharacterMoveController::.ctor()
extern void CharacterMoveController__ctor_m5935980F6D70531DC355A5667C8CC49A1BF4C6E8 ();
// 0x000000FE System.Boolean Gameplay.Tile::get_IsUsed()
extern void Tile_get_IsUsed_mD310AAD46B8142FAD5AC0A513B5869EFFF120F7E ();
// 0x000000FF System.Void Gameplay.Tile::set_IsUsed(System.Boolean)
extern void Tile_set_IsUsed_m5031C22DDB3BCBC99F6D2D7F3D0325FBA3FC5AEF ();
// 0x00000100 System.Void Gameplay.Tile::Use()
extern void Tile_Use_mEA1EC428507216A90DED2C93B4C7989499C5E363 ();
// 0x00000101 System.Boolean Gameplay.Tile::IsPerfect(UnityEngine.Vector3)
extern void Tile_IsPerfect_m9A5E2FD99066F367DB7E0A15BE53C5E095F6A77A ();
// 0x00000102 System.Void Gameplay.Tile::Reset()
extern void Tile_Reset_m3930EC14F5CF3E8AFA571D0707BE1AAED3BAB966 ();
// 0x00000103 System.Void Gameplay.Tile::.ctor()
extern void Tile__ctor_m854A642EAE84DFB59DEDD9249DAF0A13F4F66063 ();
// 0x00000104 System.Void Gameplay.Player.AimController::UpdateAim(System.Single)
extern void AimController_UpdateAim_mE325AB7638764B0CA208C775F475E8774CF366CF ();
// 0x00000105 System.Void Gameplay.Player.AimController::.ctor()
extern void AimController__ctor_m0A5435C925331F988DB0B16DD7F3EAA82058088F ();
// 0x00000106 System.Void Gameplay.Player.CharacterAnimationController::Initialize()
extern void CharacterAnimationController_Initialize_m1BF4A5581504AA424D9B1CBFDDEE5A0D3451D6AE ();
// 0x00000107 System.Void Gameplay.Player.CharacterAnimationController::Jump()
extern void CharacterAnimationController_Jump_m12EA44E7752EDA71B2378A7A4DE6FAC51BDE67C6 ();
// 0x00000108 System.Void Gameplay.Player.CharacterAnimationController::.ctor()
extern void CharacterAnimationController__ctor_m75A7FD33230EF6996F5DD15EB837EE750703774D ();
// 0x00000109 System.Void Gameplay.Player.CharacterController::add_TileLanded(System.Action`2<Gameplay.Tile,System.Boolean>)
extern void CharacterController_add_TileLanded_m96C3C391D561F87695AA391F634C6D44CCFEB5D0 ();
// 0x0000010A System.Void Gameplay.Player.CharacterController::remove_TileLanded(System.Action`2<Gameplay.Tile,System.Boolean>)
extern void CharacterController_remove_TileLanded_m56846D5A4A4A4048EC3BA101B90C025CA927D374 ();
// 0x0000010B System.Void Gameplay.Player.CharacterController::add_Complete(System.Action)
extern void CharacterController_add_Complete_m73B0C27685CCFD6C284E81E96E3C9321B0FD92D1 ();
// 0x0000010C System.Void Gameplay.Player.CharacterController::remove_Complete(System.Action)
extern void CharacterController_remove_Complete_mB8A70BB52AE0586DD49895F21786B4D9043C27AF ();
// 0x0000010D System.Void Gameplay.Player.CharacterController::add_Crash(System.Action)
extern void CharacterController_add_Crash_mA4307E546A64E8B5DDD88A788E9989F80B1FF369 ();
// 0x0000010E System.Void Gameplay.Player.CharacterController::remove_Crash(System.Action)
extern void CharacterController_remove_Crash_m52B349ED18DD3F4BE547CC601247B1E3D9998468 ();
// 0x0000010F UnityEngine.Vector3 Gameplay.Player.CharacterController::get_Position()
extern void CharacterController_get_Position_m487DC39F7211172EAC44159BADAEBF7E9E2D2B83 ();
// 0x00000110 System.Void Gameplay.Player.CharacterController::Initialize()
extern void CharacterController_Initialize_m31DFBC02FEF7CDF74127DBFA81FB0424D9C866C7 ();
// 0x00000111 System.Void Gameplay.Player.CharacterController::Tick()
extern void CharacterController_Tick_mC96AE378D718362804284DBFE6E88E86D2ADD665 ();
// 0x00000112 System.Void Gameplay.Player.CharacterController::FixedTick()
extern void CharacterController_FixedTick_mCEC487C47A8C66E7AC24C92FC5D19369477DC455 ();
// 0x00000113 System.Void Gameplay.Player.CharacterController::StartMove()
extern void CharacterController_StartMove_m648072970A7F81EE8A8C59A080D2E949AF584D13 ();
// 0x00000114 System.Void Gameplay.Player.CharacterController::Move()
extern void CharacterController_Move_m1C2CFA5610FBD3525125CD51F2FFEEFE95FABF60 ();
// 0x00000115 System.Void Gameplay.Player.CharacterController::OnCollisionEnter(UnityEngine.Collision)
extern void CharacterController_OnCollisionEnter_m25DC6B82872AB12DAAC31E827AC12287EB2DA427 ();
// 0x00000116 System.Void Gameplay.Player.CharacterController::OnTriggerEnter(UnityEngine.Collider)
extern void CharacterController_OnTriggerEnter_m450170CFD881B5A9C90334A8A191C56DD00608CC ();
// 0x00000117 System.Void Gameplay.Player.CharacterController::OnFinishEnter(Gameplay.FinishTile)
extern void CharacterController_OnFinishEnter_m09CCFCF48F73F0E158C8100C21B7B19B4E2076C1 ();
// 0x00000118 System.Void Gameplay.Player.CharacterController::OnTileEnter(Gameplay.Tile)
extern void CharacterController_OnTileEnter_mE5DAD9DA4B6BB0E7532324D3E058F214302491C1 ();
// 0x00000119 System.Void Gameplay.Player.CharacterController::Jump()
extern void CharacterController_Jump_m0D04BB50A55FFCA78C031AAAF1C3B7C96285CB6E ();
// 0x0000011A System.Void Gameplay.Player.CharacterController::Reset()
extern void CharacterController_Reset_m8E6193C410CBD7223D3CA3AA3883A2B39D502E85 ();
// 0x0000011B System.Void Gameplay.Player.CharacterController::.ctor()
extern void CharacterController__ctor_mD63A1653BA263C0276F74081166E28AD1F955773 ();
// 0x0000011C System.Void Extensions.TransformExtension::SetX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetX_m85248C96202568A7F550061FD100A7ECAF7B80CD ();
// 0x0000011D System.Void Extensions.TransformExtension::SetY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetY_m74489F788842D013C156C5DC7340E09D7CB23A30 ();
// 0x0000011E System.Void Extensions.TransformExtension::SetZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetZ_m297ED015318BFF2FD6F817A2B1A9B582BF378D1B ();
// 0x0000011F System.Void Extensions.TransformExtension::SetLocalX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalX_m6B32F1FF56E9E4BED0D65C1C8E63FFF3394E101C ();
// 0x00000120 System.Void Extensions.TransformExtension::SetLocalY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalY_m247C4C4A7CDBA140E00B0E7A8B7D80FC9047A888 ();
// 0x00000121 System.Void Extensions.TransformExtension::SetLocalZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalZ_mA5BDF976F3733C5D2DB81D53B9DFB96651046947 ();
// 0x00000122 System.Void Extensions.TransformExtension::SetLocalScaleX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalScaleX_m90EC54B348DF8F38C5B3AC439A4D5D5A46D379CD ();
// 0x00000123 System.Void Extensions.TransformExtension::SetLocalScaleY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalScaleY_mC3FF01BA6083F6BF573043A20D426E038BDBAAE3 ();
// 0x00000124 System.Void Extensions.TransformExtension::SetLocalScaleZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalScaleZ_m31EB24B2DBD59B727F4456F1964D2511A23E398A ();
// 0x00000125 UnityEngine.Vector3 Extensions.TransformExtension::WidthDepthDifference(UnityEngine.Vector3,UnityEngine.Vector3)
extern void TransformExtension_WidthDepthDifference_m80F1CBD833D7F03C2CD3CFCD55BC178CEB623FB1 ();
// 0x00000126 UnityEngine.Vector3 Extensions.TransformExtension::WidthDepthDifference(UnityEngine.Transform,UnityEngine.Vector3)
extern void TransformExtension_WidthDepthDifference_mEF6D7C0BFF5D4E8C220B077923194B1D4ED9DA14 ();
// 0x00000127 UnityEngine.Vector3 Extensions.TransformExtension::RotateVector3(UnityEngine.Vector3,System.Single)
extern void TransformExtension_RotateVector3_m9CB2453D82A365384E61A073A6BDCF991F8DBA4B ();
// 0x00000128 System.Void Controllers.ColorsController::add_OnColorsUpdated(System.Action)
extern void ColorsController_add_OnColorsUpdated_m47671107E2C9C34A9BE219DA5F85EFDDA1B81164 ();
// 0x00000129 System.Void Controllers.ColorsController::remove_OnColorsUpdated(System.Action)
extern void ColorsController_remove_OnColorsUpdated_mF9D4CE3490CA57E129A006606BBD2CDD4D9DA17B ();
// 0x0000012A System.Void Controllers.ColorsController::UpdateColors(System.Int32)
extern void ColorsController_UpdateColors_m200DCA42FB65A4180D63E5637D53299A89242D8B ();
// 0x0000012B System.Void Controllers.ColorsController::.ctor()
extern void ColorsController__ctor_m36B0848D279F0CDEE7007C887687E375568E0327 ();
// 0x0000012C System.Void Controllers.EnvironmentController::OnStart()
extern void EnvironmentController_OnStart_m928696FFFE5661FF76B427C107F6F5CFF5DB663A ();
// 0x0000012D System.Void Controllers.EnvironmentController::OnComplete()
extern void EnvironmentController_OnComplete_m5D5AFB57C70FF1E9220C1D1779AF68530E76C344 ();
// 0x0000012E System.Void Controllers.EnvironmentController::OnGameOver()
extern void EnvironmentController_OnGameOver_mEEB8FA57FE3D63CA8AB3491AD2BF56BB5473AE90 ();
// 0x0000012F System.Void Controllers.EnvironmentController::Tick()
extern void EnvironmentController_Tick_mB17FBB2F63DB87A42FEF677FA69374DCE2420B62 ();
// 0x00000130 System.Void Controllers.EnvironmentController::FixedTick()
extern void EnvironmentController_FixedTick_m101D7FD8EC6B084E84883AA9A666B989A3CB0D95 ();
// 0x00000131 System.Void Controllers.EnvironmentController::ShowCompleteEffect()
extern void EnvironmentController_ShowCompleteEffect_m503A5A18397AC60EB766AF5187750B1AC86B4B86 ();
// 0x00000132 System.Void Controllers.EnvironmentController::.ctor()
extern void EnvironmentController__ctor_m7C4ECD8AED4B3CB7057D301E552F370ACA406F1B ();
// 0x00000133 System.Void Controllers.FogController::Update(System.Single)
extern void FogController_Update_mF7D3189BEDF0D77BD7FD7BE985F095D4CC89427A ();
// 0x00000134 System.Void Controllers.FogController::.ctor()
extern void FogController__ctor_mE371ECFDF0172DCCEF8A6B628C776ED11895C81F ();
// 0x00000135 UnityEngine.Vector2 Controllers.InputController::get_Difference()
extern void InputController_get_Difference_mCD75A4E844123CE8EE5B0DD15ED496F7A8FBEBD9 ();
// 0x00000136 System.Void Controllers.InputController::set_Difference(UnityEngine.Vector2)
extern void InputController_set_Difference_mC4358EEF8B664A28649F48DF5B3D295464C4056A ();
// 0x00000137 System.Void Controllers.InputController::StartMove()
extern void InputController_StartMove_m4322C0611FB856DB610354E7F587D75A6A52ACC9 ();
// 0x00000138 System.Void Controllers.InputController::Move()
extern void InputController_Move_mEFEFB7A96D6EED7141D28BF290FBC007CF2A0B66 ();
// 0x00000139 System.Void Controllers.InputController::Reset()
extern void InputController_Reset_m1435B7D3D7C868F125F7A7B22C5FA73C1C146253 ();
// 0x0000013A System.Void Controllers.InputController::.ctor()
extern void InputController__ctor_m314EE80EAF8DA10CE0FC95EA0A4C58B8B481F57A ();
// 0x0000013B System.Void Controllers.EffectsManager::Initialize()
extern void EffectsManager_Initialize_m98B3B762C7F2B2EA4107F00DBF87B1EDBD44CDEC ();
// 0x0000013C System.Void Controllers.EffectsManager::ShowTileEffect(UnityEngine.Vector3)
extern void EffectsManager_ShowTileEffect_mB3C414AFE30CD7FA1FABC80CFBEDC23B7F424EC9 ();
// 0x0000013D System.Void Controllers.EffectsManager::ShowTilePerfectEffect(UnityEngine.Vector3)
extern void EffectsManager_ShowTilePerfectEffect_m7A0BBDCA51F55F20E3C01587AFBEBA8DEED28A8F ();
// 0x0000013E System.Void Controllers.EffectsManager::.ctor()
extern void EffectsManager__ctor_m44C4B5BAC0CCC00E805B384F64E89FBC2E54AE8B ();
// 0x0000013F System.Void Cam.CameraController::Awake()
extern void CameraController_Awake_mB04003159F6CF6FFA692D223B0F3D5EC86C08075 ();
// 0x00000140 System.Void Cam.CameraController::Initialize()
extern void CameraController_Initialize_m153BF25CD0EDF1BBA32DC75F14ECE44C2CBCFFBE ();
// 0x00000141 System.Void Cam.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m3A6B954830DF5AA2E6F3B33A8199616D507D1536 ();
// 0x00000142 System.Void Cam.CameraController::Follow()
extern void CameraController_Follow_m0A45C6D3983AE25D51C652F4247F25BC3329DC81 ();
// 0x00000143 System.Void Cam.CameraController::StartFollow()
extern void CameraController_StartFollow_m8306E0C0469F8F41FF20663AF0AF5622F9771281 ();
// 0x00000144 System.Void Cam.CameraController::StopFollow()
extern void CameraController_StopFollow_m3470ADB2669A1A96FD1962E2D27EE28C0828A205 ();
// 0x00000145 System.Void Cam.CameraController::SetTarget(UnityEngine.Transform)
extern void CameraController_SetTarget_m2AA5C6C2A4481E1D31787C907306455CA9B2D0B7 ();
// 0x00000146 System.Void Cam.CameraController::Reset()
extern void CameraController_Reset_mC6EE68CD96653D39D961D917D6C18229695F9237 ();
// 0x00000147 System.Void Cam.CameraController::.ctor()
extern void CameraController__ctor_m59F6E2426EB9D73CC4C196A371191BECDB0BBE0C ();
// 0x00000148 System.Void Pool.PoolEffect::Awake()
extern void PoolEffect_Awake_m70918BA5F076B91A5A86E37C3C4BC7F4700342BF ();
// 0x00000149 System.Void Pool.PoolEffect::OnPop()
extern void PoolEffect_OnPop_m192ACBD07D539B410B17915AA33F334DD27DCFCF ();
// 0x0000014A System.Collections.IEnumerator Pool.PoolEffect::HideWaiter()
extern void PoolEffect_HideWaiter_mD5145FFB389AC9C5C754A27DC419B6B1D67EFCAC ();
// 0x0000014B System.Void Pool.PoolEffect::UpdateColor(UnityEngine.Color32)
extern void PoolEffect_UpdateColor_m320C128E011652076E0D5F838CCB3FD2C00F0385 ();
// 0x0000014C System.Void Pool.PoolEffect::.ctor()
extern void PoolEffect__ctor_m2238AC9049505F847DE513B22E6A7ECC6107E437 ();
// 0x0000014D System.Void Pool.PoolManager::Awake()
extern void PoolManager_Awake_m8386AAF529210AF156EF2D890721699A328F5C0D ();
// 0x0000014E System.Void Pool.PoolManager::Load(Pool.PoolObject,System.Int32,UnityEngine.Transform)
extern void PoolManager_Load_m6823585491887FCF8A1AD8E1000542EB6021DF07 ();
// 0x0000014F System.Void Pool.PoolManager::Push(System.String,Pool.PoolObject)
extern void PoolManager_Push_mC83C318264CA933B5058677B8488FD833DA87D9D ();
// 0x00000150 T Pool.PoolManager::PopOrCreate(T,UnityEngine.Transform)
// 0x00000151 T Pool.PoolManager::PopOrCreate(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
// 0x00000152 T Pool.PoolManager::Pop(System.String)
// 0x00000153 T Pool.PoolManager::CreateObject(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
// 0x00000154 System.Void Pool.PoolManager::ClearList(System.Collections.Generic.List`1<T>&)
// 0x00000155 System.Void Pool.PoolManager::HideObject(T,System.Collections.Generic.List`1<T>&)
// 0x00000156 System.Void Pool.PoolManager::.ctor()
extern void PoolManager__ctor_m730D64E792DA6A66A27BB9463BAA88B5C567E999 ();
// 0x00000157 System.String Pool.PoolObject::get_Group()
extern void PoolObject_get_Group_mC19841EEBD25FC2708E71E9E30EE6DB1C53DE203 ();
// 0x00000158 UnityEngine.Transform Pool.PoolObject::get_Transform()
extern void PoolObject_get_Transform_m21704CCBE47AE85D078EEF961204EAF62CA0FD5C ();
// 0x00000159 UnityEngine.GameObject Pool.PoolObject::get_GameObject()
extern void PoolObject_get_GameObject_mD4A7E961D00654F015BD8C801CAA50EAE8A4A415 ();
// 0x0000015A UnityEngine.Vector3 Pool.PoolObject::get_Position()
extern void PoolObject_get_Position_m624F5532B87C624DA2C2EFE77DD2C369A2BE44A8 ();
// 0x0000015B UnityEngine.Quaternion Pool.PoolObject::get_Rotation()
extern void PoolObject_get_Rotation_m9FABC65FA77D7C8B805C8D9BBAA489BB36584F0B ();
// 0x0000015C System.Void Pool.PoolObject::Awake()
extern void PoolObject_Awake_mFE67639091B18BAA550C767BABAEBBFC0D120042 ();
// 0x0000015D System.Void Pool.PoolObject::SetTransform(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void PoolObject_SetTransform_m683E19DC14E91561E56C0CDB5FE6336580889374 ();
// 0x0000015E System.Void Pool.PoolObject::OnPop()
extern void PoolObject_OnPop_m4D1E3AFED28A614726F70910F1919029E2C9935E ();
// 0x0000015F System.Void Pool.PoolObject::OnPush()
extern void PoolObject_OnPush_mF38D3C5F324F8326E6693DFB45695FE4EB3FCC7F ();
// 0x00000160 System.Void Pool.PoolObject::Push()
extern void PoolObject_Push_mA4CF561A1E8CA1A7298226035624989239876682 ();
// 0x00000161 System.Void Pool.PoolObject::.ctor()
extern void PoolObject__ctor_m762BD42B6B16C17460CCC68AA14B43833C05EDC1 ();
// 0x00000162 System.Void Packages.AnimatedText.Scripts.AnimatedText::Initialize()
extern void AnimatedText_Initialize_m092CB4B304EE09032587C1297CCF1A77D3275F3D ();
// 0x00000163 System.Void Packages.AnimatedText.Scripts.AnimatedText::Show()
extern void AnimatedText_Show_mADC02B4DBAEF07F4A1921B07EEC2A9D724AF5926 ();
// 0x00000164 System.Void Packages.AnimatedText.Scripts.AnimatedText::Hide()
extern void AnimatedText_Hide_m06459822604E2715E83A10BB42C132F3C95ADE98 ();
// 0x00000165 System.Collections.IEnumerator Packages.AnimatedText.Scripts.AnimatedText::HideWaiter()
extern void AnimatedText_HideWaiter_m21A1D77615D7A6CC45D24FCE0A803ADEE5716D39 ();
// 0x00000166 System.Void Packages.AnimatedText.Scripts.AnimatedText::.ctor()
extern void AnimatedText__ctor_m68AF487B10A84B1A5202B80ADD364A413F661671 ();
// 0x00000167 System.Void Packages.AnimatedText.Scripts.PerfectAnimatedText::Initialize()
extern void PerfectAnimatedText_Initialize_m2889576025488E2585C8B0DC74309B67598C1648 ();
// 0x00000168 System.Void Packages.AnimatedText.Scripts.PerfectAnimatedText::Show(System.Int32)
extern void PerfectAnimatedText_Show_mDC5F6DAFE86C4F4CCE806B66A3203A085D74E865 ();
// 0x00000169 System.Void Packages.AnimatedText.Scripts.PerfectAnimatedText::.ctor()
extern void PerfectAnimatedText__ctor_m36A4759C2C050D66F4C72CC821D7482ABA6A8903 ();
// 0x0000016A DG.Tweening.Sequence Packages.AnimatedText.Scripts.PerfectTextAnimation::Preload(UnityEngine.RectTransform)
extern void PerfectTextAnimation_Preload_m301BCFE4633DF5279906A4F2D50F3C0ED52C352B ();
// 0x0000016B DG.Tweening.Sequence Packages.AnimatedText.Scripts.PerfectTextAnimation::Animate(UnityEngine.RectTransform)
extern void PerfectTextAnimation_Animate_mD8C196704C9697976A06AA4E0AE3175D0651A3B3 ();
// 0x0000016C System.Void Packages.AnimatedText.Scripts.PerfectTextAnimation::.ctor()
extern void PerfectTextAnimation__ctor_m3C82F23072D59B5FC4734107395441CF3276A423 ();
// 0x0000016D System.Void Packages.AnimatedText.Scripts.PerfectTweenText::Initialize()
extern void PerfectTweenText_Initialize_m98468F8C1D1409A86CA81FCBC06CE9E072FF82DD ();
// 0x0000016E System.Void Packages.AnimatedText.Scripts.PerfectTweenText::Show()
extern void PerfectTweenText_Show_mBC1EA688C21440BBC8599D2ED09D0DE6AD16AEAE ();
// 0x0000016F System.Void Packages.AnimatedText.Scripts.PerfectTweenText::Hide()
extern void PerfectTweenText_Hide_m5DF2BE0A3F8389D591BB8BF4CFE39D066A7768AD ();
// 0x00000170 System.Void Packages.AnimatedText.Scripts.PerfectTweenText::.ctor()
extern void PerfectTweenText__ctor_m27E99231EC19A18755E573FDC43CBE475DCEB975 ();
// 0x00000171 System.Void SubjectNerd.Utilities.EditScriptableAttribute::.ctor()
extern void EditScriptableAttribute__ctor_m2C1BB85C54BA2A238B777A039A82C1647D822AF7 ();
// 0x00000172 System.String SubjectNerd.Utilities.ReorderableAttribute::get_ElementHeader()
extern void ReorderableAttribute_get_ElementHeader_m9F925EB93C054EC37AFBA718E69F09B6F80D68C5 ();
// 0x00000173 System.Void SubjectNerd.Utilities.ReorderableAttribute::set_ElementHeader(System.String)
extern void ReorderableAttribute_set_ElementHeader_mAB4E7063220371048A6D571EC307031FF8542166 ();
// 0x00000174 System.Boolean SubjectNerd.Utilities.ReorderableAttribute::get_HeaderZeroIndex()
extern void ReorderableAttribute_get_HeaderZeroIndex_m562F07706A155B2F24C8107708F7ED2640FDE7B0 ();
// 0x00000175 System.Void SubjectNerd.Utilities.ReorderableAttribute::set_HeaderZeroIndex(System.Boolean)
extern void ReorderableAttribute_set_HeaderZeroIndex_m5385D781E070E6C3703D6CD7079EB1C1BA012527 ();
// 0x00000176 System.Boolean SubjectNerd.Utilities.ReorderableAttribute::get_ElementSingleLine()
extern void ReorderableAttribute_get_ElementSingleLine_mFB2DEBCEC012E58268033D44E310A2DDF1719DB0 ();
// 0x00000177 System.Void SubjectNerd.Utilities.ReorderableAttribute::set_ElementSingleLine(System.Boolean)
extern void ReorderableAttribute_set_ElementSingleLine_m8436CFBDFAF2841AAF05FF909D91232B5095BCFB ();
// 0x00000178 System.Void SubjectNerd.Utilities.ReorderableAttribute::.ctor()
extern void ReorderableAttribute__ctor_m204BEA4B27CD13051A4B6376B02E2450F36E9A5A ();
// 0x00000179 System.Void SubjectNerd.Utilities.ReorderableAttribute::.ctor(System.String,System.Boolean,System.Boolean)
extern void ReorderableAttribute__ctor_m2CB65342F0F4EE772BE961CD6B4C2AEBED2C51B6 ();
// 0x0000017A System.Void Kirnu.MarvelousBloom::Start()
extern void MarvelousBloom_Start_m0A7798D557FEB8C7B135BC3B611A83BC62EFC89E ();
// 0x0000017B System.Void Kirnu.MarvelousBloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void MarvelousBloom_OnRenderImage_m7CDD7242C9450F15B8A1ACC7A8AD6DB03D2A881B ();
// 0x0000017C System.Void Kirnu.MarvelousBloom::OnDisable()
extern void MarvelousBloom_OnDisable_m76C8DBA023761BB853709D0A164D56C7CD1C2A97 ();
// 0x0000017D System.Void Kirnu.MarvelousBloom::.ctor()
extern void MarvelousBloom__ctor_mD26EAE618053C702EA140C3446242A36FBD78B2D ();
// 0x0000017E UnityEngine.Mesh Kirnu.OceanCreator::createPlaneMesh(System.Int32,System.Int32,System.Single,System.Single)
extern void OceanCreator_createPlaneMesh_m8C60ABCBCE8ECB4F61B658732CE416CFA1154ADD ();
// 0x0000017F UnityEngine.Mesh Kirnu.OceanCreator::createOcean()
extern void OceanCreator_createOcean_m00580FFA689CBB139049BBA6E47D69E53698E245 ();
// 0x00000180 System.Void Kirnu.OceanCreator::.ctor()
extern void OceanCreator__ctor_m2BBDB32A2A5A1CD8C10E016ED67B79F2B7E50909 ();
// 0x00000181 System.Void Kirnu.FloatingObject::.ctor()
extern void FloatingObject__ctor_m6FCA4BA3AA5B7880009D5B02E00D818DF6035634 ();
// 0x00000182 System.Void Kirnu.OceanWaver::preCalculateFloatingObjects()
extern void OceanWaver_preCalculateFloatingObjects_m7A2C5B1B66B2889450BBDB2E0602F957A775927B ();
// 0x00000183 System.Void Kirnu.OceanWaver::calculateFloatingObjects()
extern void OceanWaver_calculateFloatingObjects_mCD988B0542A059BDA259F935D127E3266A5929A7 ();
// 0x00000184 System.Void Kirnu.OceanWaver::Start()
extern void OceanWaver_Start_mBFFCE5F640A7594C475A391A9157FA1DE781A375 ();
// 0x00000185 System.Void Kirnu.OceanWaver::Update()
extern void OceanWaver_Update_m302066EF6988B94DCA15D58BFCEACC1A32A94F73 ();
// 0x00000186 System.Void Kirnu.OceanWaver::.ctor()
extern void OceanWaver__ctor_m9CDFDEF5BEEBD4512C99382321CFFAC84F6D6E56 ();
// 0x00000187 System.Void Kirnu.ScreenTextureBlend::Start()
extern void ScreenTextureBlend_Start_m44936525D05514E4D1BBC6F437DF07351365A860 ();
// 0x00000188 System.Void Kirnu.ScreenTextureBlend::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenTextureBlend_OnRenderImage_m701736CCC96004E60575DA1301D23CA87E7BB715 ();
// 0x00000189 System.Void Kirnu.ScreenTextureBlend::OnDisable()
extern void ScreenTextureBlend_OnDisable_m39C198B70E029D65EFE071268B7E10935DD760DE ();
// 0x0000018A System.Void Kirnu.ScreenTextureBlend::.ctor()
extern void ScreenTextureBlend__ctor_m25F0415D8942A9DE849F8DA59F3F9300FE48BC47 ();
// 0x0000018B DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mAD40AF255234B54E5BA8F81219C9C30E520EFB10 ();
// 0x0000018C DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m224983D24DA5CC099E78D8FA2C4FC159F6385CA6 ();
// 0x0000018D DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mD3FD501A6776914AE93FF9B9722A9EAD4B2DBAF9 ();
// 0x0000018E DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m60BCBDB46E4B4E67BE9D9984CAEDA8065AC3F050 ();
// 0x0000018F DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mF285AA1CEFC18A119A640DFEC3FC8216E92C7407 ();
// 0x00000190 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_mEFF5F991C07ADB2611F22051613B7ACEF66B9E4A ();
// 0x00000191 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_m6CC119CCB3273C03076FCBDAFB9E0CBD169D2D0B ();
// 0x00000192 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_mFDE51E1609ADFE1E5D1161ED9B2BB55B140DCEEE ();
// 0x00000193 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m0BF4109021D52DE8577A8FADFC069409C0BEC366 ();
// 0x00000194 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_mFCEDB7320442DCD6D7853C62F049BDA0185EF278 ();
// 0x00000195 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mB023CF7E6FFCFC847151C5AAA0CAEA00C80046E1 ();
// 0x00000196 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m4DB434C44B5B2906B99B8926B6FDA2CADEF8A7B7 ();
// 0x00000197 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_m9E98A3E1A4F40F42336B7A83EDE00B4EFA25C4B5 ();
// 0x00000198 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_mCB314AB88AABF23417E5671CB1CC2C35591920AD ();
// 0x00000199 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_mC9A3B16335FD5A5157688FF499FE782E5F5C7A33 ();
// 0x0000019A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_m839C67C3E8C54701B7FC5EAB46B911C0DF192E97 ();
// 0x0000019B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m595AA5252C74166C89DFC53A9052E4DCCEAF1921 ();
// 0x0000019C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_m83F31312715DF4E65F5968115CEDB185A4648B8F ();
// 0x0000019D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_mB024133ED57232447FF4EB17C861AD70A44F304A ();
// 0x0000019E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m56234AB8B466C82A5488AFAE36F7153B82BE2CB9 ();
// 0x0000019F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_m789E726166F500902973D121DE47E178BF772AB7 ();
// 0x000001A0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m3E6515C288D04F55674513677FE209E2727448BA ();
// 0x000001A1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_m080B5E1942554C274704FE5E024EA9CDF6909492 ();
// 0x000001A2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mF68B3F4FE5D0CAAA9E1203C6A3BE2557B2365688 ();
// 0x000001A3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m2A449ECB26A50F9CA43F852CACE4B876A5094CAC ();
// 0x000001A4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_m1E034E8BDB38D7F4F563AB9523891D7F215AA8CF ();
// 0x000001A5 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_mF564424EB231C6B1EC4262BD235C6F4C7B954EAE ();
// 0x000001A6 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m19C5739A636820F747EA76567409F0773A8C7CB8 ();
// 0x000001A7 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m14E79F46BF244846CD0C57D4F0ECCF44943875AE ();
// 0x000001A8 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_mF579D5EC1A7F51586A2C0409B34CC105BF480F41 ();
// 0x000001A9 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_mAF52FC4BCBDE295E6A2F3C297188469D7BE4DF56 ();
// 0x000001AA DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_m4DC3AE19F8B0437E7D26930A2897CC479161A134 ();
// 0x000001AB DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_m0B870F709BF1790B6DB4C8F05A6B7ECE8CB1D231 ();
// 0x000001AC DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_m6A01D22DCB8142450DCC0C43046C2F0D5C39D00E ();
// 0x000001AD DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m981384A20E9E9EA3BF1B105E1850D8DCFA1D1F60 ();
// 0x000001AE DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m0A792EC1128D0C1C77D39B0E859CFFB45A51E479 ();
// 0x000001AF DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_m811BBD8FA9215A129160FC0A7D746E1A3741465F ();
// 0x000001B0 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m2F817AE8F4922B274AF2B1E7BFDD0E5D7C623BFC ();
// 0x000001B1 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m140B9C6CD1E6402158382C49918DB303E39369B7 ();
// 0x000001B2 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m8666A89B76B8D202DC81C1AB12D8B45688212D51 ();
// 0x000001B3 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_mD3871F90FCE6FE5B23E23FE12F7826E44F8A05CB ();
// 0x000001B4 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m1AFB6E6AED804D3261D124D2D1429134B34AFD1E ();
// 0x000001B5 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_m59773AF9A35804797C6C389A516DA63A0DBAC246 ();
// 0x000001B6 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_mC6F53B8F61682B52F390434AFCEC775A4EB49F53 ();
// 0x000001B7 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m871BB15AC97A221A77DEE06B90AD1EB1DD97667D ();
// 0x000001B8 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m10EBF8473875B14ADA1C065043BE47B9C3DE99D4 ();
// 0x000001B9 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_mF7210B505549168AF420CC3453D42DC85ACFE977 ();
// 0x000001BA UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_mC3E5A8737A87F6DD8ED2380957AE33B786DA1401 ();
// 0x000001BB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_m2A623BF72AA0289033B412F4D93DF5317FF620CF ();
// 0x000001BC DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_mE9A75D5B8CFD751EDCB4231A9FBB2EC003752D97 ();
// 0x000001BD System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_mB7D252D24842502558B1DF4DEAC5B08887202DBC ();
// 0x000001BE System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mE88FFEE9E520275AC0F42ACBE27D7F2265E33DBB ();
// 0x000001BF System.Void Fps_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m8154908E5EBDC04454E52BD0336EDE8C2526F575 ();
// 0x000001C0 System.Void Fps_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m9301E500349C0547BF1806EF3F5F85E44D4B5468 ();
// 0x000001C1 System.Boolean Fps_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mDCF2005E0ED44E2FA30D888FC6E4DBB200D42B91 ();
// 0x000001C2 System.Object Fps_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m991F0933C435514859C030C497BFE215E77607BF ();
// 0x000001C3 System.Void Fps_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mD1589E54368884F980B654F6D0D6F9EA82118B31 ();
// 0x000001C4 System.Object Fps_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m7A5FFE01437D34364EA31B57835080D5445942E7 ();
// 0x000001C5 System.Void ObjectRotator_<slowRotationDown>d__29::.ctor(System.Int32)
extern void U3CslowRotationDownU3Ed__29__ctor_m17A9214B26089E4435EECE1B21A67DF4F5ED65AA ();
// 0x000001C6 System.Void ObjectRotator_<slowRotationDown>d__29::System.IDisposable.Dispose()
extern void U3CslowRotationDownU3Ed__29_System_IDisposable_Dispose_m4DFA9A2D32070E835E83960C7D7FDAB16340044E ();
// 0x000001C7 System.Boolean ObjectRotator_<slowRotationDown>d__29::MoveNext()
extern void U3CslowRotationDownU3Ed__29_MoveNext_m32B3D78433F77A6A0558842D557521B24D4E8218 ();
// 0x000001C8 System.Object ObjectRotator_<slowRotationDown>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CslowRotationDownU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D16E3A80E8441D8152318C60FC5165A76AF6086 ();
// 0x000001C9 System.Void ObjectRotator_<slowRotationDown>d__29::System.Collections.IEnumerator.Reset()
extern void U3CslowRotationDownU3Ed__29_System_Collections_IEnumerator_Reset_mE574AE6B10B2689547EAE5E383FAA230BBB63684 ();
// 0x000001CA System.Object ObjectRotator_<slowRotationDown>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CslowRotationDownU3Ed__29_System_Collections_IEnumerator_get_Current_mFEAECB7C867296E284812D052C11B4E15C0CAB35 ();
// 0x000001CB System.Void Managers.LevelsManager_Settings::.ctor()
extern void Settings__ctor_mA8C3AD7B7882E54B943EA68ABC1B9F6654D522F7 ();
// 0x000001CC System.Void Gameplay.CharacterMoveController_Settings::.ctor()
extern void Settings__ctor_mF30AE690F5567D67A1D8B25F1E4EA0445C6D46DF ();
// 0x000001CD System.Void Controllers.ColorsController_Settings::.ctor()
extern void Settings__ctor_m8BEE139FD5A19966C549CF2C3222C4D142B53F7B ();
// 0x000001CE System.Void Controllers.ColorsController_LevelColors::.ctor()
extern void LevelColors__ctor_mE815AC6973DF6F02AF7CF655BF2FDD4CEE2893DE ();
// 0x000001CF System.Void Controllers.ColorsController_ColorsPair::.ctor()
extern void ColorsPair__ctor_m9C15C18D0CBCD5B2F47985489F862282E9C34000 ();
// 0x000001D0 System.Void Controllers.EnvironmentController_Settings::.ctor()
extern void Settings__ctor_mC80D58D05D4AFD0FC46224122A1DB90E94ECB0A5 ();
// 0x000001D1 System.Void Controllers.FogController_Settings::.ctor()
extern void Settings__ctor_m4B0C4C8D0CAF92669710615563BC6CCEEDB47257 ();
// 0x000001D2 System.Void Controllers.EffectsManager_Settings::.ctor()
extern void Settings__ctor_m8A2CFD15CE7F100FFF101D602E5C4D2252BE15AC ();
// 0x000001D3 System.Void Cam.CameraController_Settings::.ctor()
extern void Settings__ctor_m2186E5B3711BFDAF32A7CA8543E7E307EEB68212 ();
// 0x000001D4 System.Void Pool.PoolEffect_<HideWaiter>d__3::.ctor(System.Int32)
extern void U3CHideWaiterU3Ed__3__ctor_m68992F987C20FCC176A92A7060F2C4F10B6B9865 ();
// 0x000001D5 System.Void Pool.PoolEffect_<HideWaiter>d__3::System.IDisposable.Dispose()
extern void U3CHideWaiterU3Ed__3_System_IDisposable_Dispose_mC540425D36AEF2541C599F15BC7601ACE4AB67B4 ();
// 0x000001D6 System.Boolean Pool.PoolEffect_<HideWaiter>d__3::MoveNext()
extern void U3CHideWaiterU3Ed__3_MoveNext_m119AC90CC3E0D3F0E69DE5B9E528225E2D3E01A9 ();
// 0x000001D7 System.Object Pool.PoolEffect_<HideWaiter>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHideWaiterU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB6E5627197BBE623A9D57762A1C75D3416039FA ();
// 0x000001D8 System.Void Pool.PoolEffect_<HideWaiter>d__3::System.Collections.IEnumerator.Reset()
extern void U3CHideWaiterU3Ed__3_System_Collections_IEnumerator_Reset_mA610A8110196292862B32D0F8189925928D8D939 ();
// 0x000001D9 System.Object Pool.PoolEffect_<HideWaiter>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CHideWaiterU3Ed__3_System_Collections_IEnumerator_get_Current_mC3DC20721EF251E684CB1A20B481C60721017F26 ();
// 0x000001DA System.Void Packages.AnimatedText.Scripts.AnimatedText_<HideWaiter>d__8::.ctor(System.Int32)
extern void U3CHideWaiterU3Ed__8__ctor_mFA32CAF739AF4EFF56F66A54E82002BCC4409C1D ();
// 0x000001DB System.Void Packages.AnimatedText.Scripts.AnimatedText_<HideWaiter>d__8::System.IDisposable.Dispose()
extern void U3CHideWaiterU3Ed__8_System_IDisposable_Dispose_m9AC06FD69942E702DB3C174FBF9BC33B1538644E ();
// 0x000001DC System.Boolean Packages.AnimatedText.Scripts.AnimatedText_<HideWaiter>d__8::MoveNext()
extern void U3CHideWaiterU3Ed__8_MoveNext_m259E6F4021D313CF26F2589D449DFBE5FCD1CAD5 ();
// 0x000001DD System.Object Packages.AnimatedText.Scripts.AnimatedText_<HideWaiter>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHideWaiterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63E295693A0C37C97946C6589D758E688B90EF2D ();
// 0x000001DE System.Void Packages.AnimatedText.Scripts.AnimatedText_<HideWaiter>d__8::System.Collections.IEnumerator.Reset()
extern void U3CHideWaiterU3Ed__8_System_Collections_IEnumerator_Reset_mA9AB887CA9EC0E1F152E6D89068B5377CDF792A8 ();
// 0x000001DF System.Object Packages.AnimatedText.Scripts.AnimatedText_<HideWaiter>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CHideWaiterU3Ed__8_System_Collections_IEnumerator_get_Current_mA9DD829CE8E79BBD6A083AD5A221BBAE164773EA ();
// 0x000001E0 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m953E8B35B59142D580B1EC5A3CB48163D94FE270 ();
// 0x000001E1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m18A8356F5FF1FB67DC8B3E62188C603519B3124B ();
// 0x000001E2 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m32C5F6AA875203D30B95F0239833E503B58A4080 ();
// 0x000001E3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m35704DDE3B470E932AE2394E444E236EF73D0950 ();
// 0x000001E4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m2F3D328081E4370B02A8DC65A5595FD7779319F7 ();
// 0x000001E5 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mD5A08FA687B4F3D4ACDE03C5F4747ADD5A85ACE8 ();
// 0x000001E6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m17953C82468E26BC336CB919379BEA94DB403CF5 ();
// 0x000001E7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m42ECD7DED23372FD451B099F33986255C1F05F24 ();
// 0x000001E8 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m96E8793AEB0F5454A98699F2A492CD0C6A6F29D9 ();
// 0x000001E9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m22E508875493477F1B41CE7B93A3327597C7CA64 ();
// 0x000001EA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mE21167957C3A82E6A2212EA06099E19A61C888F5 ();
// 0x000001EB UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mD60F568B3218FB775B0F2F472DEF66F12F240EB8 ();
// 0x000001EC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mF414A2392F00D79E324899FFFD8BCCA32B63113A ();
// 0x000001ED System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mBBFBBB66427C51F249BBFF57BB9DBE3738E70DF0 ();
// 0x000001EE UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m2ABDE00A2CE387D94979FF4772233C32E0433002 ();
// 0x000001EF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m62D514A0CBEE93DDB5E11A83FFEF88F70887B006 ();
// 0x000001F0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m9C45A8A17A10C098C69BBAF60D2639D9ED29A978 ();
// 0x000001F1 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m2CFEEB05691BF628CD9723B772220FBF1B5DEFC5 ();
// 0x000001F2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_mC2B9793EB03BCFC06E022CB10D2AE3D4CDC2DC9B ();
// 0x000001F3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m5080623FCA82C0A8E5638EDE08125692828AB793 ();
// 0x000001F4 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mFA007EE23041AC3DB626E202537641F7CCC10895 ();
// 0x000001F5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_mBFC566A897CBAFAFCC74D5DBCEC136A07F1A5F9D ();
// 0x000001F6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m70D737AA33CF6139F30568CAA4BFAD2E9F1D66BA ();
// 0x000001F7 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m5AADB9E424374D66A929CAE4FE7C91FAAD9C1F92 ();
// 0x000001F8 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_mF51654F8E42E61B6D410192E9C4493BC7B50B7A6 ();
// 0x000001F9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m6054B368E828204FC04AC31B20D57ECE521DA2CE ();
// 0x000001FA UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF3EEFDE0596A75FA4C9B5E3CBB8D27B37286B7D7 ();
// 0x000001FB System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m723F0F3AD18E54882602EF10C41A9138C2094C92 ();
// 0x000001FC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m3C2AC3BA68FE823C5EE4ABF01CF043FB14BC77D4 ();
// 0x000001FD UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mD23FC3413F649B7175F76650D99C969D080A52B6 ();
// 0x000001FE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m2A91553D7D78ABD3E8125820476F8026322DD1AD ();
// 0x000001FF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m922DA2833A1846F6E644BACDA5360882FE3CBD5C ();
// 0x00000200 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m97CEC28FB18E8EC25DE68CBBC9363C1969199D92 ();
// 0x00000201 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m7C9745929EF3497843A96CE489528E972E1696D2 ();
// 0x00000202 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mDB084173826CEF2F054C58D71259B79FABE1AFB1 ();
// 0x00000203 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mD4ACC050241FAF74718A82BAA2FAD799B221D52F ();
// 0x00000204 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_mB98E48B7CD64FAEDBF41990DFA5EDFB84BEAA635 ();
// 0x00000205 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mC4153B76DCBFB2268DBBA69D56E679A477FCBE2C ();
// 0x00000206 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mEC1BFCBC158066EE11442276BAF4904FC8167F78 ();
// 0x00000207 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mE62D5B3135C3EA0973694EDF23FEFBE0879E0431 ();
// 0x00000208 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m179837AE08CE76F84AD7C2DE1C4536FB2BC6724E ();
// 0x00000209 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m946B10547ACAFC05288900D6C8AE8BE29FA8E769 ();
// 0x0000020A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_mC4EA8586148CB5AE824CA16BE35F0C7E15F13927 ();
// 0x0000020B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mD9D66D4A39054D51E9A3A43A3E47FE76D14E8E2A ();
// 0x0000020C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE74B0E96F68D07C8FB2B95F696DDB3F3EF90633E ();
// 0x0000020D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m818E4ABC9402DC49D5DC6F8042472F3D08891D95 ();
// 0x0000020E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m266310E4E7253E8AD0DB494DC917560C7312B6E3 ();
// 0x0000020F UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m54AA9D5E75139ECB7FB5D0BF9518849E9258416F ();
// 0x00000210 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mE14500ADC3593F53F54C4D66F17888A87B84344E ();
// 0x00000211 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m6866C080BE78AEC33050A808BD28DD4FF4B7004B ();
// 0x00000212 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m7C05989ECA103CD60B5CF731D6C7920B1FC985E1 ();
// 0x00000213 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mFB2895AADD1CDF00CFBA835E56A9E13904BAD620 ();
// 0x00000214 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mCDA7BC09C26B4712D699A1FB257E8ACC7F8B2AA6 ();
// 0x00000215 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m9D5E91C0F8BF3135CA706B2D09504A99E384E3F6 ();
// 0x00000216 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mE4A2B4E589B150E943E18A30D33E95B1754E2886 ();
// 0x00000217 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mFDD1B1264FCD4CDDB8A38EA8AD5909CC0FEF92D1 ();
// 0x00000218 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mCE069FE82ADB466CBC0DAFC700BC15C8613B138A ();
// 0x00000219 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD34C0057C888DD82D38BE7593C1E92FF925914DA ();
// 0x0000021A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m5410B8E63F947DE005A20DE7E85E3CCE2D72079B ();
// 0x0000021B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m0D36293A6361A326C0853382366377CE10796BC9 ();
// 0x0000021C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m5729647091DBBF0D726254ABADF2377ADA0101AD ();
// 0x0000021D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mFB3C178CAA653C95BEE3093D575FEB85F8C7367F ();
// 0x0000021E UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD66DD96927DDAF369C64AE446E76D6F0F03AE0F5 ();
// 0x0000021F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m7189572DD336DDE6EC85F27770F731FEEAA2FD50 ();
// 0x00000220 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m7D1B522F01E0419DDBA693585300D0B01AF984FB ();
// 0x00000221 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m7DC62E4EE195C844218B14EB4EF6E5AED8989D0D ();
// 0x00000222 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m10B5380A3AA5CEAE992081A449EE1F74EC79B15C ();
// 0x00000223 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m75C045C8DEAC70D81B02EAFC1EA366AE0A7AA8F5 ();
// 0x00000224 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mF72A07116DE5256A691DBAFB766E637F55C4EF45 ();
// 0x00000225 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m8EABFE51EA9393CC2EC7F0055F02DED35D3B9186 ();
// 0x00000226 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m7157DCBC4845023E76ECA85BE6C3F81EFCD04239 ();
// 0x00000227 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_mADD1E688B9294ECD2A7CCD79F78CA0CD68E055B4 ();
// 0x00000228 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m62A33BFDF3873451B14AE3EBA0791AF52D082F15 ();
// 0x00000229 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mAE01B70E706B6F8A90EA06CCE17FC489F1FC71D2 ();
// 0x0000022A UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m036AB1FD8ACAAB27669C6F82B807258A7CF95161 ();
// 0x0000022B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m3F1A0A60B77F9C9AADC0C9BE1969FD929AEC15AE ();
// 0x0000022C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m143FC1CEE9576076B1C3F954E13D9F709DE22292 ();
// 0x0000022D UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mA1CDB05B94AFD8D48E1B1DB4B6F2F53FDDD274EC ();
// 0x0000022E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m2F2781A19452E8F74222AABE3617555A4CEA2DC4 ();
// 0x0000022F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m3A3C7488BD686D2417811F19408B70D93B0FA02F ();
// 0x00000230 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m23ADE1EDABB1FF436DCB15B4D2956341A403F86F ();
// 0x00000231 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mED3FDE11B0B31284228C7D6A01C66ADFAD06D05B ();
// 0x00000232 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_mC9FC42C7A712D81D3E1FFD069C16C866CF7F3A2C ();
// 0x00000233 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mBD11B2BA0B8A07753156C1F9CA22B33A041F6A28 ();
// 0x00000234 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m560D14C7C7B82350B44D745D7DB7AE161730A086 ();
// 0x00000235 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mC30B5380B8DE2DCAC55DA68241D5EF1266F79909 ();
// 0x00000236 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m3336D670200C9F13D96D7F52F123920132234850 ();
// 0x00000237 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m5B9935C76D1779CA20BEC5C8434C34BDF9ACA7EF ();
// 0x00000238 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mFAA2F463E2CED9DE0BA520D44BDC7A64ED56BC6E ();
// 0x00000239 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m0D9E7219678950432D0E913F31C5A3E3B3D1F449 ();
// 0x0000023A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m41FE20829CE35CC646EF346A2ED2D42A8F7FFF6D ();
// 0x0000023B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m46AB05BB46F4343B5372269801C929C6BEA4E78C ();
// 0x0000023C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m8786A94537CDE17C192F816195A8D5095A147611 ();
// 0x0000023D UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_mE69AF155BCC63288298C03F1FDE8264894DC5112 ();
// 0x0000023E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m34D4384708B00929EC0936A3D9818FBB9320124A ();
// 0x0000023F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m430D95F36B8C311BCA555E6C9EB4E3D5B6CB52DD ();
// 0x00000240 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m4B50B77000E1A5B0959F7EFEC780C43A2B3C21A6 ();
// 0x00000241 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mE79549F5C6F07819B8FCA4A3670548B94B794980 ();
// 0x00000242 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m03AFF4E74AF489641F31A7ED7BB588DC48C86B47 ();
// 0x00000243 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_mDFC279CC6199107052C6C57F7C80069AF08B727E ();
// 0x00000244 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m69F269F657FA4BD2821E0451B328802CA74E5E4D ();
// 0x00000245 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m44794063561EE5542E8D44B8047DA0EB476D9D99 ();
// 0x00000246 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m49CBBBEC21C5C4D3517ED220CE99FECCA32D8E17 ();
// 0x00000247 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mC23A71E0D945D05C253DAA3C6B29F017AC3C8B7A ();
// 0x00000248 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m31CDFCCAA9BC664B12E06164FE6FD3F6BCF1CE10 ();
// 0x00000249 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_mC0172F2115741150E114BCEB8C6366E8DC05F0FE ();
// 0x0000024A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_mC68427660102A2FD7D41ED52EA99141A48A176E5 ();
// 0x0000024B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m5119796185D9945B3DADE92E11D912791044F5F1 ();
// 0x0000024C UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m7AEADCAF76E9378EF16998AF06C981E238317A14 ();
// 0x0000024D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m6F73BC7514BC81F58222707B2FEA9F1775452C1B ();
// 0x0000024E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m266E0D445578A8CF42FF8E6E28F8DD0D5816B6C6 ();
// 0x0000024F System.String DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m8450222712B70BE65D184CE038DDA30929396E09 ();
// 0x00000250 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m5EBCE38D43719324BFF117A085F469D6C7A53A4C ();
// 0x00000251 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mF7EE64DF2C6519BFCE36A53802C870392DCA681E ();
// 0x00000252 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m5C210BC6DB5E81E37C5DA7F0ED05DCEBF88A838F ();
// 0x00000253 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m001CE2BECACB83DE0BB4252DFAEFF93D51676EDF ();
// 0x00000254 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m029E260AE14F07692FEC152AA854A3E7E818F5A8 ();
// 0x00000255 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mD045F88560E9C38E36C045962A24AEEE7E220146 ();
// 0x00000256 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF9673E803B1ED13D3937EA5093B07828944B0D33 ();
// 0x00000257 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m2D3EEE4A819B3C837A38B0F228978C5DECA02157 ();
// 0x00000258 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m769FD067F1DD156E6737065978D723626F684DD4 ();
// 0x00000259 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m9E000E92EC974F7B26E70A9DB887A7B5ECD13C4A ();
// 0x0000025A System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mCB2FA585275686213140C65F568AF5B895DE515D ();
// 0x0000025B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8CB04FB886F55DF914E092EAB3E88CADA5E4BFEB ();
// 0x0000025C System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m70E3E83723C4C320D2E78FBB32E3C5AB9E483413 ();
// 0x0000025D System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m71E4A32D334F5A1DDFE2DB998E25634EC8C9E0D8 ();
// 0x0000025E UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m45117726269A56BEFE7511E185A5EB5DC6614C08 ();
// 0x0000025F System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mEC4AEA77E8E47513A16390421DFE3814DB87DFBD ();
// 0x00000260 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_m30EF66A11D003F93F4CBF834A856E7298D5955B0 ();
// 0x00000261 System.Void DG.Tweening.DOTweenCYInstruction_WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_m69692E16010A98F06E13B8668A2C9AC26E470727 ();
// 0x00000262 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_m8E68A912E510F991CC524EA2DCB6634BE7F7DF65 ();
// 0x00000263 System.Void DG.Tweening.DOTweenCYInstruction_WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m111D38D621831BAE9D00CCC7F195DBC48A9483D4 ();
// 0x00000264 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m410BE82C7AEB9A7E45E5E07EEBDA1D2E2CEB50B4 ();
// 0x00000265 System.Void DG.Tweening.DOTweenCYInstruction_WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_mD127E54E87A255CD19BB9539CE786DD6953C5719 ();
// 0x00000266 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_m6A50A75A89879252500ED70BD15C0C04CF6DC2D7 ();
// 0x00000267 System.Void DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_mE11D912E8954AB448C21081560A6FBB9D974B7F3 ();
// 0x00000268 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m78A4DAE5D866FA838D2A843113F275E169B03A30 ();
// 0x00000269 System.Void DG.Tweening.DOTweenCYInstruction_WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_mB0DA06151DE0C3690CCF8A3117189FA2800CB3FC ();
// 0x0000026A System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_m218C9D65E9141136A148DD636B155272308A9F5E ();
// 0x0000026B System.Void DG.Tweening.DOTweenCYInstruction_WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_m9088C849D74DBB8D2E12FA05AD5A2E17AA48E77B ();
// 0x0000026C System.Void DG.Tweening.DOTweenModuleUtils_Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_m8BE0D531D59E8E8D9F092FB6C9F881C98BDE429B ();
// 0x0000026D System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_mA2B48BE3FC3BB297B8227F0ECA26BAE6B49732A8 ();
// 0x0000026E System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m5C3BC90BFBC15B8787E33728CBC9FD4DBAF10DE6 ();
// 0x0000026F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils_Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_m5026DCBFA244A145B5076E485DA4643EFA331B42 ();
static Il2CppMethodPointer s_methodPointers[623] = 
{
	CameraOverlay_Start_mC9B778CBC5AAB94083AC40A1BABC95FA6AB17127,
	CameraOverlay_Update_m973486C41CC119B454D3EDA386FB5551C4C6264F,
	CameraOverlay__ctor_mACDB65E8D2A652A0BAD046531C1A376D9E593EB4,
	CubeMover_Start_m570A20598D617A3BD6E5D1A330B19A44A8B4D950,
	CubeMover_Update_m0AAF543E15B3D4622F114123F21937986B450D3C,
	CubeMover__ctor_m6E0ECE3CBFACAB6CCAA098470E9F52AECE69D980,
	CustomLightingManager_Start_m5ABA9134C79F1FDC3C52D374BFFEE9D3B76457A4,
	CustomLightingManager_findMaterials_mE2EA9ABC1F6AC40206049F43664833DA510848C5,
	CustomLightingManager_updateMaterials_m4D5E86D2A03751D0B042E9F46B45D8A1A45884E5,
	CustomLightingManager_Update_mF9137C459BEF1D9DAF01C6BC010756C3D19695FD,
	CustomLightingManager__ctor_m7FA3AB09E209D25F594CDCB7F209265004BB5D6D,
	DirectionalLightManager_Start_mC4A8F1038A28506BA8C2D1572879F33C97BC1096,
	DirectionalLightManager_findMaterials_m35E9BA77334513D3EEFD8E58FFCA878E8BADDD72,
	DirectionalLightManager_updateMaterials_m083F16E64F71C199D82E6030B1EB218B46C479E6,
	DirectionalLightManager_Update_m6418256EBC4969D21B7DEF052DC83CB0AAA82C09,
	DirectionalLightManager__ctor_m95AC2244142A3BDB8FCDD04107066CB46FEE5944,
	DisableRendererOnMouseDown_OnMouseDown_mF628E85DE4E59C6C6CD3B1DAD636866233165318,
	DisableRendererOnMouseDown__ctor_mD1FF4B17391A1B8FA7D24319C050916DE2B779E8,
	DistanceLight_Start_m5F91023F4416AD4123358CEBC33555022F29B688,
	DistanceLight_distanceLightChanged_m460F9CAD9CF0DC430160B2FE2901AD2CF43B561D,
	DistanceLight_checkMaterial_m188A61EC3ADF3EC7CD6E3B73BC6863026D367A1D,
	DistanceLight_findMaterials_m48EAE9347642A5FAE228583B3B4C3EFB19114470,
	DistanceLight_updateMaterials_m3B9EE839CFC8BF80D322D63EE84C0108B6347CB6,
	DistanceLight_Update_mF0D5B24E08A5DB9780B256F61F4F2FEE66ABAB77,
	DistanceLight__ctor_m02861263920E8908B49AB81FF1899EBC77468EA2,
	DistanceLightMulti_Start_m716C5AA49D99434A906735B13B8D36F40D4FE31A,
	DistanceLightMulti_distanceLightChanged_m68FDDB44C0E9482A45E5E4FEA169B1267A0C2E6F,
	DistanceLightMulti_checkMaterial_mAAB724E632DFC86F70764EECD27B56237B2F2900,
	DistanceLightMulti_findMaterials_m426D4C33D58241F3036BC4CFBD5BF0C10A704F88,
	DistanceLightMulti_updatePositions_m3EF921EE4C6806192748F745B9231B58FFE05444,
	DistanceLightMulti_updateMaterials_mB7B8CC867A8E02C9ED850D697F58CC52A32B06C8,
	DistanceLightMulti_Update_m55FCF69457310E685C6391BCD5677F981EDDE649,
	DistanceLightMulti__ctor_mD5BB0DE9BDE294A1E34974401DEF3C5D4E1FE14D,
	EditModeGridSnap_Update_mB8AC12C90FF3381863F4F7B879F1664FEC762852,
	EditModeGridSnap__ctor_m4615ABB8702E5B9CE946C80DE4A87437CD2849DD,
	EnableCameraDepthTexture_Start_mB06120463A9270FBABAA5A04A5877C9F7D77FBAF,
	EnableCameraDepthTexture__ctor_m78491BF2B304F7FFDFA3C0FB2607B0EAB14B19BD,
	Fps_Start_mC7AB37773117CB2EDFC9C80D5A9AE19E454FE5AB,
	Fps_OnGUI_m95053FF3C95E986C136935AA7B68C8F06B839A87,
	Fps__ctor_m42437A4084EE57B3A5C1AC534E09C0C3586BDE15,
	FrameRate_Start_mFE1A1664BAD62484071C323578428CCF0BC10BAB,
	FrameRate_Update_m4324AAAF07D6B09E953C27E1B412BCD70058F76E,
	FrameRate__ctor_mC75295DC110A4DEEF16C0050CB2F5FB937A42A67,
	DragDirection_addItem_mB3B712CE7A7808A7A82D22A18DFB5DC7190F4D26,
	DragDirection_getDirection_m85E50536F04175E8FF3F4A5988D1CA9A4D5E1D3C,
	DragDirection_clear_m35FAAD482C8CE13EC88FB2EED2C5A2AB56E2555E,
	DragDirection__ctor_m83CC82FE715AE59D2BB225420E8A36AA97AE0FDF,
	DragDirection__cctor_m4ACDE286136C008D27F6BDBF5EF42435C28C4C8E,
	DragForce_addItem_m6F1A8C2381285976DBA9FBD44AB6A7166A467FC0,
	DragForce_getForce_m793157C614B698F7EAA679786D8708FFCF532240,
	DragForce_clear_m22694A1420B3C6F6EECE81566A37F567CF7252DB,
	DragForce__ctor_mA54B2865F0FD575C6B30235FD9A61A949C564885,
	DragForce__cctor_mA616B3EB92ADA1BF7344F58A195F395CE635A98D,
	ObjectRotator_Start_mD8C03B9ACE581E5444A717E90F5E537B60C3668F,
	ObjectRotator_OnMouseDown_mCBBD95E7E3FA3FFA9ABA2D8A2B04114B78CFF3E9,
	ObjectRotator_OnMouseUp_m894FEA80B05E05A2B35F2572E8C2FEC1E9C97035,
	ObjectRotator_OnMouseDrag_m99057105141B9C61CBC9EACC218B991B52DF27D3,
	ObjectRotator_rotate_mAE96F20FF82DEAA8253B745FA22A3961811C3665,
	ObjectRotator_setRotation_m1F7E0B59D156A43ECF846D5E5D5E2578364556B7,
	ObjectRotator_getRotationForce_mD08364BF6146802E71B9FA29B6C7E9B2B857FBD7,
	ObjectRotator_getCurrentMousePosition_m6E5EFF7951556BA3BE61EFA272D85AC9C5F8A936,
	ObjectRotator_snapToRotation_mA618FE00ECEDF4AD00963FBFCEB5B2135ECA7214,
	ObjectRotator_snapToRotationWithDir_mD0FBF6AAF5F62BF314D17F06D7E0FFF8AD4A5CEB,
	ObjectRotator_getSnapRotationFromDirection_m81C4942BB107F7010F9F01061FE728858B1D2259,
	ObjectRotator_getClosestSnapDirection_mD80AE39E7B9F76982E13126913043DB8D420FE1D,
	ObjectRotator_getClosestSnapRotation_m83E259C0AAB43060E2492BBAC650FFD704611C7D,
	ObjectRotator_elasticEaseInOut_mAF0874FBED9B404CC30886CDB95C820A7C3F85B8,
	ObjectRotator_slowRotationDown_mDEB957419D12E918A3E3CC246AA2B1DD07DFE291,
	ObjectRotator__ctor_m54BA472CDB703812D24B017F92EE629C57C2D55B,
	ObjectRotator__cctor_m2E967B166231BAFBD3C854944859B0B9846009B8,
	SimpleRotator_Start_m744E4CE23BB83905D331B6A96F733802F8DF5C27,
	SimpleRotator_Update_m8A71CB924200645D8BF84F3232253C647D3A1700,
	SimpleRotator__ctor_m248AFE6DFB960EE28988DF90765F8EE22B29344A,
	Splasher_Start_m4BBF1A524A8ACC699B3B64067788DD890066FAFA,
	Splasher_nextValues_m9B09224E42B015F5CAF04B5547C059FE2C847AA8,
	Splasher_Update_mFBE425A06DD5CF2B6461DFA4CBA60E5EFDFAFD32,
	Splasher_createSplash_mAE5CF3D886863A8B5563971C6EA683355121FD1A,
	Splasher__ctor_m72CD0BA5E8FCEAAB3216CD4D0AD488917D81DBBA,
	UVHeightGenerator_GenerateUVs_m19019A8493204D5AE90D724525F7256EFC2EB4B5,
	UVHeightGenerator_calculateUVs_m74443DA34DEB58708CFACA07DFF9CC58A17FE8E0,
	UVHeightGenerator_Vabs_mC274D6FB1C4C96A81859CC9B8457A3D3C13BBC04,
	UVHeightGenerator__ctor_mB09C8842DB7703D32AF45D6521EAADD3F149DC63,
	UVLayoutGenerator_GenerateUVs_m09E1B1D11D73E7950EE17704B7B4E3D372A55EC4,
	UVLayoutGenerator_calculateUVs_m454B507E34C60168FC213396604ABCBC9917F33E,
	UVLayoutGenerator_Vabs_m586DF019306DB4F4FC0A5F077292893564B0343A,
	UVLayoutGenerator__ctor_m68711D87C70C2F91B13F4E03D49CF99AE85623FF,
	CameraLogic_Start_m3182E65DE6B1B969ACD025225E7986AA5D138EF5,
	CameraLogic_SwitchTarget_m7527A4DD144BBEE099D8601BC963F1E41F0E25BF,
	CameraLogic_NextTarget_m8BA15C99203E83F2392EB68E9CD286B6AF170EE6,
	CameraLogic_PreviousTarget_mF370A3DE9C4B270DBCCAE7038E21F993A31D6564,
	CameraLogic_Update_mF99E78C85C3F976A437FEAFE8AF7CA4B143A2E57,
	CameraLogic_LateUpdate_mDC5EA32B5AC8A556267CCF2F262D81CA75E1A99B,
	CameraLogic__ctor_m16E26336513324615189BC600743C3353EB44A51,
	CharacterEffectsController_ShowPerfect_m6F5C2661E8BCF4AB6DA1C21EBDDBAB3E3AFD0D33,
	CharacterEffectsController__ctor_m435261A99E38B43C98D31DB899D4CD95FB9103D3,
	ScoreManager_get_Score_m67AD6782CB6F36C23CB29ABB02B4B0C30E2C40D9,
	ScoreManager_set_Score_m4DD1EAC6BCFC820F2B16F210C00EAAB6C6E4BECA,
	ScoreManager_get_BestScore_m264AC512BFFE95DD179986C12A7BFD8AA7C0F4AC,
	ScoreManager_set_BestScore_m3C7F4995025C7C087CF09EB3524FD9B5C1347BE3,
	ScoreManager_get_Multiplier_m09434D375EEB5C6BB124365A2C6098F9775D1808,
	ScoreManager_set_Multiplier_m202CF780B2380DABBE6BBAE383B187217E8958CE,
	ScoreManager_Initialize_m5D38C63945FFEB8BF5311C18B82C8AC57C2B2D8F,
	ScoreManager_Add_m84AF805BDB17F1DFB19EAC83146B3FBBFEF1EDC4,
	ScoreManager_Add_m66E7520C9D936F3933546242DE8FC54128969FB8,
	ScoreManager_TrySaveBestScore_mC822CAD6B86C3F8DA4BDEB0A9F0A26D31A9BC654,
	ScoreManager_Reset_m23F7E7B0959CAF8614B9DFC1AC0CAF2015C42483,
	ScoreManager__ctor_mFA7EC3F474306DB06EAA63B19DEFA71F4E00B9E4,
	AndroidVibration_Vibrate_m1F2FD1AFB0C4ECB51BFA238AAD7DFD31138FF261,
	AndroidVibration_Vibrate_m8108BECB9EC7A829E6E82B954007CF1B44ECD9A6,
	AndroidVibration_Vibrate_m12D2CB4A920E85DC9AA554931FE0E733B5B527ED,
	AndroidVibration_HasVibrator_mA885636E8951BFD8C32888CA353C5FE03EA3A1FB,
	AndroidVibration_Cancel_m9AEFDF629BE64051C3D4D877737F8B8BA1C6717B,
	AndroidVibration_isAndroid_mBEEB3605503CED75911A510D4AD4806D447CDE9F,
	GameInstaller_InstallBindings_mE6C40F4F868E9ED79A497A9F74493E3C5D618BBA,
	GameInstaller__ctor_m7A805F750419749CEB11F2CEE1F40CB2DDF5DCC1,
	GameSettingsInstaller_InstallBindings_m687E5D756E89B55934FB0BCC4E0636972750AB4B,
	GameSettingsInstaller__ctor_m05CC86031A7F7FD97661E07409BFEAFFCB1CAF72,
	CharacterSettingsInstaller_InstallBindings_m8661901FEE8E8838DE4B3E7CBB022880F546C7EC,
	CharacterSettingsInstaller__ctor_m69447442475EFB8374304EBE632A24254AB43407,
	GameplayInstaller_InstallBindings_mEB4CCD4634EF923F48BFEFC02289FB256CB647CD,
	GameplayInstaller__ctor_m1B639320411928209C40EF1AFB61B287FE94AC06,
	GameplaySettingsInstaller_InstallBindings_mCC74ACF8CD4DD9303E50D9E36739B2ACBE85D066,
	GameplaySettingsInstaller__ctor_mDF3D1A1A9ACF9FF32885BEECBA1C4C19B3EF796A,
	UiInstaller_InstallBindings_mE1749C9FDE46FCCBFB401D5AB35BCA9D58717246,
	UiInstaller__ctor_mC759712B46EC7611C402410863430B256BF9EEA0,
	APanel_Initialize_mC27E64C4C080AC2A4FEB792669BA6D87DF71B526,
	APanel_Show_m1E17F80E7790CDF8285D23F7F77281A0E65F0592,
	APanel_Hide_mEA987CA1D6C59CF472B62CDBF2F528FDB2C0B934,
	APanel__ctor_m13C84A4F1417E607427838CC5331A85629210E66,
	CompletePanel_UpdateContent_m17F42E7C9B66B6B4DABB8ED0F7273F208B5BB452,
	CompletePanel_UpdateText_mAEA15174FC19B5D5137985307A57E47C95CA566B,
	CompletePanel__ctor_m3EA8F33F1588DD8A5B0D7DBE663E687EBDFE2CA1,
	GameOverPanel_UpdateContent_m408AE77CE043528AE817EDADB96471B113C34A33,
	GameOverPanel_UpdateText_mD75C6DA7F5147D089185B6392AE455EC9A004C16,
	GameOverPanel__ctor_mEA66650214E894E76AF04C4680D8EF273553FAEE,
	GamePanel_Initialize_m2CB57DE055ACA3C6A5B6035A23A184B13D71090D,
	GamePanel_ShowMotivationText_mCFEF23DFF1E58387B44CA7D59FE327F21325346A,
	GamePanel_Reset_mFBB16C3075168912F33CC2F82707FEE3D05F87D2,
	GamePanel_UpdateProgression_m75C7411159C917C58004EE865D6831769F45690B,
	GamePanel_UpdateProgressLevel_m72DE951C7553C9FA29E968869448275582AFC3D3,
	GamePanel__ctor_mB7F94623EE13AC71D62AD9A54D6660E4F53C1A0E,
	MenuPanel__ctor_mDCC9E913ED6054BC65292999FB5910A04A3D9788,
	ProgressBar_Update_mCB439C15888996CFE7BEF8F3D21CC156CF4D1AC4,
	ProgressBar_FillSmooth_m4227F6605D50755C5623526381D19D2232ED015B,
	ProgressBar_UpdateFill_m41549F9624D27646B0E6D4C5733540E33F6847DF,
	ProgressBar_UpdateLevels_m4AC2CC4E7F709AB1C7AFD0F7A6C59F466DA22BF6,
	ProgressBar_Show_mF35ACF511FA286256FA873A1FC637C66E5E6F467,
	ProgressBar_Hide_m008E27E5B0DA5CA66C23AA7A8139E505835ADEF3,
	ProgressBar_Reset_m90C606E7B9DC72EB462EAE98E1C583F1F1A8B4EF,
	ProgressBar__ctor_m2D16B36718B42D1BB55FDF14BA40DA26363759A0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AState_Initialize_m570936E7588BBE0E27189B826EC543D4BEE0893B,
	AState__ctor_m3346E6690A7E25A7F60FD2A59102BDBA479398C2,
	CompleteState_Enter_m70B6CD481FCA2C1A312EEEA89802A4CCA057BE71,
	CompleteState_ShowPanel_m8B944EAC8D86F0A24042D14F857D9C85F4062A1F,
	CompleteState_Exit_mACE98F1A0FEB22A10E09BFE4FB33942D4208AFE5,
	CompleteState_Tick_m49C78A5D6F61436D52DBC7496B709D0EE83FF536,
	CompleteState_FixedTick_m2742FC6DC7EA0BAA34CB60EBBAF10FD510279EE5,
	CompleteState_get_Name_mC7EF31549E2950D1AB85837656D3FCABC5EFA8C6,
	CompleteState__ctor_m1A6977601A37985D4BC66A03865FF48DBE179033,
	GameOverState_Enter_m08D99C85675060A41600B86A5FD8A1EA45726435,
	GameOverState_ShowPanel_m5C1AC4D06E6535AADFF2D278280FC2C7456E2E2B,
	GameOverState_Exit_mCF8524311EBAE70DB0E96017DF5AD1A327A09CB1,
	GameOverState_Tick_mB2F2AAB3F381471B6ABC0B27C03D960E3026710A,
	GameOverState_FixedTick_m745ADE789B1200A75B1D7CD1B49B07ED8B768D33,
	GameOverState_get_Name_m13FEAAD38B9D23F29C48E535E7B92E911411E8BB,
	GameOverState__ctor_m3FA2CCC87D34B231FE1267C6AF26FE6073F80310,
	GameState_Initialize_mD0714CE299F41175DFE16CA19FC9AD01ED5EFDF6,
	GameState_Enter_mA7A9956094B9F6A70BD817E3242CB0BA81540F39,
	GameState_Exit_m72D3EEF58BDCA4DF8B4906EAEEC2C0BE53F69DA1,
	GameState_Tick_m759B141FCB4171D99AD581AD6CD8264E7D07880F,
	GameState_FixedTick_mADE597D74FA0E5E78EF48EA68AE3F920E6A3DD03,
	GameState_TileLandedHandler_mDB1C6D3127C03724735FFFF67DA4781BB7E0725D,
	GameState_get_Name_m7000C8EB4A4579150E4606CC0E0F2D4FA2CD070E,
	GameState__ctor_m21808F8C9BC761BCFAFDD27B60FFC1AA6DB58A65,
	GameState_U3CInitializeU3Eb__6_0_m53072A32A4D6FAA63DC17F44C8483DBBACB442F0,
	GameState_U3CInitializeU3Eb__6_1_m2D3C53F283B3458F2712E933E0BCDD5FF25DB915,
	GameStatesManager_get_Instance_m25708E0AF02A2BE401FF9EB23686752C3456D2B5,
	GameStatesManager_set_Instance_m2348E6E491ACF2FC7569280496AA46EB4BB2F694,
	GameStatesManager_get_TopState_m94AE7AD29894A96791FA06EE710CA267410261B4,
	GameStatesManager_Initialize_mD2AF1BA9B70F158E462AC373D473A7712AB45537,
	GameStatesManager_Start_mA26409D35499F51BE93266121D90AA7BD0777ADB,
	GameStatesManager_Update_mFF21E7011A6BF997B711F9C60FE54B0291FAF7E8,
	GameStatesManager_FixedUpdate_m46F816288497A2CA381DB21AA8D0B929B533BF48,
	GameStatesManager_StartGame_m5CB6004BC340660F94591B637E4F8DF3A829A12A,
	GameStatesManager_ToMenu_m662DB236586D5D7EBA71FEC98B61D6A9E73E349A,
	GameStatesManager_SwitchState_m2EB61F21296FD884EE659A1305729F69CDDB7E0D,
	GameStatesManager_FindState_mEBC5C966AE0F55CB426013336397C10A439F6D25,
	GameStatesManager_PopState_m35355865F02EC0B431E6324C7BD293A19D232749,
	GameStatesManager_PushState_m52FAE643766A075BABEA0C5062490BFE33C613BD,
	GameStatesManager__ctor_m7DF3694EA6442E23885D846988F8A9B0500F9FC2,
	MenuState_Initialize_mC37BFE060CE95EE3B464BF831F98B7D84B3C22B5,
	MenuState_Enter_m07640E719CF0DED99917CC8BA186A64C6DC05DF7,
	MenuState_Exit_m27417206C5C425607D0CB27E3AAACEDBD91A01E4,
	MenuState_Tick_mB72FF3890494C65D341E83BE68F00E9CBACD6911,
	MenuState_FixedTick_m579F348793D631A33606989B860B0A8FF71339B7,
	MenuState_get_Name_m4776E385588A20AD09E97D541C2837CB32F99F22,
	MenuState__ctor_mC4073E3C9236E32B338CF7F6EDDF39E35A9F4032,
	SublevelCompleteState_Initialize_m90DF53E0C145598BA065BE9F804ACE3AD1776A84,
	SublevelCompleteState_Enter_m8696BE10F9ED26385C0EBD77E58C453D46642542,
	SublevelCompleteState_Exit_m5FB6D8A5AD4E286AFA4A2E722DCB6488736730FC,
	SublevelCompleteState_Tick_m2AD065D622C5AFB3C20BE6A2A0263A2F93E72CDE,
	SublevelCompleteState_FixedTick_m3A3D7EFE44952949A4CF14CF0733E841652A9CC1,
	SublevelCompleteState_get_Name_mF84C041A7423846C608D3E54EB45F314782CC031,
	SublevelCompleteState__ctor_m1608C5026256CD09220093012B19447204201321,
	Bounds_Lerp_m34286B19C815FCF4C83A2D72F6CBF7EF782607C2,
	Bounds_InverseLerp_mF1083736A8BC1491EC3B9C620C3A0213915553AC,
	Bounds_Lerp_m13DFF59389B2F90AB609177B1A287AA784CABF48,
	Bounds_InverseLerp_mD9C636F05360A047F318C9330038BF59BBF6E4FA,
	Bounds_Lerp_mAA4FAD37539B1D079CBF75133BF3A4710F5AD063,
	Bounds_InverseLerp_m55F7B3E11F377AB1EC448FEAA5DB18B13C0B9E2F,
	Bounds_Lerp_m66AA0601E31BB65270178C06C56A8AE67392D4DB,
	Bounds_InverseLerp_mC0564AEF339A3882B34FE1AD32024F1C8D58E9CF,
	FloatBounds__ctor_m64232EFD95559216A26784D6098C337300DC598E,
	IntBounds__ctor_m24E4006218E0BD1900E824CD370BC4F43C8879A4,
	Vector2Bounds__ctor_m33D91F897BA6C1DD2D45E7CEE6A10CE72AC33FD0,
	Vector3Bounds__ctor_m6579707B250DBB743D4FDA459801A3C21C130B91,
	NULL,
	NULL,
	NULL,
	DataSaver_GetIntProperty_mD2E9AC95C603DBA4B3BF0C0AA9E5D40B89B4CF13,
	DataSaver_Clear_mA777DD1256F6217433148F78E7E00CF156CA9824,
	DataSaver_Encrypt_m4638014303EB3E481E24AE53F5D84689D12E9F5A,
	DataSaver_Decrypt_m24ACE7043E0CC4C1AADB08C9E79E1794C6EEA42E,
	IndexNormalizer_GetLoop_m94E9FC9C50F6D3FB082356470DA05DA20342CE30,
	IndexNormalizer_GetRandom_m0E6831B541673AA31313E2D39231435AD02ECB79,
	LevelsManager_get_Level_m08CB2E5521CE685B66161E7D0C675F2D0567F6E1,
	LevelsManager_set_Level_m769B5AAD411EE858B0565AE088394641751058C5,
	LevelsManager_get_SubLevel_m93FF655A7FE26A08EC26077C1F2B2660287906B4,
	LevelsManager_set_SubLevel_m560C57125B6EF618E0580492AB1E6A3B15C1BF69,
	LevelsManager_get_Step_mD06868D3025B8180F3B04691F60B1FC038545B2F,
	LevelsManager_set_Step_mAD7098F023D00E75843BB325C94E872AA559F8BB,
	LevelsManager_get_Progression_m708A0FA6AF9B029D5BB718459144782C19A27920,
	LevelsManager_Initialize_m3648F2ACEB2513C88F0A7B7D51A075A05969FCE4,
	LevelsManager_NextStep_mB0B5CCACA48BEE7A27F0BBA925F5C4628A8FF8D8,
	LevelsManager_NextSubLevel_mA3878E21A2D0C43124AE1DBEE0428EFD04E99476,
	LevelsManager_NextLevel_m57755737CAB78CC2B7CA527367EF7E04617681AD,
	LevelsManager_Reset_mF16722E0C1E3F3AFBAC1475B6675083477692CAE,
	LevelsManager__ctor_mD2EDB80C1971C44558046A6F83E4CF40AAB39C02,
	TilesManager_GetProgression_m01C3A7A88C4F0A7C6AD70439CF1C1F4FC7DB58BE,
	TilesManager_Reset_m70C510580D819F6CB120CA42BB0A93E417B56B56,
	TilesManager__ctor_m4B6020293F135B3C73F48E35EA9811DC338738AC,
	FinishTile_PlayCompleteEffects_m8C3F4419AECBC07AE7D8937EFDBF3FF0D4D522A0,
	FinishTile__ctor_m113F0F8E86C69E2BEB0A0ADA0C224254C56158C0,
	CharacterMoveController_Initialize_m57249762D8D876E29EA24CD9E51C368AF12A57CF,
	CharacterMoveController_Move_m6F6E2696A81FE90614FAC2AC24383EAD278E6851,
	CharacterMoveController_Jump_m258CC21069974A46BCE2CE02DF6CC7CA4DB46869,
	CharacterMoveController_Reset_mD6A1E4650E370D9AE4F5B220E43D35A7608D2C28,
	CharacterMoveController__ctor_m5935980F6D70531DC355A5667C8CC49A1BF4C6E8,
	Tile_get_IsUsed_mD310AAD46B8142FAD5AC0A513B5869EFFF120F7E,
	Tile_set_IsUsed_m5031C22DDB3BCBC99F6D2D7F3D0325FBA3FC5AEF,
	Tile_Use_mEA1EC428507216A90DED2C93B4C7989499C5E363,
	Tile_IsPerfect_m9A5E2FD99066F367DB7E0A15BE53C5E095F6A77A,
	Tile_Reset_m3930EC14F5CF3E8AFA571D0707BE1AAED3BAB966,
	Tile__ctor_m854A642EAE84DFB59DEDD9249DAF0A13F4F66063,
	AimController_UpdateAim_mE325AB7638764B0CA208C775F475E8774CF366CF,
	AimController__ctor_m0A5435C925331F988DB0B16DD7F3EAA82058088F,
	CharacterAnimationController_Initialize_m1BF4A5581504AA424D9B1CBFDDEE5A0D3451D6AE,
	CharacterAnimationController_Jump_m12EA44E7752EDA71B2378A7A4DE6FAC51BDE67C6,
	CharacterAnimationController__ctor_m75A7FD33230EF6996F5DD15EB837EE750703774D,
	CharacterController_add_TileLanded_m96C3C391D561F87695AA391F634C6D44CCFEB5D0,
	CharacterController_remove_TileLanded_m56846D5A4A4A4048EC3BA101B90C025CA927D374,
	CharacterController_add_Complete_m73B0C27685CCFD6C284E81E96E3C9321B0FD92D1,
	CharacterController_remove_Complete_mB8A70BB52AE0586DD49895F21786B4D9043C27AF,
	CharacterController_add_Crash_mA4307E546A64E8B5DDD88A788E9989F80B1FF369,
	CharacterController_remove_Crash_m52B349ED18DD3F4BE547CC601247B1E3D9998468,
	CharacterController_get_Position_m487DC39F7211172EAC44159BADAEBF7E9E2D2B83,
	CharacterController_Initialize_m31DFBC02FEF7CDF74127DBFA81FB0424D9C866C7,
	CharacterController_Tick_mC96AE378D718362804284DBFE6E88E86D2ADD665,
	CharacterController_FixedTick_mCEC487C47A8C66E7AC24C92FC5D19369477DC455,
	CharacterController_StartMove_m648072970A7F81EE8A8C59A080D2E949AF584D13,
	CharacterController_Move_m1C2CFA5610FBD3525125CD51F2FFEEFE95FABF60,
	CharacterController_OnCollisionEnter_m25DC6B82872AB12DAAC31E827AC12287EB2DA427,
	CharacterController_OnTriggerEnter_m450170CFD881B5A9C90334A8A191C56DD00608CC,
	CharacterController_OnFinishEnter_m09CCFCF48F73F0E158C8100C21B7B19B4E2076C1,
	CharacterController_OnTileEnter_mE5DAD9DA4B6BB0E7532324D3E058F214302491C1,
	CharacterController_Jump_m0D04BB50A55FFCA78C031AAAF1C3B7C96285CB6E,
	CharacterController_Reset_m8E6193C410CBD7223D3CA3AA3883A2B39D502E85,
	CharacterController__ctor_mD63A1653BA263C0276F74081166E28AD1F955773,
	TransformExtension_SetX_m85248C96202568A7F550061FD100A7ECAF7B80CD,
	TransformExtension_SetY_m74489F788842D013C156C5DC7340E09D7CB23A30,
	TransformExtension_SetZ_m297ED015318BFF2FD6F817A2B1A9B582BF378D1B,
	TransformExtension_SetLocalX_m6B32F1FF56E9E4BED0D65C1C8E63FFF3394E101C,
	TransformExtension_SetLocalY_m247C4C4A7CDBA140E00B0E7A8B7D80FC9047A888,
	TransformExtension_SetLocalZ_mA5BDF976F3733C5D2DB81D53B9DFB96651046947,
	TransformExtension_SetLocalScaleX_m90EC54B348DF8F38C5B3AC439A4D5D5A46D379CD,
	TransformExtension_SetLocalScaleY_mC3FF01BA6083F6BF573043A20D426E038BDBAAE3,
	TransformExtension_SetLocalScaleZ_m31EB24B2DBD59B727F4456F1964D2511A23E398A,
	TransformExtension_WidthDepthDifference_m80F1CBD833D7F03C2CD3CFCD55BC178CEB623FB1,
	TransformExtension_WidthDepthDifference_mEF6D7C0BFF5D4E8C220B077923194B1D4ED9DA14,
	TransformExtension_RotateVector3_m9CB2453D82A365384E61A073A6BDCF991F8DBA4B,
	ColorsController_add_OnColorsUpdated_m47671107E2C9C34A9BE219DA5F85EFDDA1B81164,
	ColorsController_remove_OnColorsUpdated_mF9D4CE3490CA57E129A006606BBD2CDD4D9DA17B,
	ColorsController_UpdateColors_m200DCA42FB65A4180D63E5637D53299A89242D8B,
	ColorsController__ctor_m36B0848D279F0CDEE7007C887687E375568E0327,
	EnvironmentController_OnStart_m928696FFFE5661FF76B427C107F6F5CFF5DB663A,
	EnvironmentController_OnComplete_m5D5AFB57C70FF1E9220C1D1779AF68530E76C344,
	EnvironmentController_OnGameOver_mEEB8FA57FE3D63CA8AB3491AD2BF56BB5473AE90,
	EnvironmentController_Tick_mB17FBB2F63DB87A42FEF677FA69374DCE2420B62,
	EnvironmentController_FixedTick_m101D7FD8EC6B084E84883AA9A666B989A3CB0D95,
	EnvironmentController_ShowCompleteEffect_m503A5A18397AC60EB766AF5187750B1AC86B4B86,
	EnvironmentController__ctor_m7C4ECD8AED4B3CB7057D301E552F370ACA406F1B,
	FogController_Update_mF7D3189BEDF0D77BD7FD7BE985F095D4CC89427A,
	FogController__ctor_mE371ECFDF0172DCCEF8A6B628C776ED11895C81F,
	InputController_get_Difference_mCD75A4E844123CE8EE5B0DD15ED496F7A8FBEBD9,
	InputController_set_Difference_mC4358EEF8B664A28649F48DF5B3D295464C4056A,
	InputController_StartMove_m4322C0611FB856DB610354E7F587D75A6A52ACC9,
	InputController_Move_mEFEFB7A96D6EED7141D28BF290FBC007CF2A0B66,
	InputController_Reset_m1435B7D3D7C868F125F7A7B22C5FA73C1C146253,
	InputController__ctor_m314EE80EAF8DA10CE0FC95EA0A4C58B8B481F57A,
	EffectsManager_Initialize_m98B3B762C7F2B2EA4107F00DBF87B1EDBD44CDEC,
	EffectsManager_ShowTileEffect_mB3C414AFE30CD7FA1FABC80CFBEDC23B7F424EC9,
	EffectsManager_ShowTilePerfectEffect_m7A0BBDCA51F55F20E3C01587AFBEBA8DEED28A8F,
	EffectsManager__ctor_m44C4B5BAC0CCC00E805B384F64E89FBC2E54AE8B,
	CameraController_Awake_mB04003159F6CF6FFA692D223B0F3D5EC86C08075,
	CameraController_Initialize_m153BF25CD0EDF1BBA32DC75F14ECE44C2CBCFFBE,
	CameraController_LateUpdate_m3A6B954830DF5AA2E6F3B33A8199616D507D1536,
	CameraController_Follow_m0A45C6D3983AE25D51C652F4247F25BC3329DC81,
	CameraController_StartFollow_m8306E0C0469F8F41FF20663AF0AF5622F9771281,
	CameraController_StopFollow_m3470ADB2669A1A96FD1962E2D27EE28C0828A205,
	CameraController_SetTarget_m2AA5C6C2A4481E1D31787C907306455CA9B2D0B7,
	CameraController_Reset_mC6EE68CD96653D39D961D917D6C18229695F9237,
	CameraController__ctor_m59F6E2426EB9D73CC4C196A371191BECDB0BBE0C,
	PoolEffect_Awake_m70918BA5F076B91A5A86E37C3C4BC7F4700342BF,
	PoolEffect_OnPop_m192ACBD07D539B410B17915AA33F334DD27DCFCF,
	PoolEffect_HideWaiter_mD5145FFB389AC9C5C754A27DC419B6B1D67EFCAC,
	PoolEffect_UpdateColor_m320C128E011652076E0D5F838CCB3FD2C00F0385,
	PoolEffect__ctor_m2238AC9049505F847DE513B22E6A7ECC6107E437,
	PoolManager_Awake_m8386AAF529210AF156EF2D890721699A328F5C0D,
	PoolManager_Load_m6823585491887FCF8A1AD8E1000542EB6021DF07,
	PoolManager_Push_mC83C318264CA933B5058677B8488FD833DA87D9D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PoolManager__ctor_m730D64E792DA6A66A27BB9463BAA88B5C567E999,
	PoolObject_get_Group_mC19841EEBD25FC2708E71E9E30EE6DB1C53DE203,
	PoolObject_get_Transform_m21704CCBE47AE85D078EEF961204EAF62CA0FD5C,
	PoolObject_get_GameObject_mD4A7E961D00654F015BD8C801CAA50EAE8A4A415,
	PoolObject_get_Position_m624F5532B87C624DA2C2EFE77DD2C369A2BE44A8,
	PoolObject_get_Rotation_m9FABC65FA77D7C8B805C8D9BBAA489BB36584F0B,
	PoolObject_Awake_mFE67639091B18BAA550C767BABAEBBFC0D120042,
	PoolObject_SetTransform_m683E19DC14E91561E56C0CDB5FE6336580889374,
	PoolObject_OnPop_m4D1E3AFED28A614726F70910F1919029E2C9935E,
	PoolObject_OnPush_mF38D3C5F324F8326E6693DFB45695FE4EB3FCC7F,
	PoolObject_Push_mA4CF561A1E8CA1A7298226035624989239876682,
	PoolObject__ctor_m762BD42B6B16C17460CCC68AA14B43833C05EDC1,
	AnimatedText_Initialize_m092CB4B304EE09032587C1297CCF1A77D3275F3D,
	AnimatedText_Show_mADC02B4DBAEF07F4A1921B07EEC2A9D724AF5926,
	AnimatedText_Hide_m06459822604E2715E83A10BB42C132F3C95ADE98,
	AnimatedText_HideWaiter_m21A1D77615D7A6CC45D24FCE0A803ADEE5716D39,
	AnimatedText__ctor_m68AF487B10A84B1A5202B80ADD364A413F661671,
	PerfectAnimatedText_Initialize_m2889576025488E2585C8B0DC74309B67598C1648,
	PerfectAnimatedText_Show_mDC5F6DAFE86C4F4CCE806B66A3203A085D74E865,
	PerfectAnimatedText__ctor_m36A4759C2C050D66F4C72CC821D7482ABA6A8903,
	PerfectTextAnimation_Preload_m301BCFE4633DF5279906A4F2D50F3C0ED52C352B,
	PerfectTextAnimation_Animate_mD8C196704C9697976A06AA4E0AE3175D0651A3B3,
	PerfectTextAnimation__ctor_m3C82F23072D59B5FC4734107395441CF3276A423,
	PerfectTweenText_Initialize_m98468F8C1D1409A86CA81FCBC06CE9E072FF82DD,
	PerfectTweenText_Show_mBC1EA688C21440BBC8599D2ED09D0DE6AD16AEAE,
	PerfectTweenText_Hide_m5DF2BE0A3F8389D591BB8BF4CFE39D066A7768AD,
	PerfectTweenText__ctor_m27E99231EC19A18755E573FDC43CBE475DCEB975,
	EditScriptableAttribute__ctor_m2C1BB85C54BA2A238B777A039A82C1647D822AF7,
	ReorderableAttribute_get_ElementHeader_m9F925EB93C054EC37AFBA718E69F09B6F80D68C5,
	ReorderableAttribute_set_ElementHeader_mAB4E7063220371048A6D571EC307031FF8542166,
	ReorderableAttribute_get_HeaderZeroIndex_m562F07706A155B2F24C8107708F7ED2640FDE7B0,
	ReorderableAttribute_set_HeaderZeroIndex_m5385D781E070E6C3703D6CD7079EB1C1BA012527,
	ReorderableAttribute_get_ElementSingleLine_mFB2DEBCEC012E58268033D44E310A2DDF1719DB0,
	ReorderableAttribute_set_ElementSingleLine_m8436CFBDFAF2841AAF05FF909D91232B5095BCFB,
	ReorderableAttribute__ctor_m204BEA4B27CD13051A4B6376B02E2450F36E9A5A,
	ReorderableAttribute__ctor_m2CB65342F0F4EE772BE961CD6B4C2AEBED2C51B6,
	MarvelousBloom_Start_m0A7798D557FEB8C7B135BC3B611A83BC62EFC89E,
	MarvelousBloom_OnRenderImage_m7CDD7242C9450F15B8A1ACC7A8AD6DB03D2A881B,
	MarvelousBloom_OnDisable_m76C8DBA023761BB853709D0A164D56C7CD1C2A97,
	MarvelousBloom__ctor_mD26EAE618053C702EA140C3446242A36FBD78B2D,
	OceanCreator_createPlaneMesh_m8C60ABCBCE8ECB4F61B658732CE416CFA1154ADD,
	OceanCreator_createOcean_m00580FFA689CBB139049BBA6E47D69E53698E245,
	OceanCreator__ctor_m2BBDB32A2A5A1CD8C10E016ED67B79F2B7E50909,
	FloatingObject__ctor_m6FCA4BA3AA5B7880009D5B02E00D818DF6035634,
	OceanWaver_preCalculateFloatingObjects_m7A2C5B1B66B2889450BBDB2E0602F957A775927B,
	OceanWaver_calculateFloatingObjects_mCD988B0542A059BDA259F935D127E3266A5929A7,
	OceanWaver_Start_mBFFCE5F640A7594C475A391A9157FA1DE781A375,
	OceanWaver_Update_m302066EF6988B94DCA15D58BFCEACC1A32A94F73,
	OceanWaver__ctor_m9CDFDEF5BEEBD4512C99382321CFFAC84F6D6E56,
	ScreenTextureBlend_Start_m44936525D05514E4D1BBC6F437DF07351365A860,
	ScreenTextureBlend_OnRenderImage_m701736CCC96004E60575DA1301D23CA87E7BB715,
	ScreenTextureBlend_OnDisable_m39C198B70E029D65EFE071268B7E10935DD760DE,
	ScreenTextureBlend__ctor_m25F0415D8942A9DE849F8DA59F3F9300FE48BC47,
	DOTweenModuleUI_DOFade_mAD40AF255234B54E5BA8F81219C9C30E520EFB10,
	DOTweenModuleUI_DOColor_m224983D24DA5CC099E78D8FA2C4FC159F6385CA6,
	DOTweenModuleUI_DOFade_mD3FD501A6776914AE93FF9B9722A9EAD4B2DBAF9,
	DOTweenModuleUI_DOColor_m60BCBDB46E4B4E67BE9D9984CAEDA8065AC3F050,
	DOTweenModuleUI_DOFade_mF285AA1CEFC18A119A640DFEC3FC8216E92C7407,
	DOTweenModuleUI_DOFillAmount_mEFF5F991C07ADB2611F22051613B7ACEF66B9E4A,
	DOTweenModuleUI_DOGradientColor_m6CC119CCB3273C03076FCBDAFB9E0CBD169D2D0B,
	DOTweenModuleUI_DOFlexibleSize_mFDE51E1609ADFE1E5D1161ED9B2BB55B140DCEEE,
	DOTweenModuleUI_DOMinSize_m0BF4109021D52DE8577A8FADFC069409C0BEC366,
	DOTweenModuleUI_DOPreferredSize_mFCEDB7320442DCD6D7853C62F049BDA0185EF278,
	DOTweenModuleUI_DOColor_mB023CF7E6FFCFC847151C5AAA0CAEA00C80046E1,
	DOTweenModuleUI_DOFade_m4DB434C44B5B2906B99B8926B6FDA2CADEF8A7B7,
	DOTweenModuleUI_DOScale_m9E98A3E1A4F40F42336B7A83EDE00B4EFA25C4B5,
	DOTweenModuleUI_DOAnchorPos_mCB314AB88AABF23417E5671CB1CC2C35591920AD,
	DOTweenModuleUI_DOAnchorPosX_mC9A3B16335FD5A5157688FF499FE782E5F5C7A33,
	DOTweenModuleUI_DOAnchorPosY_m839C67C3E8C54701B7FC5EAB46B911C0DF192E97,
	DOTweenModuleUI_DOAnchorPos3D_m595AA5252C74166C89DFC53A9052E4DCCEAF1921,
	DOTweenModuleUI_DOAnchorPos3DX_m83F31312715DF4E65F5968115CEDB185A4648B8F,
	DOTweenModuleUI_DOAnchorPos3DY_mB024133ED57232447FF4EB17C861AD70A44F304A,
	DOTweenModuleUI_DOAnchorPos3DZ_m56234AB8B466C82A5488AFAE36F7153B82BE2CB9,
	DOTweenModuleUI_DOAnchorMax_m789E726166F500902973D121DE47E178BF772AB7,
	DOTweenModuleUI_DOAnchorMin_m3E6515C288D04F55674513677FE209E2727448BA,
	DOTweenModuleUI_DOPivot_m080B5E1942554C274704FE5E024EA9CDF6909492,
	DOTweenModuleUI_DOPivotX_mF68B3F4FE5D0CAAA9E1203C6A3BE2557B2365688,
	DOTweenModuleUI_DOPivotY_m2A449ECB26A50F9CA43F852CACE4B876A5094CAC,
	DOTweenModuleUI_DOSizeDelta_m1E034E8BDB38D7F4F563AB9523891D7F215AA8CF,
	DOTweenModuleUI_DOPunchAnchorPos_mF564424EB231C6B1EC4262BD235C6F4C7B954EAE,
	DOTweenModuleUI_DOShakeAnchorPos_m19C5739A636820F747EA76567409F0773A8C7CB8,
	DOTweenModuleUI_DOShakeAnchorPos_m14E79F46BF244846CD0C57D4F0ECCF44943875AE,
	DOTweenModuleUI_DOJumpAnchorPos_mF579D5EC1A7F51586A2C0409B34CC105BF480F41,
	DOTweenModuleUI_DONormalizedPos_mAF52FC4BCBDE295E6A2F3C297188469D7BE4DF56,
	DOTweenModuleUI_DOHorizontalNormalizedPos_m4DC3AE19F8B0437E7D26930A2897CC479161A134,
	DOTweenModuleUI_DOVerticalNormalizedPos_m0B870F709BF1790B6DB4C8F05A6B7ECE8CB1D231,
	DOTweenModuleUI_DOValue_m6A01D22DCB8142450DCC0C43046C2F0D5C39D00E,
	DOTweenModuleUI_DOColor_m981384A20E9E9EA3BF1B105E1850D8DCFA1D1F60,
	DOTweenModuleUI_DOFade_m0A792EC1128D0C1C77D39B0E859CFFB45A51E479,
	DOTweenModuleUI_DOText_m811BBD8FA9215A129160FC0A7D746E1A3741465F,
	DOTweenModuleUI_DOBlendableColor_m2F817AE8F4922B274AF2B1E7BFDD0E5D7C623BFC,
	DOTweenModuleUI_DOBlendableColor_m140B9C6CD1E6402158382C49918DB303E39369B7,
	DOTweenModuleUI_DOBlendableColor_m8666A89B76B8D202DC81C1AB12D8B45688212D51,
	DOTweenModuleUnityVersion_DOGradientColor_mD3871F90FCE6FE5B23E23FE12F7826E44F8A05CB,
	DOTweenModuleUnityVersion_DOGradientColor_m1AFB6E6AED804D3261D124D2D1429134B34AFD1E,
	DOTweenModuleUnityVersion_WaitForCompletion_m59773AF9A35804797C6C389A516DA63A0DBAC246,
	DOTweenModuleUnityVersion_WaitForRewind_mC6F53B8F61682B52F390434AFCEC775A4EB49F53,
	DOTweenModuleUnityVersion_WaitForKill_m871BB15AC97A221A77DEE06B90AD1EB1DD97667D,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m10EBF8473875B14ADA1C065043BE47B9C3DE99D4,
	DOTweenModuleUnityVersion_WaitForPosition_mF7210B505549168AF420CC3453D42DC85ACFE977,
	DOTweenModuleUnityVersion_WaitForStart_mC3E5A8737A87F6DD8ED2380957AE33B786DA1401,
	DOTweenModuleUnityVersion_DOOffset_m2A623BF72AA0289033B412F4D93DF5317FF620CF,
	DOTweenModuleUnityVersion_DOTiling_mE9A75D5B8CFD751EDCB4231A9FBB2EC003752D97,
	DOTweenModuleUtils_Init_mB7D252D24842502558B1DF4DEAC5B08887202DBC,
	DOTweenModuleUtils_Preserver_mE88FFEE9E520275AC0F42ACBE27D7F2265E33DBB,
	U3CStartU3Ed__4__ctor_m8154908E5EBDC04454E52BD0336EDE8C2526F575,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m9301E500349C0547BF1806EF3F5F85E44D4B5468,
	U3CStartU3Ed__4_MoveNext_mDCF2005E0ED44E2FA30D888FC6E4DBB200D42B91,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m991F0933C435514859C030C497BFE215E77607BF,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mD1589E54368884F980B654F6D0D6F9EA82118B31,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m7A5FFE01437D34364EA31B57835080D5445942E7,
	U3CslowRotationDownU3Ed__29__ctor_m17A9214B26089E4435EECE1B21A67DF4F5ED65AA,
	U3CslowRotationDownU3Ed__29_System_IDisposable_Dispose_m4DFA9A2D32070E835E83960C7D7FDAB16340044E,
	U3CslowRotationDownU3Ed__29_MoveNext_m32B3D78433F77A6A0558842D557521B24D4E8218,
	U3CslowRotationDownU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D16E3A80E8441D8152318C60FC5165A76AF6086,
	U3CslowRotationDownU3Ed__29_System_Collections_IEnumerator_Reset_mE574AE6B10B2689547EAE5E383FAA230BBB63684,
	U3CslowRotationDownU3Ed__29_System_Collections_IEnumerator_get_Current_mFEAECB7C867296E284812D052C11B4E15C0CAB35,
	Settings__ctor_mA8C3AD7B7882E54B943EA68ABC1B9F6654D522F7,
	Settings__ctor_mF30AE690F5567D67A1D8B25F1E4EA0445C6D46DF,
	Settings__ctor_m8BEE139FD5A19966C549CF2C3222C4D142B53F7B,
	LevelColors__ctor_mE815AC6973DF6F02AF7CF655BF2FDD4CEE2893DE,
	ColorsPair__ctor_m9C15C18D0CBCD5B2F47985489F862282E9C34000,
	Settings__ctor_mC80D58D05D4AFD0FC46224122A1DB90E94ECB0A5,
	Settings__ctor_m4B0C4C8D0CAF92669710615563BC6CCEEDB47257,
	Settings__ctor_m8A2CFD15CE7F100FFF101D602E5C4D2252BE15AC,
	Settings__ctor_m2186E5B3711BFDAF32A7CA8543E7E307EEB68212,
	U3CHideWaiterU3Ed__3__ctor_m68992F987C20FCC176A92A7060F2C4F10B6B9865,
	U3CHideWaiterU3Ed__3_System_IDisposable_Dispose_mC540425D36AEF2541C599F15BC7601ACE4AB67B4,
	U3CHideWaiterU3Ed__3_MoveNext_m119AC90CC3E0D3F0E69DE5B9E528225E2D3E01A9,
	U3CHideWaiterU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB6E5627197BBE623A9D57762A1C75D3416039FA,
	U3CHideWaiterU3Ed__3_System_Collections_IEnumerator_Reset_mA610A8110196292862B32D0F8189925928D8D939,
	U3CHideWaiterU3Ed__3_System_Collections_IEnumerator_get_Current_mC3DC20721EF251E684CB1A20B481C60721017F26,
	U3CHideWaiterU3Ed__8__ctor_mFA32CAF739AF4EFF56F66A54E82002BCC4409C1D,
	U3CHideWaiterU3Ed__8_System_IDisposable_Dispose_m9AC06FD69942E702DB3C174FBF9BC33B1538644E,
	U3CHideWaiterU3Ed__8_MoveNext_m259E6F4021D313CF26F2589D449DFBE5FCD1CAD5,
	U3CHideWaiterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63E295693A0C37C97946C6589D758E688B90EF2D,
	U3CHideWaiterU3Ed__8_System_Collections_IEnumerator_Reset_mA9AB887CA9EC0E1F152E6D89068B5377CDF792A8,
	U3CHideWaiterU3Ed__8_System_Collections_IEnumerator_get_Current_mA9DD829CE8E79BBD6A083AD5A221BBAE164773EA,
	Utils_SwitchToRectTransform_m953E8B35B59142D580B1EC5A3CB48163D94FE270,
	U3CU3Ec__DisplayClass0_0__ctor_m18A8356F5FF1FB67DC8B3E62188C603519B3124B,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m32C5F6AA875203D30B95F0239833E503B58A4080,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m35704DDE3B470E932AE2394E444E236EF73D0950,
	U3CU3Ec__DisplayClass1_0__ctor_m2F3D328081E4370B02A8DC65A5595FD7779319F7,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mD5A08FA687B4F3D4ACDE03C5F4747ADD5A85ACE8,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m17953C82468E26BC336CB919379BEA94DB403CF5,
	U3CU3Ec__DisplayClass2_0__ctor_m42ECD7DED23372FD451B099F33986255C1F05F24,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m96E8793AEB0F5454A98699F2A492CD0C6A6F29D9,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m22E508875493477F1B41CE7B93A3327597C7CA64,
	U3CU3Ec__DisplayClass3_0__ctor_mE21167957C3A82E6A2212EA06099E19A61C888F5,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mD60F568B3218FB775B0F2F472DEF66F12F240EB8,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mF414A2392F00D79E324899FFFD8BCCA32B63113A,
	U3CU3Ec__DisplayClass4_0__ctor_mBBFBBB66427C51F249BBFF57BB9DBE3738E70DF0,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m2ABDE00A2CE387D94979FF4772233C32E0433002,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m62D514A0CBEE93DDB5E11A83FFEF88F70887B006,
	U3CU3Ec__DisplayClass5_0__ctor_m9C45A8A17A10C098C69BBAF60D2639D9ED29A978,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m2CFEEB05691BF628CD9723B772220FBF1B5DEFC5,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_mC2B9793EB03BCFC06E022CB10D2AE3D4CDC2DC9B,
	U3CU3Ec__DisplayClass7_0__ctor_m5080623FCA82C0A8E5638EDE08125692828AB793,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mFA007EE23041AC3DB626E202537641F7CCC10895,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_mBFC566A897CBAFAFCC74D5DBCEC136A07F1A5F9D,
	U3CU3Ec__DisplayClass8_0__ctor_m70D737AA33CF6139F30568CAA4BFAD2E9F1D66BA,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m5AADB9E424374D66A929CAE4FE7C91FAAD9C1F92,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_mF51654F8E42E61B6D410192E9C4493BC7B50B7A6,
	U3CU3Ec__DisplayClass9_0__ctor_m6054B368E828204FC04AC31B20D57ECE521DA2CE,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF3EEFDE0596A75FA4C9B5E3CBB8D27B37286B7D7,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m723F0F3AD18E54882602EF10C41A9138C2094C92,
	U3CU3Ec__DisplayClass10_0__ctor_m3C2AC3BA68FE823C5EE4ABF01CF043FB14BC77D4,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mD23FC3413F649B7175F76650D99C969D080A52B6,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m2A91553D7D78ABD3E8125820476F8026322DD1AD,
	U3CU3Ec__DisplayClass11_0__ctor_m922DA2833A1846F6E644BACDA5360882FE3CBD5C,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m97CEC28FB18E8EC25DE68CBBC9363C1969199D92,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m7C9745929EF3497843A96CE489528E972E1696D2,
	U3CU3Ec__DisplayClass12_0__ctor_mDB084173826CEF2F054C58D71259B79FABE1AFB1,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mD4ACC050241FAF74718A82BAA2FAD799B221D52F,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_mB98E48B7CD64FAEDBF41990DFA5EDFB84BEAA635,
	U3CU3Ec__DisplayClass13_0__ctor_mC4153B76DCBFB2268DBBA69D56E679A477FCBE2C,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mEC1BFCBC158066EE11442276BAF4904FC8167F78,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mE62D5B3135C3EA0973694EDF23FEFBE0879E0431,
	U3CU3Ec__DisplayClass14_0__ctor_m179837AE08CE76F84AD7C2DE1C4536FB2BC6724E,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m946B10547ACAFC05288900D6C8AE8BE29FA8E769,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_mC4EA8586148CB5AE824CA16BE35F0C7E15F13927,
	U3CU3Ec__DisplayClass15_0__ctor_mD9D66D4A39054D51E9A3A43A3E47FE76D14E8E2A,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE74B0E96F68D07C8FB2B95F696DDB3F3EF90633E,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m818E4ABC9402DC49D5DC6F8042472F3D08891D95,
	U3CU3Ec__DisplayClass16_0__ctor_m266310E4E7253E8AD0DB494DC917560C7312B6E3,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m54AA9D5E75139ECB7FB5D0BF9518849E9258416F,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mE14500ADC3593F53F54C4D66F17888A87B84344E,
	U3CU3Ec__DisplayClass17_0__ctor_m6866C080BE78AEC33050A808BD28DD4FF4B7004B,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m7C05989ECA103CD60B5CF731D6C7920B1FC985E1,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mFB2895AADD1CDF00CFBA835E56A9E13904BAD620,
	U3CU3Ec__DisplayClass18_0__ctor_mCDA7BC09C26B4712D699A1FB257E8ACC7F8B2AA6,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m9D5E91C0F8BF3135CA706B2D09504A99E384E3F6,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mE4A2B4E589B150E943E18A30D33E95B1754E2886,
	U3CU3Ec__DisplayClass19_0__ctor_mFDD1B1264FCD4CDDB8A38EA8AD5909CC0FEF92D1,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mCE069FE82ADB466CBC0DAFC700BC15C8613B138A,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD34C0057C888DD82D38BE7593C1E92FF925914DA,
	U3CU3Ec__DisplayClass20_0__ctor_m5410B8E63F947DE005A20DE7E85E3CCE2D72079B,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m0D36293A6361A326C0853382366377CE10796BC9,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m5729647091DBBF0D726254ABADF2377ADA0101AD,
	U3CU3Ec__DisplayClass21_0__ctor_mFB3C178CAA653C95BEE3093D575FEB85F8C7367F,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD66DD96927DDAF369C64AE446E76D6F0F03AE0F5,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m7189572DD336DDE6EC85F27770F731FEEAA2FD50,
	U3CU3Ec__DisplayClass22_0__ctor_m7D1B522F01E0419DDBA693585300D0B01AF984FB,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m7DC62E4EE195C844218B14EB4EF6E5AED8989D0D,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m10B5380A3AA5CEAE992081A449EE1F74EC79B15C,
	U3CU3Ec__DisplayClass23_0__ctor_m75C045C8DEAC70D81B02EAFC1EA366AE0A7AA8F5,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mF72A07116DE5256A691DBAFB766E637F55C4EF45,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m8EABFE51EA9393CC2EC7F0055F02DED35D3B9186,
	U3CU3Ec__DisplayClass24_0__ctor_m7157DCBC4845023E76ECA85BE6C3F81EFCD04239,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_mADD1E688B9294ECD2A7CCD79F78CA0CD68E055B4,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m62A33BFDF3873451B14AE3EBA0791AF52D082F15,
	U3CU3Ec__DisplayClass25_0__ctor_mAE01B70E706B6F8A90EA06CCE17FC489F1FC71D2,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m036AB1FD8ACAAB27669C6F82B807258A7CF95161,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m3F1A0A60B77F9C9AADC0C9BE1969FD929AEC15AE,
	U3CU3Ec__DisplayClass26_0__ctor_m143FC1CEE9576076B1C3F954E13D9F709DE22292,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mA1CDB05B94AFD8D48E1B1DB4B6F2F53FDDD274EC,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m2F2781A19452E8F74222AABE3617555A4CEA2DC4,
	U3CU3Ec__DisplayClass27_0__ctor_m3A3C7488BD686D2417811F19408B70D93B0FA02F,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m23ADE1EDABB1FF436DCB15B4D2956341A403F86F,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mED3FDE11B0B31284228C7D6A01C66ADFAD06D05B,
	U3CU3Ec__DisplayClass28_0__ctor_mC9FC42C7A712D81D3E1FFD069C16C866CF7F3A2C,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mBD11B2BA0B8A07753156C1F9CA22B33A041F6A28,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m560D14C7C7B82350B44D745D7DB7AE161730A086,
	U3CU3Ec__DisplayClass29_0__ctor_mC30B5380B8DE2DCAC55DA68241D5EF1266F79909,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m3336D670200C9F13D96D7F52F123920132234850,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m5B9935C76D1779CA20BEC5C8434C34BDF9ACA7EF,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mFAA2F463E2CED9DE0BA520D44BDC7A64ED56BC6E,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m0D9E7219678950432D0E913F31C5A3E3B3D1F449,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m41FE20829CE35CC646EF346A2ED2D42A8F7FFF6D,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m46AB05BB46F4343B5372269801C929C6BEA4E78C,
	U3CU3Ec__DisplayClass30_0__ctor_m8786A94537CDE17C192F816195A8D5095A147611,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_mE69AF155BCC63288298C03F1FDE8264894DC5112,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m34D4384708B00929EC0936A3D9818FBB9320124A,
	U3CU3Ec__DisplayClass31_0__ctor_m430D95F36B8C311BCA555E6C9EB4E3D5B6CB52DD,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m4B50B77000E1A5B0959F7EFEC780C43A2B3C21A6,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mE79549F5C6F07819B8FCA4A3670548B94B794980,
	U3CU3Ec__DisplayClass32_0__ctor_m03AFF4E74AF489641F31A7ED7BB588DC48C86B47,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_mDFC279CC6199107052C6C57F7C80069AF08B727E,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m69F269F657FA4BD2821E0451B328802CA74E5E4D,
	U3CU3Ec__DisplayClass33_0__ctor_m44794063561EE5542E8D44B8047DA0EB476D9D99,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m49CBBBEC21C5C4D3517ED220CE99FECCA32D8E17,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mC23A71E0D945D05C253DAA3C6B29F017AC3C8B7A,
	U3CU3Ec__DisplayClass34_0__ctor_m31CDFCCAA9BC664B12E06164FE6FD3F6BCF1CE10,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_mC0172F2115741150E114BCEB8C6366E8DC05F0FE,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_mC68427660102A2FD7D41ED52EA99141A48A176E5,
	U3CU3Ec__DisplayClass35_0__ctor_m5119796185D9945B3DADE92E11D912791044F5F1,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m7AEADCAF76E9378EF16998AF06C981E238317A14,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m6F73BC7514BC81F58222707B2FEA9F1775452C1B,
	U3CU3Ec__DisplayClass36_0__ctor_m266E0D445578A8CF42FF8E6E28F8DD0D5816B6C6,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m8450222712B70BE65D184CE038DDA30929396E09,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m5EBCE38D43719324BFF117A085F469D6C7A53A4C,
	U3CU3Ec__DisplayClass37_0__ctor_mF7EE64DF2C6519BFCE36A53802C870392DCA681E,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m5C210BC6DB5E81E37C5DA7F0ED05DCEBF88A838F,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m001CE2BECACB83DE0BB4252DFAEFF93D51676EDF,
	U3CU3Ec__DisplayClass38_0__ctor_m029E260AE14F07692FEC152AA854A3E7E818F5A8,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mD045F88560E9C38E36C045962A24AEEE7E220146,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF9673E803B1ED13D3937EA5093B07828944B0D33,
	U3CU3Ec__DisplayClass39_0__ctor_m2D3EEE4A819B3C837A38B0F228978C5DECA02157,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m769FD067F1DD156E6737065978D723626F684DD4,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m9E000E92EC974F7B26E70A9DB887A7B5ECD13C4A,
	U3CU3Ec__DisplayClass8_0__ctor_mCB2FA585275686213140C65F568AF5B895DE515D,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8CB04FB886F55DF914E092EAB3E88CADA5E4BFEB,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m70E3E83723C4C320D2E78FBB32E3C5AB9E483413,
	U3CU3Ec__DisplayClass9_0__ctor_m71E4A32D334F5A1DDFE2DB998E25634EC8C9E0D8,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m45117726269A56BEFE7511E185A5EB5DC6614C08,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mEC4AEA77E8E47513A16390421DFE3814DB87DFBD,
	WaitForCompletion_get_keepWaiting_m30EF66A11D003F93F4CBF834A856E7298D5955B0,
	WaitForCompletion__ctor_m69692E16010A98F06E13B8668A2C9AC26E470727,
	WaitForRewind_get_keepWaiting_m8E68A912E510F991CC524EA2DCB6634BE7F7DF65,
	WaitForRewind__ctor_m111D38D621831BAE9D00CCC7F195DBC48A9483D4,
	WaitForKill_get_keepWaiting_m410BE82C7AEB9A7E45E5E07EEBDA1D2E2CEB50B4,
	WaitForKill__ctor_mD127E54E87A255CD19BB9539CE786DD6953C5719,
	WaitForElapsedLoops_get_keepWaiting_m6A50A75A89879252500ED70BD15C0C04CF6DC2D7,
	WaitForElapsedLoops__ctor_mE11D912E8954AB448C21081560A6FBB9D974B7F3,
	WaitForPosition_get_keepWaiting_m78A4DAE5D866FA838D2A843113F275E169B03A30,
	WaitForPosition__ctor_mB0DA06151DE0C3690CCF8A3117189FA2800CB3FC,
	WaitForStart_get_keepWaiting_m218C9D65E9141136A148DD636B155272308A9F5E,
	WaitForStart__ctor_m9088C849D74DBB8D2E12FA05AD5A2E17AA48E77B,
	Physics_SetOrientationOnPath_m8BE0D531D59E8E8D9F092FB6C9F881C98BDE429B,
	Physics_HasRigidbody2D_mA2B48BE3FC3BB297B8227F0ECA26BAE6B49732A8,
	Physics_HasRigidbody_m5C3BC90BFBC15B8787E33728CBC9FD4DBAF10DE6,
	Physics_CreateDOTweenPathTween_m5026DCBFA244A145B5076E485DA4643EFA331B42,
};
static const int32_t s_InvokerIndices[623] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	289,
	679,
	23,
	23,
	3,
	289,
	679,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	289,
	289,
	1920,
	679,
	114,
	433,
	1845,
	679,
	679,
	1779,
	1921,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	1162,
	23,
	23,
	27,
	1162,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	23,
	32,
	1551,
	23,
	23,
	23,
	3,
	1922,
	325,
	49,
	3,
	49,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	38,
	27,
	23,
	1923,
	27,
	23,
	23,
	23,
	23,
	289,
	32,
	23,
	23,
	23,
	23,
	289,
	32,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	10,
	26,
	23,
	26,
	23,
	26,
	23,
	23,
	10,
	23,
	26,
	23,
	26,
	23,
	23,
	10,
	23,
	26,
	26,
	26,
	23,
	23,
	401,
	10,
	23,
	23,
	23,
	4,
	122,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	34,
	23,
	32,
	23,
	26,
	26,
	26,
	23,
	23,
	10,
	23,
	26,
	26,
	26,
	23,
	23,
	10,
	23,
	1924,
	1924,
	1924,
	1924,
	1925,
	1926,
	1658,
	1927,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	182,
	122,
	0,
	0,
	137,
	137,
	10,
	32,
	10,
	32,
	10,
	32,
	679,
	23,
	114,
	114,
	23,
	23,
	23,
	192,
	23,
	23,
	23,
	23,
	23,
	1182,
	26,
	26,
	23,
	114,
	31,
	23,
	1152,
	23,
	23,
	289,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	1142,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	1317,
	1317,
	1317,
	1317,
	1317,
	1317,
	1317,
	1317,
	1317,
	1237,
	1928,
	1239,
	122,
	122,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	289,
	23,
	1150,
	1170,
	23,
	23,
	23,
	23,
	23,
	1143,
	1143,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	14,
	1425,
	23,
	23,
	682,
	27,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	14,
	14,
	1142,
	1325,
	23,
	1929,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	32,
	23,
	28,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	114,
	31,
	114,
	31,
	23,
	748,
	23,
	27,
	23,
	23,
	1930,
	4,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	1663,
	1664,
	1663,
	1664,
	1663,
	1663,
	1659,
	1931,
	1931,
	1931,
	1664,
	1663,
	1673,
	1931,
	1678,
	1678,
	1677,
	1678,
	1678,
	1678,
	1931,
	1931,
	1673,
	1663,
	1663,
	1931,
	1932,
	1685,
	1933,
	1932,
	1931,
	1678,
	1678,
	1678,
	1664,
	1663,
	1934,
	1664,
	1664,
	1664,
	1659,
	1627,
	121,
	121,
	121,
	647,
	1694,
	121,
	1936,
	1936,
	3,
	3,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	1935,
	23,
	679,
	289,
	23,
	1094,
	1095,
	23,
	1094,
	1095,
	23,
	1094,
	1095,
	23,
	1094,
	1095,
	23,
	679,
	289,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1094,
	1095,
	23,
	1094,
	1095,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1142,
	1143,
	23,
	1142,
	1143,
	23,
	1142,
	1143,
	23,
	1142,
	1143,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	1142,
	1143,
	23,
	1142,
	1143,
	23,
	1142,
	1143,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	23,
	23,
	1150,
	1170,
	23,
	679,
	289,
	23,
	679,
	289,
	23,
	679,
	289,
	23,
	1094,
	1095,
	23,
	1094,
	1095,
	23,
	14,
	26,
	23,
	1094,
	1095,
	23,
	1094,
	1095,
	23,
	1094,
	1095,
	23,
	1150,
	1170,
	23,
	1150,
	1170,
	114,
	26,
	114,
	26,
	114,
	26,
	114,
	136,
	114,
	857,
	114,
	26,
	1769,
	94,
	94,
	1937,
};
static const Il2CppTokenRangePair s_rgctxIndices[9] = 
{
	{ 0x060000DE, { 0, 2 } },
	{ 0x060000DF, { 2, 2 } },
	{ 0x060000E0, { 4, 1 } },
	{ 0x06000150, { 5, 1 } },
	{ 0x06000151, { 6, 3 } },
	{ 0x06000152, { 9, 1 } },
	{ 0x06000153, { 10, 2 } },
	{ 0x06000154, { 12, 4 } },
	{ 0x06000155, { 16, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[18] = 
{
	{ (Il2CppRGCTXDataType)2, 28858 },
	{ (Il2CppRGCTXDataType)1, 28858 },
	{ (Il2CppRGCTXDataType)1, 28860 },
	{ (Il2CppRGCTXDataType)3, 20963 },
	{ (Il2CppRGCTXDataType)2, 28861 },
	{ (Il2CppRGCTXDataType)3, 20964 },
	{ (Il2CppRGCTXDataType)2, 28924 },
	{ (Il2CppRGCTXDataType)3, 20965 },
	{ (Il2CppRGCTXDataType)3, 20966 },
	{ (Il2CppRGCTXDataType)2, 28925 },
	{ (Il2CppRGCTXDataType)2, 28926 },
	{ (Il2CppRGCTXDataType)3, 20967 },
	{ (Il2CppRGCTXDataType)3, 20968 },
	{ (Il2CppRGCTXDataType)2, 28929 },
	{ (Il2CppRGCTXDataType)3, 20969 },
	{ (Il2CppRGCTXDataType)3, 20970 },
	{ (Il2CppRGCTXDataType)2, 28930 },
	{ (Il2CppRGCTXDataType)3, 20971 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	623,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	9,
	s_rgctxIndices,
	18,
	s_rgctxValues,
	NULL,
};
