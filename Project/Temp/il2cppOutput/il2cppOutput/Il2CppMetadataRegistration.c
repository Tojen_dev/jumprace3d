﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




extern Il2CppGenericClass* const s_Il2CppGenericTypes[];
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[];
extern const Il2CppGenericMethodFunctionsDefinitions s_Il2CppGenericMethodFunctions[];
extern const Il2CppType* const  g_Il2CppTypeTable[];
extern const Il2CppMethodSpec g_Il2CppMethodSpecTable[];
IL2CPP_EXTERN_C_CONST int32_t* g_FieldOffsetTable[];
IL2CPP_EXTERN_C_CONST Il2CppTypeDefinitionSizes* g_Il2CppTypeDefinitionSizesTable[];
extern void** const g_MetadataUsages[];
extern const Il2CppMetadataRegistration g_MetadataRegistration;
const Il2CppMetadataRegistration g_MetadataRegistration = 
{
	15423,
	s_Il2CppGenericTypes,
	4486,
	g_Il2CppGenericInstTable,
	17625,
	s_Il2CppGenericMethodFunctions,
	30376,
	g_Il2CppTypeTable,
	20972,
	g_Il2CppMethodSpecTable,
	3925,
	g_FieldOffsetTable,
	3925,
	g_Il2CppTypeDefinitionSizesTable,
	14011,
	g_MetadataUsages,
};
