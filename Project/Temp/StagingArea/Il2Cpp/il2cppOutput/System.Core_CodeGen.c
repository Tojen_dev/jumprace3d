﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 ();
// 0x00000004 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000005 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000006 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D ();
// 0x00000007 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000009 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Skip(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::SkipIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000011 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000012 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000013 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000014 System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000015 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000019 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ExceptIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Reverse(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ReverseIterator(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001D TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001E System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001F System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000020 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000021 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x00000022 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x00000023 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000024 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000025 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000026 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000027 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000028 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000029 TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000002A TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000002B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000002D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000002E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002F System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000030 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000031 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000032 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000033 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 ();
// 0x00000034 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000035 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000036 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000037 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000038 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000039 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003C System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000003D System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000003E System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000003F System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000040 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000041 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000042 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000043 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000044 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000045 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000046 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000047 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000048 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000049 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004A System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000004C System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000004D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000004E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004F System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000050 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000051 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000052 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000053 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000054 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000055 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000056 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000057 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000058 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000059 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000005A System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000005B System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000005C System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000005D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000005E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000005F System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000060 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000061 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000062 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000063 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000064 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000065 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000066 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000067 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000068 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000069 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x0000006A System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x0000006B System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000006C System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006D System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x0000006E System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x0000006F System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x00000070 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000071 TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000072 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x00000073 System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000074 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000075 System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000076 System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::.ctor(System.Int32)
// 0x00000077 System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.IDisposable.Dispose()
// 0x00000078 System.Boolean System.Linq.Enumerable_<SkipIterator>d__31`1::MoveNext()
// 0x00000079 System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::<>m__Finally1()
// 0x0000007A TSource System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000007B System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.Reset()
// 0x0000007C System.Object System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.get_Current()
// 0x0000007D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000007E System.Collections.IEnumerator System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007F System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x00000080 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x00000081 System.Boolean System.Linq.Enumerable_<ConcatIterator>d__59`1::MoveNext()
// 0x00000082 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally1()
// 0x00000083 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally2()
// 0x00000084 TSource System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000085 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x00000086 System.Object System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x00000087 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000088 System.Collections.IEnumerator System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000089 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x0000008A System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x0000008B System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x0000008C System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000008D TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000008E System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000008F System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000091 System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000092 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::.ctor(System.Int32)
// 0x00000093 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.IDisposable.Dispose()
// 0x00000094 System.Boolean System.Linq.Enumerable_<ExceptIterator>d__77`1::MoveNext()
// 0x00000095 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::<>m__Finally1()
// 0x00000096 TSource System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000097 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.Reset()
// 0x00000098 System.Object System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.get_Current()
// 0x00000099 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000009A System.Collections.IEnumerator System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009B System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::.ctor(System.Int32)
// 0x0000009C System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.IDisposable.Dispose()
// 0x0000009D System.Boolean System.Linq.Enumerable_<ReverseIterator>d__79`1::MoveNext()
// 0x0000009E TSource System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009F System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.Reset()
// 0x000000A0 System.Object System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.get_Current()
// 0x000000A1 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A2 System.Collections.IEnumerator System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A3 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x000000A4 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x000000A5 System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x000000A6 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x000000A7 TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000A8 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x000000A9 System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x000000AA System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000AB System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AC System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000AD System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000AE System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x000000AF System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x000000B0 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000B1 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000B2 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000B3 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000B4 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B5 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000B6 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000B7 System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x000000B8 System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x000000B9 TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x000000BA System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000BB TKey System.Linq.IGrouping`2::get_Key()
// 0x000000BC System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000BD System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000BE System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000BF System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C0 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000C1 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000C2 System.Void System.Linq.Lookup`2::Resize()
// 0x000000C3 System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x000000C4 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x000000C5 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C6 TKey System.Linq.Lookup`2_Grouping::get_Key()
// 0x000000C7 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x000000C8 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x000000C9 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x000000CA System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x000000CB System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x000000CC System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x000000CD System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x000000CE System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000CF System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000D0 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000D1 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000D2 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000D3 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x000000D4 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000D5 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000D6 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x000000D7 TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000D8 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000D9 System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000DA System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000DB System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000DC System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x000000DD System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000DE System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000DF System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000E0 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000E1 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000E2 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000E3 System.Void System.Linq.Set`1::Resize()
// 0x000000E4 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000E5 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000E6 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000E7 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E8 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000E9 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000EA System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000EB System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000EC System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000ED System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000EE System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000EF System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000F0 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000F1 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000F2 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000F3 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000F4 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000F5 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000F6 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000F7 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000F8 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000F9 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000FA System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000FB System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000FC System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000FD System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000FE TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000FF System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000100 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000101 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000102 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000103 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000104 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000105 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000106 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000107 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000108 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000109 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000010A System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000010B System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000010C System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000010D System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000010E System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000010F System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000110 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000111 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000112 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000113 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000114 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000115 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000116 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000117 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000118 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000119 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000011A T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000011B System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000011C System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[284] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[284] = 
{
	0,
	0,
	4,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	95,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[86] = 
{
	{ 0x02000004, { 124, 4 } },
	{ 0x02000005, { 128, 9 } },
	{ 0x02000006, { 139, 7 } },
	{ 0x02000007, { 148, 10 } },
	{ 0x02000008, { 160, 11 } },
	{ 0x02000009, { 174, 9 } },
	{ 0x0200000A, { 186, 12 } },
	{ 0x0200000B, { 201, 1 } },
	{ 0x0200000C, { 202, 2 } },
	{ 0x0200000D, { 204, 12 } },
	{ 0x0200000E, { 216, 8 } },
	{ 0x0200000F, { 224, 8 } },
	{ 0x02000010, { 232, 9 } },
	{ 0x02000011, { 241, 11 } },
	{ 0x02000012, { 252, 11 } },
	{ 0x02000013, { 263, 6 } },
	{ 0x02000014, { 269, 6 } },
	{ 0x02000015, { 275, 6 } },
	{ 0x02000016, { 281, 2 } },
	{ 0x02000017, { 283, 4 } },
	{ 0x02000018, { 287, 3 } },
	{ 0x0200001B, { 290, 17 } },
	{ 0x0200001C, { 311, 5 } },
	{ 0x0200001D, { 316, 1 } },
	{ 0x0200001F, { 317, 8 } },
	{ 0x02000021, { 325, 4 } },
	{ 0x02000022, { 329, 3 } },
	{ 0x02000023, { 334, 5 } },
	{ 0x02000024, { 339, 7 } },
	{ 0x02000025, { 346, 3 } },
	{ 0x02000026, { 349, 7 } },
	{ 0x02000027, { 356, 4 } },
	{ 0x02000028, { 360, 23 } },
	{ 0x0200002A, { 383, 2 } },
	{ 0x06000007, { 0, 10 } },
	{ 0x06000008, { 10, 10 } },
	{ 0x06000009, { 20, 5 } },
	{ 0x0600000A, { 25, 5 } },
	{ 0x0600000B, { 30, 1 } },
	{ 0x0600000C, { 31, 2 } },
	{ 0x0600000D, { 33, 1 } },
	{ 0x0600000E, { 34, 2 } },
	{ 0x0600000F, { 36, 1 } },
	{ 0x06000010, { 37, 2 } },
	{ 0x06000011, { 39, 2 } },
	{ 0x06000012, { 41, 2 } },
	{ 0x06000013, { 43, 1 } },
	{ 0x06000014, { 44, 4 } },
	{ 0x06000015, { 48, 1 } },
	{ 0x06000016, { 49, 2 } },
	{ 0x06000017, { 51, 1 } },
	{ 0x06000018, { 52, 2 } },
	{ 0x06000019, { 54, 1 } },
	{ 0x0600001A, { 55, 2 } },
	{ 0x0600001B, { 57, 1 } },
	{ 0x0600001C, { 58, 2 } },
	{ 0x0600001D, { 60, 3 } },
	{ 0x0600001E, { 63, 2 } },
	{ 0x0600001F, { 65, 1 } },
	{ 0x06000020, { 66, 7 } },
	{ 0x06000021, { 73, 1 } },
	{ 0x06000022, { 74, 2 } },
	{ 0x06000023, { 76, 2 } },
	{ 0x06000024, { 78, 2 } },
	{ 0x06000025, { 80, 4 } },
	{ 0x06000026, { 84, 4 } },
	{ 0x06000027, { 88, 4 } },
	{ 0x06000028, { 92, 4 } },
	{ 0x06000029, { 96, 4 } },
	{ 0x0600002A, { 100, 4 } },
	{ 0x0600002B, { 104, 3 } },
	{ 0x0600002C, { 107, 1 } },
	{ 0x0600002D, { 108, 1 } },
	{ 0x0600002E, { 109, 3 } },
	{ 0x0600002F, { 112, 3 } },
	{ 0x06000030, { 115, 2 } },
	{ 0x06000031, { 117, 2 } },
	{ 0x06000032, { 119, 5 } },
	{ 0x06000043, { 137, 2 } },
	{ 0x06000048, { 146, 2 } },
	{ 0x0600004D, { 158, 2 } },
	{ 0x06000053, { 171, 3 } },
	{ 0x06000058, { 183, 3 } },
	{ 0x0600005D, { 198, 3 } },
	{ 0x060000BC, { 307, 4 } },
	{ 0x060000EB, { 332, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[385] = 
{
	{ (Il2CppRGCTXDataType)2, 29217 },
	{ (Il2CppRGCTXDataType)3, 18920 },
	{ (Il2CppRGCTXDataType)2, 29218 },
	{ (Il2CppRGCTXDataType)2, 29219 },
	{ (Il2CppRGCTXDataType)3, 18921 },
	{ (Il2CppRGCTXDataType)2, 29220 },
	{ (Il2CppRGCTXDataType)2, 29221 },
	{ (Il2CppRGCTXDataType)3, 18922 },
	{ (Il2CppRGCTXDataType)2, 29222 },
	{ (Il2CppRGCTXDataType)3, 18923 },
	{ (Il2CppRGCTXDataType)2, 29223 },
	{ (Il2CppRGCTXDataType)3, 18924 },
	{ (Il2CppRGCTXDataType)2, 29224 },
	{ (Il2CppRGCTXDataType)2, 29225 },
	{ (Il2CppRGCTXDataType)3, 18925 },
	{ (Il2CppRGCTXDataType)2, 29226 },
	{ (Il2CppRGCTXDataType)2, 29227 },
	{ (Il2CppRGCTXDataType)3, 18926 },
	{ (Il2CppRGCTXDataType)2, 29228 },
	{ (Il2CppRGCTXDataType)3, 18927 },
	{ (Il2CppRGCTXDataType)2, 29229 },
	{ (Il2CppRGCTXDataType)3, 18928 },
	{ (Il2CppRGCTXDataType)3, 18929 },
	{ (Il2CppRGCTXDataType)2, 18924 },
	{ (Il2CppRGCTXDataType)3, 18930 },
	{ (Il2CppRGCTXDataType)2, 29230 },
	{ (Il2CppRGCTXDataType)3, 18931 },
	{ (Il2CppRGCTXDataType)3, 18932 },
	{ (Il2CppRGCTXDataType)2, 18931 },
	{ (Il2CppRGCTXDataType)3, 18933 },
	{ (Il2CppRGCTXDataType)3, 18934 },
	{ (Il2CppRGCTXDataType)2, 29231 },
	{ (Il2CppRGCTXDataType)3, 18935 },
	{ (Il2CppRGCTXDataType)3, 18936 },
	{ (Il2CppRGCTXDataType)2, 29232 },
	{ (Il2CppRGCTXDataType)3, 18937 },
	{ (Il2CppRGCTXDataType)3, 18938 },
	{ (Il2CppRGCTXDataType)2, 29233 },
	{ (Il2CppRGCTXDataType)3, 18939 },
	{ (Il2CppRGCTXDataType)2, 29234 },
	{ (Il2CppRGCTXDataType)3, 18940 },
	{ (Il2CppRGCTXDataType)2, 29235 },
	{ (Il2CppRGCTXDataType)3, 18941 },
	{ (Il2CppRGCTXDataType)3, 18942 },
	{ (Il2CppRGCTXDataType)3, 18943 },
	{ (Il2CppRGCTXDataType)2, 29236 },
	{ (Il2CppRGCTXDataType)2, 29237 },
	{ (Il2CppRGCTXDataType)3, 18944 },
	{ (Il2CppRGCTXDataType)3, 18945 },
	{ (Il2CppRGCTXDataType)2, 29238 },
	{ (Il2CppRGCTXDataType)3, 18946 },
	{ (Il2CppRGCTXDataType)3, 18947 },
	{ (Il2CppRGCTXDataType)2, 29239 },
	{ (Il2CppRGCTXDataType)3, 18948 },
	{ (Il2CppRGCTXDataType)3, 18949 },
	{ (Il2CppRGCTXDataType)2, 29240 },
	{ (Il2CppRGCTXDataType)3, 18950 },
	{ (Il2CppRGCTXDataType)3, 18951 },
	{ (Il2CppRGCTXDataType)2, 29241 },
	{ (Il2CppRGCTXDataType)3, 18952 },
	{ (Il2CppRGCTXDataType)2, 29242 },
	{ (Il2CppRGCTXDataType)3, 18953 },
	{ (Il2CppRGCTXDataType)3, 18954 },
	{ (Il2CppRGCTXDataType)2, 18993 },
	{ (Il2CppRGCTXDataType)3, 18955 },
	{ (Il2CppRGCTXDataType)3, 18956 },
	{ (Il2CppRGCTXDataType)2, 19008 },
	{ (Il2CppRGCTXDataType)3, 18957 },
	{ (Il2CppRGCTXDataType)2, 19001 },
	{ (Il2CppRGCTXDataType)2, 29243 },
	{ (Il2CppRGCTXDataType)3, 18958 },
	{ (Il2CppRGCTXDataType)3, 18959 },
	{ (Il2CppRGCTXDataType)3, 18960 },
	{ (Il2CppRGCTXDataType)3, 18961 },
	{ (Il2CppRGCTXDataType)2, 29244 },
	{ (Il2CppRGCTXDataType)3, 18962 },
	{ (Il2CppRGCTXDataType)2, 19013 },
	{ (Il2CppRGCTXDataType)3, 18963 },
	{ (Il2CppRGCTXDataType)2, 29245 },
	{ (Il2CppRGCTXDataType)3, 18964 },
	{ (Il2CppRGCTXDataType)2, 29246 },
	{ (Il2CppRGCTXDataType)2, 29247 },
	{ (Il2CppRGCTXDataType)2, 19017 },
	{ (Il2CppRGCTXDataType)2, 29248 },
	{ (Il2CppRGCTXDataType)2, 29249 },
	{ (Il2CppRGCTXDataType)2, 29250 },
	{ (Il2CppRGCTXDataType)2, 19019 },
	{ (Il2CppRGCTXDataType)2, 29251 },
	{ (Il2CppRGCTXDataType)2, 29252 },
	{ (Il2CppRGCTXDataType)2, 29253 },
	{ (Il2CppRGCTXDataType)2, 19021 },
	{ (Il2CppRGCTXDataType)2, 29254 },
	{ (Il2CppRGCTXDataType)2, 29255 },
	{ (Il2CppRGCTXDataType)2, 29256 },
	{ (Il2CppRGCTXDataType)2, 19023 },
	{ (Il2CppRGCTXDataType)2, 29257 },
	{ (Il2CppRGCTXDataType)2, 29258 },
	{ (Il2CppRGCTXDataType)2, 29259 },
	{ (Il2CppRGCTXDataType)2, 19025 },
	{ (Il2CppRGCTXDataType)2, 29260 },
	{ (Il2CppRGCTXDataType)2, 29261 },
	{ (Il2CppRGCTXDataType)2, 29262 },
	{ (Il2CppRGCTXDataType)2, 19027 },
	{ (Il2CppRGCTXDataType)2, 29263 },
	{ (Il2CppRGCTXDataType)2, 19029 },
	{ (Il2CppRGCTXDataType)2, 29264 },
	{ (Il2CppRGCTXDataType)3, 18965 },
	{ (Il2CppRGCTXDataType)2, 29265 },
	{ (Il2CppRGCTXDataType)2, 19034 },
	{ (Il2CppRGCTXDataType)2, 19036 },
	{ (Il2CppRGCTXDataType)2, 29266 },
	{ (Il2CppRGCTXDataType)3, 18966 },
	{ (Il2CppRGCTXDataType)2, 19039 },
	{ (Il2CppRGCTXDataType)2, 29267 },
	{ (Il2CppRGCTXDataType)3, 18967 },
	{ (Il2CppRGCTXDataType)2, 29268 },
	{ (Il2CppRGCTXDataType)2, 19042 },
	{ (Il2CppRGCTXDataType)2, 29269 },
	{ (Il2CppRGCTXDataType)3, 18968 },
	{ (Il2CppRGCTXDataType)3, 18969 },
	{ (Il2CppRGCTXDataType)2, 29270 },
	{ (Il2CppRGCTXDataType)2, 19046 },
	{ (Il2CppRGCTXDataType)2, 29271 },
	{ (Il2CppRGCTXDataType)2, 19048 },
	{ (Il2CppRGCTXDataType)3, 18970 },
	{ (Il2CppRGCTXDataType)3, 18971 },
	{ (Il2CppRGCTXDataType)2, 19051 },
	{ (Il2CppRGCTXDataType)3, 18972 },
	{ (Il2CppRGCTXDataType)3, 18973 },
	{ (Il2CppRGCTXDataType)2, 19063 },
	{ (Il2CppRGCTXDataType)2, 29272 },
	{ (Il2CppRGCTXDataType)3, 18974 },
	{ (Il2CppRGCTXDataType)3, 18975 },
	{ (Il2CppRGCTXDataType)2, 19065 },
	{ (Il2CppRGCTXDataType)2, 28979 },
	{ (Il2CppRGCTXDataType)3, 18976 },
	{ (Il2CppRGCTXDataType)3, 18977 },
	{ (Il2CppRGCTXDataType)2, 29273 },
	{ (Il2CppRGCTXDataType)3, 18978 },
	{ (Il2CppRGCTXDataType)3, 18979 },
	{ (Il2CppRGCTXDataType)2, 19075 },
	{ (Il2CppRGCTXDataType)2, 29274 },
	{ (Il2CppRGCTXDataType)3, 18980 },
	{ (Il2CppRGCTXDataType)3, 18981 },
	{ (Il2CppRGCTXDataType)3, 17615 },
	{ (Il2CppRGCTXDataType)3, 18982 },
	{ (Il2CppRGCTXDataType)2, 29275 },
	{ (Il2CppRGCTXDataType)3, 18983 },
	{ (Il2CppRGCTXDataType)3, 18984 },
	{ (Il2CppRGCTXDataType)2, 19087 },
	{ (Il2CppRGCTXDataType)2, 29276 },
	{ (Il2CppRGCTXDataType)3, 18985 },
	{ (Il2CppRGCTXDataType)3, 18986 },
	{ (Il2CppRGCTXDataType)3, 18987 },
	{ (Il2CppRGCTXDataType)3, 18988 },
	{ (Il2CppRGCTXDataType)3, 18989 },
	{ (Il2CppRGCTXDataType)3, 17621 },
	{ (Il2CppRGCTXDataType)3, 18990 },
	{ (Il2CppRGCTXDataType)2, 29277 },
	{ (Il2CppRGCTXDataType)3, 18991 },
	{ (Il2CppRGCTXDataType)3, 18992 },
	{ (Il2CppRGCTXDataType)2, 19100 },
	{ (Il2CppRGCTXDataType)2, 29278 },
	{ (Il2CppRGCTXDataType)3, 18993 },
	{ (Il2CppRGCTXDataType)3, 18994 },
	{ (Il2CppRGCTXDataType)2, 19102 },
	{ (Il2CppRGCTXDataType)2, 29279 },
	{ (Il2CppRGCTXDataType)3, 18995 },
	{ (Il2CppRGCTXDataType)3, 18996 },
	{ (Il2CppRGCTXDataType)2, 29280 },
	{ (Il2CppRGCTXDataType)3, 18997 },
	{ (Il2CppRGCTXDataType)3, 18998 },
	{ (Il2CppRGCTXDataType)2, 29281 },
	{ (Il2CppRGCTXDataType)3, 18999 },
	{ (Il2CppRGCTXDataType)3, 19000 },
	{ (Il2CppRGCTXDataType)2, 19117 },
	{ (Il2CppRGCTXDataType)2, 29282 },
	{ (Il2CppRGCTXDataType)3, 19001 },
	{ (Il2CppRGCTXDataType)3, 19002 },
	{ (Il2CppRGCTXDataType)3, 19003 },
	{ (Il2CppRGCTXDataType)3, 17632 },
	{ (Il2CppRGCTXDataType)2, 29283 },
	{ (Il2CppRGCTXDataType)3, 19004 },
	{ (Il2CppRGCTXDataType)3, 19005 },
	{ (Il2CppRGCTXDataType)2, 29284 },
	{ (Il2CppRGCTXDataType)3, 19006 },
	{ (Il2CppRGCTXDataType)3, 19007 },
	{ (Il2CppRGCTXDataType)2, 19133 },
	{ (Il2CppRGCTXDataType)2, 29285 },
	{ (Il2CppRGCTXDataType)3, 19008 },
	{ (Il2CppRGCTXDataType)3, 19009 },
	{ (Il2CppRGCTXDataType)3, 19010 },
	{ (Il2CppRGCTXDataType)3, 19011 },
	{ (Il2CppRGCTXDataType)3, 19012 },
	{ (Il2CppRGCTXDataType)3, 19013 },
	{ (Il2CppRGCTXDataType)3, 17638 },
	{ (Il2CppRGCTXDataType)2, 29286 },
	{ (Il2CppRGCTXDataType)3, 19014 },
	{ (Il2CppRGCTXDataType)3, 19015 },
	{ (Il2CppRGCTXDataType)2, 29287 },
	{ (Il2CppRGCTXDataType)3, 19016 },
	{ (Il2CppRGCTXDataType)3, 19017 },
	{ (Il2CppRGCTXDataType)3, 19018 },
	{ (Il2CppRGCTXDataType)3, 19019 },
	{ (Il2CppRGCTXDataType)3, 19020 },
	{ (Il2CppRGCTXDataType)3, 19021 },
	{ (Il2CppRGCTXDataType)2, 29288 },
	{ (Il2CppRGCTXDataType)2, 29289 },
	{ (Il2CppRGCTXDataType)3, 19022 },
	{ (Il2CppRGCTXDataType)2, 19168 },
	{ (Il2CppRGCTXDataType)2, 19162 },
	{ (Il2CppRGCTXDataType)3, 19023 },
	{ (Il2CppRGCTXDataType)2, 19161 },
	{ (Il2CppRGCTXDataType)2, 29290 },
	{ (Il2CppRGCTXDataType)3, 19024 },
	{ (Il2CppRGCTXDataType)3, 19025 },
	{ (Il2CppRGCTXDataType)3, 19026 },
	{ (Il2CppRGCTXDataType)2, 19181 },
	{ (Il2CppRGCTXDataType)2, 19176 },
	{ (Il2CppRGCTXDataType)3, 19027 },
	{ (Il2CppRGCTXDataType)2, 19175 },
	{ (Il2CppRGCTXDataType)2, 29291 },
	{ (Il2CppRGCTXDataType)3, 19028 },
	{ (Il2CppRGCTXDataType)3, 19029 },
	{ (Il2CppRGCTXDataType)3, 19030 },
	{ (Il2CppRGCTXDataType)2, 19191 },
	{ (Il2CppRGCTXDataType)2, 19186 },
	{ (Il2CppRGCTXDataType)3, 19031 },
	{ (Il2CppRGCTXDataType)2, 19185 },
	{ (Il2CppRGCTXDataType)2, 29292 },
	{ (Il2CppRGCTXDataType)3, 19032 },
	{ (Il2CppRGCTXDataType)3, 19033 },
	{ (Il2CppRGCTXDataType)3, 19034 },
	{ (Il2CppRGCTXDataType)3, 19035 },
	{ (Il2CppRGCTXDataType)2, 19201 },
	{ (Il2CppRGCTXDataType)2, 19196 },
	{ (Il2CppRGCTXDataType)3, 19036 },
	{ (Il2CppRGCTXDataType)2, 19195 },
	{ (Il2CppRGCTXDataType)2, 29293 },
	{ (Il2CppRGCTXDataType)3, 19037 },
	{ (Il2CppRGCTXDataType)3, 19038 },
	{ (Il2CppRGCTXDataType)3, 19039 },
	{ (Il2CppRGCTXDataType)2, 29294 },
	{ (Il2CppRGCTXDataType)3, 19040 },
	{ (Il2CppRGCTXDataType)2, 19214 },
	{ (Il2CppRGCTXDataType)2, 19206 },
	{ (Il2CppRGCTXDataType)3, 19041 },
	{ (Il2CppRGCTXDataType)3, 19042 },
	{ (Il2CppRGCTXDataType)2, 19205 },
	{ (Il2CppRGCTXDataType)2, 29295 },
	{ (Il2CppRGCTXDataType)3, 19043 },
	{ (Il2CppRGCTXDataType)3, 19044 },
	{ (Il2CppRGCTXDataType)3, 19045 },
	{ (Il2CppRGCTXDataType)2, 29296 },
	{ (Il2CppRGCTXDataType)3, 19046 },
	{ (Il2CppRGCTXDataType)2, 19227 },
	{ (Il2CppRGCTXDataType)2, 19219 },
	{ (Il2CppRGCTXDataType)3, 19047 },
	{ (Il2CppRGCTXDataType)3, 19048 },
	{ (Il2CppRGCTXDataType)2, 19218 },
	{ (Il2CppRGCTXDataType)2, 29297 },
	{ (Il2CppRGCTXDataType)3, 19049 },
	{ (Il2CppRGCTXDataType)3, 19050 },
	{ (Il2CppRGCTXDataType)2, 29298 },
	{ (Il2CppRGCTXDataType)3, 19051 },
	{ (Il2CppRGCTXDataType)2, 19231 },
	{ (Il2CppRGCTXDataType)2, 29299 },
	{ (Il2CppRGCTXDataType)3, 19052 },
	{ (Il2CppRGCTXDataType)3, 19053 },
	{ (Il2CppRGCTXDataType)3, 19054 },
	{ (Il2CppRGCTXDataType)2, 19241 },
	{ (Il2CppRGCTXDataType)3, 19055 },
	{ (Il2CppRGCTXDataType)2, 29300 },
	{ (Il2CppRGCTXDataType)3, 19056 },
	{ (Il2CppRGCTXDataType)3, 19057 },
	{ (Il2CppRGCTXDataType)3, 19058 },
	{ (Il2CppRGCTXDataType)2, 19249 },
	{ (Il2CppRGCTXDataType)3, 19059 },
	{ (Il2CppRGCTXDataType)2, 29301 },
	{ (Il2CppRGCTXDataType)3, 19060 },
	{ (Il2CppRGCTXDataType)3, 19061 },
	{ (Il2CppRGCTXDataType)2, 29302 },
	{ (Il2CppRGCTXDataType)2, 29303 },
	{ (Il2CppRGCTXDataType)2, 29304 },
	{ (Il2CppRGCTXDataType)3, 19062 },
	{ (Il2CppRGCTXDataType)2, 19260 },
	{ (Il2CppRGCTXDataType)3, 19063 },
	{ (Il2CppRGCTXDataType)2, 29305 },
	{ (Il2CppRGCTXDataType)3, 19064 },
	{ (Il2CppRGCTXDataType)2, 29305 },
	{ (Il2CppRGCTXDataType)2, 19289 },
	{ (Il2CppRGCTXDataType)3, 19065 },
	{ (Il2CppRGCTXDataType)3, 19066 },
	{ (Il2CppRGCTXDataType)3, 19067 },
	{ (Il2CppRGCTXDataType)3, 19068 },
	{ (Il2CppRGCTXDataType)2, 29306 },
	{ (Il2CppRGCTXDataType)2, 29307 },
	{ (Il2CppRGCTXDataType)2, 29308 },
	{ (Il2CppRGCTXDataType)3, 19069 },
	{ (Il2CppRGCTXDataType)3, 19070 },
	{ (Il2CppRGCTXDataType)2, 19285 },
	{ (Il2CppRGCTXDataType)2, 19288 },
	{ (Il2CppRGCTXDataType)3, 19071 },
	{ (Il2CppRGCTXDataType)3, 19072 },
	{ (Il2CppRGCTXDataType)2, 19292 },
	{ (Il2CppRGCTXDataType)3, 19073 },
	{ (Il2CppRGCTXDataType)2, 29309 },
	{ (Il2CppRGCTXDataType)2, 19282 },
	{ (Il2CppRGCTXDataType)2, 29310 },
	{ (Il2CppRGCTXDataType)3, 19074 },
	{ (Il2CppRGCTXDataType)3, 19075 },
	{ (Il2CppRGCTXDataType)3, 19076 },
	{ (Il2CppRGCTXDataType)2, 29311 },
	{ (Il2CppRGCTXDataType)3, 19077 },
	{ (Il2CppRGCTXDataType)3, 19078 },
	{ (Il2CppRGCTXDataType)3, 19079 },
	{ (Il2CppRGCTXDataType)2, 19307 },
	{ (Il2CppRGCTXDataType)3, 19080 },
	{ (Il2CppRGCTXDataType)2, 29312 },
	{ (Il2CppRGCTXDataType)2, 29313 },
	{ (Il2CppRGCTXDataType)3, 19081 },
	{ (Il2CppRGCTXDataType)3, 19082 },
	{ (Il2CppRGCTXDataType)2, 19328 },
	{ (Il2CppRGCTXDataType)3, 19083 },
	{ (Il2CppRGCTXDataType)2, 19329 },
	{ (Il2CppRGCTXDataType)3, 19084 },
	{ (Il2CppRGCTXDataType)2, 29314 },
	{ (Il2CppRGCTXDataType)3, 19085 },
	{ (Il2CppRGCTXDataType)3, 19086 },
	{ (Il2CppRGCTXDataType)2, 29315 },
	{ (Il2CppRGCTXDataType)3, 19087 },
	{ (Il2CppRGCTXDataType)3, 19088 },
	{ (Il2CppRGCTXDataType)2, 29316 },
	{ (Il2CppRGCTXDataType)3, 19089 },
	{ (Il2CppRGCTXDataType)2, 29317 },
	{ (Il2CppRGCTXDataType)3, 19090 },
	{ (Il2CppRGCTXDataType)3, 19091 },
	{ (Il2CppRGCTXDataType)3, 19092 },
	{ (Il2CppRGCTXDataType)2, 19364 },
	{ (Il2CppRGCTXDataType)3, 19093 },
	{ (Il2CppRGCTXDataType)2, 19372 },
	{ (Il2CppRGCTXDataType)3, 19094 },
	{ (Il2CppRGCTXDataType)2, 29318 },
	{ (Il2CppRGCTXDataType)2, 29319 },
	{ (Il2CppRGCTXDataType)3, 19095 },
	{ (Il2CppRGCTXDataType)3, 19096 },
	{ (Il2CppRGCTXDataType)3, 19097 },
	{ (Il2CppRGCTXDataType)3, 19098 },
	{ (Il2CppRGCTXDataType)3, 19099 },
	{ (Il2CppRGCTXDataType)3, 19100 },
	{ (Il2CppRGCTXDataType)2, 19388 },
	{ (Il2CppRGCTXDataType)2, 29320 },
	{ (Il2CppRGCTXDataType)3, 19101 },
	{ (Il2CppRGCTXDataType)3, 19102 },
	{ (Il2CppRGCTXDataType)2, 19392 },
	{ (Il2CppRGCTXDataType)3, 19103 },
	{ (Il2CppRGCTXDataType)2, 29321 },
	{ (Il2CppRGCTXDataType)2, 19402 },
	{ (Il2CppRGCTXDataType)2, 19400 },
	{ (Il2CppRGCTXDataType)2, 29322 },
	{ (Il2CppRGCTXDataType)3, 19104 },
	{ (Il2CppRGCTXDataType)2, 29323 },
	{ (Il2CppRGCTXDataType)3, 19105 },
	{ (Il2CppRGCTXDataType)3, 19106 },
	{ (Il2CppRGCTXDataType)3, 19107 },
	{ (Il2CppRGCTXDataType)2, 19406 },
	{ (Il2CppRGCTXDataType)3, 19108 },
	{ (Il2CppRGCTXDataType)3, 19109 },
	{ (Il2CppRGCTXDataType)2, 19409 },
	{ (Il2CppRGCTXDataType)3, 19110 },
	{ (Il2CppRGCTXDataType)1, 29324 },
	{ (Il2CppRGCTXDataType)2, 19408 },
	{ (Il2CppRGCTXDataType)3, 19111 },
	{ (Il2CppRGCTXDataType)1, 19408 },
	{ (Il2CppRGCTXDataType)1, 19406 },
	{ (Il2CppRGCTXDataType)2, 29325 },
	{ (Il2CppRGCTXDataType)2, 19408 },
	{ (Il2CppRGCTXDataType)2, 19411 },
	{ (Il2CppRGCTXDataType)2, 19410 },
	{ (Il2CppRGCTXDataType)3, 19112 },
	{ (Il2CppRGCTXDataType)3, 19113 },
	{ (Il2CppRGCTXDataType)3, 19114 },
	{ (Il2CppRGCTXDataType)2, 19407 },
	{ (Il2CppRGCTXDataType)3, 19115 },
	{ (Il2CppRGCTXDataType)2, 19421 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	284,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	86,
	s_rgctxIndices,
	385,
	s_rgctxValues,
	NULL,
};
