﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// ModestTree.Util.Action`11<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5;
// ModestTree.Util.Action`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D;
// ModestTree.Util.Action`5<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object>
struct Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7;
// ModestTree.Util.Action`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716;
// ModestTree.Util.Action`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436;
// ModestTree.Util.Action`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE;
// ModestTree.Util.Action`7<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98;
// ModestTree.Util.Action`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<Zenject.DiContainer>
struct Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Action`2<Zenject.DiContainer,System.Object>
struct Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849;
// System.Action`3<Zenject.DiContainer,System.Object,System.Object>
struct Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E;
// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02;
// System.Action`4<Zenject.DiContainer,System.Object,System.Object,System.Object>
struct Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,Zenject.Internal.IDecoratorProvider>
struct Dictionary_2_t96A3AED3582C80B84ACB93401D69B317CBF0D0C4;
// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer/ProviderInfo>>
struct Dictionary_2_t0015F167341446CF132A158D9AA8181E35AD62C8;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t0AADC413B8AE37E5F22243315AD1388C97139717;
// System.Collections.Generic.HashSet`1<Zenject.Internal.LookupId>
struct HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402;
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct IEnumerable_1_tF4FF4857226623CD196019543C1716799A2DD83D;
// System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.BindStatement>
struct List_1_tDEB5DB2F48B7B68378607F2160A2568FB6006838;
// System.Collections.Generic.List`1<Zenject.IValidatable>
struct List_1_t5641176595AAD1305F98671F818E431B2F48AFFA;
// System.Collections.Generic.List`1<Zenject.InstallerBase>
struct List_1_tED41AE82F98DF9C43E55F5E27BF9DF208C29E870;
// System.Collections.Generic.List`1<Zenject.MonoInstaller>
struct List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4;
// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller>
struct List_1_tD19F9EA21326BFA6873B6DFA21EF71F278745931;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3;
// System.Collections.Generic.Queue`1<Zenject.BindStatement>
struct Queue_1_tE82A52D54AAB980687E5EBAC89F6A219AD4AB498;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<Zenject.InjectContext,UnityEngine.Transform>
struct Func_2_t633D1A4DF778F0B0CCCCCB2CC76D14A005537EF0;
// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<System.Object>,System.Object>
struct Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.ActionInstaller
struct ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788;
// Zenject.Context
struct Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1;
// Zenject.DiContainer
struct DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD;
// Zenject.DiContainer[][]
struct DiContainerU5BU5DU5BU5D_t22493FE43E7D69D826610983995A88E649F905D0;
// Zenject.GameObjectContext
struct GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3;
// Zenject.IPrefabProvider
struct IPrefabProvider_t5BFFC9CC079B7749FB3BE14B615D941BDCDAC2FF;
// Zenject.InjectContext
struct InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31;
// Zenject.InstallerBase
struct InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C;
// Zenject.Internal.SingletonMarkRegistry
struct SingletonMarkRegistry_t1CC71C2389111B0958A3382C2CF48235E19146EF;
// Zenject.LazyInstanceInjector
struct LazyInstanceInjector_t3304C20D9E4B02B77A968E5E06A7FE576066A1DC;
// Zenject.MonoKernel
struct MonoKernel_tEAA6CF5616476382B597DF6FD141BD83E31D3403;
// Zenject.ScriptableObjectInstallerBase
struct ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54;
// Zenject.ScriptableObjectInstaller`2<System.Object,System.Object>
struct ScriptableObjectInstaller_2_tBAA795F1506AFB607E16B35B7A4337DAA0002DA4;
// Zenject.ScriptableObjectInstaller`3<System.Object,System.Object,System.Object>
struct ScriptableObjectInstaller_3_t79F4C7B68872800D6DC876C4DCEBA1D1A9CB27CB;
// Zenject.ScriptableObjectInstaller`4<System.Object,System.Object,System.Object,System.Object>
struct ScriptableObjectInstaller_4_t5D634B3ACB23B7B62430C5C6F2AD9CA91FF404FE;
// Zenject.ScriptableObjectInstaller`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ScriptableObjectInstaller_5_tDC9D534685DE3245705BC3C63081086DA268F493;
// Zenject.StaticMemoryPoolBaseBase`1<System.Object>
struct StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB;
// Zenject.StaticMemoryPoolBase`1<System.Object>
struct StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79;
// Zenject.StaticMemoryPool`1<System.Object>
struct StaticMemoryPool_1_tADD8B93F60A6E6DC7E59D7A91F7557D01507E6F7;
// Zenject.StaticMemoryPool`2<System.Object,System.Object>
struct StaticMemoryPool_2_t63EB594E16C7E78C6EDAC86CE39B59FABDC3C457;
// Zenject.StaticMemoryPool`3<System.Object,System.Object,System.Object>
struct StaticMemoryPool_3_tA3947EBC0BA4452560E09FA0013577D47D0A8D60;
// Zenject.StaticMemoryPool`4<System.Object,System.Object,System.Object,System.Object>
struct StaticMemoryPool_4_t6DF0187E9ABA0C47B8DBF11D3DC9AE3DFB4A7DA3;
// Zenject.StaticMemoryPool`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct StaticMemoryPool_5_t96603A65CB9F3F56CB468EB64D1F6EFBF83CF030;
// Zenject.StaticMemoryPool`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct StaticMemoryPool_6_t1FFF7C2B7C5AFFADA7DC36973805FA8C6B2E875E;
// Zenject.StaticMemoryPool`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct StaticMemoryPool_7_tFB2DA9394241B43A5C50B8A689D945B135D990C0;
// Zenject.StaticMemoryPool`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct StaticMemoryPool_8_tF8EAF86FA5D397071FAB0F840D76CD52658C3D9C;
// Zenject.SubContainerCreatorBindInfo
struct SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188;
// Zenject.SubContainerCreatorByMethodBase
struct SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF;
// Zenject.SubContainerCreatorByMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByMethod_10_tC7ED152395C2E669AD9049049C18C57B849BA4E7;
// Zenject.SubContainerCreatorByMethod`1<System.Object>
struct SubContainerCreatorByMethod_1_t1DD94EA8E04732E929542EB4EC171FC8B8227260;
// Zenject.SubContainerCreatorByMethod`2<System.Object,System.Object>
struct SubContainerCreatorByMethod_2_t1C128269485A9B8897C6231192A0BA84F86E8DEB;
// Zenject.SubContainerCreatorByMethod`3<System.Object,System.Object,System.Object>
struct SubContainerCreatorByMethod_3_t9813F91B43CED08E56AEB643D4C0CCE7078B5147;
// Zenject.SubContainerCreatorByMethod`4<System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByMethod_4_tB92E29E6DDC4935B55C75CA8663CA69EFAAC2A86;
// Zenject.SubContainerCreatorByMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByMethod_5_t16E209E58E41BDC695873B2E897DD84B210C2F07;
// Zenject.SubContainerCreatorByMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByMethod_6_tAFCC4BDF101B491171C43984391D402DFED600C5;
// Zenject.SubContainerCreatorByNewGameObjectDynamicContext
struct SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2;
// Zenject.SubContainerCreatorByNewGameObjectMethod`1/<>c__DisplayClass2_0<System.Object>
struct U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C;
// Zenject.SubContainerCreatorByNewGameObjectMethod`10/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3;
// Zenject.SubContainerCreatorByNewGameObjectMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2;
// Zenject.SubContainerCreatorByNewGameObjectMethod`1<System.Object>
struct SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652;
// Zenject.SubContainerCreatorByNewGameObjectMethod`2/<>c__DisplayClass2_0<System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991;
// Zenject.SubContainerCreatorByNewGameObjectMethod`2<System.Object,System.Object>
struct SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB;
// Zenject.SubContainerCreatorByNewGameObjectMethod`3/<>c__DisplayClass2_0<System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F;
// Zenject.SubContainerCreatorByNewGameObjectMethod`3<System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427;
// Zenject.SubContainerCreatorByNewGameObjectMethod`4/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340;
// Zenject.SubContainerCreatorByNewGameObjectMethod`4<System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0;
// Zenject.SubContainerCreatorByNewGameObjectMethod`5/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318;
// Zenject.SubContainerCreatorByNewGameObjectMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D;
// Zenject.SubContainerCreatorByNewGameObjectMethod`6/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9;
// Zenject.SubContainerCreatorByNewGameObjectMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0;
// Zenject.SubContainerCreatorByNewPrefabDynamicContext
struct SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB;
// Zenject.SubContainerCreatorByNewPrefabMethod`1/<>c__DisplayClass2_0<System.Object>
struct U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0;
// Zenject.SubContainerCreatorByNewPrefabMethod`10/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8;
// Zenject.SubContainerCreatorByNewPrefabMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33;
// Zenject.SubContainerCreatorByNewPrefabMethod`1<System.Object>
struct SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA;
// Zenject.SubContainerCreatorByNewPrefabMethod`2/<>c__DisplayClass2_0<System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E;
// Zenject.SubContainerCreatorByNewPrefabMethod`2<System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0;
// Zenject.SubContainerCreatorByNewPrefabMethod`3/<>c__DisplayClass2_0<System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E;
// Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066;
// Zenject.SubContainerCreatorByNewPrefabMethod`4/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9;
// Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D;
// Zenject.SubContainerCreatorByNewPrefabMethod`5/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A;
// Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06;
// Zenject.SubContainerCreatorByNewPrefabMethod`6/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36;
// Zenject.SubContainerCreatorByNewPrefabMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99;
// Zenject.TaskUpdater`1/<>c<System.Object>
struct U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22;
// Zenject.TaskUpdater`1/<>c__DisplayClass8_0<System.Object>
struct U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>[]
struct TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135;
// Zenject.TaskUpdater`1<System.Object>
struct TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7;
// Zenject.TypeValuePair[]
struct TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2;
// Zenject.ZenjectSettings
struct ZenjectSettings_t28FCBC1F7514F4F44F8E63915D8895ACD6F9A5B6;

IL2CPP_EXTERN_C RuntimeClass* ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral232943ADE493F438D7C9C6675E2E2051DB9C0DC8;
IL2CPP_EXTERN_C String_t* _stringLiteral25458F856CEB4BFBD8F8627119406BC1F8394D8E;
IL2CPP_EXTERN_C String_t* _stringLiteral9C9C4ECE5F3F5332F9E4B20785827F8F41DEDADB;
IL2CPP_EXTERN_C String_t* _stringLiteralA0921AD23D7C750DF2A7BCB64D6425DF00D19960;
IL2CPP_EXTERN_C String_t* _stringLiteralA6B57A6430A9D8D7FE0D573C76778648C9F58C6B;
IL2CPP_EXTERN_C String_t* _stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t StaticMemoryPoolBaseBase_1_Despawn_m80DE31AAB2D613005C155E5E7C61BA0A7066022B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StaticMemoryPoolBaseBase_1_ResizeInternal_mBC830B0EC91075C7839B2D6AB0C298AA31864994_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StaticMemoryPoolBaseBase_1_get_ItemType_mE6BBE545C5517ACA528B612FF1A471ADB1714F28_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByMethod_10_CreateSubContainer_m44C3B5BF8B5C6757D5A91AB5D469018F13D9F73D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByMethod_1_CreateSubContainer_m5D013A3A2D0213F5E14A156423A0F4B6A5E2F659_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByMethod_2_CreateSubContainer_mA22D2A0A3F1A8DBBBE761AB55228A16E8CF3493F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByMethod_3_CreateSubContainer_mEFE490353EFB06DCA6B19311AA25BB657ED41BF1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByMethod_4_CreateSubContainer_m04858A904A8D755E3C0EFEFB73D7B3434BA6AE3B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByMethod_5_CreateSubContainer_m9DA731F7C713DD6228B2BC0BC4883286C6B94FA6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByMethod_6_CreateSubContainer_mBBF5BEB545E031C782AF74D5FC154515A41C3977_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewGameObjectMethod_10_AddInstallers_mAAD6B59C6D878EFCF65081228C6229FAE54CA056_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewGameObjectMethod_1_AddInstallers_mFF44F85DA9A324B4B5C0514039FA62A18134D070_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewGameObjectMethod_2_AddInstallers_m0EA016B842429538CDE9E9FBAC9AACBFEDAA4A56_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewGameObjectMethod_3_AddInstallers_mEB871A4B1C6EC575CCC4EEA9A78C89C12B5996D7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewGameObjectMethod_4_AddInstallers_m11C7E83B51E32A581E20C34495C1E53811735FBD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewGameObjectMethod_5_AddInstallers_mD3F66F81F2D3F32C65D1660605BAD4CF5324AAA0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewGameObjectMethod_6_AddInstallers_m571A78434A01AECF5BC12C6AD2E4920A20739D2C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewPrefabMethod_10_AddInstallers_mA4AA2ECC64481159B01AE5B27A0AC6530AA52560_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewPrefabMethod_1_AddInstallers_m23E81B8029A6B4A5210F0F3F4F4C8B18AD1B14C8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewPrefabMethod_2_AddInstallers_m69A239DA9A22F230DDB1512B156B36E0DD1E8D0A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewPrefabMethod_3_AddInstallers_m80F6002F7D667B1B1693943F44C5EF03C2813C87_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewPrefabMethod_4_AddInstallers_m86869CC3A33A2C9A6B99A4229877D34E3F3A0BED_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewPrefabMethod_5_AddInstallers_m7466E5005D14D8F365D6D50143618BE41802A634_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SubContainerCreatorByNewPrefabMethod_6_AddInstallers_m4156B4E21E5547175907A14CB5FC70A2CAD5E31E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TaskUpdater_1_AddTaskInternal_mBC9A15D14E3713CF972114C837961E6D2927286D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TaskUpdater_1_RemoveTask_m0C34F9BF8B24882D761322FBDA5E20F83EDDB80B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m0A2C62741A89DF5C9DFAE4CC00666492964008EB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m20829D28F34B2E1E6781594988BD80F17A558FA5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m24BA475C392B1E398380111677B17EA17608B404_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m294A041BBB4EBBBDCF38B7576013F0F4069D5AA2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m2AF4286DEBB498329BAFA505E92CF2946BACE799_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m3DB72A1FEB9FDBA4CBB0FA76F4BDB6CAB4B1C691_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m481B9C5C76B4F25666FEEF121AD64A5C5739C0E3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m5EC27D5AC7BB9339DEB8B47A940EE8C73DC668DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m80975CF201A3EBC72CEDC4F204483524B9A64900_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m995471C8D22C7D441C4CFD882CB9008F6A848177_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mC84F2B261C09B708A5C3B2EC1F71486E3D56ADEE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mD2E13603487D3FC3F66EF46356CAAED9BBECA68A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mD76112FEB5D17E857868720A9F6CA5C7724214FF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mF64DDE3C1DF908032A6777FC1BE78B74A55C0E3C_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1_TaskInfo<System.Object>>
struct  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * ___item_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA, ___list_0)); }
	inline LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA, ___next_1)); }
	inline LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * get_next_1() const { return ___next_1; }
	inline LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_1), (void*)value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA, ___prev_2)); }
	inline LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * get_prev_2() const { return ___prev_2; }
	inline LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA ** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prev_2), (void*)value);
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA, ___item_3)); }
	inline TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * get_item_3() const { return ___item_3; }
	inline TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_3), (void*)value);
	}
};


// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1_TaskInfo<System.Object>>
struct  LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject * ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ____siInfo_4;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4, ___head_0)); }
	inline LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * get_head_0() const { return ___head_0; }
	inline LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___head_0), (void*)value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}

	inline static int32_t get_offset_of__siInfo_4() { return static_cast<int32_t>(offsetof(LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4, ____siInfo_4)); }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * get__siInfo_4() const { return ____siInfo_4; }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 ** get_address_of__siInfo_4() { return &____siInfo_4; }
	inline void set__siInfo_4(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * value)
	{
		____siInfo_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____siInfo_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<Zenject.TaskUpdater`1_TaskInfo<System.Object>>
struct  List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5, ____items_1)); }
	inline TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135* get__items_1() const { return ____items_1; }
	inline TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5_StaticFields, ____emptyArray_5)); }
	inline TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TaskInfoU5BU5D_tA9B7876A5E505EE9D70063C6CF1EA9C0569EF135* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct  List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3, ____items_1)); }
	inline TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2* get__items_1() const { return ____items_1; }
	inline TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3_StaticFields, ____emptyArray_5)); }
	inline TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Stack`1<System.Object>
struct  Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8, ____array_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// Zenject.DiContainer
struct  DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Zenject.Internal.IDecoratorProvider> Zenject.DiContainer::_decorators
	Dictionary_2_t96A3AED3582C80B84ACB93401D69B317CBF0D0C4 * ____decorators_0;
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer_ProviderInfo>> Zenject.DiContainer::_providers
	Dictionary_2_t0015F167341446CF132A158D9AA8181E35AD62C8 * ____providers_1;
	// Zenject.DiContainer[][] Zenject.DiContainer::_containerLookups
	DiContainerU5BU5DU5BU5D_t22493FE43E7D69D826610983995A88E649F905D0* ____containerLookups_2;
	// System.Collections.Generic.HashSet`1<Zenject.Internal.LookupId> Zenject.DiContainer::_resolvesInProgress
	HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 * ____resolvesInProgress_3;
	// System.Collections.Generic.HashSet`1<Zenject.Internal.LookupId> Zenject.DiContainer::_resolvesTwiceInProgress
	HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 * ____resolvesTwiceInProgress_4;
	// Zenject.LazyInstanceInjector Zenject.DiContainer::_lazyInjector
	LazyInstanceInjector_t3304C20D9E4B02B77A968E5E06A7FE576066A1DC * ____lazyInjector_5;
	// Zenject.Internal.SingletonMarkRegistry Zenject.DiContainer::_singletonMarkRegistry
	SingletonMarkRegistry_t1CC71C2389111B0958A3382C2CF48235E19146EF * ____singletonMarkRegistry_6;
	// System.Collections.Generic.Queue`1<Zenject.BindStatement> Zenject.DiContainer::_currentBindings
	Queue_1_tE82A52D54AAB980687E5EBAC89F6A219AD4AB498 * ____currentBindings_7;
	// System.Collections.Generic.List`1<Zenject.BindStatement> Zenject.DiContainer::_childBindings
	List_1_tDEB5DB2F48B7B68378607F2160A2568FB6006838 * ____childBindings_8;
	// System.Collections.Generic.HashSet`1<System.Type> Zenject.DiContainer::_validatedTypes
	HashSet_1_t0AADC413B8AE37E5F22243315AD1388C97139717 * ____validatedTypes_9;
	// System.Collections.Generic.List`1<Zenject.IValidatable> Zenject.DiContainer::_validationQueue
	List_1_t5641176595AAD1305F98671F818E431B2F48AFFA * ____validationQueue_10;
	// UnityEngine.Transform Zenject.DiContainer::_contextTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____contextTransform_11;
	// System.Boolean Zenject.DiContainer::_hasLookedUpContextTransform
	bool ____hasLookedUpContextTransform_12;
	// UnityEngine.Transform Zenject.DiContainer::_inheritedDefaultParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____inheritedDefaultParent_13;
	// UnityEngine.Transform Zenject.DiContainer::_explicitDefaultParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____explicitDefaultParent_14;
	// System.Boolean Zenject.DiContainer::_hasExplicitDefaultParent
	bool ____hasExplicitDefaultParent_15;
	// Zenject.ZenjectSettings Zenject.DiContainer::_settings
	ZenjectSettings_t28FCBC1F7514F4F44F8E63915D8895ACD6F9A5B6 * ____settings_16;
	// System.Boolean Zenject.DiContainer::_hasResolvedRoots
	bool ____hasResolvedRoots_17;
	// System.Boolean Zenject.DiContainer::_isFinalizingBinding
	bool ____isFinalizingBinding_18;
	// System.Boolean Zenject.DiContainer::_isValidating
	bool ____isValidating_19;
	// System.Boolean Zenject.DiContainer::_isInstalling
	bool ____isInstalling_20;
	// System.Boolean Zenject.DiContainer::<AssertOnNewGameObjects>k__BackingField
	bool ___U3CAssertOnNewGameObjectsU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of__decorators_0() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____decorators_0)); }
	inline Dictionary_2_t96A3AED3582C80B84ACB93401D69B317CBF0D0C4 * get__decorators_0() const { return ____decorators_0; }
	inline Dictionary_2_t96A3AED3582C80B84ACB93401D69B317CBF0D0C4 ** get_address_of__decorators_0() { return &____decorators_0; }
	inline void set__decorators_0(Dictionary_2_t96A3AED3582C80B84ACB93401D69B317CBF0D0C4 * value)
	{
		____decorators_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____decorators_0), (void*)value);
	}

	inline static int32_t get_offset_of__providers_1() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____providers_1)); }
	inline Dictionary_2_t0015F167341446CF132A158D9AA8181E35AD62C8 * get__providers_1() const { return ____providers_1; }
	inline Dictionary_2_t0015F167341446CF132A158D9AA8181E35AD62C8 ** get_address_of__providers_1() { return &____providers_1; }
	inline void set__providers_1(Dictionary_2_t0015F167341446CF132A158D9AA8181E35AD62C8 * value)
	{
		____providers_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____providers_1), (void*)value);
	}

	inline static int32_t get_offset_of__containerLookups_2() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____containerLookups_2)); }
	inline DiContainerU5BU5DU5BU5D_t22493FE43E7D69D826610983995A88E649F905D0* get__containerLookups_2() const { return ____containerLookups_2; }
	inline DiContainerU5BU5DU5BU5D_t22493FE43E7D69D826610983995A88E649F905D0** get_address_of__containerLookups_2() { return &____containerLookups_2; }
	inline void set__containerLookups_2(DiContainerU5BU5DU5BU5D_t22493FE43E7D69D826610983995A88E649F905D0* value)
	{
		____containerLookups_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____containerLookups_2), (void*)value);
	}

	inline static int32_t get_offset_of__resolvesInProgress_3() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____resolvesInProgress_3)); }
	inline HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 * get__resolvesInProgress_3() const { return ____resolvesInProgress_3; }
	inline HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 ** get_address_of__resolvesInProgress_3() { return &____resolvesInProgress_3; }
	inline void set__resolvesInProgress_3(HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 * value)
	{
		____resolvesInProgress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____resolvesInProgress_3), (void*)value);
	}

	inline static int32_t get_offset_of__resolvesTwiceInProgress_4() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____resolvesTwiceInProgress_4)); }
	inline HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 * get__resolvesTwiceInProgress_4() const { return ____resolvesTwiceInProgress_4; }
	inline HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 ** get_address_of__resolvesTwiceInProgress_4() { return &____resolvesTwiceInProgress_4; }
	inline void set__resolvesTwiceInProgress_4(HashSet_1_tBE8072AA2394D56A5F9EA69DD714386B7B64F402 * value)
	{
		____resolvesTwiceInProgress_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____resolvesTwiceInProgress_4), (void*)value);
	}

	inline static int32_t get_offset_of__lazyInjector_5() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____lazyInjector_5)); }
	inline LazyInstanceInjector_t3304C20D9E4B02B77A968E5E06A7FE576066A1DC * get__lazyInjector_5() const { return ____lazyInjector_5; }
	inline LazyInstanceInjector_t3304C20D9E4B02B77A968E5E06A7FE576066A1DC ** get_address_of__lazyInjector_5() { return &____lazyInjector_5; }
	inline void set__lazyInjector_5(LazyInstanceInjector_t3304C20D9E4B02B77A968E5E06A7FE576066A1DC * value)
	{
		____lazyInjector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lazyInjector_5), (void*)value);
	}

	inline static int32_t get_offset_of__singletonMarkRegistry_6() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____singletonMarkRegistry_6)); }
	inline SingletonMarkRegistry_t1CC71C2389111B0958A3382C2CF48235E19146EF * get__singletonMarkRegistry_6() const { return ____singletonMarkRegistry_6; }
	inline SingletonMarkRegistry_t1CC71C2389111B0958A3382C2CF48235E19146EF ** get_address_of__singletonMarkRegistry_6() { return &____singletonMarkRegistry_6; }
	inline void set__singletonMarkRegistry_6(SingletonMarkRegistry_t1CC71C2389111B0958A3382C2CF48235E19146EF * value)
	{
		____singletonMarkRegistry_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____singletonMarkRegistry_6), (void*)value);
	}

	inline static int32_t get_offset_of__currentBindings_7() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____currentBindings_7)); }
	inline Queue_1_tE82A52D54AAB980687E5EBAC89F6A219AD4AB498 * get__currentBindings_7() const { return ____currentBindings_7; }
	inline Queue_1_tE82A52D54AAB980687E5EBAC89F6A219AD4AB498 ** get_address_of__currentBindings_7() { return &____currentBindings_7; }
	inline void set__currentBindings_7(Queue_1_tE82A52D54AAB980687E5EBAC89F6A219AD4AB498 * value)
	{
		____currentBindings_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentBindings_7), (void*)value);
	}

	inline static int32_t get_offset_of__childBindings_8() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____childBindings_8)); }
	inline List_1_tDEB5DB2F48B7B68378607F2160A2568FB6006838 * get__childBindings_8() const { return ____childBindings_8; }
	inline List_1_tDEB5DB2F48B7B68378607F2160A2568FB6006838 ** get_address_of__childBindings_8() { return &____childBindings_8; }
	inline void set__childBindings_8(List_1_tDEB5DB2F48B7B68378607F2160A2568FB6006838 * value)
	{
		____childBindings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____childBindings_8), (void*)value);
	}

	inline static int32_t get_offset_of__validatedTypes_9() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____validatedTypes_9)); }
	inline HashSet_1_t0AADC413B8AE37E5F22243315AD1388C97139717 * get__validatedTypes_9() const { return ____validatedTypes_9; }
	inline HashSet_1_t0AADC413B8AE37E5F22243315AD1388C97139717 ** get_address_of__validatedTypes_9() { return &____validatedTypes_9; }
	inline void set__validatedTypes_9(HashSet_1_t0AADC413B8AE37E5F22243315AD1388C97139717 * value)
	{
		____validatedTypes_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____validatedTypes_9), (void*)value);
	}

	inline static int32_t get_offset_of__validationQueue_10() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____validationQueue_10)); }
	inline List_1_t5641176595AAD1305F98671F818E431B2F48AFFA * get__validationQueue_10() const { return ____validationQueue_10; }
	inline List_1_t5641176595AAD1305F98671F818E431B2F48AFFA ** get_address_of__validationQueue_10() { return &____validationQueue_10; }
	inline void set__validationQueue_10(List_1_t5641176595AAD1305F98671F818E431B2F48AFFA * value)
	{
		____validationQueue_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____validationQueue_10), (void*)value);
	}

	inline static int32_t get_offset_of__contextTransform_11() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____contextTransform_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__contextTransform_11() const { return ____contextTransform_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__contextTransform_11() { return &____contextTransform_11; }
	inline void set__contextTransform_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____contextTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____contextTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of__hasLookedUpContextTransform_12() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____hasLookedUpContextTransform_12)); }
	inline bool get__hasLookedUpContextTransform_12() const { return ____hasLookedUpContextTransform_12; }
	inline bool* get_address_of__hasLookedUpContextTransform_12() { return &____hasLookedUpContextTransform_12; }
	inline void set__hasLookedUpContextTransform_12(bool value)
	{
		____hasLookedUpContextTransform_12 = value;
	}

	inline static int32_t get_offset_of__inheritedDefaultParent_13() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____inheritedDefaultParent_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__inheritedDefaultParent_13() const { return ____inheritedDefaultParent_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__inheritedDefaultParent_13() { return &____inheritedDefaultParent_13; }
	inline void set__inheritedDefaultParent_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____inheritedDefaultParent_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____inheritedDefaultParent_13), (void*)value);
	}

	inline static int32_t get_offset_of__explicitDefaultParent_14() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____explicitDefaultParent_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__explicitDefaultParent_14() const { return ____explicitDefaultParent_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__explicitDefaultParent_14() { return &____explicitDefaultParent_14; }
	inline void set__explicitDefaultParent_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____explicitDefaultParent_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____explicitDefaultParent_14), (void*)value);
	}

	inline static int32_t get_offset_of__hasExplicitDefaultParent_15() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____hasExplicitDefaultParent_15)); }
	inline bool get__hasExplicitDefaultParent_15() const { return ____hasExplicitDefaultParent_15; }
	inline bool* get_address_of__hasExplicitDefaultParent_15() { return &____hasExplicitDefaultParent_15; }
	inline void set__hasExplicitDefaultParent_15(bool value)
	{
		____hasExplicitDefaultParent_15 = value;
	}

	inline static int32_t get_offset_of__settings_16() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____settings_16)); }
	inline ZenjectSettings_t28FCBC1F7514F4F44F8E63915D8895ACD6F9A5B6 * get__settings_16() const { return ____settings_16; }
	inline ZenjectSettings_t28FCBC1F7514F4F44F8E63915D8895ACD6F9A5B6 ** get_address_of__settings_16() { return &____settings_16; }
	inline void set__settings_16(ZenjectSettings_t28FCBC1F7514F4F44F8E63915D8895ACD6F9A5B6 * value)
	{
		____settings_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____settings_16), (void*)value);
	}

	inline static int32_t get_offset_of__hasResolvedRoots_17() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____hasResolvedRoots_17)); }
	inline bool get__hasResolvedRoots_17() const { return ____hasResolvedRoots_17; }
	inline bool* get_address_of__hasResolvedRoots_17() { return &____hasResolvedRoots_17; }
	inline void set__hasResolvedRoots_17(bool value)
	{
		____hasResolvedRoots_17 = value;
	}

	inline static int32_t get_offset_of__isFinalizingBinding_18() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____isFinalizingBinding_18)); }
	inline bool get__isFinalizingBinding_18() const { return ____isFinalizingBinding_18; }
	inline bool* get_address_of__isFinalizingBinding_18() { return &____isFinalizingBinding_18; }
	inline void set__isFinalizingBinding_18(bool value)
	{
		____isFinalizingBinding_18 = value;
	}

	inline static int32_t get_offset_of__isValidating_19() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____isValidating_19)); }
	inline bool get__isValidating_19() const { return ____isValidating_19; }
	inline bool* get_address_of__isValidating_19() { return &____isValidating_19; }
	inline void set__isValidating_19(bool value)
	{
		____isValidating_19 = value;
	}

	inline static int32_t get_offset_of__isInstalling_20() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ____isInstalling_20)); }
	inline bool get__isInstalling_20() const { return ____isInstalling_20; }
	inline bool* get_address_of__isInstalling_20() { return &____isInstalling_20; }
	inline void set__isInstalling_20(bool value)
	{
		____isInstalling_20 = value;
	}

	inline static int32_t get_offset_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD, ___U3CAssertOnNewGameObjectsU3Ek__BackingField_21)); }
	inline bool get_U3CAssertOnNewGameObjectsU3Ek__BackingField_21() const { return ___U3CAssertOnNewGameObjectsU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_21() { return &___U3CAssertOnNewGameObjectsU3Ek__BackingField_21; }
	inline void set_U3CAssertOnNewGameObjectsU3Ek__BackingField_21(bool value)
	{
		___U3CAssertOnNewGameObjectsU3Ek__BackingField_21 = value;
	}
};


// Zenject.InstallerBase
struct  InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C, ____container_0)); }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * get__container_0() const { return ____container_0; }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____container_0), (void*)value);
	}
};


// Zenject.StaticMemoryPoolBaseBase`1<System.Object>
struct  StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<TValue> Zenject.StaticMemoryPoolBaseBase`1::_stack
	Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * ____stack_0;
	// System.Action`1<TValue> Zenject.StaticMemoryPoolBaseBase`1::_onDespawnedMethod
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____onDespawnedMethod_1;
	// System.Int32 Zenject.StaticMemoryPoolBaseBase`1::_activeCount
	int32_t ____activeCount_2;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB, ____stack_0)); }
	inline Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * get__stack_0() const { return ____stack_0; }
	inline Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stack_0), (void*)value);
	}

	inline static int32_t get_offset_of__onDespawnedMethod_1() { return static_cast<int32_t>(offsetof(StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB, ____onDespawnedMethod_1)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__onDespawnedMethod_1() const { return ____onDespawnedMethod_1; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__onDespawnedMethod_1() { return &____onDespawnedMethod_1; }
	inline void set__onDespawnedMethod_1(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____onDespawnedMethod_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onDespawnedMethod_1), (void*)value);
	}

	inline static int32_t get_offset_of__activeCount_2() { return static_cast<int32_t>(offsetof(StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB, ____activeCount_2)); }
	inline int32_t get__activeCount_2() const { return ____activeCount_2; }
	inline int32_t* get_address_of__activeCount_2() { return &____activeCount_2; }
	inline void set__activeCount_2(int32_t value)
	{
		____activeCount_2 = value;
	}
};


// Zenject.SubContainerCreatorBindInfo
struct  SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188  : public RuntimeObject
{
public:
	// System.String Zenject.SubContainerCreatorBindInfo::<DefaultParentName>k__BackingField
	String_t* ___U3CDefaultParentNameU3Ek__BackingField_0;
	// System.Boolean Zenject.SubContainerCreatorBindInfo::<CreateKernel>k__BackingField
	bool ___U3CCreateKernelU3Ek__BackingField_1;
	// System.Type Zenject.SubContainerCreatorBindInfo::<KernelType>k__BackingField
	Type_t * ___U3CKernelTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDefaultParentNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188, ___U3CDefaultParentNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDefaultParentNameU3Ek__BackingField_0() const { return ___U3CDefaultParentNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDefaultParentNameU3Ek__BackingField_0() { return &___U3CDefaultParentNameU3Ek__BackingField_0; }
	inline void set_U3CDefaultParentNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDefaultParentNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDefaultParentNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCreateKernelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188, ___U3CCreateKernelU3Ek__BackingField_1)); }
	inline bool get_U3CCreateKernelU3Ek__BackingField_1() const { return ___U3CCreateKernelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCreateKernelU3Ek__BackingField_1() { return &___U3CCreateKernelU3Ek__BackingField_1; }
	inline void set_U3CCreateKernelU3Ek__BackingField_1(bool value)
	{
		___U3CCreateKernelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CKernelTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188, ___U3CKernelTypeU3Ek__BackingField_2)); }
	inline Type_t * get_U3CKernelTypeU3Ek__BackingField_2() const { return ___U3CKernelTypeU3Ek__BackingField_2; }
	inline Type_t ** get_address_of_U3CKernelTypeU3Ek__BackingField_2() { return &___U3CKernelTypeU3Ek__BackingField_2; }
	inline void set_U3CKernelTypeU3Ek__BackingField_2(Type_t * value)
	{
		___U3CKernelTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CKernelTypeU3Ek__BackingField_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByMethodBase
struct  SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerCreatorByMethodBase::_container
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ____container_0;
	// Zenject.SubContainerCreatorBindInfo Zenject.SubContainerCreatorByMethodBase::_containerBindInfo
	SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ____containerBindInfo_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF, ____container_0)); }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * get__container_0() const { return ____container_0; }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____container_0), (void*)value);
	}

	inline static int32_t get_offset_of__containerBindInfo_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF, ____containerBindInfo_1)); }
	inline SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * get__containerBindInfo_1() const { return ____containerBindInfo_1; }
	inline SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 ** get_address_of__containerBindInfo_1() { return &____containerBindInfo_1; }
	inline void set__containerBindInfo_1(SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * value)
	{
		____containerBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____containerBindInfo_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`1_<>c__DisplayClass2_0<System.Object>
struct  U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewGameObjectMethod`1<TParam1> Zenject.SubContainerCreatorByNewGameObjectMethod`1_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewGameObjectMethod`1_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`10_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewGameObjectMethod`10<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10> Zenject.SubContainerCreatorByNewGameObjectMethod`10_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewGameObjectMethod`10_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`2_<>c__DisplayClass2_0<System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewGameObjectMethod`2<TParam1,TParam2> Zenject.SubContainerCreatorByNewGameObjectMethod`2_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewGameObjectMethod`2_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`3_<>c__DisplayClass2_0<System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewGameObjectMethod`3<TParam1,TParam2,TParam3> Zenject.SubContainerCreatorByNewGameObjectMethod`3_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewGameObjectMethod`3_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`4_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewGameObjectMethod`4<TParam1,TParam2,TParam3,TParam4> Zenject.SubContainerCreatorByNewGameObjectMethod`4_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewGameObjectMethod`4_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`5_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewGameObjectMethod`5<TParam1,TParam2,TParam3,TParam4,TParam5> Zenject.SubContainerCreatorByNewGameObjectMethod`5_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewGameObjectMethod`5_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`6_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewGameObjectMethod`6<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6> Zenject.SubContainerCreatorByNewGameObjectMethod`6_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewGameObjectMethod`6_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`1_<>c__DisplayClass2_0<System.Object>
struct  U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`1<TParam1> Zenject.SubContainerCreatorByNewPrefabMethod`1_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`1_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`10_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`10<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10> Zenject.SubContainerCreatorByNewPrefabMethod`10_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`10_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`2_<>c__DisplayClass2_0<System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`2<TParam1,TParam2> Zenject.SubContainerCreatorByNewPrefabMethod`2_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`2_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`3_<>c__DisplayClass2_0<System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`3<TParam1,TParam2,TParam3> Zenject.SubContainerCreatorByNewPrefabMethod`3_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`3_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`4_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`4<TParam1,TParam2,TParam3,TParam4> Zenject.SubContainerCreatorByNewPrefabMethod`4_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`4_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`5_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`5<TParam1,TParam2,TParam3,TParam4,TParam5> Zenject.SubContainerCreatorByNewPrefabMethod`5_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`5_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`6_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`6<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6> Zenject.SubContainerCreatorByNewPrefabMethod`6_<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`6_<>c__DisplayClass2_0::args
	List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36, ___args_1)); }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * get_args_1() const { return ___args_1; }
	inline List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorDynamicContext
struct  SubContainerCreatorDynamicContext_t1238F323B9AFC85C95B9208CC9D427DE757968D6  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerCreatorDynamicContext::_container
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorDynamicContext_t1238F323B9AFC85C95B9208CC9D427DE757968D6, ____container_0)); }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * get__container_0() const { return ____container_0; }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____container_0), (void*)value);
	}
};


// Zenject.TaskUpdater`1_<>c<System.Object>
struct  U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22_StaticFields
{
public:
	// Zenject.TaskUpdater`1_<>c<TTask> Zenject.TaskUpdater`1_<>c::<>9
	U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 * ___U3CU3E9_0;
	// System.Func`2<Zenject.TaskUpdater`1_TaskInfo<TTask>,TTask> Zenject.TaskUpdater`1_<>c::<>9__7_0
	Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__7_0_1), (void*)value);
	}
};


// Zenject.TaskUpdater`1_<>c__DisplayClass8_0<System.Object>
struct  U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4  : public RuntimeObject
{
public:
	// TTask Zenject.TaskUpdater`1_<>c__DisplayClass8_0::task
	RuntimeObject * ___task_0;

public:
	inline static int32_t get_offset_of_task_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4, ___task_0)); }
	inline RuntimeObject * get_task_0() const { return ___task_0; }
	inline RuntimeObject ** get_address_of_task_0() { return &___task_0; }
	inline void set_task_0(RuntimeObject * value)
	{
		___task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___task_0), (void*)value);
	}
};


// Zenject.TaskUpdater`1_TaskInfo<System.Object>
struct  TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200  : public RuntimeObject
{
public:
	// TTask Zenject.TaskUpdater`1_TaskInfo::Task
	RuntimeObject * ___Task_0;
	// System.Int32 Zenject.TaskUpdater`1_TaskInfo::Priority
	int32_t ___Priority_1;
	// System.Boolean Zenject.TaskUpdater`1_TaskInfo::IsRemoved
	bool ___IsRemoved_2;

public:
	inline static int32_t get_offset_of_Task_0() { return static_cast<int32_t>(offsetof(TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200, ___Task_0)); }
	inline RuntimeObject * get_Task_0() const { return ___Task_0; }
	inline RuntimeObject ** get_address_of_Task_0() { return &___Task_0; }
	inline void set_Task_0(RuntimeObject * value)
	{
		___Task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Task_0), (void*)value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}

	inline static int32_t get_offset_of_IsRemoved_2() { return static_cast<int32_t>(offsetof(TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200, ___IsRemoved_2)); }
	inline bool get_IsRemoved_2() const { return ___IsRemoved_2; }
	inline bool* get_address_of_IsRemoved_2() { return &___IsRemoved_2; }
	inline void set_IsRemoved_2(bool value)
	{
		___IsRemoved_2 = value;
	}
};


// Zenject.TaskUpdater`1<System.Object>
struct  TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7, ____tasks_0)); }
	inline LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tasks_0), (void*)value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7, ____queuedTasks_1)); }
	inline List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____queuedTasks_1), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// Zenject.BindingId
struct  BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE 
{
public:
	// System.Type Zenject.BindingId::_type
	Type_t * ____type_0;
	// System.Object Zenject.BindingId::_identifier
	RuntimeObject * ____identifier_1;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____type_0), (void*)value);
	}

	inline static int32_t get_offset_of__identifier_1() { return static_cast<int32_t>(offsetof(BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE, ____identifier_1)); }
	inline RuntimeObject * get__identifier_1() const { return ____identifier_1; }
	inline RuntimeObject ** get_address_of__identifier_1() { return &____identifier_1; }
	inline void set__identifier_1(RuntimeObject * value)
	{
		____identifier_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identifier_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Zenject.BindingId
struct BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE_marshaled_pinvoke
{
	Type_t * ____type_0;
	Il2CppIUnknown* ____identifier_1;
};
// Native definition for COM marshalling of Zenject.BindingId
struct BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE_marshaled_com
{
	Type_t * ____type_0;
	Il2CppIUnknown* ____identifier_1;
};

// Zenject.Installer`1<Zenject.ActionInstaller>
struct  Installer_1_t442345569BA061FFADA9B642FF769D78DFFEAF95  : public InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C
{
public:

public:
};


// Zenject.StaticMemoryPoolBase`1<System.Object>
struct  StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79  : public StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB
{
public:

public:
};


// Zenject.SubContainerCreatorByMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByMethod_10_tC7ED152395C2E669AD9049049C18C57B849BA4E7  : public SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF
{
public:
	// ModestTree.Util.Action`11<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10> Zenject.SubContainerCreatorByMethod`10::_installMethod
	Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * ____installMethod_2;

public:
	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_10_tC7ED152395C2E669AD9049049C18C57B849BA4E7, ____installMethod_2)); }
	inline Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByMethod`1<System.Object>
struct  SubContainerCreatorByMethod_1_t1DD94EA8E04732E929542EB4EC171FC8B8227260  : public SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF
{
public:
	// System.Action`2<Zenject.DiContainer,TParam1> Zenject.SubContainerCreatorByMethod`1::_installMethod
	Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * ____installMethod_2;

public:
	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_1_t1DD94EA8E04732E929542EB4EC171FC8B8227260, ____installMethod_2)); }
	inline Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByMethod`2<System.Object,System.Object>
struct  SubContainerCreatorByMethod_2_t1C128269485A9B8897C6231192A0BA84F86E8DEB  : public SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF
{
public:
	// System.Action`3<Zenject.DiContainer,TParam1,TParam2> Zenject.SubContainerCreatorByMethod`2::_installMethod
	Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * ____installMethod_2;

public:
	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_2_t1C128269485A9B8897C6231192A0BA84F86E8DEB, ____installMethod_2)); }
	inline Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByMethod`3<System.Object,System.Object,System.Object>
struct  SubContainerCreatorByMethod_3_t9813F91B43CED08E56AEB643D4C0CCE7078B5147  : public SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF
{
public:
	// System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3> Zenject.SubContainerCreatorByMethod`3::_installMethod
	Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * ____installMethod_2;

public:
	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_3_t9813F91B43CED08E56AEB643D4C0CCE7078B5147, ____installMethod_2)); }
	inline Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByMethod`4<System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByMethod_4_tB92E29E6DDC4935B55C75CA8663CA69EFAAC2A86  : public SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF
{
public:
	// ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4> Zenject.SubContainerCreatorByMethod`4::_installMethod
	Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * ____installMethod_2;

public:
	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_4_tB92E29E6DDC4935B55C75CA8663CA69EFAAC2A86, ____installMethod_2)); }
	inline Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByMethod_5_t16E209E58E41BDC695873B2E897DD84B210C2F07  : public SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF
{
public:
	// ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5> Zenject.SubContainerCreatorByMethod`5::_installMethod
	Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * ____installMethod_2;

public:
	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_5_t16E209E58E41BDC695873B2E897DD84B210C2F07, ____installMethod_2)); }
	inline Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByMethod_6_tAFCC4BDF101B491171C43984391D402DFED600C5  : public SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF
{
public:
	// ModestTree.Util.Action`7<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6> Zenject.SubContainerCreatorByMethod`6::_installMethod
	Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * ____installMethod_2;

public:
	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_6_tAFCC4BDF101B491171C43984391D402DFED600C5, ____installMethod_2)); }
	inline Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectDynamicContext
struct  SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2  : public SubContainerCreatorDynamicContext_t1238F323B9AFC85C95B9208CC9D427DE757968D6
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewGameObjectDynamicContext::_gameObjectBindInfo
	GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ____gameObjectBindInfo_1;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2, ____gameObjectBindInfo_1)); }
	inline GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * get__gameObjectBindInfo_1() const { return ____gameObjectBindInfo_1; }
	inline GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 ** get_address_of__gameObjectBindInfo_1() { return &____gameObjectBindInfo_1; }
	inline void set__gameObjectBindInfo_1(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * value)
	{
		____gameObjectBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____gameObjectBindInfo_1), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabDynamicContext
struct  SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB  : public SubContainerCreatorDynamicContext_t1238F323B9AFC85C95B9208CC9D427DE757968D6
{
public:
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefabDynamicContext::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefabDynamicContext::_gameObjectBindInfo
	GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ____gameObjectBindInfo_2;

public:
	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____prefabProvider_1), (void*)value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB, ____gameObjectBindInfo_2)); }
	inline GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * get__gameObjectBindInfo_2() const { return ____gameObjectBindInfo_2; }
	inline GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 ** get_address_of__gameObjectBindInfo_2() { return &____gameObjectBindInfo_2; }
	inline void set__gameObjectBindInfo_2(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * value)
	{
		____gameObjectBindInfo_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____gameObjectBindInfo_2), (void*)value);
	}
};


// Zenject.TypeValuePair
struct  TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE 
{
public:
	// System.Type Zenject.TypeValuePair::Type
	Type_t * ___Type_0;
	// System.Object Zenject.TypeValuePair::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Type_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Zenject.TypeValuePair
struct TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE_marshaled_pinvoke
{
	Type_t * ___Type_0;
	Il2CppIUnknown* ___Value_1;
};
// Native definition for COM marshalling of Zenject.TypeValuePair
struct TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE_marshaled_com
{
	Type_t * ___Type_0;
	Il2CppIUnknown* ___Value_1;
};

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Nullable`1<UnityEngine.Quaternion>
struct  Nullable_1_t40E12A71C967E3B1DF0272DD0258B8D1592121EE 
{
public:
	// T System.Nullable`1::value
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t40E12A71C967E3B1DF0272DD0258B8D1592121EE, ___value_0)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_value_0() const { return ___value_0; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t40E12A71C967E3B1DF0272DD0258B8D1592121EE, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_tCE53BD40AA999E709C1D2369B2A531373CDD89EE 
{
public:
	// T System.Nullable`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCE53BD40AA999E709C1D2369B2A531373CDD89EE, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCE53BD40AA999E709C1D2369B2A531373CDD89EE, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Zenject.ActionInstaller
struct  ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788  : public Installer_1_t442345569BA061FFADA9B642FF769D78DFFEAF95
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.ActionInstaller::_installMethod
	Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * ____installMethod_1;

public:
	inline static int32_t get_offset_of__installMethod_1() { return static_cast<int32_t>(offsetof(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788, ____installMethod_1)); }
	inline Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * get__installMethod_1() const { return ____installMethod_1; }
	inline Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 ** get_address_of__installMethod_1() { return &____installMethod_1; }
	inline void set__installMethod_1(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * value)
	{
		____installMethod_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installMethod_1), (void*)value);
	}
};


// Zenject.InjectSources
struct  InjectSources_tA4A3519C44AAFA01BA177DCA963CAD6D25CB72E2 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_tA4A3519C44AAFA01BA177DCA963CAD6D25CB72E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Zenject.StaticMemoryPool`1<System.Object>
struct  StaticMemoryPool_1_tADD8B93F60A6E6DC7E59D7A91F7557D01507E6F7  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// System.Action`1<TValue> Zenject.StaticMemoryPool`1::_onSpawnMethod
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_1_tADD8B93F60A6E6DC7E59D7A91F7557D01507E6F7, ____onSpawnMethod_3)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.StaticMemoryPool`2<System.Object,System.Object>
struct  StaticMemoryPool_2_t63EB594E16C7E78C6EDAC86CE39B59FABDC3C457  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// System.Action`2<TParam1,TValue> Zenject.StaticMemoryPool`2::_onSpawnMethod
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_2_t63EB594E16C7E78C6EDAC86CE39B59FABDC3C457, ____onSpawnMethod_3)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.StaticMemoryPool`3<System.Object,System.Object,System.Object>
struct  StaticMemoryPool_3_tA3947EBC0BA4452560E09FA0013577D47D0A8D60  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// System.Action`3<TParam1,TParam2,TValue> Zenject.StaticMemoryPool`3::_onSpawnMethod
	Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_3_tA3947EBC0BA4452560E09FA0013577D47D0A8D60, ____onSpawnMethod_3)); }
	inline Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.StaticMemoryPool`4<System.Object,System.Object,System.Object,System.Object>
struct  StaticMemoryPool_4_t6DF0187E9ABA0C47B8DBF11D3DC9AE3DFB4A7DA3  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// System.Action`4<TParam1,TParam2,TParam3,TValue> Zenject.StaticMemoryPool`4::_onSpawnMethod
	Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_4_t6DF0187E9ABA0C47B8DBF11D3DC9AE3DFB4A7DA3, ____onSpawnMethod_3)); }
	inline Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.StaticMemoryPool`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  StaticMemoryPool_5_t96603A65CB9F3F56CB468EB64D1F6EFBF83CF030  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// ModestTree.Util.Action`5<TParam1,TParam2,TParam3,TParam4,TValue> Zenject.StaticMemoryPool`5::_onSpawnMethod
	Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_5_t96603A65CB9F3F56CB468EB64D1F6EFBF83CF030, ____onSpawnMethod_3)); }
	inline Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.StaticMemoryPool`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  StaticMemoryPool_6_t1FFF7C2B7C5AFFADA7DC36973805FA8C6B2E875E  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// ModestTree.Util.Action`6<TParam1,TParam2,TParam3,TParam4,TParam5,TValue> Zenject.StaticMemoryPool`6::_onSpawnMethod
	Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_6_t1FFF7C2B7C5AFFADA7DC36973805FA8C6B2E875E, ____onSpawnMethod_3)); }
	inline Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.StaticMemoryPool`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  StaticMemoryPool_7_tFB2DA9394241B43A5C50B8A689D945B135D990C0  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// ModestTree.Util.Action`7<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TValue> Zenject.StaticMemoryPool`7::_onSpawnMethod
	Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_7_tFB2DA9394241B43A5C50B8A689D945B135D990C0, ____onSpawnMethod_3)); }
	inline Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.StaticMemoryPool`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  StaticMemoryPool_8_tF8EAF86FA5D397071FAB0F840D76CD52658C3D9C  : public StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79
{
public:
	// ModestTree.Util.Action`8<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TValue> Zenject.StaticMemoryPool`8::_onSpawnMethod
	Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * ____onSpawnMethod_3;

public:
	inline static int32_t get_offset_of__onSpawnMethod_3() { return static_cast<int32_t>(offsetof(StaticMemoryPool_8_tF8EAF86FA5D397071FAB0F840D76CD52658C3D9C, ____onSpawnMethod_3)); }
	inline Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * get__onSpawnMethod_3() const { return ____onSpawnMethod_3; }
	inline Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 ** get_address_of__onSpawnMethod_3() { return &____onSpawnMethod_3; }
	inline void set__onSpawnMethod_3(Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * value)
	{
		____onSpawnMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____onSpawnMethod_3), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2  : public SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2
{
public:
	// ModestTree.Util.Action`11<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10> Zenject.SubContainerCreatorByNewGameObjectMethod`10::_installerMethod
	Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * ____installerMethod_2;

public:
	inline static int32_t get_offset_of__installerMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2, ____installerMethod_2)); }
	inline Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * get__installerMethod_2() const { return ____installerMethod_2; }
	inline Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 ** get_address_of__installerMethod_2() { return &____installerMethod_2; }
	inline void set__installerMethod_2(Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * value)
	{
		____installerMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`1<System.Object>
struct  SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652  : public SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2
{
public:
	// System.Action`2<Zenject.DiContainer,TParam1> Zenject.SubContainerCreatorByNewGameObjectMethod`1::_installerMethod
	Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * ____installerMethod_2;

public:
	inline static int32_t get_offset_of__installerMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652, ____installerMethod_2)); }
	inline Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * get__installerMethod_2() const { return ____installerMethod_2; }
	inline Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 ** get_address_of__installerMethod_2() { return &____installerMethod_2; }
	inline void set__installerMethod_2(Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * value)
	{
		____installerMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`2<System.Object,System.Object>
struct  SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB  : public SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2
{
public:
	// System.Action`3<Zenject.DiContainer,TParam1,TParam2> Zenject.SubContainerCreatorByNewGameObjectMethod`2::_installerMethod
	Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * ____installerMethod_2;

public:
	inline static int32_t get_offset_of__installerMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB, ____installerMethod_2)); }
	inline Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * get__installerMethod_2() const { return ____installerMethod_2; }
	inline Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E ** get_address_of__installerMethod_2() { return &____installerMethod_2; }
	inline void set__installerMethod_2(Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * value)
	{
		____installerMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`3<System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427  : public SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2
{
public:
	// System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3> Zenject.SubContainerCreatorByNewGameObjectMethod`3::_installerMethod
	Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * ____installerMethod_2;

public:
	inline static int32_t get_offset_of__installerMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427, ____installerMethod_2)); }
	inline Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * get__installerMethod_2() const { return ____installerMethod_2; }
	inline Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A ** get_address_of__installerMethod_2() { return &____installerMethod_2; }
	inline void set__installerMethod_2(Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * value)
	{
		____installerMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`4<System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0  : public SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2
{
public:
	// ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4> Zenject.SubContainerCreatorByNewGameObjectMethod`4::_installerMethod
	Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * ____installerMethod_2;

public:
	inline static int32_t get_offset_of__installerMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0, ____installerMethod_2)); }
	inline Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * get__installerMethod_2() const { return ____installerMethod_2; }
	inline Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 ** get_address_of__installerMethod_2() { return &____installerMethod_2; }
	inline void set__installerMethod_2(Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * value)
	{
		____installerMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D  : public SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2
{
public:
	// ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5> Zenject.SubContainerCreatorByNewGameObjectMethod`5::_installerMethod
	Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * ____installerMethod_2;

public:
	inline static int32_t get_offset_of__installerMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D, ____installerMethod_2)); }
	inline Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * get__installerMethod_2() const { return ____installerMethod_2; }
	inline Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 ** get_address_of__installerMethod_2() { return &____installerMethod_2; }
	inline void set__installerMethod_2(Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * value)
	{
		____installerMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewGameObjectMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0  : public SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2
{
public:
	// ModestTree.Util.Action`7<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6> Zenject.SubContainerCreatorByNewGameObjectMethod`6::_installerMethod
	Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * ____installerMethod_2;

public:
	inline static int32_t get_offset_of__installerMethod_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0, ____installerMethod_2)); }
	inline Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * get__installerMethod_2() const { return ____installerMethod_2; }
	inline Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 ** get_address_of__installerMethod_2() { return &____installerMethod_2; }
	inline void set__installerMethod_2(Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * value)
	{
		____installerMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_2), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33  : public SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB
{
public:
	// ModestTree.Util.Action`11<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10> Zenject.SubContainerCreatorByNewPrefabMethod`10::_installerMethod
	Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33, ____installerMethod_3)); }
	inline Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_3), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`1<System.Object>
struct  SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA  : public SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB
{
public:
	// System.Action`2<Zenject.DiContainer,TParam1> Zenject.SubContainerCreatorByNewPrefabMethod`1::_installerMethod
	Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA, ____installerMethod_3)); }
	inline Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_3), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`2<System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0  : public SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB
{
public:
	// System.Action`3<Zenject.DiContainer,TParam1,TParam2> Zenject.SubContainerCreatorByNewPrefabMethod`2::_installerMethod
	Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0, ____installerMethod_3)); }
	inline Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_3), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066  : public SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB
{
public:
	// System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3> Zenject.SubContainerCreatorByNewPrefabMethod`3::_installerMethod
	Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066, ____installerMethod_3)); }
	inline Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_3), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D  : public SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB
{
public:
	// ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4> Zenject.SubContainerCreatorByNewPrefabMethod`4::_installerMethod
	Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D, ____installerMethod_3)); }
	inline Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_3), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06  : public SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB
{
public:
	// ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5> Zenject.SubContainerCreatorByNewPrefabMethod`5::_installerMethod
	Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06, ____installerMethod_3)); }
	inline Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_3), (void*)value);
	}
};


// Zenject.SubContainerCreatorByNewPrefabMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99  : public SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB
{
public:
	// ModestTree.Util.Action`7<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6> Zenject.SubContainerCreatorByNewPrefabMethod`6::_installerMethod
	Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99, ____installerMethod_3)); }
	inline Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerMethod_3), (void*)value);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// Zenject.GameObjectCreationParameters
struct  GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3  : public RuntimeObject
{
public:
	// System.String Zenject.GameObjectCreationParameters::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String Zenject.GameObjectCreationParameters::<GroupName>k__BackingField
	String_t* ___U3CGroupNameU3Ek__BackingField_1;
	// UnityEngine.Transform Zenject.GameObjectCreationParameters::<ParentTransform>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CParentTransformU3Ek__BackingField_2;
	// System.Func`2<Zenject.InjectContext,UnityEngine.Transform> Zenject.GameObjectCreationParameters::<ParentTransformGetter>k__BackingField
	Func_2_t633D1A4DF778F0B0CCCCCB2CC76D14A005537EF0 * ___U3CParentTransformGetterU3Ek__BackingField_3;
	// System.Nullable`1<UnityEngine.Vector3> Zenject.GameObjectCreationParameters::<Position>k__BackingField
	Nullable_1_tCE53BD40AA999E709C1D2369B2A531373CDD89EE  ___U3CPositionU3Ek__BackingField_4;
	// System.Nullable`1<UnityEngine.Quaternion> Zenject.GameObjectCreationParameters::<Rotation>k__BackingField
	Nullable_1_t40E12A71C967E3B1DF0272DD0258B8D1592121EE  ___U3CRotationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGroupNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3, ___U3CGroupNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CGroupNameU3Ek__BackingField_1() const { return ___U3CGroupNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CGroupNameU3Ek__BackingField_1() { return &___U3CGroupNameU3Ek__BackingField_1; }
	inline void set_U3CGroupNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CGroupNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGroupNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CParentTransformU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3, ___U3CParentTransformU3Ek__BackingField_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CParentTransformU3Ek__BackingField_2() const { return ___U3CParentTransformU3Ek__BackingField_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CParentTransformU3Ek__BackingField_2() { return &___U3CParentTransformU3Ek__BackingField_2; }
	inline void set_U3CParentTransformU3Ek__BackingField_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CParentTransformU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CParentTransformU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CParentTransformGetterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3, ___U3CParentTransformGetterU3Ek__BackingField_3)); }
	inline Func_2_t633D1A4DF778F0B0CCCCCB2CC76D14A005537EF0 * get_U3CParentTransformGetterU3Ek__BackingField_3() const { return ___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline Func_2_t633D1A4DF778F0B0CCCCCB2CC76D14A005537EF0 ** get_address_of_U3CParentTransformGetterU3Ek__BackingField_3() { return &___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline void set_U3CParentTransformGetterU3Ek__BackingField_3(Func_2_t633D1A4DF778F0B0CCCCCB2CC76D14A005537EF0 * value)
	{
		___U3CParentTransformGetterU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CParentTransformGetterU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3, ___U3CPositionU3Ek__BackingField_4)); }
	inline Nullable_1_tCE53BD40AA999E709C1D2369B2A531373CDD89EE  get_U3CPositionU3Ek__BackingField_4() const { return ___U3CPositionU3Ek__BackingField_4; }
	inline Nullable_1_tCE53BD40AA999E709C1D2369B2A531373CDD89EE * get_address_of_U3CPositionU3Ek__BackingField_4() { return &___U3CPositionU3Ek__BackingField_4; }
	inline void set_U3CPositionU3Ek__BackingField_4(Nullable_1_tCE53BD40AA999E709C1D2369B2A531373CDD89EE  value)
	{
		___U3CPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3, ___U3CRotationU3Ek__BackingField_5)); }
	inline Nullable_1_t40E12A71C967E3B1DF0272DD0258B8D1592121EE  get_U3CRotationU3Ek__BackingField_5() const { return ___U3CRotationU3Ek__BackingField_5; }
	inline Nullable_1_t40E12A71C967E3B1DF0272DD0258B8D1592121EE * get_address_of_U3CRotationU3Ek__BackingField_5() { return &___U3CRotationU3Ek__BackingField_5; }
	inline void set_U3CRotationU3Ek__BackingField_5(Nullable_1_t40E12A71C967E3B1DF0272DD0258B8D1592121EE  value)
	{
		___U3CRotationU3Ek__BackingField_5 = value;
	}
};

struct GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3_StaticFields
{
public:
	// Zenject.GameObjectCreationParameters Zenject.GameObjectCreationParameters::Default
	GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___Default_6;

public:
	inline static int32_t get_offset_of_Default_6() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3_StaticFields, ___Default_6)); }
	inline GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * get_Default_6() const { return ___Default_6; }
	inline GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 ** get_address_of_Default_6() { return &___Default_6; }
	inline void set_Default_6(GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * value)
	{
		___Default_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_6), (void*)value);
	}
};


// Zenject.InjectContext
struct  InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31  : public RuntimeObject
{
public:
	// Zenject.BindingId Zenject.InjectContext::_bindingId
	BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE  ____bindingId_0;
	// System.Type Zenject.InjectContext::_objectType
	Type_t * ____objectType_1;
	// Zenject.InjectContext Zenject.InjectContext::_parentContext
	InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ____parentContext_2;
	// System.Object Zenject.InjectContext::_objectInstance
	RuntimeObject * ____objectInstance_3;
	// System.String Zenject.InjectContext::_memberName
	String_t* ____memberName_4;
	// System.Boolean Zenject.InjectContext::_optional
	bool ____optional_5;
	// Zenject.InjectSources Zenject.InjectContext::_sourceType
	int32_t ____sourceType_6;
	// System.Object Zenject.InjectContext::_fallBackValue
	RuntimeObject * ____fallBackValue_7;
	// System.Object Zenject.InjectContext::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_8;
	// Zenject.DiContainer Zenject.InjectContext::_container
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ____container_9;

public:
	inline static int32_t get_offset_of__bindingId_0() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____bindingId_0)); }
	inline BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE  get__bindingId_0() const { return ____bindingId_0; }
	inline BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE * get_address_of__bindingId_0() { return &____bindingId_0; }
	inline void set__bindingId_0(BindingId_tE0888E373DA23CA36EB93879E8CEA2413B3B53DE  value)
	{
		____bindingId_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____bindingId_0))->____type_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____bindingId_0))->____identifier_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of__objectType_1() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____objectType_1)); }
	inline Type_t * get__objectType_1() const { return ____objectType_1; }
	inline Type_t ** get_address_of__objectType_1() { return &____objectType_1; }
	inline void set__objectType_1(Type_t * value)
	{
		____objectType_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____objectType_1), (void*)value);
	}

	inline static int32_t get_offset_of__parentContext_2() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____parentContext_2)); }
	inline InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * get__parentContext_2() const { return ____parentContext_2; }
	inline InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 ** get_address_of__parentContext_2() { return &____parentContext_2; }
	inline void set__parentContext_2(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * value)
	{
		____parentContext_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parentContext_2), (void*)value);
	}

	inline static int32_t get_offset_of__objectInstance_3() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____objectInstance_3)); }
	inline RuntimeObject * get__objectInstance_3() const { return ____objectInstance_3; }
	inline RuntimeObject ** get_address_of__objectInstance_3() { return &____objectInstance_3; }
	inline void set__objectInstance_3(RuntimeObject * value)
	{
		____objectInstance_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____objectInstance_3), (void*)value);
	}

	inline static int32_t get_offset_of__memberName_4() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____memberName_4)); }
	inline String_t* get__memberName_4() const { return ____memberName_4; }
	inline String_t** get_address_of__memberName_4() { return &____memberName_4; }
	inline void set__memberName_4(String_t* value)
	{
		____memberName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____memberName_4), (void*)value);
	}

	inline static int32_t get_offset_of__optional_5() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____optional_5)); }
	inline bool get__optional_5() const { return ____optional_5; }
	inline bool* get_address_of__optional_5() { return &____optional_5; }
	inline void set__optional_5(bool value)
	{
		____optional_5 = value;
	}

	inline static int32_t get_offset_of__sourceType_6() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____sourceType_6)); }
	inline int32_t get__sourceType_6() const { return ____sourceType_6; }
	inline int32_t* get_address_of__sourceType_6() { return &____sourceType_6; }
	inline void set__sourceType_6(int32_t value)
	{
		____sourceType_6 = value;
	}

	inline static int32_t get_offset_of__fallBackValue_7() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____fallBackValue_7)); }
	inline RuntimeObject * get__fallBackValue_7() const { return ____fallBackValue_7; }
	inline RuntimeObject ** get_address_of__fallBackValue_7() { return &____fallBackValue_7; }
	inline void set__fallBackValue_7(RuntimeObject * value)
	{
		____fallBackValue_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fallBackValue_7), (void*)value);
	}

	inline static int32_t get_offset_of__concreteIdentifier_8() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____concreteIdentifier_8)); }
	inline RuntimeObject * get__concreteIdentifier_8() const { return ____concreteIdentifier_8; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_8() { return &____concreteIdentifier_8; }
	inline void set__concreteIdentifier_8(RuntimeObject * value)
	{
		____concreteIdentifier_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____concreteIdentifier_8), (void*)value);
	}

	inline static int32_t get_offset_of__container_9() { return static_cast<int32_t>(offsetof(InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31, ____container_9)); }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * get__container_9() const { return ____container_9; }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD ** get_address_of__container_9() { return &____container_9; }
	inline void set__container_9(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * value)
	{
		____container_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____container_9), (void*)value);
	}
};


// ModestTree.Util.Action`11<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5  : public MulticastDelegate_t
{
public:

public:
};


// ModestTree.Util.Action`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D  : public MulticastDelegate_t
{
public:

public:
};


// ModestTree.Util.Action`5<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object>
struct  Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7  : public MulticastDelegate_t
{
public:

public:
};


// ModestTree.Util.Action`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716  : public MulticastDelegate_t
{
public:

public:
};


// ModestTree.Util.Action`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436  : public MulticastDelegate_t
{
public:

public:
};


// ModestTree.Util.Action`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE  : public MulticastDelegate_t
{
public:

public:
};


// ModestTree.Util.Action`7<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98  : public MulticastDelegate_t
{
public:

public:
};


// ModestTree.Util.Action`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.Object>
struct  Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<Zenject.DiContainer>
struct  Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`2<System.Object,System.Object>
struct  Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`2<Zenject.DiContainer,System.Object>
struct  Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`3<System.Object,System.Object,System.Object>
struct  Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`3<Zenject.DiContainer,System.Object,System.Object>
struct  Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct  Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`4<Zenject.DiContainer,System.Object,System.Object,System.Object>
struct  Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<Zenject.TaskUpdater`1_TaskInfo<System.Object>,System.Boolean>
struct  Func_2_t681D785C4AF716880EE11E874319E30326BF1AC4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<Zenject.TaskUpdater`1_TaskInfo<System.Object>,System.Object>
struct  Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// Zenject.ScriptableObjectInstallerBase
struct  ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Zenject.DiContainer Zenject.ScriptableObjectInstallerBase::_container
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54, ____container_4)); }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * get__container_4() const { return ____container_4; }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____container_4), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Zenject.ScriptableObjectInstaller`2<System.Object,System.Object>
struct  ScriptableObjectInstaller_2_tBAA795F1506AFB607E16B35B7A4337DAA0002DA4  : public ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54
{
public:

public:
};


// Zenject.ScriptableObjectInstaller`3<System.Object,System.Object,System.Object>
struct  ScriptableObjectInstaller_3_t79F4C7B68872800D6DC876C4DCEBA1D1A9CB27CB  : public ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54
{
public:

public:
};


// Zenject.ScriptableObjectInstaller`4<System.Object,System.Object,System.Object,System.Object>
struct  ScriptableObjectInstaller_4_t5D634B3ACB23B7B62430C5C6F2AD9CA91FF404FE  : public ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54
{
public:

public:
};


// Zenject.ScriptableObjectInstaller`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ScriptableObjectInstaller_5_tDC9D534685DE3245705BC3C63081086DA268F493  : public ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54
{
public:

public:
};


// Zenject.Context
struct  Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller> Zenject.Context::_scriptableObjectInstallers
	List_1_tD19F9EA21326BFA6873B6DFA21EF71F278745931 * ____scriptableObjectInstallers_4;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_monoInstallers
	List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 * ____monoInstallers_5;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installerPrefabs
	List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 * ____installerPrefabs_6;
	// System.Collections.Generic.List`1<Zenject.InstallerBase> Zenject.Context::_normalInstallers
	List_1_tED41AE82F98DF9C43E55F5E27BF9DF208C29E870 * ____normalInstallers_7;
	// System.Collections.Generic.List`1<System.Type> Zenject.Context::_normalInstallerTypes
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ____normalInstallerTypes_8;

public:
	inline static int32_t get_offset_of__scriptableObjectInstallers_4() { return static_cast<int32_t>(offsetof(Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1, ____scriptableObjectInstallers_4)); }
	inline List_1_tD19F9EA21326BFA6873B6DFA21EF71F278745931 * get__scriptableObjectInstallers_4() const { return ____scriptableObjectInstallers_4; }
	inline List_1_tD19F9EA21326BFA6873B6DFA21EF71F278745931 ** get_address_of__scriptableObjectInstallers_4() { return &____scriptableObjectInstallers_4; }
	inline void set__scriptableObjectInstallers_4(List_1_tD19F9EA21326BFA6873B6DFA21EF71F278745931 * value)
	{
		____scriptableObjectInstallers_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scriptableObjectInstallers_4), (void*)value);
	}

	inline static int32_t get_offset_of__monoInstallers_5() { return static_cast<int32_t>(offsetof(Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1, ____monoInstallers_5)); }
	inline List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 * get__monoInstallers_5() const { return ____monoInstallers_5; }
	inline List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 ** get_address_of__monoInstallers_5() { return &____monoInstallers_5; }
	inline void set__monoInstallers_5(List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 * value)
	{
		____monoInstallers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____monoInstallers_5), (void*)value);
	}

	inline static int32_t get_offset_of__installerPrefabs_6() { return static_cast<int32_t>(offsetof(Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1, ____installerPrefabs_6)); }
	inline List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 * get__installerPrefabs_6() const { return ____installerPrefabs_6; }
	inline List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 ** get_address_of__installerPrefabs_6() { return &____installerPrefabs_6; }
	inline void set__installerPrefabs_6(List_1_tE3F13D3E82F85E8E706B75250C1C07D68A4043D4 * value)
	{
		____installerPrefabs_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____installerPrefabs_6), (void*)value);
	}

	inline static int32_t get_offset_of__normalInstallers_7() { return static_cast<int32_t>(offsetof(Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1, ____normalInstallers_7)); }
	inline List_1_tED41AE82F98DF9C43E55F5E27BF9DF208C29E870 * get__normalInstallers_7() const { return ____normalInstallers_7; }
	inline List_1_tED41AE82F98DF9C43E55F5E27BF9DF208C29E870 ** get_address_of__normalInstallers_7() { return &____normalInstallers_7; }
	inline void set__normalInstallers_7(List_1_tED41AE82F98DF9C43E55F5E27BF9DF208C29E870 * value)
	{
		____normalInstallers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____normalInstallers_7), (void*)value);
	}

	inline static int32_t get_offset_of__normalInstallerTypes_8() { return static_cast<int32_t>(offsetof(Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1, ____normalInstallerTypes_8)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get__normalInstallerTypes_8() const { return ____normalInstallerTypes_8; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of__normalInstallerTypes_8() { return &____normalInstallerTypes_8; }
	inline void set__normalInstallerTypes_8(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		____normalInstallerTypes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____normalInstallerTypes_8), (void*)value);
	}
};


// Zenject.RunnableContext
struct  RunnableContext_t729CD4A4075F43CD5C792CB3511BD3331856E3FC  : public Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1
{
public:
	// System.Boolean Zenject.RunnableContext::_autoRun
	bool ____autoRun_9;
	// System.Boolean Zenject.RunnableContext::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__autoRun_9() { return static_cast<int32_t>(offsetof(RunnableContext_t729CD4A4075F43CD5C792CB3511BD3331856E3FC, ____autoRun_9)); }
	inline bool get__autoRun_9() const { return ____autoRun_9; }
	inline bool* get_address_of__autoRun_9() { return &____autoRun_9; }
	inline void set__autoRun_9(bool value)
	{
		____autoRun_9 = value;
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RunnableContext_t729CD4A4075F43CD5C792CB3511BD3331856E3FC, ___U3CInitializedU3Ek__BackingField_11)); }
	inline bool get_U3CInitializedU3Ek__BackingField_11() const { return ___U3CInitializedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_11() { return &___U3CInitializedU3Ek__BackingField_11; }
	inline void set_U3CInitializedU3Ek__BackingField_11(bool value)
	{
		___U3CInitializedU3Ek__BackingField_11 = value;
	}
};

struct RunnableContext_t729CD4A4075F43CD5C792CB3511BD3331856E3FC_StaticFields
{
public:
	// System.Boolean Zenject.RunnableContext::_staticAutoRun
	bool ____staticAutoRun_10;

public:
	inline static int32_t get_offset_of__staticAutoRun_10() { return static_cast<int32_t>(offsetof(RunnableContext_t729CD4A4075F43CD5C792CB3511BD3331856E3FC_StaticFields, ____staticAutoRun_10)); }
	inline bool get__staticAutoRun_10() const { return ____staticAutoRun_10; }
	inline bool* get_address_of__staticAutoRun_10() { return &____staticAutoRun_10; }
	inline void set__staticAutoRun_10(bool value)
	{
		____staticAutoRun_10 = value;
	}
};


// Zenject.GameObjectContext
struct  GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6  : public RunnableContext_t729CD4A4075F43CD5C792CB3511BD3331856E3FC
{
public:
	// System.Action Zenject.GameObjectContext::PreInstall
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PreInstall_12;
	// System.Action Zenject.GameObjectContext::PostInstall
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PostInstall_13;
	// System.Action Zenject.GameObjectContext::PreResolve
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PreResolve_14;
	// System.Action Zenject.GameObjectContext::PostResolve
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PostResolve_15;
	// Zenject.MonoKernel Zenject.GameObjectContext::_kernel
	MonoKernel_tEAA6CF5616476382B597DF6FD141BD83E31D3403 * ____kernel_16;
	// Zenject.DiContainer Zenject.GameObjectContext::_container
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ____container_17;

public:
	inline static int32_t get_offset_of_PreInstall_12() { return static_cast<int32_t>(offsetof(GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6, ___PreInstall_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PreInstall_12() const { return ___PreInstall_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PreInstall_12() { return &___PreInstall_12; }
	inline void set_PreInstall_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PreInstall_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PreInstall_12), (void*)value);
	}

	inline static int32_t get_offset_of_PostInstall_13() { return static_cast<int32_t>(offsetof(GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6, ___PostInstall_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PostInstall_13() const { return ___PostInstall_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PostInstall_13() { return &___PostInstall_13; }
	inline void set_PostInstall_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PostInstall_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PostInstall_13), (void*)value);
	}

	inline static int32_t get_offset_of_PreResolve_14() { return static_cast<int32_t>(offsetof(GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6, ___PreResolve_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PreResolve_14() const { return ___PreResolve_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PreResolve_14() { return &___PreResolve_14; }
	inline void set_PreResolve_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PreResolve_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PreResolve_14), (void*)value);
	}

	inline static int32_t get_offset_of_PostResolve_15() { return static_cast<int32_t>(offsetof(GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6, ___PostResolve_15)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PostResolve_15() const { return ___PostResolve_15; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PostResolve_15() { return &___PostResolve_15; }
	inline void set_PostResolve_15(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PostResolve_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PostResolve_15), (void*)value);
	}

	inline static int32_t get_offset_of__kernel_16() { return static_cast<int32_t>(offsetof(GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6, ____kernel_16)); }
	inline MonoKernel_tEAA6CF5616476382B597DF6FD141BD83E31D3403 * get__kernel_16() const { return ____kernel_16; }
	inline MonoKernel_tEAA6CF5616476382B597DF6FD141BD83E31D3403 ** get_address_of__kernel_16() { return &____kernel_16; }
	inline void set__kernel_16(MonoKernel_tEAA6CF5616476382B597DF6FD141BD83E31D3403 * value)
	{
		____kernel_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____kernel_16), (void*)value);
	}

	inline static int32_t get_offset_of__container_17() { return static_cast<int32_t>(offsetof(GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6, ____container_17)); }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * get__container_17() const { return ____container_17; }
	inline DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD ** get_address_of__container_17() { return &____container_17; }
	inline void set__container_17(DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * value)
	{
		____container_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____container_17), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Zenject.TypeValuePair[]
struct TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  m_Items[1];

public:
	inline TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Type_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Value_1), (void*)NULL);
		#endif
	}
	inline TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Type_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Value_1), (void*)NULL);
		#endif
	}
};


// System.Int32 System.Collections.Generic.List`1<Zenject.TypeValuePair>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_gshared_inline (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Zenject.TypeValuePair>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_gshared_inline (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);

// System.Void Zenject.DiContainer::InjectExplicit(System.Object,System.Collections.Generic.List`1<Zenject.TypeValuePair>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DiContainer_InjectExplicit_mC846B24CE1DC46B128ED58C01212DA8C2996E3BE (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * __this, RuntimeObject * ___injectable0, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___extraArgs1, const RuntimeMethod* method);
// System.Void Zenject.ScriptableObjectInstallerBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObjectInstallerBase__ctor_m1ECB23823641678053FED41171819E50378F9950 (ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ___handle0, const RuntimeMethod* method);
// System.Void ModestTree.Assert::That(System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Assert_That_m56605D736D31A10295F61A6E2C168B5240316A23 (bool ___condition0, String_t* ___message1, const RuntimeMethod* method);
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673 (RuntimeObject * ___left0, RuntimeObject * ___right1, const RuntimeMethod* method);
// System.Void ModestTree.Assert::IsNotNull(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030 (RuntimeObject * ___val0, const RuntimeMethod* method);
// System.Void Zenject.SubContainerCreatorByMethodBase::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140 (SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Zenject.TypeValuePair>::get_Count()
inline int32_t List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *, const RuntimeMethod*))List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<Zenject.TypeValuePair>::get_Item(System.Int32)
inline TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  (*) (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *, int32_t, const RuntimeMethod*))List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_gshared_inline)(__this, ___index0, method);
}
// System.Void ModestTree.Assert::That(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Assert_That_m4F95208E44565736161D0F43FCF484B27574566D (bool ___condition0, const RuntimeMethod* method);
// Zenject.DiContainer Zenject.SubContainerCreatorByMethodBase::CreateEmptySubContainer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB (SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF * __this, const RuntimeMethod* method);
// System.Void Zenject.DiContainer::ResolveRoots()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * __this, const RuntimeMethod* method);
// System.Void Zenject.SubContainerCreatorByNewGameObjectDynamicContext::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C (SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, const RuntimeMethod* method);
// System.Void System.Action`1<Zenject.DiContainer>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Zenject.ActionInstaller::.ctor(System.Action`1<Zenject.DiContainer>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6 (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * __this, Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * ___installMethod0, const RuntimeMethod* method);
// System.Void Zenject.Context::AddNormalInstaller(Zenject.InstallerBase)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076 (Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 * __this, InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C * ___installer0, const RuntimeMethod* method);
// System.Void Zenject.SubContainerCreatorByNewPrefabDynamicContext::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B (SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void ModestTree.Assert::IsNotNull(System.Object,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Assert_IsNotNull_mBAAAFF0F52AC1E6C5BA858401B0D419B643FCB15 (RuntimeObject * ___val0, String_t* ___message1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TDerived Zenject.ScriptableObjectInstaller`2<System.Object,System.Object>::InstallFromResource(Zenject.DiContainer,TParam1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_2_InstallFromResource_mB90F9C40D5DE48F0FF54761D216E01598D7CE660_gshared (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject * ___p11, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container0;
		RuntimeObject * L_2 = ___p11;
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		return L_3;
	}
}
// TDerived Zenject.ScriptableObjectInstaller`2<System.Object,System.Object>::InstallFromResource(System.String,Zenject.DiContainer,TParam1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_2_InstallFromResource_m007074D76C1769BE87327A1C34AC59FB69AD402B_gshared (String_t* ___resourcePath0, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container1, RuntimeObject * ___p12, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		String_t* L_0 = ___resourcePath0;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container1;
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_2;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_3 = ___container1;
		RuntimeObject * L_4 = V_0;
		RuntimeObject * L_5 = ___p12;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ((  List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * (*) (RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3);
		DiContainer_InjectExplicit_mC846B24CE1DC46B128ED58C01212DA8C2996E3BE((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3, (RuntimeObject *)L_4, (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, /*hidden argument*/NULL);
		RuntimeObject * L_7 = V_0;
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_7);
		VirtActionInvoker0::Invoke(6 /* System.Void Zenject.ScriptableObjectInstallerBase::InstallBindings() */, (ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_7);
		RuntimeObject * L_8 = V_0;
		return L_8;
	}
}
// System.Void Zenject.ScriptableObjectInstaller`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObjectInstaller_2__ctor_m2D3B69E404FC51A9E47D108C4607468298B35847_gshared (ScriptableObjectInstaller_2_tBAA795F1506AFB607E16B35B7A4337DAA0002DA4 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this);
		ScriptableObjectInstallerBase__ctor_m1ECB23823641678053FED41171819E50378F9950((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TDerived Zenject.ScriptableObjectInstaller`3<System.Object,System.Object,System.Object>::InstallFromResource(Zenject.DiContainer,TParam1,TParam2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_3_InstallFromResource_m40F1BE696A7F894F2A2BB1D4816B213A901EA538_gshared (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject * ___p11, RuntimeObject * ___p22, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container0;
		RuntimeObject * L_2 = ___p11;
		RuntimeObject * L_3 = ___p22;
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		return L_4;
	}
}
// TDerived Zenject.ScriptableObjectInstaller`3<System.Object,System.Object,System.Object>::InstallFromResource(System.String,Zenject.DiContainer,TParam1,TParam2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_3_InstallFromResource_m71888442F54E61C4DE33DB7325519424658DA4A5_gshared (String_t* ___resourcePath0, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container1, RuntimeObject * ___p12, RuntimeObject * ___p23, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		String_t* L_0 = ___resourcePath0;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container1;
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_2;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_3 = ___container1;
		RuntimeObject * L_4 = V_0;
		RuntimeObject * L_5 = ___p12;
		RuntimeObject * L_6 = ___p23;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_7 = ((  List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * (*) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((RuntimeObject *)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3);
		DiContainer_InjectExplicit_mC846B24CE1DC46B128ED58C01212DA8C2996E3BE((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3, (RuntimeObject *)L_4, (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_7, /*hidden argument*/NULL);
		RuntimeObject * L_8 = V_0;
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_8);
		VirtActionInvoker0::Invoke(6 /* System.Void Zenject.ScriptableObjectInstallerBase::InstallBindings() */, (ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_8);
		RuntimeObject * L_9 = V_0;
		return L_9;
	}
}
// System.Void Zenject.ScriptableObjectInstaller`3<System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObjectInstaller_3__ctor_m9EB1B762EA3775FF9C4C6408BC7A67CC24C27ADC_gshared (ScriptableObjectInstaller_3_t79F4C7B68872800D6DC876C4DCEBA1D1A9CB27CB * __this, const RuntimeMethod* method)
{
	{
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this);
		ScriptableObjectInstallerBase__ctor_m1ECB23823641678053FED41171819E50378F9950((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TDerived Zenject.ScriptableObjectInstaller`4<System.Object,System.Object,System.Object,System.Object>::InstallFromResource(Zenject.DiContainer,TParam1,TParam2,TParam3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_4_InstallFromResource_m2044DDCF936AD91F3612398E3CE04FCE423B1455_gshared (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject * ___p11, RuntimeObject * ___p22, RuntimeObject * ___p33, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container0;
		RuntimeObject * L_2 = ___p11;
		RuntimeObject * L_3 = ___p22;
		RuntimeObject * L_4 = ___p33;
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		return L_5;
	}
}
// TDerived Zenject.ScriptableObjectInstaller`4<System.Object,System.Object,System.Object,System.Object>::InstallFromResource(System.String,Zenject.DiContainer,TParam1,TParam2,TParam3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_4_InstallFromResource_m909FC2F3A7035463864BDC726A5BC8F33F625714_gshared (String_t* ___resourcePath0, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container1, RuntimeObject * ___p12, RuntimeObject * ___p23, RuntimeObject * ___p34, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		String_t* L_0 = ___resourcePath0;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container1;
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_2;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_3 = ___container1;
		RuntimeObject * L_4 = V_0;
		RuntimeObject * L_5 = ___p12;
		RuntimeObject * L_6 = ___p23;
		RuntimeObject * L_7 = ___p34;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_8 = ((  List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * (*) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((RuntimeObject *)L_5, (RuntimeObject *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3);
		DiContainer_InjectExplicit_mC846B24CE1DC46B128ED58C01212DA8C2996E3BE((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3, (RuntimeObject *)L_4, (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_8, /*hidden argument*/NULL);
		RuntimeObject * L_9 = V_0;
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_9);
		VirtActionInvoker0::Invoke(6 /* System.Void Zenject.ScriptableObjectInstallerBase::InstallBindings() */, (ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_9);
		RuntimeObject * L_10 = V_0;
		return L_10;
	}
}
// System.Void Zenject.ScriptableObjectInstaller`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObjectInstaller_4__ctor_m3B348893682151556921E85F1493FDADD0EEB894_gshared (ScriptableObjectInstaller_4_t5D634B3ACB23B7B62430C5C6F2AD9CA91FF404FE * __this, const RuntimeMethod* method)
{
	{
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this);
		ScriptableObjectInstallerBase__ctor_m1ECB23823641678053FED41171819E50378F9950((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TDerived Zenject.ScriptableObjectInstaller`5<System.Object,System.Object,System.Object,System.Object,System.Object>::InstallFromResource(Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_5_InstallFromResource_m6E4C1D995081E11D481CEB13A9BB0F291ED2491E_gshared (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject * ___p11, RuntimeObject * ___p22, RuntimeObject * ___p33, RuntimeObject * ___p44, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container0;
		RuntimeObject * L_2 = ___p11;
		RuntimeObject * L_3 = ___p22;
		RuntimeObject * L_4 = ___p33;
		RuntimeObject * L_5 = ___p44;
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		return L_6;
	}
}
// TDerived Zenject.ScriptableObjectInstaller`5<System.Object,System.Object,System.Object,System.Object,System.Object>::InstallFromResource(System.String,Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObjectInstaller_5_InstallFromResource_m0A26489AC811EA01C67B8A0B927CE9780440BFE8_gshared (String_t* ___resourcePath0, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container1, RuntimeObject * ___p12, RuntimeObject * ___p23, RuntimeObject * ___p34, RuntimeObject * ___p45, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		String_t* L_0 = ___resourcePath0;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_1 = ___container1;
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (String_t*, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((String_t*)L_0, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_2;
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_3 = ___container1;
		RuntimeObject * L_4 = V_0;
		RuntimeObject * L_5 = ___p12;
		RuntimeObject * L_6 = ___p23;
		RuntimeObject * L_7 = ___p34;
		RuntimeObject * L_8 = ___p45;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = ((  List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * (*) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((RuntimeObject *)L_5, (RuntimeObject *)L_6, (RuntimeObject *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3);
		DiContainer_InjectExplicit_mC846B24CE1DC46B128ED58C01212DA8C2996E3BE((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_3, (RuntimeObject *)L_4, (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, /*hidden argument*/NULL);
		RuntimeObject * L_10 = V_0;
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_10);
		VirtActionInvoker0::Invoke(6 /* System.Void Zenject.ScriptableObjectInstallerBase::InstallBindings() */, (ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)L_10);
		RuntimeObject * L_11 = V_0;
		return L_11;
	}
}
// System.Void Zenject.ScriptableObjectInstaller`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObjectInstaller_5__ctor_m532AB8A6209AC3267E54DF84542799AEEA78011F_gshared (ScriptableObjectInstaller_5_tDC9D534685DE3245705BC3C63081086DA268F493 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this);
		ScriptableObjectInstallerBase__ctor_m1ECB23823641678053FED41171819E50378F9950((ScriptableObjectInstallerBase_tD8C42934AF373285DA9B810B8FB8692EB3026F54 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::.ctor(System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1__ctor_m191CFE0E7D3787F0AD3420B0CB7A9BF5D7FF191F_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod0, const RuntimeMethod* method)
{
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_0 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set__stack_0(L_0);
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_1 = ___onDespawnedMethod0;
		__this->set__onDespawnedMethod_1(L_1);
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::set_OnDespawnedMethod(System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_set_OnDespawnedMethod_mDF5B0FB86309A23A1E44C8E9E939B317FD639A06_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___value0, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___value0;
		__this->set__onDespawnedMethod_1(L_0);
		return;
	}
}
// System.Int32 Zenject.StaticMemoryPoolBaseBase`1<System.Object>::get_NumTotal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StaticMemoryPoolBaseBase_1_get_NumTotal_m730F94901BC85792E02051D3E4408DD6332BC0E4_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		int32_t L_0 = ((  int32_t (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		int32_t L_1 = ((  int32_t (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1));
	}
}
// System.Int32 Zenject.StaticMemoryPoolBaseBase`1<System.Object>::get_NumActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StaticMemoryPoolBaseBase_1_get_NumActive_mF85DA3CA0962B085EAD972F62FD68E0D065C2370_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__activeCount_2();
		return L_0;
	}
}
// System.Int32 Zenject.StaticMemoryPoolBaseBase`1<System.Object>::get_NumInactive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StaticMemoryPoolBaseBase_1_get_NumInactive_m21A3EA8036C15DA3A2440D34A4C173AD98C62151_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_0 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return L_1;
	}
}
// System.Type Zenject.StaticMemoryPoolBaseBase`1<System.Object>::get_ItemType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * StaticMemoryPoolBaseBase_1_get_ItemType_mE6BBE545C5517ACA528B612FF1A471ADB1714F28_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StaticMemoryPoolBaseBase_1_get_ItemType_mE6BBE545C5517ACA528B612FF1A471ADB1714F28_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->klass->rgctx_data, 5)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::Resize(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_Resize_mB1ED6D1B4C111F51F8EBDCA1819DAFDF1381B272_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, int32_t ___desiredPoolSize0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___desiredPoolSize0;
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		((  void (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::ResizeInternal(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_ResizeInternal_mBC830B0EC91075C7839B2D6AB0C298AA31864994_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, int32_t ___desiredPoolSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StaticMemoryPoolBaseBase_1_ResizeInternal_mBC830B0EC91075C7839B2D6AB0C298AA31864994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___desiredPoolSize0;
		Assert_That_m56605D736D31A10295F61A6E2C168B5240316A23((bool)((((int32_t)((((int32_t)L_0) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteralA0921AD23D7C750DF2A7BCB64D6425DF00D19960, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0013:
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_1 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_1);
		((  RuntimeObject * (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
	}

IL_001f:
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_2 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_2);
		int32_t L_3 = ((  int32_t (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		int32_t L_4 = ___desiredPoolSize0;
		if ((((int32_t)L_3) > ((int32_t)L_4)))
		{
			goto IL_0013;
		}
	}
	{
		goto IL_0040;
	}

IL_002f:
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_5 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_6 = VirtFuncInvoker0< RuntimeObject * >::Invoke(15 /* TValue Zenject.StaticMemoryPoolBaseBase`1<System.Object>::Alloc() */, (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_5);
		((  void (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
	}

IL_0040:
	{
		int32_t L_7 = ___desiredPoolSize0;
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_8 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_8);
		int32_t L_9 = ((  int32_t (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if ((((int32_t)L_7) > ((int32_t)L_9)))
		{
			goto IL_002f;
		}
	}
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_10 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_10);
		int32_t L_11 = ((  int32_t (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_12);
		int32_t L_14 = ___desiredPoolSize0;
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_15);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_13, (RuntimeObject *)L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_Dispose_mBD8BDD69694AC6574CA02D95BD5D6403783C9628_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::ClearActiveCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_ClearActiveCount_m02D230907F85558D2356CF81DF59CBB4CC0D8187_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	{
		__this->set__activeCount_2(0);
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_Clear_m86A14DD2D0DE6B0483F145C2A6978B1B66707B9D_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		((  void (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::ShrinkBy(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_ShrinkBy_m1BBB1D48C4468CDA04784899515443AB0B2747C3_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, int32_t ___numToRemove0, const RuntimeMethod* method)
{
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_0 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		int32_t L_2 = ___numToRemove0;
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		((  void (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)L_2)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::ExpandBy(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_ExpandBy_mF5DF4812C99864047D775BEDE453E7BCBE66090F_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, int32_t ___numToAdd0, const RuntimeMethod* method)
{
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_0 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		int32_t L_2 = ___numToAdd0;
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		((  void (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_2)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return;
	}
}
// TValue Zenject.StaticMemoryPoolBaseBase`1<System.Object>::SpawnInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPoolBaseBase_1_SpawnInternal_m68E082B5A7669BB6FFF3F65405FBC445A9AB140E_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_0 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_2 = VirtFuncInvoker0< RuntimeObject * >::Invoke(15 /* TValue Zenject.StaticMemoryPoolBaseBase`1<System.Object>::Alloc() */, (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		V_0 = (RuntimeObject *)L_2;
		goto IL_0022;
	}

IL_0016:
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_3 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_3);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		V_0 = (RuntimeObject *)L_4;
	}

IL_0022:
	{
		int32_t L_5 = (int32_t)__this->get__activeCount_2();
		__this->set__activeCount_2(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		RuntimeObject * L_6 = V_0;
		return L_6;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::Zenject.IMemoryPool.Despawn(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_Zenject_IMemoryPool_Despawn_mA48F1A52B28BCA044F1FC11B9A72B1F193AFF104_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item0;
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		((  void (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		return;
	}
}
// System.Void Zenject.StaticMemoryPoolBaseBase`1<System.Object>::Despawn(TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBaseBase_1_Despawn_m80DE31AAB2D613005C155E5E7C61BA0A7066022B_gshared (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StaticMemoryPoolBaseBase_1_Despawn_m80DE31AAB2D613005C155E5E7C61BA0A7066022B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)__this->get__onDespawnedMethod_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_1 = (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)__this->get__onDespawnedMethod_1();
		RuntimeObject * L_2 = ___element0;
		NullCheck((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_1);
		((  void (*) (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
	}

IL_0014:
	{
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_3 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		RuntimeObject * L_4 = ___element0;
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_3);
		bool L_5 = ((  bool (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		Assert_That_m56605D736D31A10295F61A6E2C168B5240316A23((bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteral232943ADE493F438D7C9C6675E2E2051DB9C0DC8, /*hidden argument*/NULL);
		int32_t L_6 = (int32_t)__this->get__activeCount_2();
		__this->set__activeCount_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1)));
		Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 * L_7 = (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)__this->get__stack_0();
		RuntimeObject * L_8 = ___element0;
		NullCheck((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_7);
		((  void (*) (Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Stack_1_t576E16A8DBBF0430E26B9525C7A047671F793CA8 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPoolBase`1<System.Object>::.ctor(System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPoolBase_1__ctor_mB97C4397BACA69B2CE84792AACF463B83204BB8E_gshared (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod0, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod0;
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		((  void (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
// TValue Zenject.StaticMemoryPoolBase`1<System.Object>::Alloc()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPoolBase_1_Alloc_m241646BF6B67436C3529AB3EAA538A8EE4B40540_gshared (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`1<System.Object>::.ctor(System.Action`1<TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_1__ctor_m27D2B23BDD0A3C9E028082824F367B2EACFF6B3F_gshared (StaticMemoryPool_1_tADD8B93F60A6E6DC7E59D7A91F7557D01507E6F7 * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_1 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_1);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`1<System.Object>::set_OnSpawnMethod(System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_1_set_OnSpawnMethod_mD94490FB34BD1F8F4BB15752CCDE0A92EC9A6CC5_gshared (StaticMemoryPool_1_tADD8B93F60A6E6DC7E59D7A91F7557D01507E6F7 * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___value0, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`1<System.Object>::Spawn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_1_Spawn_m554C02FA9D33F82B09FF4D17E3779A60A18463D0_gshared (StaticMemoryPool_1_tADD8B93F60A6E6DC7E59D7A91F7557D01507E6F7 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_1 = (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_2 = (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = V_0;
		NullCheck((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_2);
		((  void (*) (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_2, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_001b:
	{
		RuntimeObject * L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`2<System.Object,System.Object>::.ctor(System.Action`2<TParam1,TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_2__ctor_m1CFDACDCF13E6E9101F72E740D10D060299654D0_gshared (StaticMemoryPool_2_t63EB594E16C7E78C6EDAC86CE39B59FABDC3C457 * __this, Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_1 = ___onSpawnMethod0;
		Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030((RuntimeObject *)L_1, /*hidden argument*/NULL);
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_2 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_2);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`2<System.Object,System.Object>::set_OnSpawnMethod(System.Action`2<TParam1,TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_2_set_OnSpawnMethod_mA16C9DDA796AD87C9E7121CE4D068E8336108495_gshared (StaticMemoryPool_2_t63EB594E16C7E78C6EDAC86CE39B59FABDC3C457 * __this, Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___value0, const RuntimeMethod* method)
{
	{
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`2<System.Object,System.Object>::Spawn(TParam1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_2_Spawn_m217047BE241485D81E8A6D60BA87386F47DF295B_gshared (StaticMemoryPool_2_t63EB594E16C7E78C6EDAC86CE39B59FABDC3C457 * __this, RuntimeObject * ___param0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_1 = (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_2 = (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = ___param0;
		RuntimeObject * L_4 = V_0;
		NullCheck((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)L_2);
		((  void (*) (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_001c:
	{
		RuntimeObject * L_5 = V_0;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`3<System.Object,System.Object,System.Object>::.ctor(System.Action`3<TParam1,TParam2,TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_3__ctor_m79443B71EDCE7F01713ACC96E9C16BB65585C488_gshared (StaticMemoryPool_3_tA3947EBC0BA4452560E09FA0013577D47D0A8D60 * __this, Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * L_1 = ___onSpawnMethod0;
		Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030((RuntimeObject *)L_1, /*hidden argument*/NULL);
		Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * L_2 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_2);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`3<System.Object,System.Object,System.Object>::set_OnSpawnMethod(System.Action`3<TParam1,TParam2,TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_3_set_OnSpawnMethod_mBE6E0F11AF2D8882A8CD262FA5D3F2CAFBFC28F5_gshared (StaticMemoryPool_3_tA3947EBC0BA4452560E09FA0013577D47D0A8D60 * __this, Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * ___value0, const RuntimeMethod* method)
{
	{
		Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`3<System.Object,System.Object,System.Object>::Spawn(TParam1,TParam2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_3_Spawn_m9069345CF12A6B29BD1B2D96D968C10D07023FE2_gshared (StaticMemoryPool_3_tA3947EBC0BA4452560E09FA0013577D47D0A8D60 * __this, RuntimeObject * ___p10, RuntimeObject * ___p21, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * L_1 = (Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * L_2 = (Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = ___p10;
		RuntimeObject * L_4 = ___p21;
		RuntimeObject * L_5 = V_0;
		NullCheck((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)L_2);
		((  void (*) (Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_001d:
	{
		RuntimeObject * L_6 = V_0;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Action`4<TParam1,TParam2,TParam3,TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_4__ctor_m4C0CD80A7A3412140BE6688632089A0D0134D89E_gshared (StaticMemoryPool_4_t6DF0187E9ABA0C47B8DBF11D3DC9AE3DFB4A7DA3 * __this, Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * L_1 = ___onSpawnMethod0;
		Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030((RuntimeObject *)L_1, /*hidden argument*/NULL);
		Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * L_2 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_2);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`4<System.Object,System.Object,System.Object,System.Object>::set_OnSpawnMethod(System.Action`4<TParam1,TParam2,TParam3,TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_4_set_OnSpawnMethod_m1DCAC46B1F13EBF495046975A23229E6720BC8CC_gshared (StaticMemoryPool_4_t6DF0187E9ABA0C47B8DBF11D3DC9AE3DFB4A7DA3 * __this, Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * ___value0, const RuntimeMethod* method)
{
	{
		Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`4<System.Object,System.Object,System.Object,System.Object>::Spawn(TParam1,TParam2,TParam3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_4_Spawn_m8DED083769FC2CEE4F4085AD354C1559124E2776_gshared (StaticMemoryPool_4_t6DF0187E9ABA0C47B8DBF11D3DC9AE3DFB4A7DA3 * __this, RuntimeObject * ___p10, RuntimeObject * ___p21, RuntimeObject * ___p32, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * L_1 = (Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 * L_2 = (Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = ___p10;
		RuntimeObject * L_4 = ___p21;
		RuntimeObject * L_5 = ___p32;
		RuntimeObject * L_6 = V_0;
		NullCheck((Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 *)L_2);
		((  void (*) (Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_4_t3A069DD10DB5F65F3E9FEBAD38A7E7E0C681AA02 *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_001e:
	{
		RuntimeObject * L_7 = V_0;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(ModestTree.Util.Action`5<TParam1,TParam2,TParam3,TParam4,TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_5__ctor_mD23876F848FEB88A536039026E4E1F30022FB5E8_gshared (StaticMemoryPool_5_t96603A65CB9F3F56CB468EB64D1F6EFBF83CF030 * __this, Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * L_1 = ___onSpawnMethod0;
		Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030((RuntimeObject *)L_1, /*hidden argument*/NULL);
		Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * L_2 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_2);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`5<System.Object,System.Object,System.Object,System.Object,System.Object>::set_OnSpawnMethod(ModestTree.Util.Action`5<TParam1,TParam2,TParam3,TParam4,TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_5_set_OnSpawnMethod_mC4BB12002C46049C2F58E2FAA09152B981DD9A1D_gshared (StaticMemoryPool_5_t96603A65CB9F3F56CB468EB64D1F6EFBF83CF030 * __this, Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * ___value0, const RuntimeMethod* method)
{
	{
		Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Spawn(TParam1,TParam2,TParam3,TParam4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_5_Spawn_m2254D0965CDCC4D31B363EB3C81069E6B867759C_gshared (StaticMemoryPool_5_t96603A65CB9F3F56CB468EB64D1F6EFBF83CF030 * __this, RuntimeObject * ___p10, RuntimeObject * ___p21, RuntimeObject * ___p32, RuntimeObject * ___p43, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * L_1 = (Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D * L_2 = (Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = ___p10;
		RuntimeObject * L_4 = ___p21;
		RuntimeObject * L_5 = ___p32;
		RuntimeObject * L_6 = ___p43;
		RuntimeObject * L_7 = V_0;
		NullCheck((Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D *)L_2);
		((  void (*) (Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_5_tA0098F4C1486B6F791863548BAD881AE125BD60D *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_0020:
	{
		RuntimeObject * L_8 = V_0;
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(ModestTree.Util.Action`6<TParam1,TParam2,TParam3,TParam4,TParam5,TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_6__ctor_m6CEE7E722A6C2F33503972A1FE07C9EA94B06C40_gshared (StaticMemoryPool_6_t1FFF7C2B7C5AFFADA7DC36973805FA8C6B2E875E * __this, Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * L_1 = ___onSpawnMethod0;
		Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030((RuntimeObject *)L_1, /*hidden argument*/NULL);
		Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * L_2 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_2);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::set_OnSpawnMethod(ModestTree.Util.Action`6<TParam1,TParam2,TParam3,TParam4,TParam5,TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_6_set_OnSpawnMethod_m80617AF4F3B34BE048F9E214A9B0CA439D1B8958_gshared (StaticMemoryPool_6_t1FFF7C2B7C5AFFADA7DC36973805FA8C6B2E875E * __this, Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * ___value0, const RuntimeMethod* method)
{
	{
		Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Spawn(TParam1,TParam2,TParam3,TParam4,TParam5)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_6_Spawn_mB6084F8C7B9543F8524DC7FA5D53B65A21149E2B_gshared (StaticMemoryPool_6_t1FFF7C2B7C5AFFADA7DC36973805FA8C6B2E875E * __this, RuntimeObject * ___p10, RuntimeObject * ___p21, RuntimeObject * ___p32, RuntimeObject * ___p43, RuntimeObject * ___p54, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * L_1 = (Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 * L_2 = (Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = ___p10;
		RuntimeObject * L_4 = ___p21;
		RuntimeObject * L_5 = ___p32;
		RuntimeObject * L_6 = ___p43;
		RuntimeObject * L_7 = ___p54;
		RuntimeObject * L_8 = V_0;
		NullCheck((Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 *)L_2);
		((  void (*) (Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_6_t830D6C0FA4AE9425B54C8E2958608A7BFA53D716 *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6, (RuntimeObject *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_0022:
	{
		RuntimeObject * L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(ModestTree.Util.Action`7<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_7__ctor_mF37BCF73B08DB10EB794AD176D1D38DC34F42FB2_gshared (StaticMemoryPool_7_tFB2DA9394241B43A5C50B8A689D945B135D990C0 * __this, Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * L_1 = ___onSpawnMethod0;
		Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030((RuntimeObject *)L_1, /*hidden argument*/NULL);
		Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * L_2 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_2);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::set_OnSpawnMethod(ModestTree.Util.Action`7<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_7_set_OnSpawnMethod_m9EDA6FE61F3DDFA7468D47D80176F07A130ABCA1_gshared (StaticMemoryPool_7_tFB2DA9394241B43A5C50B8A689D945B135D990C0 * __this, Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * ___value0, const RuntimeMethod* method)
{
	{
		Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Spawn(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_7_Spawn_m4B5C9602E39AE56C792A091311D72C357B404584_gshared (StaticMemoryPool_7_tFB2DA9394241B43A5C50B8A689D945B135D990C0 * __this, RuntimeObject * ___p10, RuntimeObject * ___p21, RuntimeObject * ___p32, RuntimeObject * ___p43, RuntimeObject * ___p54, RuntimeObject * ___p65, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * L_1 = (Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE * L_2 = (Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = ___p10;
		RuntimeObject * L_4 = ___p21;
		RuntimeObject * L_5 = ___p32;
		RuntimeObject * L_6 = ___p43;
		RuntimeObject * L_7 = ___p54;
		RuntimeObject * L_8 = ___p65;
		RuntimeObject * L_9 = V_0;
		NullCheck((Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE *)L_2);
		((  void (*) (Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_7_t946DD91724E6EC773BEF89ECDE7C65D974C949EE *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6, (RuntimeObject *)L_7, (RuntimeObject *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_0024:
	{
		RuntimeObject * L_10 = V_0;
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.StaticMemoryPool`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(ModestTree.Util.Action`8<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TValue>,System.Action`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_8__ctor_mDAF9987564B55FD67FE49CBC56489B4CF8F88BE2_gshared (StaticMemoryPool_8_tF8EAF86FA5D397071FAB0F840D76CD52658C3D9C * __this, Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * ___onSpawnMethod0, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___onDespawnedMethod1, const RuntimeMethod* method)
{
	{
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_0 = ___onDespawnedMethod1;
		NullCheck((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this);
		((  void (*) (StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((StaticMemoryPoolBase_1_tBA7BCC36CFF1189B662E2732D739FB964D0CCC79 *)__this, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * L_1 = ___onSpawnMethod0;
		Assert_IsNotNull_m55CD8CB061CCE0EBFB6C96A8FE00FEAF5A89C030((RuntimeObject *)L_1, /*hidden argument*/NULL);
		Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * L_2 = ___onSpawnMethod0;
		__this->set__onSpawnMethod_3(L_2);
		return;
	}
}
// System.Void Zenject.StaticMemoryPool`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::set_OnSpawnMethod(ModestTree.Util.Action`8<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticMemoryPool_8_set_OnSpawnMethod_m4FD304A28112DE053D31602138CE40813686AF05_gshared (StaticMemoryPool_8_tF8EAF86FA5D397071FAB0F840D76CD52658C3D9C * __this, Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * ___value0, const RuntimeMethod* method)
{
	{
		Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * L_0 = ___value0;
		__this->set__onSpawnMethod_3(L_0);
		return;
	}
}
// TValue Zenject.StaticMemoryPool`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Spawn(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StaticMemoryPool_8_Spawn_mBAB24D817BEDF40FFE3C3DB26CA25D156FFB6F83_gshared (StaticMemoryPool_8_tF8EAF86FA5D397071FAB0F840D76CD52658C3D9C * __this, RuntimeObject * ___p10, RuntimeObject * ___p21, RuntimeObject * ___p32, RuntimeObject * ___p43, RuntimeObject * ___p54, RuntimeObject * ___p65, RuntimeObject * ___p76, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		NullCheck((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((StaticMemoryPoolBaseBase_1_tBA86E94B84C7C65EC896EA0C4A685601F128DECB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_0;
		Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * L_1 = (Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 *)__this->get__onSpawnMethod_3();
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 * L_2 = (Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 *)__this->get__onSpawnMethod_3();
		RuntimeObject * L_3 = ___p10;
		RuntimeObject * L_4 = ___p21;
		RuntimeObject * L_5 = ___p32;
		RuntimeObject * L_6 = ___p43;
		RuntimeObject * L_7 = ___p54;
		RuntimeObject * L_8 = ___p65;
		RuntimeObject * L_9 = ___p76;
		RuntimeObject * L_10 = V_0;
		NullCheck((Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 *)L_2);
		((  void (*) (Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_8_tB18B4C9533BAA58F41AE0906C7C5165621B37C62 *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6, (RuntimeObject *)L_7, (RuntimeObject *)L_8, (RuntimeObject *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
	}

IL_0026:
	{
		RuntimeObject * L_11 = V_0;
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo,ModestTree.Util.Action`11<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethod_10__ctor_m44BCC7E735D9323F05B07455C969F8C21CBB3672_gshared (SubContainerCreatorByMethod_10_tC7ED152395C2E669AD9049049C18C57B849BA4E7 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * ___installMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * L_1 = ___containerBindInfo1;
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 *)L_1, /*hidden argument*/NULL);
		Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * L_2 = ___installMethod2;
		__this->set__installMethod_2(L_2);
		return;
	}
}
// Zenject.DiContainer Zenject.SubContainerCreatorByMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::CreateSubContainer(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethod_10_CreateSubContainer_m44C3B5BF8B5C6757D5A91AB5D469018F13D9F73D_gshared (SubContainerCreatorByMethod_10_tC7ED152395C2E669AD9049049C18C57B849BA4E7 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByMethod_10_CreateSubContainer_m44C3B5BF8B5C6757D5A91AB5D469018F13D9F73D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * V_0 = NULL;
	{
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_0 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0);
		int32_t L_1 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = ((int32_t)10);
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_3, (RuntimeObject *)L_5, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_8 = (Type_t *)L_7.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_9 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Type_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_9, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_10 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_11 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_12 = (Type_t *)L_11.get_Type_0();
		bool L_13 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Type_t *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_13, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_14 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_15 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_16 = (Type_t *)L_15.get_Type_0();
		bool L_17 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_17, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_20 = (Type_t *)L_19.get_Type_0();
		bool L_21 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_21, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_26 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_27 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_28 = (Type_t *)L_27.get_Type_0();
		bool L_29 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_29, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_30 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_30);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_31 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_30, (int32_t)6, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_32 = (Type_t *)L_31.get_Type_0();
		bool L_33 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_33, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_34 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_34);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_35 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_34, (int32_t)7, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_36 = (Type_t *)L_35.get_Type_0();
		bool L_37 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Type_t *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_37, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_38 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_38);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_39 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_38, (int32_t)8, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_40 = (Type_t *)L_39.get_Type_0();
		bool L_41 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Type_t *)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_41, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_42 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_43 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42, (int32_t)((int32_t)9), /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_44 = (Type_t *)L_43.get_Type_0();
		bool L_45 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Type_t *)L_44, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_45, /*hidden argument*/NULL);
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_46 = SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, /*hidden argument*/NULL);
		V_0 = (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_46;
		Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * L_47 = (Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)__this->get__installMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_48 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_49 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_49);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_50 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_49, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_51 = (RuntimeObject *)L_50.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_52 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_52);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_53 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_52, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_54 = (RuntimeObject *)L_53.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_55 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_55);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_56 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_55, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_57 = (RuntimeObject *)L_56.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_58 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_58);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_59 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_58, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_60 = (RuntimeObject *)L_59.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_61 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_61);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_62 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_61, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_63 = (RuntimeObject *)L_62.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_64 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_64);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_65 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_64, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_66 = (RuntimeObject *)L_65.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_67 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_67);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_68 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_67, (int32_t)6, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_69 = (RuntimeObject *)L_68.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_70 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_70);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_71 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_70, (int32_t)7, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_72 = (RuntimeObject *)L_71.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_73 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_73);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_74 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_73, (int32_t)8, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_75 = (RuntimeObject *)L_74.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_76 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_76);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_77 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_76, (int32_t)((int32_t)9), /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_78 = (RuntimeObject *)L_77.get_Value_1();
		NullCheck((Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_47);
		((  void (*) (Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20)->methodPointer)((Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_47, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_48, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_51, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_54, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_57, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_60, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_63, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 14))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_66, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 15))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_69, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_72, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_75, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_78, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 19))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_79 = V_0;
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_79);
		DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_79, /*hidden argument*/NULL);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_80 = V_0;
		return L_80;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByMethod`1<System.Object>::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo,System.Action`2<Zenject.DiContainer,TParam1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethod_1__ctor_m0C8CD3422E7239189E7F0F84AA54E7DD7F3F0EF3_gshared (SubContainerCreatorByMethod_1_t1DD94EA8E04732E929542EB4EC171FC8B8227260 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * ___installMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * L_1 = ___containerBindInfo1;
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 *)L_1, /*hidden argument*/NULL);
		Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * L_2 = ___installMethod2;
		__this->set__installMethod_2(L_2);
		return;
	}
}
// Zenject.DiContainer Zenject.SubContainerCreatorByMethod`1<System.Object>::CreateSubContainer(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethod_1_CreateSubContainer_m5D013A3A2D0213F5E14A156423A0F4B6A5E2F659_gshared (SubContainerCreatorByMethod_1_t1DD94EA8E04732E929542EB4EC171FC8B8227260 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByMethod_1_CreateSubContainer_m5D013A3A2D0213F5E14A156423A0F4B6A5E2F659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * V_0 = NULL;
	{
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_0 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0);
		int32_t L_1 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = 1;
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_3, (RuntimeObject *)L_5, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_8 = (Type_t *)L_7.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_9 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Type_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_9, /*hidden argument*/NULL);
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_10 = SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, /*hidden argument*/NULL);
		V_0 = (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_10;
		Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * L_11 = (Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)__this->get__installMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_12 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_13 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_13);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_14 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_13, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_15 = (RuntimeObject *)L_14.get_Value_1();
		NullCheck((Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_11);
		((  void (*) (Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_11, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_12, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_15, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_16 = V_0;
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_16);
		DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_16, /*hidden argument*/NULL);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_17 = V_0;
		return L_17;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByMethod`2<System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo,System.Action`3<Zenject.DiContainer,TParam1,TParam2>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethod_2__ctor_mD471FDB8D5EF256D8491CD57DDE7D189788E128C_gshared (SubContainerCreatorByMethod_2_t1C128269485A9B8897C6231192A0BA84F86E8DEB * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * ___installMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * L_1 = ___containerBindInfo1;
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 *)L_1, /*hidden argument*/NULL);
		Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * L_2 = ___installMethod2;
		__this->set__installMethod_2(L_2);
		return;
	}
}
// Zenject.DiContainer Zenject.SubContainerCreatorByMethod`2<System.Object,System.Object>::CreateSubContainer(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethod_2_CreateSubContainer_mA22D2A0A3F1A8DBBBE761AB55228A16E8CF3493F_gshared (SubContainerCreatorByMethod_2_t1C128269485A9B8897C6231192A0BA84F86E8DEB * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByMethod_2_CreateSubContainer_mA22D2A0A3F1A8DBBBE761AB55228A16E8CF3493F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * V_0 = NULL;
	{
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_0 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0);
		int32_t L_1 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = 2;
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_3, (RuntimeObject *)L_5, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_8 = (Type_t *)L_7.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_9 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Type_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_9, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_10 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_11 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_12 = (Type_t *)L_11.get_Type_0();
		bool L_13 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Type_t *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_13, /*hidden argument*/NULL);
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_14 = SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, /*hidden argument*/NULL);
		V_0 = (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_14;
		Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * L_15 = (Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)__this->get__installMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_16 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_19 = (RuntimeObject *)L_18.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_20 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_20);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_21 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_20, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_22 = (RuntimeObject *)L_21.get_Value_1();
		NullCheck((Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_15);
		((  void (*) (Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_15, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_16, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_19, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_22, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_23 = V_0;
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_23);
		DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_23, /*hidden argument*/NULL);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_24 = V_0;
		return L_24;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByMethod`3<System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo,System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethod_3__ctor_m07132605977F8BEE468E8EF6B3007C0E7FB90D00_gshared (SubContainerCreatorByMethod_3_t9813F91B43CED08E56AEB643D4C0CCE7078B5147 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * ___installMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * L_1 = ___containerBindInfo1;
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 *)L_1, /*hidden argument*/NULL);
		Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * L_2 = ___installMethod2;
		__this->set__installMethod_2(L_2);
		return;
	}
}
// Zenject.DiContainer Zenject.SubContainerCreatorByMethod`3<System.Object,System.Object,System.Object>::CreateSubContainer(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethod_3_CreateSubContainer_mEFE490353EFB06DCA6B19311AA25BB657ED41BF1_gshared (SubContainerCreatorByMethod_3_t9813F91B43CED08E56AEB643D4C0CCE7078B5147 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByMethod_3_CreateSubContainer_mEFE490353EFB06DCA6B19311AA25BB657ED41BF1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * V_0 = NULL;
	{
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_0 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0);
		int32_t L_1 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = 3;
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_3, (RuntimeObject *)L_5, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_8 = (Type_t *)L_7.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_9 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Type_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_9, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_10 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_11 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_12 = (Type_t *)L_11.get_Type_0();
		bool L_13 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Type_t *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_13, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_14 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_15 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_16 = (Type_t *)L_15.get_Type_0();
		bool L_17 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_17, /*hidden argument*/NULL);
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_18 = SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, /*hidden argument*/NULL);
		V_0 = (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_18;
		Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * L_19 = (Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)__this->get__installMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_20 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_21 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_22 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_23 = (RuntimeObject *)L_22.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_24 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_24);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_25 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_24, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_26 = (RuntimeObject *)L_25.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_29 = (RuntimeObject *)L_28.get_Value_1();
		NullCheck((Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_19);
		((  void (*) (Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_19, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_20, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_23, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_26, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_29, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_30 = V_0;
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_30);
		DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_30, /*hidden argument*/NULL);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_31 = V_0;
		return L_31;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByMethod`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo,ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethod_4__ctor_mEF2825C4FB70531C9B7FD6D6A10C15872591A61F_gshared (SubContainerCreatorByMethod_4_tB92E29E6DDC4935B55C75CA8663CA69EFAAC2A86 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * ___installMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * L_1 = ___containerBindInfo1;
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 *)L_1, /*hidden argument*/NULL);
		Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * L_2 = ___installMethod2;
		__this->set__installMethod_2(L_2);
		return;
	}
}
// Zenject.DiContainer Zenject.SubContainerCreatorByMethod`4<System.Object,System.Object,System.Object,System.Object>::CreateSubContainer(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethod_4_CreateSubContainer_m04858A904A8D755E3C0EFEFB73D7B3434BA6AE3B_gshared (SubContainerCreatorByMethod_4_tB92E29E6DDC4935B55C75CA8663CA69EFAAC2A86 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByMethod_4_CreateSubContainer_m04858A904A8D755E3C0EFEFB73D7B3434BA6AE3B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * V_0 = NULL;
	{
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_0 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0);
		int32_t L_1 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = 4;
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_3, (RuntimeObject *)L_5, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_8 = (Type_t *)L_7.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_9 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Type_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_9, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_10 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_11 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_12 = (Type_t *)L_11.get_Type_0();
		bool L_13 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Type_t *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_13, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_14 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_15 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_16 = (Type_t *)L_15.get_Type_0();
		bool L_17 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_17, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_20 = (Type_t *)L_19.get_Type_0();
		bool L_21 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_21, /*hidden argument*/NULL);
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_22 = SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, /*hidden argument*/NULL);
		V_0 = (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_22;
		Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * L_23 = (Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)__this->get__installMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_24 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_25 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_25);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_26 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_25, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_27 = (RuntimeObject *)L_26.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_28 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_28);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_29 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_28, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_30 = (RuntimeObject *)L_29.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_31 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_32 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_33 = (RuntimeObject *)L_32.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_34 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_34);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_35 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_34, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_36 = (RuntimeObject *)L_35.get_Value_1();
		NullCheck((Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_23);
		((  void (*) (Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_23, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_24, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_27, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_30, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_33, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_36, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_37 = V_0;
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_37);
		DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_37, /*hidden argument*/NULL);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_38 = V_0;
		return L_38;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo,ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethod_5__ctor_mA3902126207B9CDDD24EC3ABF2B9C5BF60847645_gshared (SubContainerCreatorByMethod_5_t16E209E58E41BDC695873B2E897DD84B210C2F07 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * ___installMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * L_1 = ___containerBindInfo1;
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 *)L_1, /*hidden argument*/NULL);
		Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * L_2 = ___installMethod2;
		__this->set__installMethod_2(L_2);
		return;
	}
}
// Zenject.DiContainer Zenject.SubContainerCreatorByMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::CreateSubContainer(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethod_5_CreateSubContainer_m9DA731F7C713DD6228B2BC0BC4883286C6B94FA6_gshared (SubContainerCreatorByMethod_5_t16E209E58E41BDC695873B2E897DD84B210C2F07 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByMethod_5_CreateSubContainer_m9DA731F7C713DD6228B2BC0BC4883286C6B94FA6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * V_0 = NULL;
	{
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_0 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0);
		int32_t L_1 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = 5;
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_3, (RuntimeObject *)L_5, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_8 = (Type_t *)L_7.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_9 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Type_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_9, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_10 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_11 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_12 = (Type_t *)L_11.get_Type_0();
		bool L_13 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Type_t *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_13, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_14 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_15 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_16 = (Type_t *)L_15.get_Type_0();
		bool L_17 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_17, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_20 = (Type_t *)L_19.get_Type_0();
		bool L_21 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_21, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_26 = SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, /*hidden argument*/NULL);
		V_0 = (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_26;
		Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * L_27 = (Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)__this->get__installMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_28 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_29 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_29);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_30 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_29, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_31 = (RuntimeObject *)L_30.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_32 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_33 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_34 = (RuntimeObject *)L_33.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_35 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_35);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_36 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_35, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_37 = (RuntimeObject *)L_36.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_38 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_38);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_39 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_38, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_40 = (RuntimeObject *)L_39.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_41 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_41);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_42 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_41, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_43 = (RuntimeObject *)L_42.get_Value_1();
		NullCheck((Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_27);
		((  void (*) (Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_27, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_28, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_31, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_34, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_37, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_40, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_43, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_44 = V_0;
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_44);
		DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_44, /*hidden argument*/NULL);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_45 = V_0;
		return L_45;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.SubContainerCreatorBindInfo,ModestTree.Util.Action`7<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByMethod_6__ctor_mF598BBFC83A72D23A470303AC77AAF076C7DD3C4_gshared (SubContainerCreatorByMethod_6_tAFCC4BDF101B491171C43984391D402DFED600C5 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * ___containerBindInfo1, Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * ___installMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 * L_1 = ___containerBindInfo1;
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		SubContainerCreatorByMethodBase__ctor_mE04118875A4F9182359C2C613632177D4A8CD140((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (SubContainerCreatorBindInfo_t3EE434029ECDEF10C03B233E30B6FEE3D460F188 *)L_1, /*hidden argument*/NULL);
		Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * L_2 = ___installMethod2;
		__this->set__installMethod_2(L_2);
		return;
	}
}
// Zenject.DiContainer Zenject.SubContainerCreatorByMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::CreateSubContainer(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * SubContainerCreatorByMethod_6_CreateSubContainer_mBBF5BEB545E031C782AF74D5FC154515A41C3977_gshared (SubContainerCreatorByMethod_6_tAFCC4BDF101B491171C43984391D402DFED600C5 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, InjectContext_tA58BE378AA47824EAF1F224198D8A77C9F828C31 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByMethod_6_CreateSubContainer_mBBF5BEB545E031C782AF74D5FC154515A41C3977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * V_0 = NULL;
	{
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_0 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0);
		int32_t L_1 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_0, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = 5;
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_3, (RuntimeObject *)L_5, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_8 = (Type_t *)L_7.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_9 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Type_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_9, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_10 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_11 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_10, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_12 = (Type_t *)L_11.get_Type_0();
		bool L_13 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Type_t *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_13, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_14 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_15 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_14, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_16 = (Type_t *)L_15.get_Type_0();
		bool L_17 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_17, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_20 = (Type_t *)L_19.get_Type_0();
		bool L_21 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_21, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_26 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_27 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_28 = (Type_t *)L_27.get_Type_0();
		bool L_29 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_29, /*hidden argument*/NULL);
		NullCheck((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_30 = SubContainerCreatorByMethodBase_CreateEmptySubContainer_m611B58196FCD1A3822A56D1950BB613AFEC09DEB((SubContainerCreatorByMethodBase_t134B4ED1710D81BD20D8E7DFA28F8F3F1D9D58DF *)__this, /*hidden argument*/NULL);
		V_0 = (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_30;
		Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * L_31 = (Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)__this->get__installMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_32 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_33 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_33);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_34 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_33, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_35 = (RuntimeObject *)L_34.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_36 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_36);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_37 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_36, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_38 = (RuntimeObject *)L_37.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_39 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_39);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_40 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_39, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_41 = (RuntimeObject *)L_40.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_42 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_43 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_44 = (RuntimeObject *)L_43.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_45 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_45);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_46 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_45, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_47 = (RuntimeObject *)L_46.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_48 = ___args0;
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_48);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_49 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_48, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_50 = (RuntimeObject *)L_49.get_Value_1();
		NullCheck((Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_31);
		((  void (*) (Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)((Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_31, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_32, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_35, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_38, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_41, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_44, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_47, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_50, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_51 = V_0;
		NullCheck((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_51);
		DiContainer_ResolveRoots_mFABDB562DF8F5011A33B9BAB6D342D415212033B((DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_51, /*hidden argument*/NULL);
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_52 = V_0;
		return L_52;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`1_<>c__DisplayClass2_0<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m837AFCF4EC013941BC8C363DF47BBDDD5F85BB07_gshared (U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`1_<>c__DisplayClass2_0<System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m995471C8D22C7D441C4CFD882CB9008F6A848177_gshared (U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m995471C8D22C7D441C4CFD882CB9008F6A848177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 * L_0 = (SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * L_1 = (Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_0->get__installerMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		NullCheck((Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_1);
		((  void (*) (Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`10_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m825470166AC4B8C105F0EB2E281C37049504D53B_gshared (U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`10_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mF64DDE3C1DF908032A6777FC1BE78B74A55C0E3C_gshared (U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mF64DDE3C1DF908032A6777FC1BE78B74A55C0E3C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 * L_0 = (SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * L_1 = (Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_0->get__installerMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_15 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_16 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_17 = (RuntimeObject *)L_16.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_20 = (RuntimeObject *)L_19.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_21 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_22 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21, (int32_t)6, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_23 = (RuntimeObject *)L_22.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_24 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_24);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_25 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_24, (int32_t)7, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_26 = (RuntimeObject *)L_25.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)8, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_29 = (RuntimeObject *)L_28.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_30 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_30);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_31 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_30, (int32_t)((int32_t)9), /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_32 = (RuntimeObject *)L_31.get_Value_1();
		NullCheck((Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_1);
		((  void (*) (Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_20, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_23, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_26, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_29, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_32, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`11<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_10__ctor_m8A41A223EAE8500D5EA8B7B8B88C1305514B87D7_gshared (SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * ___installerMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_1 = ___gameObjectBindInfo1;
		NullCheck((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this);
		SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_1, /*hidden argument*/NULL);
		Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * L_2 = ___installerMethod2;
		__this->set__installerMethod_2(L_2);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_10_AddInstallers_mAAD6B59C6D878EFCF65081228C6229FAE54CA056_gshared (SubContainerCreatorByNewGameObjectMethod_10_tF4073EE8D478F72B261089275769BD07F57F89C2 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewGameObjectMethod_10_AddInstallers_mAAD6B59C6D878EFCF65081228C6229FAE54CA056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_0 = (U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 *)L_0;
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = ((int32_t)10);
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_31 = V_0;
		NullCheck(L_31);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_32 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_33 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_34 = (Type_t *)L_33.get_Type_0();
		bool L_35 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_35, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_36 = V_0;
		NullCheck(L_36);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_37 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_36->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_38 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_39 = (Type_t *)L_38.get_Type_0();
		bool L_40 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Type_t *)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_40, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_41 = V_0;
		NullCheck(L_41);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_42 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_41->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_43 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42, (int32_t)6, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_44 = (Type_t *)L_43.get_Type_0();
		bool L_45 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Type_t *)L_44, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_45, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_46 = V_0;
		NullCheck(L_46);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_47 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_46->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_47);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_48 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_47, (int32_t)7, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_49 = (Type_t *)L_48.get_Type_0();
		bool L_50 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Type_t *)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_50, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_51 = V_0;
		NullCheck(L_51);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_52 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_51->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_52);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_53 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_52, (int32_t)8, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_54 = (Type_t *)L_53.get_Type_0();
		bool L_55 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((Type_t *)L_54, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_55, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_56 = V_0;
		NullCheck(L_56);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_57 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_56->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_57);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_58 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_57, (int32_t)((int32_t)9), /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_59 = (Type_t *)L_58.get_Type_0();
		bool L_60 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Type_t *)L_59, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_60, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_61 = ___context1;
		U3CU3Ec__DisplayClass2_0_t4EA4D33144331430AB5580AB9936CF018B48BCA3 * L_62 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_63 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_63, (RuntimeObject *)L_62, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_64 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_64, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_63, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_61);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_61, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_64, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`1<System.Object>::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters,System.Action`2<Zenject.DiContainer,TParam1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_1__ctor_mBCDC25D77BBAA9A3296FEE1BA6837105D059D16F_gshared (SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * ___installerMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_1 = ___gameObjectBindInfo1;
		NullCheck((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this);
		SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_1, /*hidden argument*/NULL);
		Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * L_2 = ___installerMethod2;
		__this->set__installerMethod_2(L_2);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`1<System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_1_AddInstallers_mFF44F85DA9A324B4B5C0514039FA62A18134D070_gshared (SubContainerCreatorByNewGameObjectMethod_1_t260E009658BA9115F6327F4051F500E156591652 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewGameObjectMethod_1_AddInstallers_mFF44F85DA9A324B4B5C0514039FA62A18134D070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * L_0 = (U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C *)L_0;
		U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 1;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_16 = ___context1;
		U3CU3Ec__DisplayClass2_0_t12E51744A9CD522B6BCEE81FD345ABBC4C76D28C * L_17 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_18 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_18, (RuntimeObject *)L_17, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_19 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_19, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_18, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_16);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_16, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_19, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`2_<>c__DisplayClass2_0<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m0C23E44DEA2EA93FB70C2171E0A2EDCE45451D82_gshared (U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`2_<>c__DisplayClass2_0<System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m3DB72A1FEB9FDBA4CBB0FA76F4BDB6CAB4B1C691_gshared (U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m3DB72A1FEB9FDBA4CBB0FA76F4BDB6CAB4B1C691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB * L_0 = (SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * L_1 = (Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_0->get__installerMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		NullCheck((Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_1);
		((  void (*) (Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`2<System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters,System.Action`3<Zenject.DiContainer,TParam1,TParam2>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_2__ctor_m325B09B25B7E8D3DCB1ADA8D41DFACBC298F0987_gshared (SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * ___installerMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_1 = ___gameObjectBindInfo1;
		NullCheck((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this);
		SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_1, /*hidden argument*/NULL);
		Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * L_2 = ___installerMethod2;
		__this->set__installerMethod_2(L_2);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`2<System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_2_AddInstallers_m0EA016B842429538CDE9E9FBAC9AACBFEDAA4A56_gshared (SubContainerCreatorByNewGameObjectMethod_2_t90D6723C19E00AE4ECB4E47C8BF1F325AB8E34BB * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewGameObjectMethod_2_AddInstallers_m0EA016B842429538CDE9E9FBAC9AACBFEDAA4A56_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * L_0 = (U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 *)L_0;
		U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 2;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_21 = ___context1;
		U3CU3Ec__DisplayClass2_0_tB92778014E447F43673EDFA82D6C3BE807803991 * L_22 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_23 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_23, (RuntimeObject *)L_22, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_24 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_24, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_23, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_21);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_21, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_24, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`3_<>c__DisplayClass2_0<System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m118B0BFE855993329EC7A2DC3C62822394FB4ECB_gshared (U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`3_<>c__DisplayClass2_0<System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m80975CF201A3EBC72CEDC4F204483524B9A64900_gshared (U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m80975CF201A3EBC72CEDC4F204483524B9A64900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 * L_0 = (SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * L_1 = (Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_0->get__installerMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		NullCheck((Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_1);
		((  void (*) (Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`3<System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters,System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_3__ctor_m834E5CDFB250AE22BD7CB49F5FE36E176D7F4FB1_gshared (SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * ___installerMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_1 = ___gameObjectBindInfo1;
		NullCheck((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this);
		SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_1, /*hidden argument*/NULL);
		Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * L_2 = ___installerMethod2;
		__this->set__installerMethod_2(L_2);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`3<System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_3_AddInstallers_mEB871A4B1C6EC575CCC4EEA9A78C89C12B5996D7_gshared (SubContainerCreatorByNewGameObjectMethod_3_t7A6660644467A33DC2FABD85F801EAD31B330427 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewGameObjectMethod_3_AddInstallers_mEB871A4B1C6EC575CCC4EEA9A78C89C12B5996D7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_0 = (U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F *)L_0;
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 3;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_26 = ___context1;
		U3CU3Ec__DisplayClass2_0_tBD7341A945767E701757EDDF1B9FFCFC0A90870F * L_27 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_28 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_28, (RuntimeObject *)L_27, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_29 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_29, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_28, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_26);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_26, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_29, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`4_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mDB9784400177000DD569138108DAF2430BDDF319_gshared (U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`4_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mD76112FEB5D17E857868720A9F6CA5C7724214FF_gshared (U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mD76112FEB5D17E857868720A9F6CA5C7724214FF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 * L_0 = (SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * L_1 = (Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_0->get__installerMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		NullCheck((Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_1);
		((  void (*) (Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_4__ctor_m28657FD66ACBE6C007EBC965063D7B0723FD7CC1_gshared (SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * ___installerMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_1 = ___gameObjectBindInfo1;
		NullCheck((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this);
		SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_1, /*hidden argument*/NULL);
		Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * L_2 = ___installerMethod2;
		__this->set__installerMethod_2(L_2);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`4<System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_4_AddInstallers_m11C7E83B51E32A581E20C34495C1E53811735FBD_gshared (SubContainerCreatorByNewGameObjectMethod_4_t00ADCA1D6F4454205580E8682B542A47117E74B0 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewGameObjectMethod_4_AddInstallers_m11C7E83B51E32A581E20C34495C1E53811735FBD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_0 = (U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 *)L_0;
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 4;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_31 = ___context1;
		U3CU3Ec__DisplayClass2_0_t3DD07891200E492647335993A4F9523484519340 * L_32 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_33 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_33, (RuntimeObject *)L_32, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_34 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_34, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_33, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_31);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_31, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_34, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`5_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m6B28B076B304E81AD9DC937CF8E2A1B9813E095B_gshared (U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`5_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m20829D28F34B2E1E6781594988BD80F17A558FA5_gshared (U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m20829D28F34B2E1E6781594988BD80F17A558FA5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D * L_0 = (SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * L_1 = (Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_0->get__installerMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_15 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_16 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_17 = (RuntimeObject *)L_16.get_Value_1();
		NullCheck((Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_1);
		((  void (*) (Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_5__ctor_m2FB3B373128164A052287A4FD83697F2810B6ABB_gshared (SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * ___installerMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_1 = ___gameObjectBindInfo1;
		NullCheck((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this);
		SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_1, /*hidden argument*/NULL);
		Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * L_2 = ___installerMethod2;
		__this->set__installerMethod_2(L_2);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_5_AddInstallers_mD3F66F81F2D3F32C65D1660605BAD4CF5324AAA0_gshared (SubContainerCreatorByNewGameObjectMethod_5_t4E47898C96CCA9602FCD211700D327F6708FB80D * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewGameObjectMethod_5_AddInstallers_mD3F66F81F2D3F32C65D1660605BAD4CF5324AAA0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_0 = (U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 *)L_0;
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 5;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_31 = V_0;
		NullCheck(L_31);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_32 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_33 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_34 = (Type_t *)L_33.get_Type_0();
		bool L_35 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_35, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_36 = ___context1;
		U3CU3Ec__DisplayClass2_0_t089BF4F7CD352C5339B6D96310EAEC34FD634318 * L_37 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_38 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_38, (RuntimeObject *)L_37, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_39 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_39, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_38, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_36);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_36, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_39, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`6_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mCA456FB07AA683D699707A0EB86686CD8FE4FADB_gshared (U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`6_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m5EC27D5AC7BB9339DEB8B47A940EE8C73DC668DC_gshared (U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m5EC27D5AC7BB9339DEB8B47A940EE8C73DC668DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 * L_0 = (SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * L_1 = (Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_0->get__installerMethod_2();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_15 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_16 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_17 = (RuntimeObject *)L_16.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_20 = (RuntimeObject *)L_19.get_Value_1();
		NullCheck((Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_1);
		((  void (*) (Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_20, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`7<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_6__ctor_m99C3103992E1548F0D215F04C71FB3B52D8FABF9_gshared (SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo1, Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * ___installerMethod2, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_1 = ___gameObjectBindInfo1;
		NullCheck((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this);
		SubContainerCreatorByNewGameObjectDynamicContext__ctor_m02036DAA90FCE2B59DDAFB4F9B6CB69EA204B58C((SubContainerCreatorByNewGameObjectDynamicContext_tD19958408F4B998B80CE5900C44123C4DBA231B2 *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_1, /*hidden argument*/NULL);
		Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * L_2 = ___installerMethod2;
		__this->set__installerMethod_2(L_2);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewGameObjectMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewGameObjectMethod_6_AddInstallers_m571A78434A01AECF5BC12C6AD2E4920A20739D2C_gshared (SubContainerCreatorByNewGameObjectMethod_6_tE24B17E3B68752FA96615040178D7CCB6D3D60F0 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewGameObjectMethod_6_AddInstallers_m571A78434A01AECF5BC12C6AD2E4920A20739D2C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_0 = (U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 *)L_0;
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 5;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_31 = V_0;
		NullCheck(L_31);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_32 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_33 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_34 = (Type_t *)L_33.get_Type_0();
		bool L_35 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_35, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_36 = V_0;
		NullCheck(L_36);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_37 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_36->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_38 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_39 = (Type_t *)L_38.get_Type_0();
		bool L_40 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Type_t *)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_40, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_41 = ___context1;
		U3CU3Ec__DisplayClass2_0_t34752341986FB847BCE38A12552EBD63FCEAA0E9 * L_42 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_43 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_43, (RuntimeObject *)L_42, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_44 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_44, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_43, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_41);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_41, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_44, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`1_<>c__DisplayClass2_0<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m5228177B94954E0F0EFCEC16E62B1C2011DE1A8B_gshared (U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`1_<>c__DisplayClass2_0<System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m294A041BBB4EBBBDCF38B7576013F0F4069D5AA2_gshared (U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m294A041BBB4EBBBDCF38B7576013F0F4069D5AA2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA * L_0 = (SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * L_1 = (Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_0->get__installerMethod_3();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		NullCheck((Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_1);
		((  void (*) (Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`10_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m97E253B9C94AACB4D09168DF7160B6CB3B48447F_gshared (U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`10_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mD2E13603487D3FC3F66EF46356CAAED9BBECA68A_gshared (U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mD2E13603487D3FC3F66EF46356CAAED9BBECA68A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 * L_0 = (SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * L_1 = (Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_0->get__installerMethod_3();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_15 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_16 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_17 = (RuntimeObject *)L_16.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_20 = (RuntimeObject *)L_19.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_21 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_22 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21, (int32_t)6, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_23 = (RuntimeObject *)L_22.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_24 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_24);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_25 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_24, (int32_t)7, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_26 = (RuntimeObject *)L_25.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)8, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_29 = (RuntimeObject *)L_28.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_30 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_30);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_31 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_30, (int32_t)((int32_t)9), /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_32 = (RuntimeObject *)L_31.get_Value_1();
		NullCheck((Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_1);
		((  void (*) (Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_20, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_23, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_26, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_29, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_32, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`11<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_10__ctor_m20CB92350D1105B1F7450B74F208AEBDFDECB867_gshared (SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_2, /*hidden argument*/NULL);
		Action_11_tA05E9F47470D4082868EA78BEC2951872C57F0E5 * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_10_AddInstallers_mA4AA2ECC64481159B01AE5B27A0AC6530AA52560_gshared (SubContainerCreatorByNewPrefabMethod_10_t1E9EE03745C8D4BDCB8DD7BE7FCD4DF03CF75E33 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_10_AddInstallers_mA4AA2ECC64481159B01AE5B27A0AC6530AA52560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_0 = (U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 *)L_0;
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = ((int32_t)10);
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_31 = V_0;
		NullCheck(L_31);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_32 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_33 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_34 = (Type_t *)L_33.get_Type_0();
		bool L_35 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_35, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_36 = V_0;
		NullCheck(L_36);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_37 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_36->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_38 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_39 = (Type_t *)L_38.get_Type_0();
		bool L_40 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Type_t *)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_40, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_41 = V_0;
		NullCheck(L_41);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_42 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_41->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_43 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_42, (int32_t)6, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_44 = (Type_t *)L_43.get_Type_0();
		bool L_45 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Type_t *)L_44, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_45, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_46 = V_0;
		NullCheck(L_46);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_47 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_46->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_47);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_48 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_47, (int32_t)7, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_49 = (Type_t *)L_48.get_Type_0();
		bool L_50 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Type_t *)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_50, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_51 = V_0;
		NullCheck(L_51);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_52 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_51->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_52);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_53 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_52, (int32_t)8, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_54 = (Type_t *)L_53.get_Type_0();
		bool L_55 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((Type_t *)L_54, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_55, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_56 = V_0;
		NullCheck(L_56);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_57 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_56->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_57);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_58 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_57, (int32_t)((int32_t)9), /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_59 = (Type_t *)L_58.get_Type_0();
		bool L_60 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Type_t *)L_59, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_60, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_61 = ___context1;
		U3CU3Ec__DisplayClass2_0_t23BF3756D5334203C4FC84005D2EAA319DAF97E8 * L_62 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_63 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_63, (RuntimeObject *)L_62, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_64 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_64, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_63, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_61);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_61, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_64, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`1<System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,System.Action`2<Zenject.DiContainer,TParam1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_1__ctor_m815F356D941204137441E0ACF84424C620F55AE2_gshared (SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_2, /*hidden argument*/NULL);
		Action_2_t5E9F3D046BE3F18854A39BDC0304683422B2C5F2 * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`1<System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_1_AddInstallers_m23E81B8029A6B4A5210F0F3F4F4C8B18AD1B14C8_gshared (SubContainerCreatorByNewPrefabMethod_1_t4A10E7A71537E1B9B8E444E2B4FDBA02C2B931BA * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_1_AddInstallers_m23E81B8029A6B4A5210F0F3F4F4C8B18AD1B14C8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * L_0 = (U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 *)L_0;
		U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 1;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_16 = ___context1;
		U3CU3Ec__DisplayClass2_0_t31FEC3F842528798611BCFD10E1DE527DD9CE7A0 * L_17 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_18 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_18, (RuntimeObject *)L_17, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_19 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_19, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_18, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_16);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_16, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_19, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`2_<>c__DisplayClass2_0<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m42CDD666C90DCD6A334BC54A52931DC74FAA885F_gshared (U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`2_<>c__DisplayClass2_0<System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mC84F2B261C09B708A5C3B2EC1F71486E3D56ADEE_gshared (U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mC84F2B261C09B708A5C3B2EC1F71486E3D56ADEE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 * L_0 = (SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * L_1 = (Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_0->get__installerMethod_3();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		NullCheck((Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_1);
		((  void (*) (Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`2<System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,System.Action`3<Zenject.DiContainer,TParam1,TParam2>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_2__ctor_m65B364688528B0FACB015BD16988898C2A1EE784_gshared (SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_2, /*hidden argument*/NULL);
		Action_3_t922FCF3E4951D3E699A0304F81AE613FAA6C905E * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`2<System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_2_AddInstallers_m69A239DA9A22F230DDB1512B156B36E0DD1E8D0A_gshared (SubContainerCreatorByNewPrefabMethod_2_tBF99A75B8F9257C34869421D6FF76434F55ADED0 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_2_AddInstallers_m69A239DA9A22F230DDB1512B156B36E0DD1E8D0A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * L_0 = (U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E *)L_0;
		U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 2;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_21 = ___context1;
		U3CU3Ec__DisplayClass2_0_t39F294178A4884909BBCE807770BD403A0EC334E * L_22 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_23 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_23, (RuntimeObject *)L_22, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_24 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_24, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_23, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_21);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_21, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_24, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3_<>c__DisplayClass2_0<System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m6DC58F9873223DCC2480BEECE917342A3C303742_gshared (U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3_<>c__DisplayClass2_0<System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m24BA475C392B1E398380111677B17EA17608B404_gshared (U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m24BA475C392B1E398380111677B17EA17608B404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 * L_0 = (SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * L_1 = (Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_0->get__installerMethod_3();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		NullCheck((Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_1);
		((  void (*) (Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_3__ctor_m1307D77798EE1054FF48BAE737D908D724EBA834_gshared (SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_2, /*hidden argument*/NULL);
		Action_4_t06F9AE7051C13CCFA42785DC86F226808920B90A * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_3_AddInstallers_m80F6002F7D667B1B1693943F44C5EF03C2813C87_gshared (SubContainerCreatorByNewPrefabMethod_3_t8575A986676A80C1296D6DB6A2E9D69867B3D066 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_3_AddInstallers_m80F6002F7D667B1B1693943F44C5EF03C2813C87_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_0 = (U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E *)L_0;
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 3;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_26 = ___context1;
		U3CU3Ec__DisplayClass2_0_t87465878EA52D6B4F7E50B7B0C344FDA2D03C88E * L_27 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_28 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_28, (RuntimeObject *)L_27, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_29 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_29, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_28, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_26);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_26, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_29, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m259734D39EFBD789FFA69088CC8CF3D6C099D3C5_gshared (U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m2AF4286DEBB498329BAFA505E92CF2946BACE799_gshared (U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m2AF4286DEBB498329BAFA505E92CF2946BACE799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D * L_0 = (SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * L_1 = (Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_0->get__installerMethod_3();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		NullCheck((Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_1);
		((  void (*) (Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_4__ctor_m38757A5D1723A13855DA10D5CA54511FA83B4AAD_gshared (SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_2, /*hidden argument*/NULL);
		Action_5_t9F6578DBAE63865A183CC4A953DB8A50D1BC1AC7 * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_4_AddInstallers_m86869CC3A33A2C9A6B99A4229877D34E3F3A0BED_gshared (SubContainerCreatorByNewPrefabMethod_4_t8AEB529BD3285AFFB3FE6BACA5BC8662067C4C7D * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_4_AddInstallers_m86869CC3A33A2C9A6B99A4229877D34E3F3A0BED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_0 = (U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 *)L_0;
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 4;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_31 = ___context1;
		U3CU3Ec__DisplayClass2_0_tAE3919C8FAADF4C90161160405ED42514E10F4D9 * L_32 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_33 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_33, (RuntimeObject *)L_32, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_34 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_34, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_33, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_31);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_31, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_34, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m68864DB9608BA74AD0313DC23A867A38C0703892_gshared (U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m0A2C62741A89DF5C9DFAE4CC00666492964008EB_gshared (U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m0A2C62741A89DF5C9DFAE4CC00666492964008EB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 * L_0 = (SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * L_1 = (Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_0->get__installerMethod_3();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_15 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_16 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_17 = (RuntimeObject *)L_16.get_Value_1();
		NullCheck((Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_1);
		((  void (*) (Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_5__ctor_mDC63D3A740D77AB1A10D2B7B275317F85CE9C7BA_gshared (SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_2, /*hidden argument*/NULL);
		Action_6_tEF485476297B17A6F54796F75A31D8A0DD14C436 * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_5_AddInstallers_m7466E5005D14D8F365D6D50143618BE41802A634_gshared (SubContainerCreatorByNewPrefabMethod_5_t788BDEE165B7D80C35DDBD76373723655A302A06 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_5_AddInstallers_m7466E5005D14D8F365D6D50143618BE41802A634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_0 = (U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A *)L_0;
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 5;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_31 = V_0;
		NullCheck(L_31);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_32 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_33 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_34 = (Type_t *)L_33.get_Type_0();
		bool L_35 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_35, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_36 = ___context1;
		U3CU3Ec__DisplayClass2_0_t24B8BE4D343BDD2425C31E415487FA1A6E39D19A * L_37 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_38 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_38, (RuntimeObject *)L_37, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_39 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_39, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_38, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_36);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_36, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_39, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`6_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m87B9F12D07035C666293113B660BDFD38888ECCE_gshared (U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`6_<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m481B9C5C76B4F25666FEEF121AD64A5C5739C0E3_gshared (U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m481B9C5C76B4F25666FEEF121AD64A5C5739C0E3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 * L_0 = (SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * L_1 = (Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_0->get__installerMethod_3();
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_2 = ___subContainer0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_5 = (RuntimeObject *)L_4.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_6 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_7 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_8 = (RuntimeObject *)L_7.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_9 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_10 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_11 = (RuntimeObject *)L_10.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_14 = (RuntimeObject *)L_13.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_15 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_16 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_15, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_17 = (RuntimeObject *)L_16.get_Value_1();
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_18 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)__this->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_19 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_18, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		RuntimeObject * L_20 = (RuntimeObject *)L_19.get_Value_1();
		NullCheck((Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_1);
		((  void (*) (Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 *)L_1, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_20, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`7<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5,TParam6>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_6__ctor_m1589B23AFA32C9988D9CB0FD70C2E2235C0B64E4_gshared (SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 * __this, DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * ___gameObjectBindInfo2, Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m49B4CC846777AF559312241D7ACDFF205895A19B((SubContainerCreatorByNewPrefabDynamicContext_tAF5955D35165E1D750EAB01A663897C6D78E0DCB *)__this, (DiContainer_tB62152DC00E2F518B50C3D93A3AA1A8C50CB22FD *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tBF69ABB5B32BA77BAF889148AEA8625B1F5848A3 *)L_2, /*hidden argument*/NULL);
		Action_7_t0423947D29196A5539EEE1103C103117C2C1BC98 * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_6_AddInstallers_m4156B4E21E5547175907A14CB5FC70A2CAD5E31E_gshared (SubContainerCreatorByNewPrefabMethod_6_t074BA50018B96C40D1517944B7661568EBB1CA99 * __this, List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * ___args0, GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_6_AddInstallers_m4156B4E21E5547175907A14CB5FC70A2CAD5E31E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_0 = (U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 *)L_0;
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_2 = V_0;
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_5 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_4->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5);
		int32_t L_6 = List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_5, /*hidden argument*/List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 5;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mE99A20566EB7D7B6A77BBC42AAFDF36F79893673((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_12 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_11->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_13 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_14 = (Type_t *)L_13.get_Type_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t0B0C2C0B788D13DA317C18B21DE50EB5A476D94E_il2cpp_TypeInfo_var);
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_17 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_16->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_18 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_19 = (Type_t *)L_18.get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_22 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_21->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_23 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_24 = (Type_t *)L_23.get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_27 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_26->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_28 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_29 = (Type_t *)L_28.get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_30, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_31 = V_0;
		NullCheck(L_31);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_32 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_31->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_33 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_32, (int32_t)4, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_34 = (Type_t *)L_33.get_Type_0();
		bool L_35 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_35, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_36 = V_0;
		NullCheck(L_36);
		List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * L_37 = (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_36->get_args_1();
		NullCheck((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37);
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_38 = List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_inline((List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 *)L_37, (int32_t)5, /*hidden argument*/List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_RuntimeMethod_var);
		Type_t * L_39 = (Type_t *)L_38.get_Type_0();
		bool L_40 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Type_t *)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		Assert_That_m4F95208E44565736161D0F43FCF484B27574566D((bool)L_40, /*hidden argument*/NULL);
		GameObjectContext_t4419A737AEF6E522C9B98160CE85864CE6C50FA6 * L_41 = ___context1;
		U3CU3Ec__DisplayClass2_0_tEB3CE15C1F47049B354AAECA5AE851D1BC52EB36 * L_42 = V_0;
		Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 * L_43 = (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)il2cpp_codegen_object_new(Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197_il2cpp_TypeInfo_var);
		Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF(L_43, (RuntimeObject *)L_42, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)), /*hidden argument*/Action_1__ctor_mBE517F3273723D25ADF2E275D6E1C368B03390CF_RuntimeMethod_var);
		ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 * L_44 = (ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788 *)il2cpp_codegen_object_new(ActionInstaller_t839BA33CC0BADD023A090A79282F44327A27B788_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_m58D2737F17A473FEF02F7D94E407D62687C46DE6(L_44, (Action_1_t58FDE58E4F2DB09AFF6C28BF3A7980D241567197 *)L_43, /*hidden argument*/NULL);
		NullCheck((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_41);
		Context_AddNormalInstaller_mA801701549FC767A72F72891CDE5C30C8AEAD076((Context_t1CC2FD48C429B1E648CB920D36134CE214F521E1 *)L_41, (InstallerBase_tC22389E06001F8D606D5524B9BBEC838A712AA6C *)L_44, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.TaskUpdater`1_<>c<System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mADEA3A43E50638E28725D8EE86B59611DEC74F0D_gshared (const RuntimeMethod* method)
{
	{
		U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 * L_0 = (U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2)))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1_<>c<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC5426EAC304868868B0657AACEBE439DF1553A8F_gshared (U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TTask Zenject.TaskUpdater`1_<>c<System.Object>::<AddTaskInternal>b__7_0(Zenject.TaskUpdater`1_TaskInfo<TTask>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CU3Ec_U3CAddTaskInternalU3Eb__7_0_m7E90A1F6D7577A57DACC386C955A66ACC2584CB9_gshared (U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 * __this, TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * ___x0, const RuntimeMethod* method)
{
	{
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_0 = ___x0;
		NullCheck(L_0);
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_Task_0();
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.TaskUpdater`1_<>c__DisplayClass8_0<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass8_0__ctor_mE2A9FFA6C559636DAB438F29EFFF1BB712CFDD34_gshared (U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.TaskUpdater`1_<>c__DisplayClass8_0<System.Object>::<RemoveTask>b__0(Zenject.TaskUpdater`1_TaskInfo<TTask>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass8_0_U3CRemoveTaskU3Eb__0_m61CC04CBD8C001DA9B6FA79E2F06E8E921AA93F6_gshared (U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * __this, TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * ___x0, const RuntimeMethod* method)
{
	{
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_0 = ___x0;
		NullCheck(L_0);
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_Task_0();
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_task_0();
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.TaskUpdater`1_TaskInfo<System.Object>::.ctor(TTask,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskInfo__ctor_m81F19A8C1FCE623404690E398D08839418875A5C_gshared (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * __this, RuntimeObject * ___task0, int32_t ___priority1, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___task0;
		__this->set_Task_0(L_0);
		int32_t L_1 = ___priority1;
		__this->set_Priority_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_AllTasks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TaskUpdater_1_get_AllTasks_m3C9B72FFE8CB0DBB0F9EF688193EE60F7168021D_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * L_1 = (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)__this->get__queuedTasks_1();
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_ActiveTasks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TaskUpdater_1_get_ActiveTasks_m0898D91076BDE02161A2E9637A5E4EDDEEC23A08_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, const RuntimeMethod* method)
{
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_0 = (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)__this->get__tasks_0();
		return L_0;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTask(TTask,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_AddTask_m0159351C81783D6ADDA33B2E1331739762E3E7B4_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, RuntimeObject * ___task0, int32_t ___priority1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___task0;
		int32_t L_1 = ___priority1;
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		((  void (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, (RuntimeObject *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTaskInternal(TTask,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_AddTaskInternal_mBC9A15D14E3713CF972114C837961E6D2927286D_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, RuntimeObject * ___task0, int32_t ___priority1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskUpdater_1_AddTaskInternal_mBC9A15D14E3713CF972114C837961E6D2927286D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * G_B2_0 = NULL;
	RuntimeObject* G_B2_1 = NULL;
	Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * G_B1_0 = NULL;
	RuntimeObject* G_B1_1 = NULL;
	{
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * L_1 = ((U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4)))->get_U3CU3E9__7_0_1();
		Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * L_2 = (Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 *)L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22 * L_3 = ((U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4)))->get_U3CU3E9_0();
		Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * L_4 = (Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_4, (RuntimeObject *)L_3, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 * L_5 = (Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 *)L_4;
		((U3CU3Ec_t4F5BB1BDCD13A07EF62BB73A46EA93854A6B9E22_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4)))->set_U3CU3E9__7_0_1(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0025:
	{
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((RuntimeObject*)G_B2_1, (Func_2_t03716AEE9BF678A23894F147E61094216AA6EC86 *)G_B2_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		RuntimeObject * L_7 = ___task0;
		bool L_8 = ((  bool (*) (RuntimeObject*, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((RuntimeObject*)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		NullCheck((RuntimeObject *)(___task0));
		Type_t * L_9 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60((RuntimeObject *)(___task0), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Type::get_FullName() */, (Type_t *)L_9);
		String_t* L_11 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923((String_t*)_stringLiteral25458F856CEB4BFBD8F8627119406BC1F8394D8E, (String_t*)L_10, (String_t*)_stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8, /*hidden argument*/NULL);
		Assert_That_m56605D736D31A10295F61A6E2C168B5240316A23((bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0), (String_t*)L_11, /*hidden argument*/NULL);
		List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * L_12 = (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)__this->get__queuedTasks_1();
		RuntimeObject * L_13 = ___task0;
		int32_t L_14 = ___priority1;
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_15 = (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11));
		((  void (*) (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)(L_15, (RuntimeObject *)L_13, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		NullCheck((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_12);
		((  void (*) (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *, TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_12, (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::RemoveTask(TTask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_RemoveTask_m0C34F9BF8B24882D761322FBDA5E20F83EDDB80B_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, RuntimeObject * ___task0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskUpdater_1_RemoveTask_m0C34F9BF8B24882D761322FBDA5E20F83EDDB80B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * L_0 = (U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 14));
		((  void (*) (U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		V_0 = (U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 *)L_0;
		U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * L_1 = V_0;
		RuntimeObject * L_2 = ___task0;
		NullCheck(L_1);
		L_1->set_task_0(L_2);
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * L_4 = V_0;
		Func_2_t681D785C4AF716880EE11E874319E30326BF1AC4 * L_5 = (Func_2_t681D785C4AF716880EE11E874319E30326BF1AC4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17));
		((  void (*) (Func_2_t681D785C4AF716880EE11E874319E30326BF1AC4 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)(L_5, (RuntimeObject *)L_4, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t681D785C4AF716880EE11E874319E30326BF1AC4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)((RuntimeObject*)L_3, (Func_2_t681D785C4AF716880EE11E874319E30326BF1AC4 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_7 = ((  TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20));
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_8 = (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_7;
		U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * L_9 = V_0;
		NullCheck(L_9);
		RuntimeObject ** L_10 = (RuntimeObject **)L_9->get_address_of_task_0();
		NullCheck((RuntimeObject *)(*L_10));
		Type_t * L_11 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60((RuntimeObject *)(*L_10), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_11);
		String_t* L_13 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)_stringLiteralA6B57A6430A9D8D7FE0D573C76778648C9F58C6B, (String_t*)L_12, /*hidden argument*/NULL);
		Assert_IsNotNull_mBAAAFF0F52AC1E6C5BA858401B0D419B643FCB15((RuntimeObject *)L_8, (String_t*)L_13, /*hidden argument*/NULL);
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_14 = (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_8;
		NullCheck(L_14);
		bool L_15 = (bool)L_14->get_IsRemoved_2();
		U3CU3Ec__DisplayClass8_0_t3C2CF7BF8DED2702282E4B4529E0D2D1088950D4 * L_16 = V_0;
		NullCheck(L_16);
		RuntimeObject ** L_17 = (RuntimeObject **)L_16->get_address_of_task_0();
		NullCheck((RuntimeObject *)(*L_17));
		Type_t * L_18 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60((RuntimeObject *)(*L_17), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_18);
		String_t* L_20 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)_stringLiteral9C9C4ECE5F3F5332F9E4B20785827F8F41DEDADB, (String_t*)L_19, /*hidden argument*/NULL);
		Assert_That_m56605D736D31A10295F61A6E2C168B5240316A23((bool)((((int32_t)L_15) == ((int32_t)0))? 1 : 0), (String_t*)L_20, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_IsRemoved_2((bool)1);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::OnFrameStart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_OnFrameStart_m0A74CE435DB5A4D8B27613E525FE784CE5B9562A_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		((  void (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_UpdateAll_mBF5BF25D6D7892493BFF089B2C456DE4280F09B5_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		((  void (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, (int32_t)((int32_t)-2147483648LL), (int32_t)((int32_t)2147483647LL), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateRange(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_UpdateRange_m125058D6F299715DD05E6C35B46D3B5B23C94A15_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, int32_t ___minPriority0, int32_t ___maxPriority1, const RuntimeMethod* method)
{
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * V_0 = NULL;
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * V_1 = NULL;
	TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * V_2 = NULL;
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_0 = (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)__this->get__tasks_0();
		NullCheck((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_0);
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_1 = ((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		V_0 = (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_1;
		goto IL_004c;
	}

IL_000e:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_2 = V_0;
		NullCheck((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_2);
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_3 = ((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		V_1 = (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_3;
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_4 = V_0;
		NullCheck((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_4);
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_5 = ((  TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * (*) (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		V_2 = (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_5;
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_6 = V_2;
		NullCheck(L_6);
		bool L_7 = (bool)L_6->get_IsRemoved_2();
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_8 = V_2;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)L_8->get_Priority_1();
		int32_t L_10 = ___minPriority0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_11 = ___maxPriority1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_003e;
		}
	}
	{
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_Priority_1();
		int32_t L_14 = ___maxPriority1;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_004a;
		}
	}

IL_003e:
	{
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_15 = V_2;
		NullCheck(L_15);
		RuntimeObject * L_16 = (RuntimeObject *)L_15->get_Task_0();
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		VirtActionInvoker1< RuntimeObject * >::Invoke(4 /* System.Void Zenject.TaskUpdater`1<System.Object>::UpdateItem(TTask) */, (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, (RuntimeObject *)L_16);
	}

IL_004a:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_17 = V_1;
		V_0 = (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_17;
	}

IL_004c:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_18 = V_0;
		if (L_18)
		{
			goto IL_000e;
		}
	}
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_19 = (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)__this->get__tasks_0();
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		((  void (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::ClearRemovedTasks(System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1_TaskInfo<TTask>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_ClearRemovedTasks_m9FEE19A33CA6913F207ED3BB2A7D546ADAF24A80_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * ___tasks0, const RuntimeMethod* method)
{
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * V_0 = NULL;
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * G_B3_0 = NULL;
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * G_B2_0 = NULL;
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_0 = ___tasks0;
		NullCheck((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_0);
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_1 = ((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		V_0 = (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_1;
		goto IL_0024;
	}

IL_0009:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_2 = V_0;
		NullCheck((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_2);
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_3 = ((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_4 = V_0;
		NullCheck((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_4);
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_5 = ((  TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * (*) (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		NullCheck(L_5);
		bool L_6 = (bool)L_5->get_IsRemoved_2();
		G_B2_0 = L_3;
		if (!L_6)
		{
			G_B3_0 = L_3;
			goto IL_0023;
		}
	}
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_7 = ___tasks0;
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_8 = V_0;
		NullCheck((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_7);
		((  void (*) (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28)->methodPointer)((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_7, (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28));
		G_B3_0 = G_B2_0;
	}

IL_0023:
	{
		V_0 = (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)G_B3_0;
	}

IL_0024:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_9 = V_0;
		if (L_9)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddQueuedTasks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_AddQueuedTasks_m0A291E96F77D6ED5009939D55A6AE82BAA114D37_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0024;
	}

IL_0004:
	{
		List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * L_0 = (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)__this->get__queuedTasks_1();
		int32_t L_1 = V_0;
		NullCheck((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_0);
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_2 = ((  TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * (*) (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)->methodPointer)((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29));
		V_1 = (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_2;
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_3 = V_1;
		NullCheck(L_3);
		bool L_4 = (bool)L_3->get_IsRemoved_2();
		if (L_4)
		{
			goto IL_0020;
		}
	}
	{
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_5 = V_1;
		NullCheck((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this);
		((  void (*) (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *, TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30)->methodPointer)((TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 *)__this, (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30));
	}

IL_0020:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0024:
	{
		int32_t L_7 = V_0;
		List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * L_8 = (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)__this->get__queuedTasks_1();
		NullCheck((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_8);
		int32_t L_9 = ((  int32_t (*) (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31)->methodPointer)((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31));
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0004;
		}
	}
	{
		List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * L_10 = (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)__this->get__queuedTasks_1();
		NullCheck((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_10);
		((  void (*) (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32)->methodPointer)((List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::InsertTaskSorted(Zenject.TaskUpdater`1_TaskInfo<TTask>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1_InsertTaskSorted_mB399C78988A280F120C404A4909699AE98171545_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * ___task0, const RuntimeMethod* method)
{
	LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * V_0 = NULL;
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_0 = (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)__this->get__tasks_0();
		NullCheck((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_0);
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_1 = ((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		V_0 = (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_1;
		goto IL_0037;
	}

IL_000e:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_2 = V_0;
		NullCheck((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_2);
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_3 = ((  TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * (*) (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_Priority_1();
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_5 = ___task0;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get_Priority_1();
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_0030;
		}
	}
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_7 = (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)__this->get__tasks_0();
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_8 = V_0;
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_9 = ___task0;
		NullCheck((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_7);
		((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33)->methodPointer)((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_7, (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_8, (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33));
		return;
	}

IL_0030:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_10 = V_0;
		NullCheck((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_10);
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_11 = ((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		V_0 = (LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA *)L_11;
	}

IL_0037:
	{
		LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * L_12 = V_0;
		if (L_12)
		{
			goto IL_000e;
		}
	}
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_13 = (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)__this->get__tasks_0();
		TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 * L_14 = ___task0;
		NullCheck((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_13);
		((  LinkedListNode_1_t2147117C2633CA6E7753C9EB129B73883B0876EA * (*) (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34)->methodPointer)((LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)L_13, (TaskInfo_t3E01A7269CE8E7A84A923B3F5E6577E4EE16B200 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskUpdater_1__ctor_m9B59B62C9F53396891FFE3BE16E6EE55DC067625_gshared (TaskUpdater_1_t766F33F30D5274EF9FE4D8AF6B333F59F36922F7 * __this, const RuntimeMethod* method)
{
	{
		LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 * L_0 = (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 35));
		((  void (*) (LinkedList_1_tD090E2C1D69420A4F7F941DC0CB82FD6C836F0F4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 36)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 36));
		__this->set__tasks_0(L_0);
		List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 * L_1 = (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 37));
		((  void (*) (List_1_t72412C13C47064A38C2C3549EE88322C795E9BE5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38));
		__this->set__queuedTasks_1(L_1);
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m474C13271F993AFE30BEB0CE67F859A2BF14BF01_gshared_inline (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  List_1_get_Item_m7FA7E4C43DC06E8FC6DBFC1BF315521FEC555B57_gshared_inline (List_1_t82BDF974FB141BE875D4E0D45E2A9229896C25B3 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2* L_2 = (TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2*)__this->get__items_1();
		int32_t L_3 = ___index0;
		TypeValuePair_tDE4B8AA17C6B9B704CD499073763527594A81BDE  L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((TypeValuePairU5BU5D_t2B8442AD42B5FDDE228C8B50069FA471C8BC00F2*)L_2, (int32_t)L_3);
		return L_4;
	}
}
