﻿using DG.Tweening;
using UnityEngine;

namespace Packages.AnimatedText.Scripts
{
    [CreateAssetMenu(menuName = "Animations/PerfectText")]
    public class PerfectTextAnimation : ScriptableObject
    {
        public Sequence Preload(RectTransform item)
        {
            return Animate(item)
                .SetAutoKill(false)
                .Pause();
        }

        public Sequence Animate(RectTransform item)
        {
            return DOTween.Sequence()
                .Append(item.DOScale(1.35f, .15f).From(0))
                .Append(item.DOScale(.65f, .15f))
                .Append(item.DOScale(1f, .15f))
                .AppendInterval(.25f)
                .Append(item.DOScale(0f, .15f));
        }
    }
}