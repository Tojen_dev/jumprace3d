﻿using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Packages.AnimatedText.Scripts
{
    public class AnimatedText : MonoBehaviour
    {
        [SerializeField]
        protected string[] words;

        [SerializeField]
        protected Color32[] colors;

        protected GameObject GameObject;
        protected Animator Animator;
        protected TextMeshProUGUI InnerText;

       
        public virtual void Initialize()
        {
            Animator = GetComponent<Animator>();
            GameObject = gameObject;
            InnerText = GetComponentInChildren<TextMeshProUGUI>();
        }

        public virtual void Show()
        {
            InnerText.text = words[Random.Range(0, words.Length)];
            InnerText.color = colors[Random.Range(0, colors.Length)];
            GameObject.SetActive(true);

            StopAllCoroutines();
            StartCoroutine(HideWaiter());
        }

        public void Hide()
        {
            StopAllCoroutines();
            GameObject.SetActive(false);
        }

        protected IEnumerator HideWaiter()
        {
            yield return null;
            yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);

            Hide();
        }
    }
}