﻿using UnityEngine;

namespace Packages.AnimatedText.Scripts
{
    public class PerfectAnimatedText : AnimatedText
    {
        [Space]
        [SerializeField]
        private Color32 multiplierColor;

        [SerializeField]
        private int multiplierSize;

        private string hexColor;


        public override void Initialize()
        {
            hexColor = ColorUtility.ToHtmlStringRGBA(multiplierColor);
            base.Initialize();
        }

        public void Show(int multiplier)
        {
            Initialize();

            InnerText.text = words[Random.Range(0, words.Length)];
            if (multiplier > 1)
            {
                InnerText.text += "<size=" + multiplierSize + "><color=#" + hexColor + "> x" + multiplier +
                                  "</color></size>";
            }

            InnerText.color = colors[Random.Range(0, colors.Length)];
            GameObject.SetActive(true);

            StopAllCoroutines();
            StartCoroutine(HideWaiter());
        }
    }
}