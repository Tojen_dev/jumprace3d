﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Packages.AnimatedText.Scripts
{
    public class PerfectTweenText : MonoBehaviour
    {
        [SerializeField]
        protected string[] words;

        [SerializeField]
        protected Color32[] colors;

        [Space]
        [SerializeField]
        private PerfectTextAnimation animation;


        protected GameObject GameObject;
        protected TextMeshProUGUI InnerText;
        protected Sequence ShowSequence;


        public virtual void Initialize()
        {
            GameObject = gameObject;
            InnerText = GetComponentInChildren<TextMeshProUGUI>();
            ShowSequence = animation.Preload(InnerText.rectTransform).OnComplete(Hide);
        }

        public virtual void Show()
        {
            InnerText.text = words[Random.Range(0, words.Length)];
            InnerText.color = colors[Random.Range(0, colors.Length)];
            GameObject.SetActive(true);
            ShowSequence.Restart();
        }

        public void Hide()
        {
            ShowSequence?.Pause();
            GameObject.SetActive(false);
        }
    }
}