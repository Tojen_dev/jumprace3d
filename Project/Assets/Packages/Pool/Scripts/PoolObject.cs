﻿using System.Collections;
using UnityEngine;

namespace Pool
{
    public class PoolObject : MonoBehaviour
    {
        public string Group
        {
            get { return name; }
        }

        public Transform Transform
        {
            get { return _transform; }
        }

        public GameObject GameObject
        {
            get { return _gameObject; }
        }

        public Vector3 Position => _transform.position;
        public Quaternion Rotation => _transform.rotation;

        private Transform _transform;
        private GameObject _gameObject;

        protected virtual void Awake()
        {
            _transform = transform;
            _gameObject = gameObject;
        }

        public void SetTransform(Vector3 position, Quaternion rotation, Transform parent)
        {
            _transform.SetParent(parent);
            _transform.position = position;
            _transform.rotation = rotation;
        }

        public virtual void OnPop()
        {
            _gameObject.SetActive(true);
        }

        public virtual void OnPush()
        {
            _gameObject.SetActive(false);
        }

        public virtual void Push()
        {
            PoolManager.Instance.Push(Group, this);
        }
    }
}