﻿using System.Collections;
using UnityEngine;

namespace Pool
{
    public class PoolEffect : PoolObject
    {
        private ParticleSystem _particleSystem;
        
        protected override void Awake()
        {
            base.Awake();
            _particleSystem = GetComponent<ParticleSystem>();
        }

        public override void OnPop()
        {
            base.OnPop();
            StartCoroutine(HideWaiter());
        }

        private IEnumerator HideWaiter()
        {
            yield return new WaitForSeconds(_particleSystem.main.duration);
            Push();
        }

        public void UpdateColor(Color32 color)
        {
            var main = _particleSystem.main;
            main.startColor = new ParticleSystem.MinMaxGradient(color);
        }
    }
}