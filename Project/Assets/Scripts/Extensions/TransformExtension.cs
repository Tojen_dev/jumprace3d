﻿using UnityEngine;

namespace Extensions
{
    public static class TransformExtension
    {
        #region Positions

        public static void SetX(this Transform transform, float value)
        {
            Vector3 temp = transform.position;
            temp.x = value;
            transform.position = temp;
        }

        public static void SetY(this Transform transform, float value)
        {
            Vector3 temp = transform.position;
            temp.y = value;
            transform.position = temp;
        }

        public static void SetZ(this Transform transform, float value)
        {
            Vector3 temp = transform.position;
            temp.z = value;
            transform.position = temp;
        }


        public static void SetLocalX(this Transform transform, float value)
        {
            Vector3 temp = transform.localPosition;
            temp.x = value;
            transform.localPosition = temp;
        }

        public static void SetLocalY(this Transform transform, float value)
        {
            Vector3 temp = transform.localPosition;
            temp.y = value;
            transform.localPosition = temp;
        }

        public static void SetLocalZ(this Transform transform, float value)
        {
            Vector3 temp = transform.localPosition;
            temp.z = value;
            transform.localPosition = temp;
        }

        #endregion


        #region Scale

        public static void SetLocalScaleX(this Transform transform, float value)
        {
            Vector3 scale = transform.localScale;
            scale.x = value;
            transform.localScale = scale;
        }

        public static void SetLocalScaleY(this Transform transform, float value)
        {
            Vector3 scale = transform.localScale;
            scale.y = value;
            transform.localScale = scale;
        }

        public static void SetLocalScaleZ(this Transform transform, float value)
        {
            Vector3 scale = transform.localScale;
            scale.z = value;
            transform.localScale = scale;
        }

        #endregion


        #region Custom

        public static Vector3 WidthDepthDifference(this Vector3 first, Vector3 second)
        {
            Vector3 firstPosition = first;
            Vector3 secondPosition = second;
            firstPosition.y = 0;
            secondPosition.y = 0;

            return firstPosition - secondPosition;
        }

        public static Vector3 WidthDepthDifference(this Transform first, Vector3 second)
        {
            Vector3 firstPosition = first.position;
            Vector3 secondPosition = second;
            firstPosition.y = 0;
            secondPosition.y = 0;

            return firstPosition - secondPosition;
        }


        public static Vector3 RotateVector3(Vector3 direction, float angle)
        {
            float x = direction.x * Mathf.Cos(angle * Mathf.Deg2Rad) - direction.z * Mathf.Sin(angle * Mathf.Deg2Rad);
            float z = direction.x * Mathf.Sin(angle * Mathf.Deg2Rad) + direction.z * Mathf.Cos(angle * Mathf.Deg2Rad);

            Vector3 result = new Vector3(x, 0, z);
            return result;
        }

        #endregion
    }
}