﻿using Controllers;
using Managers;
using States;
using UnityEngine;
using Zenject;

namespace Plugins
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField]
        private LevelsManager levelsManager;

        [SerializeField]
        private GameStatesManager gameStatesManager;

        
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<LevelsManager>().FromInstance(levelsManager).AsSingle();
            Container.BindInterfacesAndSelfTo<ColorsController>().AsSingle();
            Container.BindInterfacesAndSelfTo<GameStatesManager>().FromInstance(gameStatesManager).AsSingle();
        }
    }
}