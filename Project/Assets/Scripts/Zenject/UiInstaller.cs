﻿using UiControllers;
using UnityEngine;

namespace Zenject
{
    public class UiInstaller : MonoInstaller
    {
        [SerializeField]
        private MenuPanel menuPanel;

        [SerializeField]
        private GamePanel gamePanel;

        [SerializeField]
        private CompletePanel completePanel;

        [SerializeField]
        private GameOverPanel gameOverPanel;


        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<MenuPanel>().FromInstance(menuPanel).AsSingle();
            Container.BindInterfacesAndSelfTo<GamePanel>().FromInstance(gamePanel).AsSingle();
            Container.BindInterfacesAndSelfTo<CompletePanel>().FromInstance(completePanel).AsSingle();
            Container.BindInterfacesAndSelfTo<GameOverPanel>().FromInstance(gameOverPanel).AsSingle();
        }
    }
}