﻿using Controllers;
using Managers;
using UnityEngine;
using Zenject;

namespace Plugins
{
    [CreateAssetMenu(menuName = "Zenject/GameSettings")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        public LevelsManager.Settings levelSettings;

        [Space]
        public ColorsController.Settings colorsSettings;


        public override void InstallBindings()
        {
            Container.BindInstance(levelSettings).IfNotBound();
            Container.BindInstance(colorsSettings).IfNotBound();
        }
    }
}