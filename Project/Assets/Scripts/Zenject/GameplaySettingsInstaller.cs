﻿using Cam;
using Controllers;
using UnityEngine;
using UnityEngine.Serialization;

namespace Zenject
{
    [CreateAssetMenu(menuName = "Zenject/GameplaySettings")]
    public class GameplaySettingsInstaller : ScriptableObjectInstaller<GameplaySettingsInstaller>
    {
        public EffectsManager.Settings effectsSettings;

        [Space]
        public FogController.Settings fogSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(effectsSettings).IfNotBound();
            Container.BindInstance(fogSettings).IfNotBound();
        }
    }
}