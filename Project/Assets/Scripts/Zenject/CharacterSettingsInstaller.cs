﻿using Gameplay;
 
using UnityEngine;
 

namespace Zenject
{
    [CreateAssetMenu(menuName = "Zenject/CharacterSettings")]
    public class CharacterSettingsInstaller : ScriptableObjectInstaller<GameplaySettingsInstaller>
    {
        [Space]
        //public CharacterController.Settings characterSettings;

        [Space]
        public CharacterMoveController.Settings moveSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(moveSettings).IfNotBound();
            //Container.BindInstance(throwSettings).IfNotBound();
        }
    }
}