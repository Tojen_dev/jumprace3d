﻿using Cam;
using Controllers;
using Managers;
using UnityEngine;
using CharacterController = Gameplay.Player.CharacterController;


namespace Zenject
{
    public class GameplayInstaller : MonoInstaller
    {
        [SerializeField]
        private TilesManager tilesManager;

        [SerializeField]
        private CameraController cameraController;

        [SerializeField]
        private CharacterController characterController;
 

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<CharacterController>().FromInstance(characterController).AsSingle();
            Container.BindInterfacesAndSelfTo<TilesManager>().FromInstance(tilesManager).AsSingle();
            Container.BindInterfacesAndSelfTo<EffectsManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<FogController>().AsSingle();
            Container.BindInterfacesAndSelfTo<CameraController>().FromInstance(cameraController).AsSingle();
        }
    }
}