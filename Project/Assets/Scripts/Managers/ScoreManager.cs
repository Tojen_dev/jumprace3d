﻿using Others;
using UnityEngine;
using Zenject;

public class ScoreManager : IInitializable
{
    public int Score { get; private set; }
    public int BestScore { get; private set; }
    public int Multiplier { get; private set; }


    private readonly string bestScoreKey = "BestScoreKey";


    public void Initialize()
    {
        BestScore = DataSaver.GetIntProperty(bestScoreKey);
        Multiplier = 1;
    }

    public void Add(int value)
    {
        Score += value;
    }

    public int Add(int value, bool isPerfect)
    {
        int toAdd = value;
        if (isPerfect)
        {
            Multiplier++;
            toAdd *= Multiplier;
        }
        else
            Multiplier = 1;

        Score += toAdd;
        return toAdd;
    }

    public void TrySaveBestScore()
    {
        if (BestScore < Score)
        {
            BestScore = Score;
            DataSaver.SaveProperty(bestScoreKey, BestScore);
        }
    }

    public void Reset()
    {
        Score = 0;
        Multiplier = 1;
    }
}