﻿using System;
using Pool;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace Controllers
{
    public class EffectsManager : IInitializable
    {
        [Inject]
        private Settings _settings;


        private Transform _container;

        public void Initialize()
        {
            _container = GameObject.Find("Effects").transform;
        }

        public void ShowTileEffect(Vector3 position)
        {
            PoolManager.Instance.PopOrCreate(_settings.tileEffect, position, Quaternion.identity, _container);
        }

        public void ShowTilePerfectEffect(Vector3 position)
        {
            PoolManager.Instance.PopOrCreate(_settings.tilePerfectEffect, position, Quaternion.identity, _container);
        }


        #region Inner

        [Serializable]
        public class Settings
        {
            public PoolEffect tilePerfectEffect;
            public PoolEffect tileEffect;
        }

        #endregion
    }
}