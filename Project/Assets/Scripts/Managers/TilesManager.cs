﻿using Gameplay;
using ModestTree;
using UnityEngine;
using UnityEngine.Serialization;

namespace Managers
{
    public class TilesManager : MonoBehaviour
    {
        public Tile[] tiles;
        public Transform finish;

        private int _maxIndex;

        public float Progression => ((float) _maxIndex + 1) / (tiles.Length + 1);

        public Vector3 NextPosition { get; private set; }


        public void UpdateInfo(Tile tile)
        {
            int index = tiles.IndexOf(tile);
            UpdateNextTilePosition(index);

            if (index > _maxIndex)
                _maxIndex = index;
        }

        private void UpdateNextTilePosition(int index)
        {
            NextPosition = index + 1 >= tiles.Length ? finish.position : tiles[index + 1].Position;
        }


        public void Reset()
        {
            _maxIndex = 0;
            NextPosition = tiles[0].Position;

            for (var i = 0; i < tiles.Length; i++)
                tiles[i].Reset();
        }
    }
}