﻿using System;
using Others;
using UnityEngine;
using Zenject;

namespace Managers
{
    public class LevelsManager : MonoBehaviour, IInitializable
    {
#if UNITY_EDITOR
        [SerializeField, Range(1, 35)]
        private int level;
#endif
      
        [Inject]
        private Settings _settings;


        public int Level { get; private set; }
        public int SubLevel { get; private set; }
        public int Step { get; private set; }

        public float Progression => Step / _settings.stepsToComplete;


        private const string LevelKey = "LevelKey";


        public void Initialize()
        {
#if !UNITY_EDITOR
             Level = DataSaver.GetIntProperty(LevelKey, 1);
#else
            Level = level;
#endif
        }


        public bool NextStep()
        {
            Step++;
            return Step == _settings.stepsToComplete;
        }

        public bool NextSubLevel()
        {
            Step = 0;
            SubLevel++;
            
            return SubLevel == _settings.subLevelsToComplete;
        }

        public void NextLevel()
        {
            Level++;
            DataSaver.SaveProperty(LevelKey, Level);
        }

        public void Reset()
        {
            Step = 0;
            SubLevel = 0;
        }

        #region Inner

        [Serializable]
        public class Settings
        {
            public int stepsToComplete;
            public int subLevelsToComplete;
        }

        #endregion
    }
}