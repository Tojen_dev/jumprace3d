﻿using Controllers;
using DG.Tweening;
using Managers;
using UiControllers;
using Zenject;

namespace States
{
    public class CompleteState : AState
    {
        [Inject]
        private LevelsManager _levelsManager;


        [Inject]
        private CompletePanel _completePanel;

        [Inject]
        private GamePanel _gamePanel;

        [Inject]
        private EffectsManager _effectsManager;


        public override void Enter(AState from)
        {
            _gamePanel.UpdateProgression(1);
            _completePanel.UpdateContent(_levelsManager.Level);
            _levelsManager.NextLevel();

            DOVirtual.DelayedCall(1, ShowPanel);
        }

        private void ShowPanel()
        {
            _gamePanel.Hide();
            _completePanel.Show();
        }

        public override void Exit(AState to)
        {
            _completePanel.Hide();
            _gamePanel.UpdateProgressLevel(_levelsManager.Level);
        }


        public override void Tick()
        {
        }

        public override void FixedTick()
        {
        }

        public override State Name => State.Complete;
    }
}