﻿using System.Diagnostics;
using Controllers;
using Managers;
using UiControllers;
using UnityEngine;
using Zenject;
using CharacterController = Gameplay.Player.CharacterController;

namespace States
{
    public class MenuState : AState
    {
        [Inject]
        private LevelsManager _levelsManager;


        [Inject]
        private GamePanel _gamePanel;

        [Inject]
        private MenuPanel _menuPanel;


        [Inject]
        private CharacterController _character;

        [Inject]
        private TilesManager _tilesManager;

        [Inject]
        private FogController _fogController;
        
        
        
        public override void Initialize(GameStatesManager manager)
        {
            base.Initialize(manager);
            Application.targetFrameRate = 60;

            _gamePanel.UpdateProgressLevel(_levelsManager.Level);
        }


        public override void Enter(AState from)
        {
            _gamePanel.Reset();
            _gamePanel.Show();
            _menuPanel.Show();
            
            _tilesManager.Reset();
            _character.Reset(_tilesManager.NextPosition);
        }


        public override void Exit(AState to)
        {
            _menuPanel.Hide();
        }

        public override void Tick()
        {
            _character.Tick();
            _fogController.Update(_character.Position.y);
        }

        public override void FixedTick()
        {
            _character.FixedTick();

        }

        public override State Name => State.Menu;
    }
}