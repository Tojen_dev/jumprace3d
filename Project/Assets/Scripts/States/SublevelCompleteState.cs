﻿namespace States
{
    public class SublevelCompleteState : AState
    {
        public virtual void Initialize(GameStatesManager manager)
        {
            base.Initialize(manager);
        }

        public override void Enter(AState @from)
        {
        }

        public override void Exit(AState to)
        {
        }

        public override void Tick()
        {
        }

        public override void FixedTick()
        {
        }

        public override State Name => State.SublevelComplete;
    }
}