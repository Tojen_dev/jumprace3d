﻿using UnityEngine;

namespace States
{
    public enum State
    {
        Menu,
        Game,
        GameOver,
        SublevelComplete,
        Complete,
    }

    public abstract class AState : MonoBehaviour
    {
        protected GameStatesManager Manager;

        public abstract void Enter(AState from);
        public abstract void Exit(AState to);
        public abstract void Tick();
        public abstract void FixedTick();

        public abstract State Name { get; }

        public virtual void Initialize(GameStatesManager manager)
        {
            Manager = manager;
        }
    }
}