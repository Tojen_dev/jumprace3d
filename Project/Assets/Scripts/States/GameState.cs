﻿using Controllers;
using Gameplay;
using Managers;
using UiControllers;
using UnityEngine;
using Zenject;
using CharacterController = Gameplay.Player.CharacterController;

namespace States
{
    public class GameState : AState
    {
        [Inject]
        private LevelsManager _levelsManager;


        [Inject]
        private GamePanel _gamePanel;


        [Inject]
        private EffectsManager _effectsManager;

        [Inject]
        private CharacterController _character;

        [Inject]
        private TilesManager _tilesManager;

        [Inject]
        private FogController _fogController;


        public override void Initialize(GameStatesManager manager)
        {
            base.Initialize(manager);

            _character.TileLanded += TileLandedHandler;
            _character.Crash += () => Manager.SwitchState(State.GameOver);
            _character.Complete += () => Manager.SwitchState(State.Complete);
        }

        public override void Enter(AState from)
        {
        }


        public override void Exit(AState to)
        {
        }

        public override void Tick()
        {
            _character.Tick();
            _fogController.Update(_character.Position.y);
        }

        public override void FixedTick()
        {
            _character.FixedTick();
        }


        #region EventHandlers

        private Vector3 TileLandedHandler(Tile tile, bool isPerfect)
        {
            _tilesManager.UpdateInfo(tile);
            _gamePanel.UpdateProgression(_tilesManager.Progression);
            _effectsManager.ShowTileEffect(tile.Position);

            if (isPerfect)
            {
                _gamePanel.ShowMotivationText();
                _effectsManager.ShowTilePerfectEffect(tile.Position);
            }

            return _tilesManager.NextPosition;
        }

        #endregion


        public override State Name => State.Game;
    }
}