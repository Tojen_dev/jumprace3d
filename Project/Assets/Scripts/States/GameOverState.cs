﻿using System.Collections;
using Controllers;
using DG.Tweening;
using Managers;
using UiControllers;
using UnityEngine;
using Zenject;

namespace States
{
    public class GameOverState : AState
    {
        [Inject]
        private LevelsManager _levelsManager;

        [Inject]
        private GamePanel _gamePanel;

        [Inject]
        private GameOverPanel _gameOverPanel;


        public override void Enter(AState from)
        {
            _gameOverPanel.UpdateContent(_levelsManager.Level);
            DOVirtual.DelayedCall(.5f, ShowPanel);
        }

        private void ShowPanel()
        {
            _gamePanel.Hide();
            _gameOverPanel.Show();
        }


        public override void Exit(AState to)
        {
            _gameOverPanel.Hide();
        }

        public override void Tick()
        {
        }

        public override void FixedTick()
        {
        }

        public override State Name => State.GameOver;
    }
}