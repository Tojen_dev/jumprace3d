﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Controllers
{
    public class InputController
    {
        public Vector2 Difference { get; private set; }
        public Vector2 LastPosition;


        public void StartMove()
        {
            Difference = Vector2.zero;
            LastPosition = Input.mousePosition;
        }

        public void Move()
        {
            Difference = ((Vector2) Input.mousePosition - LastPosition) / Screen.width;
            LastPosition = Input.mousePosition;
        }

        public void Reset()
        {
            Difference = Vector2.zero;
        }
    }
}