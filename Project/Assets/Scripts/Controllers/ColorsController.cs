﻿using System;
using Others;
using UnityEngine;

namespace Controllers
{
    public class ColorsController
    {
        public static event Action OnColorsUpdated;

        private Settings _settings;

        private readonly int _mainColor = Shader.PropertyToID("_MainColor");
        private readonly int _topColorHash = Shader.PropertyToID("_TopColor");
        private readonly int _bottomColorHash = Shader.PropertyToID("_BottomColor");


        public void UpdateColors(int level)
        {
            int index = IndexNormalizer.GetLoop(level - 1, _settings.levelsColors.Length);
            LevelColors levelColors = _settings.levelsColors[index];

            OnColorsUpdated?.Invoke();
        }


        #region Inner

        [Serializable]
        public class Settings
        {
            public LevelColors[] levelsColors;

            [Header("Materials")]
            public Material characterMaterial;

            public Material obstacleMaterial;
            public Material backgroundMaterial;
        }

        [Serializable]
        public class LevelColors
        {
            public Color32 defaultSegment;
            public Color32 obstacleSegment;
            public ColorsPair background;
        }

        [Serializable]
        public class ColorsPair
        {
            public Color32 top;
            public Color32 bottom;
        }

        #endregion
    }
}