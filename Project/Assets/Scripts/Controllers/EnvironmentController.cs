﻿using System;
using Cam;
using Zenject;

namespace Controllers
{
    public class EnvironmentController
    {
        [Inject]
        private CameraController _cameraController;

        [Inject]
        private EffectsManager _effectsManager;


        public void OnStart()
        {
        }

        public void OnComplete()
        {
            ShowCompleteEffect();
        }

        public void OnGameOver()
        {
        }

        public void Tick()
        {
        }

        public void FixedTick()
        {
        }


        public void ShowCompleteEffect()
        {
        }


        #region Inner

        [Serializable]
        public class Settings
        {
        }

        #endregion
    }
}