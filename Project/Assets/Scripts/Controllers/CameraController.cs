﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;
using Random = UnityEngine.Random;

namespace Cam
{
    public class CameraController : MonoBehaviour, IInitializable
    {
        [SerializeField]
        private Transform target;

        [SerializeField]
        private Settings _settings;

        private Transform _transform;
        private Vector3 _offset;

        private bool _isFollowing;


        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            _transform = transform;
            _offset = _transform.position - target.position;

            StartFollow();
        }

        private void LateUpdate()
        {
            Follow();
        }

        #region Following

        public void Follow()
        {
            if (!_isFollowing)
                return;

            _transform.position = target.position + _offset;
        }

        public void StartFollow()
        {
            _isFollowing = true;
        }

        public void StopFollow()
        {
            _isFollowing = false;
        }

        #endregion


        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        private void Reset()
        {
            _transform.position = _offset;
        }


        #region Inner

        [Serializable]
        public class Settings
        {
        }

        #endregion
    }
}