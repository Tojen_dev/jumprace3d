﻿using System;
using UnityEngine;
using Zenject;

namespace Controllers
{
    public class FogController 
    {
        [Inject]
        private Settings _settings;

        private int heightHash = Shader.PropertyToID("_FogHeight");
        private int yStartHash = Shader.PropertyToID("_FogYStartPos");


        public void Update(float y)
        {
            for (var i = 0; i < _settings.fogMaterials.Length; i++)
            {
                _settings.fogMaterials[i].SetFloat(yStartHash, y + _settings.startY);
            }
        }

        [Serializable]
        public class Settings
        {
            public float startY;
            public float height;

            public Material[] fogMaterials;
        }
    }
}