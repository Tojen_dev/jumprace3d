﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UiComponents
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField, Range(0, 5)]
        private float fillSpeed;

        [SerializeField]
        private Image fill;

        [SerializeField]
        private TextMeshProUGUI currentLevelText;

        [SerializeField]
        private TextMeshProUGUI nextLevelText;

        private float _targetFillValue;


        public void Update()
        {
            if (fill.fillAmount != _targetFillValue)
                FillSmooth();
        }

        private  void FillSmooth()
        {
            fill.fillAmount =
                Mathf.MoveTowards(fill.fillAmount, _targetFillValue, fillSpeed * Time.deltaTime);
        }

        public void UpdateFill(float progression)
        {
            _targetFillValue = progression;
        }

        public void UpdateLevels(int current)
        {
            currentLevelText.text = current.ToString();
            nextLevelText.text = (current + 1).ToString();
        }


        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Reset()
        {
            _targetFillValue = 0;
            fill.fillAmount = 0;
        }
    }
}