﻿using System;
using UnityEngine;

namespace Others
{
    public static class Bounds
    {
        #region Float

        public static float Lerp(FloatBounds bounds, float time)
        {
            return Mathf.Lerp(bounds.min, bounds.max, time);
        }

        public static float InverseLerp(FloatBounds bounds, float value)
        {
            return Mathf.InverseLerp(bounds.min, bounds.max, value);
        }

        #endregion

        #region Int

        public static float Lerp(IntBounds bounds, float time)
        {
            return Mathf.Lerp(bounds.min, bounds.max, time);
        }

        public static float InverseLerp(IntBounds bounds, float value)
        {
            return Mathf.InverseLerp(bounds.min, bounds.max, value);
        }

        #endregion

        #region Vector2

        public static Vector2 Lerp(Vector2Bounds bounds, float time)
        {
            return Vector2.Lerp(bounds.min, bounds.max, time);
        }


        public static float InverseLerp(Vector2Bounds bounds, Vector2 value)
        {
            Vector2 AB = bounds.max - bounds.min;
            Vector2 AV = value - bounds.min;

            return Vector2.Dot(AV, AB) / Vector2.Dot(AB, AB);
        }

        #endregion

        #region Vector3

        public static Vector3 Lerp(Vector3Bounds bounds, float time)
        {
            return Vector3.Lerp(bounds.min, bounds.max, time);
        }

        public static float InverseLerp(Vector3Bounds bounds, Vector3 value)
        {
            Vector3 AB = bounds.max - bounds.min;
            Vector3 AV = value - bounds.min;

            return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
        }

        #endregion
    }

    #region Bounds structures

    [Serializable]
    public class FloatBounds
    {
        public float min;
        public float max;
    }

    [Serializable]
    public class IntBounds
    {
        public int min;
        public int max;
    }

    [Serializable]
    public class Vector2Bounds
    {
        public Vector2 min;
        public Vector2 max;
    }

    [Serializable]
    public class Vector3Bounds
    {
        public Vector3 min;
        public Vector3 max;
    }

    #endregion
}