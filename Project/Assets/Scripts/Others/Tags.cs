﻿namespace Others
{
    public static class Tags
    {
        public const string Tile = "Tile";
        public const string ShatterTile = "ShatterTile";
        public const string Water = "Water";
        public const string Finish = "Finish";
    }
}