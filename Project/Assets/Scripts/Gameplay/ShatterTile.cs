﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;

public class ShatterTile : Tile
{
    [SerializeField]
    private float force;

    [SerializeField]
    private float upwardsModifier;

    [SerializeField]
    private float explosionRadius;

    [Space]
    [SerializeField]
    private Collider collider;

    [SerializeField]
    private Rigidbody[] parts;

    private Quaternion[] defaultRotations;
    private Vector3[] defaultPositions;


    protected override void Awake()
    {
        base.Awake();

        defaultRotations = new Quaternion[parts.Length];
        defaultPositions = new Vector3[parts.Length];

        for (var i = 0; i < parts.Length; i++)
        {
            defaultPositions[i] = parts[i].transform.localPosition;
            defaultRotations[i] = parts[i].transform.localRotation;
        }
    }

    public void Explode()
    {
        collider.enabled = false;

        for (var i = 0; i < parts.Length; i++)
        {
            parts[i].isKinematic = false;
            parts[i].useGravity = true;
            parts[i].AddExplosionForce(force, transform.position, explosionRadius, upwardsModifier, ForceMode.Impulse);
        }
    }

    public override void Reset()
    {
        base.Reset();
        collider.enabled = true;

        for (var i = 0; i < parts.Length; i++)
        {
            parts[i].isKinematic = true;
            parts[i].useGravity = false;
            parts[i].transform.localPosition = defaultPositions[i];
            parts[i].transform.localRotation = defaultRotations[i];
        }
    }
}