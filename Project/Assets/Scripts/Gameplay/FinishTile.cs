﻿using UnityEngine;

namespace Gameplay
{
    public class FinishTile : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem[] completeEffects;

        
        public void PlayCompleteEffects( )
        {
            for (var i = 0; i < completeEffects.Length; i++)
                completeEffects[i].Play(true);
        }
    }
}