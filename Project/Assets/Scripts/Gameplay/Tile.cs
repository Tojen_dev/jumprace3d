﻿using System;
using Pool;
using UnityEngine;

namespace Gameplay
{
    public class Tile : PoolObject
    {
        [SerializeField]
        private Renderer perfectRenderer;

        public bool IsUsed { get; private set; }


        public void Use()
        {
            IsUsed = true;
        }

        public bool IsPerfect(Vector3 position)
        {
            return !IsUsed && (Position - position).magnitude < perfectRenderer.bounds.size.z / 2;
        }

        public virtual void Reset()
        {
            IsUsed = false;
        }
    }
}