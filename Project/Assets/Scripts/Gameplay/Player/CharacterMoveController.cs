﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace Gameplay
{
    public class CharacterMoveController : MonoBehaviour
    {
        [Inject]
        private Settings _settings;

        private Vector3 _forwardVector;
        private Vector3 _jumpVector;
        private Quaternion _targetRotation;


        public void Initialize()
        {
            _jumpVector = Vector3.up * _settings.jumpPower;
            _forwardVector = Vector3.forward * _settings.moveSpeed;
        }

        public void Move(Rigidbody rigidbody, Vector2 difference)
        {
            Quaternion rotation = Quaternion.Euler(rigidbody.rotation.eulerAngles +
                                                   new Vector3(0, difference.x * _settings.rotationSpeed, 0));
            rigidbody.MoveRotation(rotation);

            Vector3 position = rigidbody.position + rigidbody.rotation * _forwardVector * Time.fixedDeltaTime;
            rigidbody.MovePosition(position);
        }

        public void AdjustRotation(Rigidbody rigidbody)
        {
            Quaternion rotation = Quaternion.RotateTowards(rigidbody.rotation, _targetRotation,
                _settings.adjustRotationSpeed * Time.fixedDeltaTime);
            rigidbody.MoveRotation(rotation);
        }


        public void Jump(Rigidbody rigidbody)
        {
            rigidbody.velocity = _jumpVector;
        }

        public void UpdateTargetRotation(Vector3 position, Vector3 next)
        {
            Vector3 relativePos = next - position;
            _targetRotation = Quaternion.LookRotation(relativePos, Vector3.up);
            _targetRotation.x = _targetRotation.z = 0;
            _targetRotation.Normalize();
        }

        public void Reset(Rigidbody rigidbody, Vector3 nextPosition)
        {
            rigidbody.MovePosition(Vector3.zero);
            UpdateTargetRotation(rigidbody.position, nextPosition);
            rigidbody.rotation=(_targetRotation);
        }


        [Serializable]
        public class Settings
        {
            public float jumpPower;
            public float moveSpeed;
            public float rotationSpeed;

            [FormerlySerializedAs("resetRotationSpeed")]
            public float adjustRotationSpeed;
        }
    }
}