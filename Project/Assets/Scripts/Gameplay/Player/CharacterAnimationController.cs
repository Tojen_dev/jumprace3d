﻿using UnityEngine;

namespace Gameplay.Player
{
    public class CharacterAnimationController : MonoBehaviour
    {
        private Animator _animator;

        private readonly int _jumpHash = Animator.StringToHash("Jump");
        private readonly int _landingHash = Animator.StringToHash("Landing");


        public void Initialize()
        {
            _animator = GetComponentInChildren<Animator>();
        }

        public void Jump()
        {
            _animator.Play(_jumpHash, 0, 0);
        }

        public void Land()
        {
            _animator.Play(_landingHash, 0, 0);
        }
    }
}