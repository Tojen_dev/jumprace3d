﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEffectsController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] perfectEffects;

    public void ShowPerfect (bool value)
    {
        for (var i = 0; i < perfectEffects.Length; i++)
        perfectEffects[i].SetActive(value);
    }
}
