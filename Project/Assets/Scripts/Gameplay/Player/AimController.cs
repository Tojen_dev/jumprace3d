﻿using Extensions;
using Others;
using UnityEngine;

namespace Gameplay.Player
{
    public class AimController : MonoBehaviour
    {
        [SerializeField]
        private Transform aimTransform;

        [SerializeField]
        private Transform ainMark;

        [SerializeField]
        private Material aimMaterial;

        [SerializeField]
        private Color32 good;

        [SerializeField]
        private Color32 bad;


        private readonly Vector3 _addPosition = new Vector3(0, 0.1f, 0);


        public void UpdateAim(float radius)
        {
            Ray ray = new Ray(aimTransform.position + _addPosition, Vector3.down);
            if (Physics.SphereCast(ray, radius, out RaycastHit hit))
            {
                aimMaterial.color = hit.collider.CompareTag(Tags.Tile) || hit.collider.CompareTag(Tags.Finish) ||
                                    hit.collider.CompareTag(Tags.ShatterTile)
                    ? good
                    : bad;
                aimTransform.SetLocalScaleY(hit.distance);
                ainMark.SetLocalY(-hit.distance + .01f);
            }
        }

        public void Show()
        {
            aimTransform.parent.gameObject.SetActive(true);
        }

        public void Hide()
        {
            aimTransform.parent.gameObject.SetActive(false);
        }
    }
}