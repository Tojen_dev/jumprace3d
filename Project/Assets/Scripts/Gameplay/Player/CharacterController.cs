﻿using System;
using Controllers;
using Others;
using UnityEngine;
using Zenject;

namespace Gameplay.Player
{
    public class CharacterController : MonoBehaviour, IInitializable
    {
        public event Func<Tile, bool, Vector3> TileLanded;
        public event Action Complete;
        public event Action Crash;

        private CharacterEffectsController _effectsController;
        private CharacterMoveController _moveController;
        private CharacterAnimationController _animationController;
        private InputController _inputController;
        private AimController _aimController;

        private Rigidbody _rigidbody;
        private Transform _transform;
        private CapsuleCollider _collider;

        private bool _isStarted;
        private bool _canAdjustRotation;
        private Vector3 _nextPosition;

        public Vector3 Position => _transform.position;


        public void Initialize()
        {
            _inputController = new InputController();
            _aimController = GetComponent<AimController>();
            _effectsController = GetComponent<CharacterEffectsController>();
            _moveController = GetComponent<CharacterMoveController>();
            _moveController.Initialize();

            _animationController = GetComponent<CharacterAnimationController>();
            _animationController.Initialize();

            _collider = GetComponent<CapsuleCollider>();
            _rigidbody = GetComponent<Rigidbody>();
            _transform = transform;
        }


        public void Tick()
        {
            _aimController.UpdateAim(_collider.radius);

            if (Input.GetMouseButtonDown(0))
                StartMove();

            else if (Input.GetMouseButtonUp(0))
                _isStarted = false;
        }

        public void FixedTick()
        {
            if (_isStarted && Input.GetMouseButton(0))
                Move();

            else if (_canAdjustRotation)
                _moveController.AdjustRotation(_rigidbody);
        }

        private void StartMove()
        {
            _canAdjustRotation = false;
            _isStarted = true;
            _inputController.StartMove();
        }

        private void Move()
        {
            _inputController.Move();
            _moveController.Move(_rigidbody, _inputController.Difference);
        }


        private void OnCollisionEnter(Collision other)
        {
            GameObject otherObject = other.gameObject;

            if (otherObject.CompareTag(Tags.Tile))
                OnTileEnter(otherObject.GetComponentInParent<Tile>());

            if (otherObject.CompareTag(Tags.ShatterTile))
                OnShutterTileEnter(otherObject.GetComponentInParent<ShatterTile>());

            else if (otherObject.CompareTag(Tags.Water))
                Crash?.Invoke();

            else if (otherObject.CompareTag(Tags.Finish))
                OnFinishEnter(otherObject.GetComponent<FinishTile>());

            else
                Jump();
        }

        private void OnShutterTileEnter(ShatterTile tile)
        {
            tile.Explode();
            OnTileEnter(tile);
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tags.Water))
                Crash?.Invoke();
        }


        private void OnFinishEnter(FinishTile finish)
        {
            _aimController.Hide();
            _animationController.Land();
            finish.PlayCompleteEffects();
            Complete?.Invoke();
        }

        private void OnTileEnter(Tile tile)
        {
            tile.Use();
            Jump();

            //Perfect
            bool isPerfect = tile.IsPerfect(_transform.position);
            _effectsController.ShowPerfect(isPerfect);

            if (TileLanded != null)
                _nextPosition = TileLanded.Invoke(tile, isPerfect);

            //Update reset rotation value
            if (!Input.GetMouseButton(0))
            {
                _canAdjustRotation = true;
                _moveController.UpdateTargetRotation(_rigidbody.position, _nextPosition);
            }
        }


        private void Jump()
        {
            _animationController.Jump();
            _moveController.Jump(_rigidbody);
        }

        public void Reset(Vector3 nextPosition)
        {
            _nextPosition = nextPosition;
            _isStarted = false;
            _canAdjustRotation = false;
            _moveController.Reset(_rigidbody, nextPosition);
            _aimController.Show();
        }
    }
}