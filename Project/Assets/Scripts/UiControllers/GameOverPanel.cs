﻿using TMPro;
using UnityEngine;

namespace UiControllers
{
    public class GameOverPanel : APanel
    {
        [SerializeField]
        private TextMeshProUGUI levelText;

        [SerializeField]
        private TextMeshProUGUI progressionText;

        [SerializeField]
        private TextMeshProUGUI scoreText;

        [SerializeField]
        private TextMeshProUGUI bestScoreText;


        public void UpdateContent(int level, int score = 0, int bestScore = 0, float progression = 0)
        {
            UpdateText(levelText, "LEVEL " + level);

            string progressionTextValue = "COMPLETED: " + (int) (progression * 100) + "%";
            UpdateText(progressionText, progressionTextValue);

            UpdateText(scoreText, score.ToString());
            UpdateText(bestScoreText, bestScore.ToString());

            gameObject.SetActive(true);
        }

 
        private void UpdateText(TextMeshProUGUI text, string value)
        {
            if (text)
                text.text = value;
        }
    }
}