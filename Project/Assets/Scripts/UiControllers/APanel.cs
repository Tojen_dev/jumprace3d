﻿using UnityEngine;
using Zenject;

namespace UiControllers
{
    public abstract class APanel : MonoBehaviour, IInitializable
    {
        protected GameObject GameObject;

        public virtual void Initialize()
        {
            GameObject = gameObject;
        }

        public void Show()
        {
            GameObject.SetActive(true);
        }

        public void Hide()
        {
            GameObject.SetActive(false);
        }
    }
}