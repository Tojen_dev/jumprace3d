﻿using TMPro;
using UnityEngine;

namespace UiControllers
{
    public class CompletePanel : APanel
    {
        [SerializeField]
        private TextMeshProUGUI levelText;


        [SerializeField]
        private TextMeshProUGUI scoreText;

        [SerializeField]
        private TextMeshProUGUI bestScoreText;


        public void UpdateContent(int level, int score = 0, int bestScore = 0)
        {
            UpdateText(levelText, "LEVEL " + level);
            UpdateText(scoreText, score.ToString());
            UpdateText(bestScoreText, "BEST: " + bestScore);
        }


        private void UpdateText(TextMeshProUGUI text, string value)
        {
            if (text)
                text.text = value;
        }
    }
}