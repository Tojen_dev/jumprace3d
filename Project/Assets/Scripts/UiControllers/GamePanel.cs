﻿using Packages.AnimatedText.Scripts;
using UiComponents;
using UnityEngine;

namespace UiControllers
{
    public class GamePanel : APanel
    {
        [SerializeField]
        private PerfectTweenText perfectText;

        [SerializeField]
        private ProgressBar progressBar;


        public override void Initialize()
        {
            base.Initialize();

            perfectText.Initialize();
        }

        public void ShowMotivationText()
        {
            perfectText.Show();
        }

        public void Reset()
        {
            progressBar.Reset();
            perfectText.Hide();
        }

        public void UpdateProgression(float value)
        {
            progressBar.UpdateFill(value);
        }

        public void UpdateProgressLevel(int level)
        {
            progressBar.UpdateLevels(level);
        }
    }
}